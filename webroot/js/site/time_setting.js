////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;
var product_count = 200;
var product_min_count = 1;

$(document).ready(function() {

    $('#user_setting_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            min: {
                validators: {
                    notEmpty: {
                        message: 'Cannot empty'
                    },
                    callback: {
                        message: '一括出品間隔を7分以上に設定してください。',
                        callback: function (value, validator, $field) {
                            return Number(value) > 0;
                        }
                    }
                }

            },
            max: {
                validators: {
                    notEmpty: {
                        message: 'Cannot empty'
                    },
                    callback: {
                        message: '出品商品の最大数を正しく入力してください。',
                        callback: function (value, validator, $field) {
                            return Number(value) > 0;
                        }
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {

        e.preventDefault();
        var $form = $(e.target);        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
        $(".alert-info").hide();

        if (update_user_setting($form.serialize())) {

            $("#loading_overlay").fadeOut(300);

            $(".alert-info").slideDown("400");
        }
        else{

        }
    });
});

function checkNumber(obj){
    obj.value = obj.value.replace(/[^0-9.]/g, '');
    obj.value = obj.value.replace(/(\..*)\./g, '$1');
}

function update_user_setting(serialized_data){
    var is_success = false;
    $("#loading_overlay").show();
    console.log(serialized_data);
    $.ajax(
        {
            type: "POST",
            url: "/users/time-setting",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
                $(".alert-info").hide();
                $(".alert-warning").hide();
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                    $(".alert-info").slideDown("400");
                }else if(data.result == "error"){
                    is_success = false;
                    $(".alert-warning").html(data.msg);
                    $(".alert-warning").slideDown("400");
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}

