/**
 * Created by nguyenthanhson on 6/5/17.
 */
$(document).ready(function() {

});

function check_data() {
    if ($('#username').val() == "" && $('#password').val() == "") {
        alert('ユーザー名とパスワードを入力してください。');
        $('#username').focus();
        return false;
    }
    else if ($('#username').val() == "") {
        alert('ユーザー名を入力してください。');
        $('#username').focus();
        return false;

    }
    else if ($('#password').val() == "") {
        alert('パスワードを入力してください。');
        $('#password').focus();
        return false;
    }
    return true;
}