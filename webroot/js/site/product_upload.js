//////////////////////////// Upload //////////////////////////////////////


$(document).ready(function () {

   $('#product_upload_form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err:{
            container: 'tooltip'
        },
        fields: {
            product_file: {
                validators: {
                    notEmpty: {
                        message: '選択されていません。'
                    },
                    file: {
                        extension: 'zip',
                        type: 'application/zip,application/x-compressed,application/x-zip-compressed,multipart/x-zip',
                        maxSize: 209715200,//20971520,   // 2048 * 1024
                        message: '選択したファイルが有効ではありません。アップロードできるファイルのサイズは、200MBまでです。'
                    }
                }
            },
            upload_type: {
                validators: {
                    notEmpty: {
                        message: 'アップロード形式が必要です。'
                    }
                }
            }
        }
   }).on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance
       $(".alert-warning").hide();
       $(".alert-info").hide();
       UploadProductMain();
       // UploadProductSubProduct();
   });
});

$('#product_upload_form_sub').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err:{
        container: 'tooltip'
    },
    fields: {
        product_file_sub: {
            validators: {
                notEmpty: {
                    message: '選択されていません。'
                },
                file: {
                    extension: 'zip',
                    type: 'application/zip,application/x-compressed,application/x-zip-compressed,multipart/x-zip',
                    maxSize: 209715200,//20971520,   // 2048 * 1024
                    message: '選択したファイルが有効ではありません。アップロードできるファイルのサイズは、200MBまでです。'
                }
            }
        }
    }
}).on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    // Retrieve instances
    var $form = $(e.target),        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
    $(".alert-warning").hide();
    $(".alert-info").hide();
    UploadProductSubProduct();
});


function uploadProduct()
{
    $("#product_upload_form").submit();

}

function uploadProductSub()
{
    $("#product_upload_form_sub").submit();

}
function downloadProductTemplate()
{
    console.log("Download on click");
    var link = document.createElement("a");
    // link.download = 'product_template';
    link.href = '/files/download/template.zip';
    link.click();
    console.log(link);
}

// product list zip file
function UploadProductMain()
{
    var is_sucess = false;
    var formData = new FormData($("#list_container #product_upload_form")[0]);

    $('#loading_overlay').show();
    $.ajax(
        {
            type: "POST",
            url: "/userProducts/upload",
            data: formData,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                //$("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                    $(".alert-info").slideDown("400");
                }else if(data.result == "error"){
                    is_success = false;
                    $(".alert-warning").html(data.msg);
                    $(".alert-warning").slideDown("400");
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                }
            }
        }
    );
}
function UploadProductSubProduct()
{
    var is_sucess = false;
    var formData = new FormData($("#list_container #product_upload_form_sub")[0]);
    var upload_type = $('#upload_type').val();
    $('#loading_overlay').show();
    $.ajax(
        {
            type: "POST",
            url: "/userProducts/upload-sub-product",
            data: formData,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                //$("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                    $(".alert-info").slideDown("400");
                }else if(data.result == "error"){
                    is_success = false;
                    $(".alert-warning").html(data.msg);
                    $(".alert-warning").slideDown("400");
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                }
            }
        }
    );
}


