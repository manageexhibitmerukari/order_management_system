$(document).ready(function() {
    $('#detail_address_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],

        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            name: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'お名前が必要です。'
                    }
                }
            },
            zip_code: {
                validators: {
                    notEmpty: {
                        message: '郵便番号が必要です。'
                    },
                    numeric: {
                        message: '有効な郵便番号を入力してください。'
                    },
                    stringLength: {
                        message: '郵便番号は7桁の数字です。',
                        max: 7,
                        min: 7
                    }
                }

            },
            city: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: '都道府県が必要です。'
                    }
                }
            },
            address1 : {
                validators: {
                    notEmpty: {
                        message: '住所１が必要です。'
                    }
                }
            },

            phone_number : {
                validators: {
                    notEmpty: {
                        message: '電話番号が必要です。'
                    },
                    numeric: {
                        message: '電話番号形式に誤りがあります。'
                    },
                    stringLength: {
                        message: '数字を8〜13桁で入力してくだだい。',
                        max: 13,
                        min: 8
                    }

                }
            }
        }
    });
});