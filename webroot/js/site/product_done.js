
var CONTEXT_NAME = "mercari";
var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;

var currentpage = 0;
var firstload = true;
var allchecked = false;
var selectproductary_itemid= Array();
var main_Table;

$(document).ready(function() {
    make_init_data();
    getPattenMessage();

    $("#save_product").click(function () {
        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm( {
            message: "変更を保存しますか。",
            callback:  function(result) {
                if(result == true)
                {
                    var fields = $(":input").serializeArray();

                    // call back ajax function
                    $.ajax(
                        {
                            type: "POST",
                            url: "/mercari-products/list-update",
                            data: "formobjects="+JSON.stringify(fields),
                            contentType: "application/x-www-form-urlencoded;",
                            dataType: "json",
                            async: true,
                            beforeSend: function () {
                                success_add_flag = 0;
                                $("#loading_overlay").fadeIn(300);
                            },
                            error: function (data, status, errThrown) {
                                $("#loading_overlay").fadeOut(300);
                            },
                            success: function (data) {
                                if (data.result == "success") {
                                    success_add_flag = 0;
                                    firstload = true;
                                    make_init_data();
                                } else if (data.result == "error") {
                                    success_add_flag = 0;

                                }
                            },
                            statusCode: {
                                404: function () {
                                    success_add_flag = 0;
                                    alert('page not found');
                                    $("#loading_overlay").fadeOut(300);
                                }
                            }
                        });
                    return ;
                }
            }
        });
        return;
    });
});


function make_init_data(){
    // call back ajax function
    $("#loading_overlay").show();

    $.ajax(
        {
            type: "POST",
            url: "/mercari-products/product-done",
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                if(data.result == "success"){
                    init_tbl_data(data.msg);
                    make_list(tbl_data);
                }else if(data.result == "error"){
                    alert(data.msg);
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

function make_list(input_data){
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    if(input_data == null){
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th align = "center" style="min-width:20px !important;"></th>';
        inner_html += '<th style="min-width:50px !important;text-align: center;padding-right: 0;">画像</th>';
        inner_html += '<th style="min-width:250px !important;text-align: center;padding-right: 0;">商品名</th>';
        inner_html += '<th style="min-width:70px !important;text-align: center;padding-right: 0;">商品ID</th>';
        inner_html += '<th style="min-width:100px !important;text-align: center;padding-right: 0;">購入日時</th>';
        inner_html += '<th style="min-width:80px !important;text-align: center;padding-right: 0;">価格</th>';
        inner_html += '<th style="min-width:180px !important;text-align: center;padding-right: 0;">お届け先</th>';
        inner_html += '<th style="min-width:50px !important;text-align: center;padding-right: 0;">取引メッセージ</th>';
        inner_html += '<th style="text-align: center;width:70px !important;padding-right: 0px">備考</th>';
        inner_html += '<th style="text-align: center;width:70px !important;padding-right: 0px">操作</th>';                                                                                                                
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        inner_html += '<tr>';
        inner_html += '<td style="text-align:center;vertical-align:middle;padding-top:20px;" colspan=10>';
        inner_html += '<div class="alert alert-warning alert-dismissable">';
        inner_html += '出品中の商品はありません。';
        inner_html += '</div>';
        inner_html += '</td>';
        inner_html += '</tr>';
        inner_html += '</tbody>';
    }
    else{
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th style="min-width:50px !important;text-align: center;padding-right: 0;">画像</th>';
        inner_html += '<th style="min-width:200px !important;text-align: center;padding-right: 0;">商品名</th>';
        inner_html += '<th style="min-width:70px !important;text-align: center;padding-right: 0;">商品ID</th>';
        inner_html += '<th style="min-width:100px !important;text-align: center;padding-right: 0;">購入日時</th>';
        inner_html += '<th style="min-width:80px !important;text-align: center;padding-right: 0;">価格</th>';
        inner_html += '<th style="min-width:180px !important;text-align: center;padding-right: 0;">お届け先</th>';
        inner_html += '<th style="min-width:150px !important;text-align: center;padding-right: 0;">取引メッセージ</th>';
        inner_html += '<th style="text-align: center;width:70px !important;padding-right: 0px">備考</th>';
        inner_html += '<th style="text-align: center;width:70px !important;padding-right: 0px">操作</th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';

        for (index in input_data ){

            inner_html += '<tr id="' + input_data[index].id + '" name="' + input_data[index].id + '">';

            for (key in input_data[index]) {
                if (input_data[index][key] == null) input_data[index][key] = "";
            }
            //No
            input_data[index].No = (input_data.length - index);

            inner_html += '<td style="text-align: center;vertical-align:top;">';
            inner_html += '<img style="cursor: pointer;width:100px;"  src = "'+input_data[index]['productPhoto']+'">';
            inner_html += '</td>';

            inner_html += '<td style="vertical-align:top;font-weight: bold">' ;
            inner_html += input_data[index]['productname'];
            inner_html += '</td>';

            inner_html += '<td style="vertical-align:top;font-weight: bold">' ;
            inner_html += input_data[index]['productid'];
            inner_html += '</td>';

            inner_html += '<td style="vertical-align:top;font-weight: bold">' ;
            inner_html += input_data[index]['buy_date'];
            inner_html += '</td>';

            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += (input_data[index]['price'])?input_data[index]['price'] + "円":"-";
            inner_html += '</td>';

            addressee = input_data[index]['addressee'];

            inner_html += '<td style="vertical-align:top;font-weight: bold">' ;
            inner_html += addressee;
            inner_html += '</td>';

            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += '<a  href=javascript:displayTransationMsgs('+index+')><i class="fa fa-comments fa-2x"></i></a>';
            inner_html += '</td>';

            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += '<div class="input-group input-group-sm" style="text-align: center">';
            inner_html += '<textarea name = "note' + input_data[index]['id'] + '"  id="note' + input_data[index]['id'] + '" maxlength = "2000" type="text" class="form-control" style="width:100px; height:50px; font-size: small">'+input_data[index].note+'</textarea>';
            inner_html += '</div></td>';

            inner_html +=  '<td style="vertical-align:top">' ;
            inner_html += '<p><button onclick="recreateProduct(' + input_data[index]['id'] + ');" id="recreateProduct' + input_data[index]['id'] + '" class="btn btn-primary btn-sm" style="font-size:10px;">再出品する</button></p>';
            inner_html +=  '</td>';

            inner_html += '</tr>';
        }

        inner_html += '</tbody>';
    }
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);

    if (input_data != null){

        if(typeof(Cookies.get('idisplaylength')) == "undefined" && Cookies.get('idisplaylength') === null)
            idisplaylength = 5;
        else
            idisplaylength = Cookies.get('idisplaylength');

        main_Table = $('#main_tbl').dataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "iDisplayLength": parseInt(idisplaylength, 10),
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "fnDrawCallback": function(){
                if (!firstload)
                {
                    currentpage = this.fnPagingInfo().iPage
                }

                $("#main_tbl thead th:first").removeClass("sorting_desc").removeClass("sorting_asc").addClass("sorting_disabled");
            },
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [0,6,7,8]
            } ]
        });

        $('#main_tbl').on( 'length.dt', function ( e, settings, len ) {
            Cookies.set('idisplaylength', len);
        } );
    }
}


function recreateProduct(item_id){
    $.ajax({
        type: "POST",
        url: "/user-products/recreate-product",
        data: "product_id="+item_id,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){

            }else if(data.result == "error"){

            }
            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").hide();
            }
        }
    });
    return;
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

var vuePayment = new Vue({
    el: '#trans_msgs_view_modal',
    data: {
        trans_msgs: [],
        patternComments: [],
        item_id:''
    }
});

var dataTable;
function displayTransationMsgs(itemid){
    document.getElementById("frm_trans_msgs_view_modal").reset();
    item_id = tbl_data[itemid]['productid'];
    $.ajax({
            type: "POST",
            url: "/mercari-products/get-transaction-msg",
            data: "item_id="+item_id,
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                if(data.result == "success"){
                    if (dataTable) {
                        dataTable.fnDestroy();
                    }
                    vuePayment.item_id = itemid;
                    dataTable = $('#main_comment_tbl').dataTable({
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": false,
                        "bSort": true,
                        "aaSorting": [[1, 'desc']],  // sort desc
                        "bInfo": true,
                        "bAutoWidth": true,
                        "iDisplayLength": 20,
                        "aLengthMenu": [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
                        "language": {
                            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
                        },
                        data : data.msg,
                        "columnDefs" : [
                            {
                                "targets" : [ 0, 1],
                                "render" : function(data, type, full, meta) {
                                    return (data ? data : '---');
                                }
                            }],
                        columns : [
                            {
                                "className" : 'col-comment',
                                "orderable" : false,
                                "data" : null,
                                "title" : "コメント",
                                "defaultContent" : ''
                                //"width" : "10px"
                            },
                            {
                                "className" : 'col-time',
                                "data" : "created",
                                "title" : "時間",
                                "defaultContent" : ''
                                //"orderable" : false
                            } ],
                        createdRow : function(row, data, dataIndex) {
                            $(row).attr('data-id', data.id);
                            $(row).children('td').eq(0).html('<p style="white-space: nowrap;"><img width="30px" style="border-radius: 20px" src="' + data.photo + '">&nbsp;&nbsp;<B>' + data.screenname + '</B></p>'+ data.body );

                        },
                        "order": [[ 1, "desc" ]]  //first sort field
                    });

                    $("#trans_msgs_view_modal").modal();
                }else if(data.result == "error"){

                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });


}


$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};


(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }

    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }

    return init();
}));
