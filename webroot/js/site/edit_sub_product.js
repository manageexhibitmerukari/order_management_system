////////////////////////////////////////////////////      出品       /////////////////////////////////////////////

$(document).ready(function() {

    $('#product_publish_form').formValidation({
        framework: 'bootstrap',
        //excluded: [':disabled', ':hidden'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            quantity: {
                validators: {
                    notEmpty: {
                        message: '数量が必須です。'
                    },
                    numeric: {
                        message: '在庫数、数値でなければなりません。'
                    }
                    // between: {
                    //     min: 0
                    // }
                }
            },
            color: {
                validators: {
                    // notEmpty: {
                    //     message: '色が必須です。'
                    // }
                }
            },
            price: {
                validators: {
                    notEmpty: {
                        message: '価格が要求されます。'
                    },
                    numeric: {
                        message: '価格は、数値でなければなりません。'
                    },

                }
            },
            selling_term : {
                validators: {
                    numeric: {
                        message: '数値でなければなりません。'
                    }
                }
            },
            buy_term : {
                validators: {
                    numeric: {
                        message: '数値でなければなりません。'
                    },
                    // callback: {
                    //     message: "buy_term not less than amount ",
                    //     callback: function (value, validator, $field) {
                    //         var amount = $('#amount').val();
                    //         amount = parseInt(amount);
                    //         return value <= amount;
                    //     }
                    // }
                    // between: {
                    //     min: 1,
                    //     max: 99999,
                    //     message: 'Toi thieu so luong 1'
                    // }

                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Retrieve instances
        var $form = $(e.target);
        if ($form.find("#product_preview_wrapper").is(":visible")){
            $('#loading_overlay').show();
            $("#product_publish_form").unbind('submit').submit();
        }
        else {
            $("input[type=text]").each(function (e) {
                var val = $(this).val();
                var id = $(this).attr("id");
                var preview_id = id + "_preview";

                $("#" + preview_id).html(val);
            });

            $("select").each(function (e) {
                var text = $(this).find("option:selected").text();
                var id = $(this).attr("id");
                var preview_id = id + "_preview";

                $("#" + preview_id).html($.trim(text));
            })

            $("#description_preview").html($("#description").val().replace(/\n/g, '<br \\>'));

            $("#loading_overlay").show();
            $("#product_input_wrapper").fadeOut(500, function () {
                $("#product_preview_wrapper").fadeIn(500, function () {
                    $("#loading_overlay").hide()
                });
            });

            return false;
        }
    });

    $('#product_publish_form .product_photo').change(function(e){
        if( $('#product_publish_form').data('formValidation').isValidField($(this).attr("name")) ) {
            var preview_id = "#" + $(this).attr("name") + "_preview";
            var preview_preview_id = preview_id+"_preview";
            readURL(this, preview_id);
            readURL(this, preview_preview_id);

            var attr_id = $(this).attr("id");
            var attr = attr_id.split("_");

            var next_id = parseInt(attr[1], 10) + 1;
            $(".photo_"+next_id).slideDown(300);
        }
    });

    $('#datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        language: 'ja'

    });

    $('#timepicker').timepicker({
        showInputs: false,
        defaultTime: false,
        showMeridian: false
    });

    $('[data-toggle="tooltip"]').tooltip();

});
function saveProduct() {
    var time = $('#timepicker').val();
    var date = $('#datepicker').val();

    if (time && !date) {
        $('.msg-date-time').show();
        return false;
    } else {
        $('.msg-date-time').hide();
    }

    $('#product_publish_form').submit();
}

function readURL(input, display_id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(display_id).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}



