////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;

$(document).ready(function() {
    make_init_data();

    $('#user_detail_modal').on('hidden.bs.modal', function() {
        $('#user_detail_form').formValidation('resetForm', true);
        $('#user_detail_modal #current_user').val("0");
        if(refresh_list)
            make_init_data();
    });

    $('#user_delete_modal').on('hidden.bs.modal', function() {
        $('#user_delete_modal #delete_user_id').val("0");
        if(refresh_list)
            make_init_data();
    });


    $('#user_detail_modal, #user_delete_modal').on('shown.bs.modal', function() {
        refresh_list = false;
    });

    $(document).on('click', '#delete-user',function () {
        return confirm('この商品を削除してもよろしいですか。');
    });

    $('#user_detail_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            username: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'ログインIDが必要です。'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    }
                }
            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    },
                    /*emailAddress: {
                     message: '有効なメールアドレスを入力してください。'
                     }*/
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください'
                    }
                }
            },
            phone_number : {
                validators: {
                    notEmpty: {
                        message: '電話番号が必要です。'
                    }
                }
            },
            name : {
                validators: {
                    notEmpty: {
                        message: 'お名前が必要です。'
                    }
                }
            },
            company : {
                validators: {
                    notEmpty: {
                        message: '会社名が必要です。'
                    }
                }
            },
            postal : {
                validators: {
                    notEmpty: {
                        message: '郵便番号が必要です。'
                    }
                }
            },
            state : {
                validators: {
                    notEmpty: {
                        message: '都道府県が必要です。'
                    }
                }
            },
            town : {
                validators: {
                    notEmpty: {
                        message: '市町村が必要です。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission

        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

        if (update_user_data($form.serialize())) {
            $('#user_detail_modal').modal('hide');
            refresh_list = true;
        }
    });
});

function update_user_data(serialized_data){
    // console.log(serialized_data);
    var is_success = false;
    $("#loading_overlay1").show();
    $.ajax(
        {
            type: "POST",
            url: "/users/update_user_info",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay1").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay1").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                }else if(data.result == "error"){
                    is_success = false;
                    alert(data.msg);
                }
                $("#loading_overlay1").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}

function make_init_data(){
    // call back ajax function
    $.ajax(
        {
            type: "POST",
            url: "/users",
            //contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                console.log(errThrown);
            },
            success: function(data){
                console.log(data.users);
                $("#btn_add_user").attr("disabled", false);
                init_tbl_data(data.users);
                make_list(tbl_data);
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

function make_list(input_data){
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    if(input_data.length <= 0){
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th >No</th>';
        inner_html += '<th>ログインID</th>';
        inner_html += '<th>メールアドレス</th>';
        inner_html += '<th>最大アカウント数</th>';
        inner_html += '<th>作用</th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        inner_html += '<tr>';
        inner_html += '<td style="text-align:center;vertical-align:middle;padding-top:20px;" colspan=6>';
        inner_html += '<div class="alert alert-warning alert-dismissable">';
        inner_html += '<h4><i class="icon fa fa-warning"></i> 警告!</h4>';
        inner_html += 'データはありません。';
        inner_html += '</div>';
        inner_html += '</td>';
        inner_html += '</tr>';
        inner_html += '</tbody>';
    }
    else{
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th>No</th>';
        inner_html += '<th>ログインID</th>';
        inner_html += '<th>メールアドレス</th>';
        inner_html += '<th>作用</th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        for(i=0; i < input_data.length; i++) {
            inner_html += '<tr id="' + input_data[i].id + '" name="' + input_data[i].id + '">';
            // for (key in input_data[i]) {
            //     if (input_data[i][key] == null) input_data[i][key] = "";
            // }
            //No
            inner_html += '<td>';
            inner_html += ( i + 1);
            inner_html += '</td>';
            //login id
            inner_html += '<td>' ;
            inner_html += input_data[i].username;
            inner_html += '</td>';
            
            inner_html +=  '<td>' ;
            inner_html +=  input_data[i].email;
            inner_html +=  '</td>';

            inner_html +=  '<td>' ;
            inner_html +=  '<button onclick="displayDeleteUI(this);" id="' + input_data[i]['id'] + '" class="btn btn-danger btn-sm" style="display:block"><i class="fa fa-trash-o"></i>&nbsp;削除</button>';
            inner_html +=  '</td>';

            inner_html += '</tr>';
        }
        inner_html += '</tbody>';
    }
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);
    if(input_data.length > 0) {
        $('#main_tbl').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            // "aaSorting": [[0, 'desc']],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "iDisplayLength": 20,
            "aLengthMenu": [[ 20, 40, 60, 80, 100, -1], [ 20, 40, 60, 80, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [3]
            } ]
        });
    }
}


function displayInsertUI(){
    $('#cm_no').text(tbl_data.length+1);
    tbl_data_pos = -1;
    $('#user_detail_modal #current_user').val("0");
    $('#user_detail_modal #email').prop("readonly", false);
    $('#user_detail_modal').modal();
}

function displayUpdateUI(obj){
    var edit_user_id = parseInt(obj.id, 10);
    tbl_data_pos=tbl_data.map(function (element) { return element.id;}).indexOf(edit_user_id);

    $('#user_detail_modal #username').val(tbl_data[tbl_data_pos].username);
    $('#user_detail_modal #current_user').val(tbl_data[tbl_data_pos].id);
    $('#user_detail_modal #password').val(tbl_data[tbl_data_pos].password);
    $('#user_detail_modal #email').val(tbl_data[tbl_data_pos].email);
    $('#user_detail_modal #email').val(tbl_data[tbl_data_pos].phone_number);
    $('#user_detail_modal #email').prop("readonly", true);

    $('#user_detail_modal').modal();
}

function displayDeleteUI(obj){
    var delete_user_id = parseInt(obj.id, 10);
    $("#delete_user_id").val(delete_user_id);
    $("#user_delete_modal").modal();
}

function deleteUser(){
    var delete_user_id = $("#delete_user_id").val();
    $.ajax({
        type: "POST",
        url: "/users/delete",
        data: "user_id="+delete_user_id,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay2").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay2").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                refresh_list = true;
                $("#user_delete_modal").modal('hide');
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay2").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay2").fadeOut(300);
            }
        }
    });
}