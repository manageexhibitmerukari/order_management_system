var finishAccountDataTable;
var unfinishAccountDataTable;

$(document).ready(function () {
    // viewAccountSetting();
    $("unfinishAccount").text(viewUnfinishAccountSetting());
    $("finishAccount").text(viewFinishAccountSetting());
});

function viewUnfinishAccountSetting() {
    $.ajax({
        type: "POST",
        url: "/users/view-account-purchase-setting",
        data: "status=unfinish",
        dataType: "json",
        beforeSend: function () {
            $("#loading_overlay").fadeIn(300);
        },
        success: function (data) {
            unfinishAccountTable(data.accountArray);
            $("#loading_overlay").fadeOut(300);
        }
    });
}

function viewFinishAccountSetting() {
    $.ajax({
        type: "POST",
        url: "/users/view-account-purchase-setting",
        data: "status=finish",
        dataType: "json",
        beforeSend: function () {
            $("#loading_overlay").fadeIn(300);
        },
        success: function (data) {
            finishAccountTable(data.accountArray);
            $("#loading_overlay").fadeOut(300);
        }
    });
}

function finishAccountTable(account) {
    if(finishAccountDataTable)
        finishAccountDataTable.fnDestroy();

    finishAccountDataTable = $("#finishAccTbl").dataTable({
        "bPaginate" : true,
        "bLengthChange" : true,
        "bFilter" : true,
        "bSort" : true,
        "aaSorting" : [],
        "bInfo" : true,
        "bAutoWidth" : true,
        "iDisplayLength" : 20,
        "aLengthMenu" : [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
        "language": {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        data: account,
        columns: [
            {
                "title" : "注文番号",
                "data" : 'orderPurchaseId',
                "class" : 'text-center'
            },
            {
                "title" : "ログインID",
                "data" : 'mercariId',
                "class" : 'text-center'
            },
            {
                "title" : "Email",
                "data" : 'email',
                "class" : 'text-center'
            },
            {
                "title" : "パスワード",
                "data" : 'password',
                "class" : 'text-center'
            },
            {
                "title" : "状態",
                "data" : 'statusAccount',
                "class" : 'text-center'
            }/*,
            {
                "title" : "操作",
                "data" : null,
                "defaultContent" : '',
                "class" : 'text-center'
            }*/
        ],
        /*createdRow: function (row, data, dataIndex) {
            $(row).children('td').eq(6).html('<button class="btn btn-default" type="button" onclick="goToAccountSetting(' + data.idAccount + ')">アカウント設定</button>')
        },*/
        "aoColumnDefs": [{
            'bSortable' : false,
            'aTargets' : [1,2,3,4]
        }]
    });
}

function unfinishAccountTable(account) {
    if(unfinishAccountDataTable)
        unfinishAccountDataTable.fnDestroy();

    unfinishAccountDataTable = $("#unfinishAccTbl").dataTable({
        "bPaginate" : true,
        "bLengthChange" : true,
        "bFilter" : true,
        "bSort" : true,
        "aaSorting" : [],
        "bInfo" : true,
        "bAutoWidth" : true,
        "iDisplayLength" : 20,
        "aLengthMenu" : [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
        "language": {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        data: account,
        columns: [
            {
                "title" : "注文番号",
                "data" : 'orderPurchaseId',
                "class" : 'text-center'
            },
            {
                "title" : "ログインID",
                "data" : 'mercariId',
                "class" : 'text-center'
            },
            {
                "title" : "Email",
                "data" : 'email',
                "class" : 'text-center'
            },
            {
                "title" : "パスワード",
                "data" : 'password',
                "class" : 'text-center'
            },
            {
                "title" : "状態",
                "data" : 'statusAccount',
                "class" : 'text-center'
            }/*,
             {
             "title" : "操作",
             "data" : null,
             "defaultContent" : '',
             "class" : 'text-center'
             }*/
        ],
        /*createdRow: function (row, data, dataIndex) {
         $(row).children('td').eq(6).html('<button class="btn btn-default" type="button" onclick="goToAccountSetting(' + data.idAccount + ')">アカウント設定</button>')
         },*/
        "aoColumnDefs": [{
            'bSortable' : false,
            'aTargets' : [1,2,3,4]
        }]
    });
}

function goToAccountSetting(idAccount) {
    window.location.href = '/users/view-account-purchase-setting-detail?idAccount=' + idAccount;
}