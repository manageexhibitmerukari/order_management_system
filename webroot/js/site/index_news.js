var currentpage = 0;
var firstload = true;

$(document).ready(function () {
    makeInitData();
});

function makeInitData() {
    $.ajax({
        type: "POST",
        url: "/users/index-news",
        dataType: "json",
        beforeSend: function () {
            $("#loading_overlay").fadeIn(300);
        },
        success: function (data) {
            if(data.result == 'success'){
                makeList(data.data);
            }
            else if(data.result == 'error')
            {
                alert(data.msg);
            }
            $("#loading_overlay").fadeOut(300);
        }
    })
}

function makeList(inputData) {
    var mainTblWrap = $('#main_tbl_wrap');
    var innerHtmlViewNews = '';
    innerHtmlViewNews += '<table id="main_tbl" class="table table-hover">';
    if(inputData == null){
        innerHtmlViewNews += '<thead>';
            innerHtmlViewNews += '<tr>';
                innerHtmlViewNews += '<th style="min-width: 200px; text-align: center"><h3>ニュース一覧</h3></th>';
            innerHtmlViewNews += '</tr>';
        innerHtmlViewNews += '</thead>';

        innerHtmlViewNews += '<tbody>';
            innerHtmlViewNews += '<tr>';
                innerHtmlViewNews += '<td style="padding-top: 20px; text-align: center;">';
                innerHtmlViewNews += '<div class="alert alert-warning alert-dismissable">';
                innerHtmlViewNews += '<h4><i class="icon fa fa-warning"></i> 警告!</h4>';
                innerHtmlViewNews += '商品はありません。';
                innerHtmlViewNews += '</div></td>';
            innerHtmlViewNews += '</tr>';
        innerHtmlViewNews += '</tbody>';
    } else {
        innerHtmlViewNews += '<thead>';
            innerHtmlViewNews += '<tr>';
                innerHtmlViewNews += '<th style="min-width: 200px; text-align: center"><h3>ニュース一覧</h3></th>';
            innerHtmlViewNews += '</tr>';
        innerHtmlViewNews += '</thead>';

        innerHtmlViewNews += '<tbody>';
        for (index in inputData){
            innerHtmlViewNews += '<tr id="news' + inputData[index]['id'] + '">';
                innerHtmlViewNews += '<td style="padding: 30px 30px 30px 40px; border: 1px dotted #CCCCCC; cursor: pointer;" onclick="getDetailNews(' + inputData[index]['id'] + ')">';
                    innerHtmlViewNews += '<div class="row" style="color: rgb(170,170,170);"><i class="fa fa-clock-o" style="padding-right: 10px;"></i>' + inputData[index]['date'];

                        if(inputData[index]['showHeader'] == 1){
                            innerHtmlViewNews += '<span style="background-color: red; color: white; margin-left: 15px; padding: 2px 4px;">' + inputData[index]['header'] + '</span>';
                        }
                    innerHtmlViewNews += '</div>';
                    innerHtmlViewNews += '<div class="row" style="margin-top: 10px;"><h4>' + inputData[index]['title'] + '</h4></div>';
                innerHtmlViewNews += '</td>';
            innerHtmlViewNews += '</tr>';
        }
        innerHtmlViewNews += '</tbody>';
    }
    innerHtmlViewNews += '</table>';
    mainTblWrap.html(innerHtmlViewNews);

    if(inputData != null){
        if(typeof(Cookies.get('idisplaylength')) == "undefined" && Cookies.get('idisplaylength') === null)
            idisplaylength = 5;
        else
            idisplaylength = Cookies.get('idisplaylength');

        main_Table = $('#main_tbl').dataTable({
            "initComplete": function (oSettings) {
                if(this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage - 1;

                this.fnPageChange(currentpage);
                firstload = false;
            },
            "bPaginate" : true,
            "bLengthChange" : true,
            "bFilter" : true,
            "bSort" : false,
            "aaSorting" : [],
            "bInfo" : true,
            "bAutoWidth" : true,
            "iDisplayLength" : parseInt(idisplaylength, 10),
            "aLengthMenu" : [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "すべて"]],
            "language" : {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "fnDrawCallback" : function () {
                if(!firstload){
                    currentpage = this.fnPagingInfo().iPage;
                }
            }
        });

        $('#main_tbl').on('length.dt', function (e, settings, len) {
            Cookies.set('idisplaylength', len);
        });
    }
}

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }

    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }

    return init();
}));

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

// ham lay detail news
function getDetailNews(idNews) {
    $.ajax({
        type: "POST",
        url: "/users/get-detail-news",
        data: "idNews=" + idNews,
        dataType: "json",
        beforeSend: function () {
            $('#loading_overlay').fadeIn(300);
        },
        success: function (data) {
            if(data.result == 'success'){
                makeDetailNewsData(data.data);
            } else {
                alert(data.result);
            }
            $('#loading_overlay').fadeOut(300);
        }
    });
}

// view detail news
function makeDetailNewsData(detailData) {
    var mainTblWrap = $('#main_tbl_wrap');
    var innerHtmlDetailNews = '';
    if(detailData == null){

    } else {
        innerHtmlDetailNews += '<div class="row" style="border: 1px dotted #cccccc;">';
            innerHtmlDetailNews += '<h3 class="text-center" style="padding: 20px;"><b>' + detailData['title'] + '</b></h3>';
            innerHtmlDetailNews += '<hr>';
            innerHtmlDetailNews += '<div class="row" style="padding: 10px 30px 30px 30px;">';
                innerHtmlDetailNews += '<div class="row pull-right" style="color: rgb(170,170,170);">' + detailData['date'] + '</div>';
                innerHtmlDetailNews += '<div class="row"></div>';
                innerHtmlDetailNews += '<div class="row" style="line-height: 22px; margin-top: 10px;">' + detailData['body'] + '</div>';
            innerHtmlDetailNews += '</div>';
        innerHtmlDetailNews += '</div>';
    }
    innerHtmlDetailNews += '<div class="row pull-right" style="margin-top: 6px;"><button class="btn btn-default" type="button" onclick="backIndexNews()">戻る</button></div>';
    mainTblWrap.html(innerHtmlDetailNews);
}

function backIndexNews() {
    makeInitData();
}