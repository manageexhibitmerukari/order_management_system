var tbl_data = [];
var currentAddressId = null;


$(document).ready(function() {
    // $("#side").hide();

    // make_init_data();
});

function make_init_data(){
    // call back ajax function
    $.ajax({
        type: "POST",
        url: "/users/address-management",
        dataType: "json",
        success: function(data){
            init_tbl_data(data.addresses);
            currentAddressId = data.currentAddressId;
            make_list(tbl_data);
        },
        statusCode: {
            404: function() {
                alert('page not found');
            }
        }
    });
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

function make_list(input_data)
{
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);

    $('#main_tbl').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        // "aaSorting": [[0, 'desc']],  // sort desc
        "bInfo": true,
        "bAutoWidth": true,
        "iDisplayLength": 20,
        "aLengthMenu": [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
        "language": {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [1, 2]
        }],
        data: input_data,
        columns: [
            {
                "title" : "氏名",
                "data" : "name"
            },
            {
                "title" : "住所",
                "data" : 'full_address'
            },
            {
                "title" : "",
                "data" : null,
                "defaultContent" : ''
            }
        ],
        createdRow: function (row, data, dataIndex)
        {
            var htmlCol2 = '';
            if (data.id != currentAddressId)
                htmlCol2 += '<a href="/users/change-address/' + data.id + '" class="btn btn-success btn-sm" > この住所を使う</a><br><br>';
            htmlCol2 += '<a href="/users/edit-address/' + data.id + '" class="btn btn-info btn-sm" style="margin-right: 10px;"> 削除</a>';
            if (data.id != currentAddressId && input_data.length > 1)
                htmlCol2 += '<button onclick="deleteAddress(' + data.id + ')" class="btn btn-danger btn-sm" > 削除</button>';

            $(row).children('td').eq(2).append(htmlCol2);
        }
    });
}

function deleteAddress(addressId)
{
    bootbox.setDefaults({locale:"ja"});
    bootbox.confirm( {
        message:"この住所を削除しますか。",
        callback:  function(result) {
            if(result == true)
            {
                window.location.href = "/users/delete-address/"+addressId;
            }
        }
    });
}