var tbl_data = [];
var lockAddMercari = false;
var lockAddFuriru = true;
var lockAddRakuma = true;
var listAccountDataTable = "";

$(document).ready(function()
{
    make_init_data();

    $('#addMercariModal').on('hidden.bs.modal', function() {
        $('#addMercariForm').formValidation('resetForm', true);
    });

    $('#addFuriruModal').on('hidden.bs.modal', function() {
        $('#addFuriruForm').formValidation('resetForm', true);
    });

    $('#addRakumaModal').on('hidden.bs.modal', function() {
        $('#addRakumaForm').formValidation('resetForm', true);
    });
});

function make_init_data(){
    $.ajax({
        type: "POST",
        url: "/accounts/get-list-account-management",
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
            // console.log(errThrown);
        },
        success: function(data){
            if(data.result == "success"){
                init_tbl_data(data.listAccount, data.lockAddMercari, data.lockAddFuriru, data.lockAddRakuma);
                // make_list(tbl_data);
                makeTable(tbl_data);
            }else if(data.result == "error"){
                alert(data.msg);
            }

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").hide();
            }
        }
    });
}

function init_tbl_data(input_data, lockAddMer, lockAddFur, lockAddRak){
    tbl_data = input_data;
    lockAddMercari = lockAddMer;
    lockAddFuriru = lockAddFur;
    lockAddRakuma = lockAddRak;
    return;
}

function makeTable(accountList)
{
    if(listAccountDataTable)
        listAccountDataTable.fnDestroy();

    listAccountDataTable = $("#mainTbl").dataTable({
        "bPaginate" : true,
        "bLengthChange" : true,
        "bFilter" : true,
        "bSort" : false,
        "aaSorting" : [],
        "bInfo" : true,
        "bAutoWidth" : true,
        "iDisplayLength" : 20,
        "aLengthMenu" : [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
        "language": {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        data: accountList,
        columns: [
            {
                "title" : "No",
                "data" : null,
                "defaultContent" : ''
            },
            {
                "title" : "メルカリアカウントID",
                "data" : 'email'
            },
            {
                "title" : "フリルID",
                "data" : 'furiru_email'
            },
            {
                "title" : "Rakuma",
                "data" : 'rakuma_email'
            },
            {
                "title" : "ニックネーム",
                "data" : null,
                "defaultContent" : ''
            },
            {
                "title" : "操作",
                "data" : null,
                "defaultContent" : ''
            },
            {
                "title" : "変更",
                "data" : null,
                "defaultContent" : ''
            }
        ],
        createdRow: function (row, data, dataIndex)
        {
            $(row).children('td').eq(0).append(dataIndex + 1);

            // column 3
            if(data.is_block_rakuma)
            {
                var htmlCol3 = '<br>' + '<span style="color: #ff0000;">' + data.rakuma_error_message + '</span>';
                $(row).children('td').eq(3).append(htmlCol3);
            }

            // column 4
            var htmlCol4 = '';
            if(data.email != "" && data.email != null && data.email != undefined)
                htmlCol4 += '<td>' + data.name + '</td>';
            else if(data.furiru_email != "" && data.furiru_email != null && data.furiru_email != undefined)
                htmlCol4 += '<td>' + data.screen_name_furiru + '</td>';
            else
                htmlCol4 += '<td>' + data.rakuma_name + '</td>';

            $(row).children('td').eq(4).append(htmlCol4);

            // column 5
            var htmlCol5 = '';

            if((data.email == "" || data.email == null || data.email == undefined) && !lockAddMercari)
                htmlCol5 += '<p><button onclick="displayAddMercariModal(' + data.id + ')" class="btn btn-success btn-sm" id = "btnAddMercari' + data.id + '" name="btnAddMercari' + data.id + '"><i class="fa fa-plus"></i> メルカリアカウントを追加</button></p>';

            if((data.furiru_email == "" || data.furiru_email == null || data.furiru_email == undefined) && !lockAddFuriru)
                htmlCol5 += '<p><button onclick="displayAddFuriruModal(' + data.id + ')" class="btn btn-success btn-sm" id = "btnAddFuriru' + data.id + '" name="btnAddFuriru' + data.id + '"><i class="fa fa-plus"></i> フリルアカウント追加</button></p>';

            if((data.rakuma_email == "" || data.rakuma_email == null || data.rakuma_email == undefined) && !lockAddRakuma)
                htmlCol5 += '<p><button onclick="displayAddRakumaModal(' + data.id + ')" class="btn btn-success btn-sm" id = "btnAddRakuma' + data.id + '" name="btnAddRakuma' + data.id + '"><i class="fa fa-plus"></i> ラクマアカウント追加</button></p>';

            $(row).children('td').eq(5).append(htmlCol5);

            // column 6
            var htmlCol6 = '';
            if(data.is_block_mercari)
            {
                htmlCol6 +=  '<button onclick="displayAddMercariModal(' + data.id + ');" id="' + data.id + '" class="btn btn-warning btn-sm" style="display:block; margin-bottom:5px;">重複する。</button>';
                htmlCol6 +=  '<button onclick="deleteAccountModal(' + data.id + ');" id="' + data.id + '" class="btn btn-danger btn-sm" style="display:block;margin-bottom:5px;"><i class="fa fa-trash-o"></i>&nbsp;削除</button>';
            }
            else
            {
                // htmlCol6 +=  '<input type="file" id="file-input-ava-' + data.id + '" style="display: none;" accept="image/*" onchange="changeAvatar(this);">';
                // htmlCol6 +=  '<button onclick="uploadAvatar(' + data.id + ');" id="' + data.id + '" class="btn btn-primary btn-sm" style="display:block; margin-bottom:5px;">プロフィール変更</button>';
                if(data.furiru_email != "" && data.furiru_email != null)
                    htmlCol6 +=  '<button onclick="displayMoveFuriruModal(' + data.id + ');" class="btn btn-primary btn-sm" style="display:block;margin-bottom:5px;"><i class="fa fa-exchange"></i>&nbsp;フリルアカウント入れ替え</button>';

                htmlCol6 +=  '<button onclick="deleteAccountModal(' + data.id + ');" id="' + data.id + '" class="btn btn-danger btn-sm" style="display:block;margin-bottom:5px;"><i class="fa fa-trash-o"></i>&nbsp;削除</button>';
            }

            if(data.email != "" && data.email != null && data.email != undefined && data.furiru_email != "" && data.furiru_email != null)
            {
                htmlCol6 += '<button onclick="deleteMercariAccountModal(' + data.id + ');" id="' + data.id + '" class="btn btn-danger btn-sm" style="display:block;margin-bottom:5px;"><i class="fa fa-trash-o"></i>&nbsp;メルカリ削除</button>';
            }
            $(row).children('td').eq(6).append(htmlCol6);
        }
    });
}

// ==================== Mercari Account =============================

function displayAddMercariModal(accountId)
{
    $('#cmNoMercari').text(tbl_data.length + 1);
    $('#idAccountMercari').val(accountId);
    $('#addMercariModal').modal();
}

$("#addMercariForm").formValidation({
    framework: 'bootstrap',
    excluded: [':disabled'],
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err: {
        container: 'tooltip'
    },
    fields: {
        passwordMercari: {
            validators: {
                notEmpty: {
                    message: 'パスワードが必要です。'
                }
            }
        },
        emailMercari: {
            verbose: false,
            validators: {
                notEmpty: {
                    message: 'メールアドレスは必須です。'
                }
            }
        }
    }
}).on('success.form.fv', function(e) {
    // Prevent form submission

    e.preventDefault();

    // Retrieve instances
    var $form = $(e.target),        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance

    // Do whatever you want here ... ajax request
    addMercariAccount($form.serialize());
});

function addMercariAccount(serialized_data)
{
    $("#loading_overlay_add_mercari").show();
    $.ajax({
        type: "POST",
        url: "/accounts/add_account",
        data: serialized_data + "&accountManagement=1",
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay_add_mercari").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay_add_mercari").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay_add_mercari").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay_add_mercari").fadeOut(300);
            }
        }
    });
}
// ==================== End mercari Account =============================


// ==================== Furiru Account =================================

function displayAddFuriruModal(accountId)
{
    $('#cmNoFuriru').text(tbl_data.length+1);
    $('#idAccountFuriru').val(accountId);
    $('#addFuriruModal').modal();
}

$('#addFuriruForm').formValidation({
    framework: 'bootstrap',
    excluded: [':disabled'],
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err: {
        // You can set it to popover
        // The message then will be shown in Bootstrap popover
        container: 'tooltip'
    },
    fields: {
        passwordFuriru: {
            validators: {
                notEmpty: {
                    message: 'パスワードが必要です。'
                }
            }
        },
        emailFuriru: {
            verbose: false,
            validators: {
                notEmpty: {
                    message: 'メールアドレスは必須です。'
                }
            }
        }
    }
}).on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    // Retrieve instances
    var $form = $(e.target),        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
    addFuriruAccount($form.serialize());
});

function addFuriruAccount(serialized_data)
{
    $("#loading_overlay_add_furiru").show();
    $.ajax({
        type: "POST",
        url: "/accounts/addFuriruAccount",
        data: serialized_data + "&accountManagement=1",
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay_add_furiru").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay_add_furiru").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error" ){
                alert(data.msg);
            }
            else if(data.result == "errors" ){
                alert(data.msg);
            }
            $("#loading_overlay_add_furiru").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay_add_furiru").fadeOut(300);
            }
        }
    });
}

// ==================== End furiru Account =================================


// ==================== Rakuma Account =================================

function displayAddRakumaModal(accountId){
    $('#cmNoRakuma').text(tbl_data.length+1);
    $('#idAccountRakuma').val(accountId);
    $('#addRakumaModal').modal();
}

$('#addRakumaForm').formValidation({
    framework: 'bootstrap',
    excluded: [':disabled'],
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err: {
        // You can set it to popover
        // The message then will be shown in Bootstrap popover
        container: 'tooltip'
    },
    fields: {
        passwordRakuma: {
            validators: {
                notEmpty: {
                    message: 'パスワードが必要です。'
                }
            }
        },
        emailRakuma: {
            verbose: false,
            validators: {
                notEmpty: {
                    message: 'メールアドレスは必須です。'
                }
            }
        }
    }
}).on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    // Retrieve instances
    var $form = $(e.target),        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
    addRakumaAccount($form.serialize());
});

function addRakumaAccount(serialized_data)
{
    $("#loading_overlay_add_rakuma").show();
    $.ajax({
        type: "POST",
        url: "/accounts/add-rakuma-account",
        data: serialized_data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay_add_rakuma").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay_add_rakuma").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error" ){
                alert(data.msg);
            }
            else if(data.result == "errors" ){
                alert(data.msg);
            }
            $("#loading_overlay_add_rakuma").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay_add_rakuma").fadeOut(300);
            }
        }
    });
}

// ==================== End rakuma Account =================================

// ==================== Delete Account =====================================
function deleteAccountModal(idAccount) {
    $('#deleteAccountId').val(idAccount);
    $('#accountDeleteModal').modal();
}

function deleteAccount() {
    var idAccount = $('#deleteAccountId').val();
    $.ajax({
        type: "POST",
        url: "/accounts/delete",
        data: "account_id=" + idAccount,
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay_delete_account").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay_delete_account").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay_delete_account").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay_delete_account").fadeOut(300);
            }
        }
    });
}
// ==================== End delete Account ==================================

// ==================== Upload & change avatar ==============================
function uploadAvatar(idAccount) {
    $('#file-input-ava-' + idAccount).click();
}

function changeAvatar(obj) {
    var account_id = obj.id;
    var file = $(obj).prop("files")[0];

    var formdata = new FormData();
    formdata.append("image", file);
    formdata.append("account_id", account_id);
    $.ajax({
        url: "/accounts/changeAvatar",
        method : "POST",
        data : formdata,
        dataType : "json",
        contentType : false,
        processData : false,
        maxTimeout : 4000,
        beforeSend: function() {
            $("#loading_overlay2").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay2").fadeOut(300);
        },
        success : function(data) {
            location.reload();
        },
        complete : function() {


        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay2").hide();
            }
        }
    });
}
// ==================== End upload & change avatar ==========================

// ==================== Delete Mercari Modal ================================
function deleteMercariAccountModal(idAccount) {
    $('#deleteMercariAccountId').val(idAccount);
    $('#accountMercariDeleteModal').modal();
}

function deleteMercariAccount() {
    var idAccount = $('#deleteMercariAccountId').val();
    $.ajax({
        type: "POST",
        url: "/accounts/delete-mercari-account",
        data: "account_id=" + idAccount,
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay_delete_account_mercari").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay_delete_account_mercari").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay_delete_account_mercari").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay_delete_account_mercari").fadeOut(300);
            }
        }
    });
}
// ==================== End delete Mercari Modal ============================

// ==================== Move Account Furiru =================================
function displayMoveFuriruModal(accountId)
{
    number = parseInt(tbl_data.length) + 1;
    for (index in tbl_data)
    {
        if(tbl_data[index].id == accountId)
            number = parseInt(index) + 1;
    }
    $('#cmNoFuriruMove').text(number);
    $('#idAccountFuriruMove').val(accountId);
    $('#moveFuriruModal').modal();
}

$('#moveFuriruForm').formValidation({
    framework: 'bootstrap',
    excluded: [':disabled'],
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err: {
        // You can set it to popover
        // The message then will be shown in Bootstrap popover
        container: 'tooltip'
    },
    fields: {
        passwordFuriruMove: {
            validators: {
                notEmpty: {
                    message: 'パスワードが必要です。'
                }
            }
        },
        emailFuriruMove: {
            verbose: false,
            validators: {
                notEmpty: {
                    message: 'メールアドレスは必須です。'
                }
            }
        }
    }
}).on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    // Retrieve instances
    var $form = $(e.target),        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
    moveFuriruAccount($form.serialize());
});

function moveFuriruAccount(serialized_data)
{
    $("#loading_overlay_add_furiru").show();
    $.ajax({
        type: "POST",
        url: "/accounts/move-furiru-account",
        data: serialized_data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay_move_furiru").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay_move_furiru").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error" ){
                alert(data.msg);
            }
            else if(data.result == "errors" ){
                alert(data.msg);
            }
            $("#loading_overlay_move_furiru").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay_move_furiru").fadeOut(300);
            }
        }
    });
}
// ==================== End account Furiru ==================================