$(document).ready(function () {

    // $('#fee_default_all').keyup(function () {
    //     $(this).val($(this).val().replace(/[^\d]/g, ''));
    // });
    //
    // $('#price_order_all').keyup(function () {
    //     $(this).val($(this).val().replace(/[^\d]/g, ''));
    // });
    //
    // $('#fee_up_all').keyup(function () {
    //     $(this).val($(this).val().replace(/[^\d]/g, ''));
    // });

    // $('#fee_default_area').keyup(function () {
    //     $(this).val($(this).val().replace(/[^\d]/g, ''));
    // });
    //
    // $('#price_order_area').keyup(function () {
    //     $(this).val($(this).val().replace(/[^\d]/g, ''));
    // });
    //
    // $('#fee_up').keyup(function () {
    //     $(this).val($(this).val().replace(/[^\d]/g, ''));
    // });

    $('#input_post_code').keyup(function () {
        $(this).val($(this).val().replace(/[^\d\,]/g, ''));
    });


    $('#add_fee_area').click(function () {
        addShippingFee();
    });


    $('#save_shipping_fee').click(function () {
        saveShippingFee();
    });

});

function addShippingFee() {
    var fee_default_area = $('#fee_default_area').val().trim();
    var price_order_area = $('#price_order_area').val().trim();
    var fee_up = $('#fee_up').val().trim();
    var input_post_code = $('#input_post_code').val().trim();
    var error = '';
    //console.log(html);
    if(!fee_default_area) {
        error += '<p>送料を入力してください。</p>';
    }

    if(price_order_area > 0 && !fee_up) {
        error += '<p>お得送料を入力してください。</p>';
    }

    if (fee_up > 0 && !price_order_area) {
        error += '<p>売上額を入力してください。</p>';
    }

    if (!input_post_code) {
        error += '<p>各地域の郵便番号の特定部を入力してください。</p>'
    }
    if(error) {
        $('.error').html(error);
        $('.error').show(300);

    } else {
        $('.error').hide(300);
        var html = $('#data').html();
        html += '<tr>';
        html += '<td class="area">' + input_post_code + '</td>';
        html += '<td class="fee_default">' + fee_default_area + '</td>';
        html += '<td class="price_order">' + price_order_area + '</td>';
        html += '<td class="fee_up">' + fee_up + '</td>';
        html += '<td>' +
            '<button type="button" class="btn btn-danger delete-fee" id="" onclick="deleteFee(this)">削除</button></td>' +
                '<input type="hidden" name="id_fee_area" value="">' +
            '</tr>';

        $('#data').html(html);

        $('#input_post_code').val('');
        $('#fee_default_area').val('');
        $('#price_order_area').val('');
        $('#fee_up').val('');

    }
}

function saveShippingFee() {
    var fee_default_all = $('#fee_default_all').val().trim();
    var price_order_all = $('#price_order_all').val().trim();
    var fee_up_all = $('#fee_up_all').val().trim();
    var id_fee_all = $('#id_fee_all').val().trim();

    var fee_area = [];
    var i = 0;
    $('#data tr').each(function () {
        fee_area[i] = {
            area: $(this).find('.area').html(),
            fee_default : $(this).find('.fee_default').html(),
            price_order : $(this).find('.price_order').html(),
            fee_up : $(this).find('.fee_up').html(),
            id : $(this).find('.id_fee_area').val()
        };
        i++
    });


    var error2 = '';

    if(!fee_default_all) {
        error2 += '<p>全地域発送を入力してください。</p>';
    }

    if(price_order_all && !fee_up_all) {
        error2 += '<p>全地域へのお得送料を入力してください。</p>';
    }

    if(fee_up_all && !price_order_all) {
        error2 += '<p>全地域への売上額を入力してください。</p>';
    }

    if(error2) {
        $('.error2').html(error2);
        $('.error2').show(300);
    } else {
        $('.error2').hide(300);
        var data = {
            fee_all : {
                fee_default_all: fee_default_all,
                price_order_all : price_order_all,
                fee_up_all: fee_up_all,
                id_fee_all: id_fee_all
            },
            fee_area : fee_area

        };

        $.ajax(
            {
                type: "POST",
                url: "/shipping-fee/save",
                data: data,
                dataType: "json",

                beforeSend: function() {
                },
                error: function(data, status, errThrown){
                    console.log(errThrown);
                },
                success: function(data){
                    if(data.status == 'success') {
                        bootbox.alert('保存しました');
                    } else {
                        bootbox.alert('error');
                    }
                },
                statusCode: {
                    404: function() {
                        bootbox.alert('Error');
                    }
                }
            });

    }

}

function deleteFee(obj){
    bootbox.confirm({
        message: "本当に削除しますか。",
        buttons: {
            confirm: {
                label: 'はい' // yes
            },
            cancel: {
                label: 'いええ'// no
            }
        },
        callback: function (result) {
            if(result) {

                var id = $(obj).attr('id').trim();
                if(id) {
                    $.ajax({
                        type: "POST",
                        url: "/shipping-fee/delete",
                        data: {
                            id: id
                        },
                        dataType: "json",

                        beforeSend: function() {
                        },
                        error: function(data, status, errThrown){
                            console.log(errThrown);
                        },
                        success: function(data){
                            if(data.status == 'success') {
                                $(obj).parent('td').parent('tr').remove();
                            } else {
                                bootbox.alert('Error');
                            }
                        },
                        statusCode: {
                            404: function() {
                                bootbox.alert('Error');
                            }
                        }
                    });
                } else {
                    $(obj).parent('td').parent('tr').remove();
                }
            }

        }
    });

}