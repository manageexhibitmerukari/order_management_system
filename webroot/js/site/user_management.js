var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;
var listUserDataTable = "";

$(document).ready(function() {
    make_init_data();

    $(function (argument) {
        $('#active_buy_account').bootstrapSwitch();
    });

    $('#user_detail_modal').on('hidden.bs.modal', function() {
        $('#user_detail_form').formValidation('resetForm', true);
        $('#user_detail_modal #current_user').val("0");
        if(refresh_list)
            make_init_data();
    });

    $('#user_delete_modal').on('hidden.bs.modal', function() {
        $('#user_delete_modal #delete_user_id').val("0");
        if(refresh_list)
            make_init_data();
    });


    $('#user_detail_modal, #user_delete_modal').on('shown.bs.modal', function() {
        refresh_list = false;
    });

    $('#user_detail_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            username: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'ログインIDが必要です。'
                    },
                    remote: {
                        type: 'POST',
                        url: '/users/check_duplicate',
                        data: function(validator, $field, value){
                            return {
                                check_field:"username",
                                current_user: validator.getFieldElements("current_user").val()
                            }
                        },
                        dataType: 'json',
                        validKey: 'is_valid',
                        message: 'このログインIDは既に使用されています。'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    }
                }
            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    },
                    /*emailAddress: {
                     message: '有効なメールアドレスを入力してください。'
                     }*/
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください'
                    },
                    remote: {
                        type: 'POST',
                        url: '/users/check_duplicate',
                        data: function(validator, $field, value){
                            return {
                                check_field:"email",
                                current_user: validator.getFieldElements("current_user").val()
                            }
                        },
                        dataType: 'json',
                        validKey: 'is_valid',
                        message: 'このメールはすでに使用されています。'
                    }
                }
            },
            max_account: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            max_account_mercari: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            max_account_furiru: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            max_account_rakuma: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission

        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

        if (update_user_data($form.serialize())) {
            $('#user_detail_modal').modal('hide');
            refresh_list = true;
        }
    });
});

function update_user_data(serialized_data){
    // console.log(serialized_data);
    var is_success = false;
    $("#loading_overlay1").show();
    $.ajax(
        {
            type: "POST",
            url: "/users/update-user-info-user-management",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay1").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay1").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                }else if(data.result == "error"){
                    is_success = false;
                    alert(data.msg);
                }
                $("#loading_overlay1").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}

function make_init_data(){
    // call back ajax function
    $.ajax(
        {
            type: "POST",
            url: "/users/get-list-user-management",
            //contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                console.log(errThrown);
            },
            success: function(data){
                if(data.result == "success"){
                    $("#btn_add_user").attr("disabled", false);
                    init_tbl_data(data.msg);
                    makeTable(tbl_data);
                    $('#maxAccount').html(data.maxAccounts);
                    $('#remainAccount').html(data.remainAccounts);
                    $('#maxMercariAccount').html(data.maxAccountsMercari);
                    $('#remainMercariAccount').html(data.remainAccountsMercari);
                    $('#maxFuriruAccount').html(data.maxAccountsFuriru);
                    $('#remainFuriruAccount').html(data.remainAccountsFuriru);
                    $('#maxRakumaAccount').html(data.maxAccountsRakuma);
                    $('#remainRakumaAccount').html(data.remainAccountsRakuma);
                }
                else if(data.result == "error")
                    alert(data.msg);

                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
    return;
}

function init_tbl_data(input_data){
    tbl_data = input_data;
    return;
}

function makeTable(input_data)
{
    if(listUserDataTable)
        listUserDataTable.fnDestroy();

    listUserDataTable = $('#mainTbl').dataTable({
        "bPaginate" : true,
        "bLengthChange" : true,
        "bFilter" : true,
        "bSort" : false,
        "aaSorting" : [],
        "bInfo" : true,
        "bAutoWidth" : true,
        "iDisplayLength" : 20,
        "aLengthMenu": [[ 20, 40, 60, 80, 100, -1], [ 20, 40, 60, 80, 100, "すべて"]],
        "language" : {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        data: input_data,
        columns : [
            {
                "title" : "No",
                "data" : null,
                "defaultContent" : ''
            },
            {
                "title" : "ログインID",
                "data" : "username"
            },
            {
                "title" : "メールアドレス",
                "data" : "user_profile.email"
            },
            {
                "title" : "最大アカウント数",
                "data" : "user_profile.max_account"
            },
            {
                "title" : "メルカリオプション",
                "data" : "user_profile.mercari_max_account"
            },
            {
                "title" : "最大フリルアカウント数",
                "data" : "user_profile.furiru_max_account"
            },
            {
                "title" : "Rakuma アカウント数",
                "data" : "user_profile.rakuma_max_account"
            },
            {
                "title" : "作用",
                "data" : null,
                "defaultContent" : ""
            }
        ],
        createdRow : function (row, data, dataIndex)
        {
            $(row).children('td').eq(0).append(dataIndex + 1);


            // column 7
            var htmlCol7 = '';
            var No = parseInt(dataIndex) + 1;
            htmlCol7 += '<button onclick="displayUpdateUI(this, ' + No + ');" id="' + data.id + '" class="btn btn-primary btn-sm" style="display:block; margin-bottom:5px"><i class="fa fa-edit"></i>&nbsp;更新</button>';

            if(data.level == 0){
                htmlCol7 +=  '<button onclick="displayDeleteUI(this);" id="' + data.id + '" class="btn btn-danger btn-sm" style="display:block"><i class="fa fa-trash-o"></i>&nbsp;削除</button>';
            }
            $(row).children('td').eq(7).append(htmlCol7);
        }
    });
}

function displayInsertUI(){
    $('#cm_no').text(tbl_data.length+1);
    tbl_data_pos = -1;
    $('#user_detail_modal #current_user').val("0");
    $('#user_detail_modal #email').prop("readonly", false);
    $('#user_detail_modal').modal();
}

function displayUpdateUI(obj, No){
    var edit_user_id = parseInt(obj.id, 10);

    tbl_data_pos=tbl_data.map(function (element) { return element.id;}).indexOf(edit_user_id);

    $('#user_detail_modal #cm_no').text(No);
    $('#user_detail_modal #username').val(tbl_data[tbl_data_pos].username);
    $('#user_detail_modal #current_user').val(tbl_data[tbl_data_pos].id);
    $('#user_detail_modal #password').val(tbl_data[tbl_data_pos].password);
    $('#user_detail_modal #email').val(tbl_data[tbl_data_pos].user_profile.email);
    $('#user_detail_modal #max_account').val(tbl_data[tbl_data_pos].user_profile.max_account);
    $('#user_detail_modal #max_account_mercari').val(tbl_data[tbl_data_pos].user_profile.mercari_max_account);
    $('#user_detail_modal #max_account_furiru').val(tbl_data[tbl_data_pos].user_profile.furiru_max_account);
    $('#user_detail_modal #max_account_rakuma').val(tbl_data[tbl_data_pos].user_profile.rakuma_max_account);
    (tbl_data[tbl_data_pos].user_profile.active_buy_account == 1) ? $('#user_detail_modal #active_buy_account').bootstrapSwitch('state', true) : $('#user_detail_modal #active_buy_account').bootstrapSwitch('state', false);
    $('#user_detail_modal #email').prop("readonly", true);
    if(tbl_data[tbl_data_pos].level == 5)
    {
        $('#user_detail_modal #max_account_rakuma').prop("readonly", true);
        $('#user_detail_modal #max_account_furiru').prop("readonly", true);
        $('#user_detail_modal #max_account_mercari').prop("readonly", true);
        $('#user_detail_modal #max_account').prop("readonly", true);
    } else {
        $('#user_detail_modal #max_account_rakuma').prop("readonly", false);
        $('#user_detail_modal #max_account_furiru').prop("readonly", false);
        $('#user_detail_modal #max_account_mercari').prop("readonly", false);
        $('#user_detail_modal #max_account').prop("readonly", false);
    }

    $('#user_detail_modal').modal();
}

function displayDeleteUI(obj){
    var delete_user_id = parseInt(obj.id, 10);
    $("#delete_user_id").val(delete_user_id);
    $("#user_delete_modal").modal();
}

function deleteUser(){
    var delete_user_id = $("#delete_user_id").val();
    $.ajax(
        {
            type: "POST",
            url: "/users/delete",
            data: "user_id="+delete_user_id,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay2").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay2").fadeOut(300);
            },
            success: function(data){
                if(data.result == "success"){
                    refresh_list = true;
                    $("#user_delete_modal").modal('hide');
                }else if(data.result == "error"){
                    alert(data.msg);
                }
                $("#loading_overlay2").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay2").fadeOut(300);
                }
            }
        });
    return;
}