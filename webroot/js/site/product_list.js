////////////////////////////////////////////////////      ユーザー管理       /////////////////////////////////////////////

var CONTEXT_NAME = "user-products";
var tbl_data = [];
var currentpage = 0;
var firstload = true;
var allchecked = false;
var main_Table;
var statusDelete = 0;



$(document).ready(function() {
    make_init_data();
    var item_id = [];
    $(document).on('click', '#main_tbl tbody input[type="checkbox"]', function(){
        allchecked = false;
        $('#flowcheckall').prop('checked', false);
        if ($(this).is(':checked')){
            item_id.push($(this).attr('item-id'));
        }
    });
    $(document).on('click', '#flowcheckall', function() {
        if ($(this).is(':checked')) {
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', true);
                allchecked = true;
            });
        } else {
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', false);
                allchecked = false;
            });
        }
    });
    $(document).on('click', '.paginate_button ', function(){
        if ( statusDelete == 1 ) {
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                if ($(this).is(':checked')) {
                    var selector = $(this).parent().parent();
                    main_Table.row(selector).remove().draw( false );
                }
            });
        }
        if (allchecked == true){
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', true);
            });
        }else {
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', false);
            });
        }
    });

    $(document).on('click', '.remove-checked', function() {
        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm({
            message: "この商品を削除してもよろしいですか。",
            callback: function (result) {
                if (result) {
                    var data = {
                        id: item_id
                    };
                    if (allchecked == true) {
                        data.all = true;
                    }
                    if (item_id.length > 0 || isset(data.all)) {
                        $.ajax({
                            type: "POST",
                            url: "/user-products/delete-product",
                            contentType: "application/x-www-form-urlencoded;",
                            dataType: "json",
                            data: data,
                            beforeSend: function () {
                                $("#loading_overlay").fadeIn(300);
                            },
                            error: function (data, status, errThrown) {
                                console.log(errThrown);
                                $("#loading_overlay").fadeOut(300);
                            },
                            success: function (data) {
                                console.log(data);
                                if (data.result == 'success') {
                                    if (data.msg == 'all') {
                                        main_Table.rows().remove().draw(false);
                                    } else {
                                        $('#main_tbl tbody input[type="checkbox"]').each(function () {
                                            statusDelete = 1;
                                            if ($(this).is(':checked')) {
                                                item_id.push($(this).attr('item-id'));
                                                var selector = $(this).parent().parent();
                                                main_Table.row(selector).remove().draw(false);
                                            }
                                        });
                                    }
                                    item_id = [];
                                }
                                $("#loading_overlay").fadeOut(300);
                            },
                            statusCode: {
                                404: function () {
                                    alert('page not found');
                                    $("#loading_overlay1").hide();
                                }
                            }
                        });
                    }
                }
            }
        });
    });
});

function isset(variable) {
    return typeof variable !== typeof undefined ? true : false;
}

function make_init_data(){
    // call back ajax function
    if($("#loading_overlay").not(":visible"))
        $("#loading_overlay").fadeIn(300);

    $.ajax({
            type: "POST",
            url: "/user-products/product-list",
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                init_tbl_data(data.products);
                make_list(tbl_data);
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
    return;
}

function init_tbl_data(input_data){
    tbl_data = input_data;
    return;
}

function make_list(input_data) {
    var main_tbl_wrap = $('#main_tbl_wrap');
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover" style="width: 100%;">';
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);
    if (input_data != null){
        main_Table = $('#main_tbl').DataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true,
            "aLengthMenu": [[ 20, 40, 60, 80, 100, -1], [ 20, 40, 60, 80, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            data : input_data,
            columns : [
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": "<input type='checkbox' id='flowcheckall' "+ ((allchecked)?'checked':'') + ">"
                },
                {
                    "data" : 'SKU',
                    "defaultContent" : '',
                    "title": "SKU"
                },
                {
                    "data" : 'photo',
                    "defaultContent" : '',
                    "title": "画像"
                },
                {
                    "data" : 'name',
                    "defaultContent" : '',
                    "title": "商品名"
                },
                {
                    "data" : 'color',
                    "title": "色"
                },
                {
                    "data" : 'price',
                    "title": "価格"
                },
                {
                    "data" : 'amount',
                    "defaultContent" : '',
                    "title": "在庫数",
                    "width" : "150px"
                },
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": "",
                    "width" : "150px"
                }
            ],
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [0,2,7]
            } ],
            createdRow : function(row, data, dataIndex)
            {
                var innerHtml = '<input type = checkbox id="check" class="call-checkbox" item-id="' + data.id + '">';
                $(row).children('td').eq(0).html(innerHtml);
                var timestamp = Number(new Date());
                var imgLink = '<a href="' + data.photo + '" target="_blank">' +
                    '<img src="' + data.photo + '?'+ timestamp +'" style="width:101px; height:101px;">' +
                    '</a>';
                $(row).children('td').eq(2).html(imgLink);
                // product name
                var productNameHtml = '<a href = "/user-products/product-sub-list?id=' + data.id + '">' + data.name + '</a>';
                $(row).children('td').eq(3).html(productNameHtml);

                var color = '';
                data.color.forEach(function(value) {
                    color+= '<span>' + value + '</span><br/>';
                });
                $(row).children('td').eq(4).html(color);

                var cloneButton = '<a href="/user-products/add-main-product?clone_product_id=' + data.id + '" class="btn btn-success" style="width: 130px">参照登録</a><br><br>';
                var action = '<a href="/user-products/edit-main-product?id=' + data.id + '" class="btn btn-success" style="width: 130px">商品編集</a>';
                $(row).children('td').eq(7).html(cloneButton + action);
            }
        });
    }
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

function displayCSVImportUI(){
    $("#csv_import_modal").modal();
}

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));

