/**
 * Created by root on 3/6/17.
 */
var dataTable;

$(document).ready(function () {
    console.log('ok');
    getData();
});
function getData()
{
    // $("#loading_overlay").show();
    $.ajax
    ({
        type: "GET",
        url: "/suks/show",
        data: {},
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function(xhr) {
            // xhr.setRequestHeader('X-Test-Header', 'test-value');
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data){
            console.log(data.suk);
            make_list(data.suk);
            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").fadeOut(300);
            }
        }
    });
}


function make_list(suks)
{
    if (dataTable) {
        dataTable.fnDestroy();
    }
    console.log('make_list');
    dataTable = $('#main_tbl').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "deferRender": true,
        "aaSorting": [],
        "bInfo": true,
        "bAutoWidth": true,
        "iDisplayLength": 20,
        "aLengthMenu": [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
        "language": {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        data : suks,
        columns : [
            {
                "data" : 'suk',
                "defaultContent" : '',
                "title": "販売者"
            },
            {
                "data" : 'username',
                "defaultContent" : '',
                "title": "プロフィール"
            },
            {
                "data" : 'userId',
                "defaultContent" : '',
                "title": "ニックネーム"
            },
            {
                "data" : 'description',
                "title": "個数"
            },
            {
                "data" : 'count',
                "title": "総売り上げ"
            },
            {
                "data" : 'created',
                "title": "平均単価"
            }
        ],
        createdRow : function(row, data, dataIndex) {

        },
        "aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [1]
        }]
    });
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }

    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }

    return init();
}));


// add seller to list favorite sellers in users/seller-management
function addFavoriteSeller(idSeller) {
    // console.log(idSeller);
    $.ajax({
        type: "POST",
        url: "/users/addFavoriteSeller",
        data: "idSeller="+idSeller,
        dataType: "json",
        async: true,
        beforeSend: function () {
            $("#loading_overlay").fadeIn(300);
        },
        error: function (data, status, errThrown) {
            $("#loading_overlay").fadeOut(300);
        },
        success: function (data) {
            alert(data.result);
            $("#loading_overlay").fadeOut(300);
        }
    });
}
