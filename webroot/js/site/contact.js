
$(document).ready(function()
{
    $('#contact_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            firstname: {
                validators: {
                    notEmpty: {
                        message: 'お名前を入力してください。'
                    },
                    callback: {
                        message: '20文字以下で入力してください。',
                        callback: function (value, validation, fields ) {
                            return value.length < 20;
                        }
                    }
                }
            },
            lastname: {
                validators: {
                    notEmpty: {
                        message: 'フリガナを入力してください。'
                    }

                }
            },
            address: {
                validators: {
                    callback: {
                        message: '100文字以下で入力してください。',
                        callback: function (value, validation, fields ) {
                            return value.length < 100;
                        }
                    }
                }
            },
            phone: {
                validators: {
                    numeric: {
                        message: '電話番号形式に誤りがあります。'
                    },
                    stringLength: {
                        message: '数字を8〜13桁で入力してくだだい。',
                        max: 13,
                        min: 8
                    }

                }

            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスを入力してください'
                    },
                    emailAddress: {
                        message: '有効なメールアドレスを入力してください。'
                    },
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください'
                    }
                }
            },

            content : {
                validators: {
                    notEmpty: {
                        message: '問い合わせ内容を入力してください。'
                    },
                    callback: {
                        message: '1000文字以下で入力してください。',
                        callback: function (value, validation, fields ) {
                            return value.length < 1000;
                        }
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        // Prevent form submission
        //e.preventDefault();

        // Retrieve instances
        //var $form = $(e.target),        // The form instance
        //   fv = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request
    });
});