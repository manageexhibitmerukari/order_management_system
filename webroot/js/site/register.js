$(document).ready(function () {
    $('#register_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            username: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'ログイン IDが必要です'
                    },
                    regexp: {
                        regexp: /^\S*$/i,
                        message: 'ログイン IDに空白を利用できません。'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    }
                }
            },
            confirm_password: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    },
                    callback: {
                        message: 'パスワードの確認が違います。',
                        callback: function (value, validator, $field) {
                            var item_value = $('#password').val();
                            return value == item_value;
                        }
                    }
                }
            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    },
                    emailAddress: {
                        message: '有効なメールアドレスを入力してください。'
                    },
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: '電話番号が必要です。'
                    },
                    numeric: {
                        message: '電話番号形式に誤りがあります。'
                    },
                    stringLength: {
                        message: '数字を8〜13桁で入力してくだだい。',
                        max: 13,
                        min: 8
                    }

                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'お名前が必要です。'
                    }
                }
            },
            payment_name: {
                validators: {
                    notEmpty: {
                        message: 'ご請求先名が必要です。'
                    }
                }
            },
            name_kana: {
                validators: {
                    notEmpty: {
                        message: 'フリガナが必要です。'
                    }
                }
            },
            postal: {
                validators: {
                    notEmpty: {
                        message: '郵便番号が必要です。'
                    },
                    /*numeric: {
                     message: '有効な郵便番号を入力してください。'
                     },*/
                    blank: {}
                    // stringLength: {
                    //     message: '郵便番号は7桁の数字です。',
                    //     max: 7,
                    //     min: 7
                    // }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: '都道府県が必要です。'
                    }
                }
            },
            town: {
                validators: {
                    notEmpty: {
                        message: '市町村が必要です。'
                    }
                }
            }
        }
    }).on('focusout', '[name="postal"]', function () {
        var zip_code = $(this).val().trim();
        $(this).val(zip_code);
        if(zip_code.length == 0){
            return true;
        }
        checkZipCode(zip_code);
    }).on('success.form.fv', function (e, data) {


    });
});

function checkZipCode(zip_code) {
    // var zip_code = $('#postal').val();
    $.ajax({
        type: "GET",
        url: "http://zipcloud.ibsnet.co.jp/api/search?zipcode=" + zip_code,
        async: false,
        dataType: "jsonp",
        beforeSend: function () {
        },
        error: function (data, status, errThrown) {
            $('#register_form').data('formValidation').updateMessage('postal', 'blank', "有効な郵便番号を入力してください。").updateStatus('postal', 'INVALID', 'blank');
        },
        success: function (data) {
            if (data.results === null) {
                message = (data.message!=null)?data.message:"有効な郵便番号を入力してください。";
                $('#register_form').data('formValidation').updateMessage('postal', 'blank',message).updateStatus('postal', 'INVALID', 'blank');
                // bootbox.alert('有効な郵便番号を入力してください。');
                $('#register_form')
                    .find('input[name="state"]').val('').end()
                    .find('input[name="town"]').val('').end()
                    .find('input[name="house_number"]').val('');
            }
            else {
                var address = data.results[0];
                $('#state').val(address.address1);
                $('#town').val(address.address2);
                $('#house-number').val(address.address3);
            }

            // Revalidate the fields
            $('#register_form')
                .formValidation('revalidateField', 'state')
                .formValidation('revalidateField', 'town')
                .formValidation('revalidateField', 'house_number');
        },
        statusCode: {
            404: function () {
                alert('page not found');
            }
        }
    });
}