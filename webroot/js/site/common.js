    var globalVariable = new function (){
        var isBusy = false;
        this.isBusy = isBusy;
    };

    function isEmpty(value){
        return (value == null || value.length === 0);
    }

    var vuePatternComment = new Vue({
        el: '.pattern-comment',
        data: {
            patternList: [],
            item_id:''
        }
    });

    var vuePatternMessage = new Vue({
        el: '.pattern-message',
        data: {
            patternList: [],
            item_id:''
        }
    });

    function changePattern(obj){
        var value = obj.value;
        $(".content-pattern").val(value);
    }

    function changeCmtPattern(obj){
        var value = obj.value;
        $(".content-cmt-pattern").val(value);
    }

    function changeMsgPattern(obj){
        var value = obj.value;
        $(".content-msg-pattern").val(value);
    }

    function getPattenComment(){
    $.ajax({
        type: "POST",
        url: "/pattern-comment/getCommentPatterns",
        dataType: "json",
        beforeSend: function() {
        },
        error: function(data, status, errThrown){
        },
        success: function(data){
            vuePatternComment.patternList = data.listPattern;
        },
        statusCode: {
            404: function() {
            }
        }
    });
}

    function getPattenMessage() {
        $.ajax({
            type: "POST",
            url: "/pattern-message/getMessagePatterns",
            dataType: "json",
            beforeSend: function () {
            },

            error: function (data, status, errThrown) {
            },
            success: function (data) {
                vuePatternMessage.patternList = data.listPattern;
            },
            statusCode: {
                404: function () {
                }
            }
        });
    }


    function isEmail(email) {
    // add support to + sign (Nghi: 17/1/2016) START
    var regex = /^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    // add support to + sign (Nghi: 17/1/2016) END
    if (regex.test(email)){
        return true;
    }
    return false;
}


function checkAlphabet(sentences) {
	var regex = /^[A-Za-z0-9]+$/i;
	if (!regex.test(sentences)){
        return false;
    }
	return true;
}

function checkFacebookName(sentences) {
	var regex = /^[A-Za-z0-9\.]+$/i;
	if (!regex.test(sentences)){
        return false;
    }
	return true;
}

function ValidateTypeImg() {
    var fuData = document.getElementById('fileChooser');
    var FileUploadPath = fuData.value;

    //To check if user upload any file
    if (FileUploadPath == '') {
        return "Please upload an image";

    } else {
        var Extension = FileUploadPath.substring(
            FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image

        if (Extension == "gif" || Extension == "png" || Extension == "bmp"
            || Extension == "jpeg" || Extension == "jpg") {

            // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                };

                reader.readAsDataURL(fuData.files[0]);
            }

        }

        //The file upload is NOT an image
        else {
            alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");

        }
    }
}

/**
 * Toggle header (search, favorite artist, high five)
 */ 
function isActived(obj) {
	if(obj.hasClass('active')) {
		return true;
	}
	return false;
}
function toggleHeader(obj) {	
	$('li a', obj.closest('ul')).removeClass('active');
	obj.addClass('active');
}

function formatNumber(num) {
    num += '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(num)) {
        num = num.replace(rgx, '$1' + ',' + '$2');
    }
    return num;
}
    function nl2br (str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
    }