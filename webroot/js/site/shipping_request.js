/**
 * Created by root on 4/11/17.
 */

var CONTEXT_NAME = "shipping-request";
var tbl_data = [];
var currentpage = 0;
var firstload = true;
var allchecked = false;
var main_Table;



$(document).ready(function() {
    make_init_data();

    $(document).on('click', '#change-status', function() {
        var id = $(this).attr('item-id');
        var order_id = $(this).attr('order-id');
        var user_id = $(this).attr('user-id');

        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm({
            message: "発送済にしてよろしいですか。",
            callback: function (result) {
                if (result) {

                    $.ajax({
                        type: "POST",
                        url: "/shipping-requests/change-status",
                        contentType: "application/x-www-form-urlencoded;",
                        dataType: "json",
                        data : {
                            id : id,
                            order_id : order_id,
                            user_id : user_id
                        },
                        beforeSend: function() {
                            $("#loading_overlay").fadeIn(300);
                        },
                        error: function(data, status, errThrown){
                            console.log(errThrown);
                            $("#loading_overlay").fadeOut(300);
                        },
                        success: function(data){
                            if (data.result == 'success') {
                                console.log('.change-status-'+id);
                                $('.change-status-'+id).hide();
                                $('.status-text-'+id).html("発送済");
                                // $('#main_tbl tbody #change-status').each(function() {
                                //     ()
                                //     if ($(this).attr('item-id') == id) {
                                //
                                //         var row = $(this).parents('tr');
                                //         main_Table.row(row).remove().draw(false);
                                //     }
                                // });
                            } else {
                                alert(data.msg);
                            }

                            $("#loading_overlay").fadeOut(300);
                        },
                        statusCode: {
                            404: function() {
                                alert('page not found');
                                $("#loading_overlay1").hide();
                            }
                        }
                    });
                }
            }
        });
    });

    var item_id = [];
    $(document).on('click', '#main_tbl tbody input[type="checkbox"]', function(){
        allchecked = false;
        $('#flowcheckall').prop('checked', false);
        if ($(this).is(':checked')){
            item_id.push($(this).attr('item-id'));
        } else {
            item_id.pop($(this).attr('item-id'));
        }
        console.log(item_id);
        console.log(allchecked);
    });
    $(document).on('click', '#flowcheckall', function() {
        if ($(this).is(':checked')) {
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', true);
                allchecked = true;
            });
        } else {
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', false);
                allchecked = false;
            });
            item_id = [];
        }

    });
    $(document).on('click', '.paginate_button ', function(){
        if (allchecked == true){
            $('#main_tbl tbody input[type="checkbox"]').each(function () {
                $(this).prop('checked', true);
            });
        }
    });

    $('.export-csv').click(function () {
        if (item_id.length == 0 && allchecked == false) {
            alert('選択されたデータがありません。データを選択してください。');
        } else {
            var data = {
                'id' : item_id,
                'all' : ''
            };
            if (allchecked == true)
                data.all = 'all';

            $.ajax({
                type: "POST",
                url: "/shipping-requests/get-export",
                contentType: "application/x-www-form-urlencoded;",
                dataType: "json",
                data : data,
                beforeSend: function() {
                    $("#loading_overlay").fadeIn(300);
                },
                error: function(data, status, errThrown){
                    console.log(errThrown);
                    $("#loading_overlay").fadeOut(300);
                },
                success: function(data){
                    if (data.result == 'success') {
                        window.location = location.origin + '/files/csv/admin/item.csv';
                    }
                    $("#loading_overlay").fadeOut(300);
                },
                statusCode: {
                    404: function() {
                        alert('page not found');
                        $("#loading_overlay1").hide();
                    }
                }
            });
        }

    });

});

function isset(variable) {
    return typeof variable !== typeof undefined ? true : false;
}

function make_init_data(){
    // call back ajax function
    if($("#loading_overlay").not(":visible"))
        $("#loading_overlay").fadeIn(300);

    $.ajax({
        type: "POST",
        url: "/shipping-requests/list-shipping",
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            console.log(errThrown);
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data){
            init_tbl_data(data.products);
            make_list(tbl_data);
            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").hide();
            }
        }
    });
    return;
}

function init_tbl_data(input_data){
    tbl_data = input_data;
    return;
}

function make_list(input_data) {
    var main_tbl_wrap = $('#main_tbl_wrap');
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover" style="width: 100%;">';
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);
    if (input_data != null){
        main_Table = $('#main_tbl').DataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "aLengthMenu": [[ 20, 40, 60, 80, 100, -1], [ 20, 40, 60, 80, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            data : input_data,
            columns : [
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": "<input type='checkbox' id='flowcheckall' "+ ((allchecked)?'checked':'') + ">"
                },
                {
                    "data" : 'order_id',
                    "defaultContent" : '',
                    "title": "注文番号",
                    "width" : "70px"
                },
                {
                    "data" : 'product_title',
                    "defaultContent" : '',
                    "title": "商品名"
                },
                {
                    "data" : 'product_photo1',
                    "defaultContent" : '',
                    "title": "画像"
                },
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": "発送情報",
                    "width" : "300px"
                },
                {
                    "data" : 'amount',
                    "defaultContent" : '',
                    "title": "数量",
                    "width" : "50px"
                },
                {
                    "data" : 'price',
                    "title": "価格",
                    "width" : "50px"
                },
                {
                    "data" : 'ship_status',
                    "defaultContent" : '',
                    "title": "発送状態",
                    "width" : "150px"
                },
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": "",
                    "width" : "150px"
                }
            ],
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [0,3,4,7,8]
            } ],
            createdRow : function(row, data, dataIndex)
            {
                var innerHtml = '<input type = checkbox id="check" class="call-checkbox" item-id="' + data.id + '">';
                $(row).children('td').eq(0).html(innerHtml);
                // var timestamp = Number(new Date());
                var imgLink = '<a href="' + data.product_photo1 + '" target="_blank">' +
                    '<img src="' + data.product_photo1 +'" style="width:101px; height: 101px;">' +
                    '</a>';

                var product_title = '<a href="/user-products/detail-product?id='+data.main_products.id+'" target="_blank">'+ data.product_title+'</a>';

                $(row).children('td').eq(2).html(product_title);

                $(row).children('td').eq(3).html(imgLink);
                // // product name

                var address = data.addresses.name + '<br>'
                    + data.addresses.zip_code + ' '+ data.addresses.city + data.addresses.address1 + data.addresses.address2 + '<br>'
                    + '電話番号: ' +data.addresses.phone_number;
                $(row).children('td').eq(4).html(address);

                var productNameHtml = '';
                if (data.ship_status) {
                    productNameHtml = '<span class="status-text-'+data.id+'">発送済</span>';
                } else {
                    productNameHtml = '<span class="status-text-'+data.id+'">未発送</span>';
                }
                $(row).children('td').eq(7).html(productNameHtml);
                //
                // var color = '';
                // data.color.forEach(function(value) {
                //     color+= '<span>' + value + '</span><br/>';
                // });
                // $(row).children('td').eq(4).html(color);
                /// /shipping-requests/change-status?id=' + data.id+ '
                var action = '<a href="javascript:void(0);" class="btn btn-success change-status-'+data.id+'" id="change-status" item-id="'+ data.id +'" order-id="'+ data.order_id +'" user-id="'+ data.orders['user_id'] +'">発送済</a>';

                //console.log(data.status);

                if (!data.ship_status) {
                    $(row).children('td').eq(8).html(action);
                }else{
                    $(row).children('td').eq(8).html("");
                }

            },
        });
    }
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};


(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));

