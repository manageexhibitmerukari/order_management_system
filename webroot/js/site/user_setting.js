////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;
var product_count = 200;
var product_min_count = 1;

$(document).ready(function() {
    //init_data();
    //calc_product_count();

    $(function (argument) {
        $('#switch_random_order').bootstrapSwitch();
    });

    $('#user_setting_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            publish_start_time: {
                validators: {
                    notEmpty: {
                        message: '出品時間を設定してください。'
                    }
                }
            },
            publish_interval: {
                validators: {
                    notEmpty: {
                        message: '再出品間隔を設定してください。'
                    },
                    callback: {
                        message: '再出品間隔を0以上に設定してください。',
                        callback: function (value, validator, $field) {
                            //calc_product_count();
                            return Number(value) > 0;
                        }
                    }
                }
            },
            republish_interval: {
                validators: {
                    notEmpty: {
                        message: '一括出品間隔を設定してください。'
                    },
                    callback: {
                        message: '一括出品間隔を7分以上に設定してください。',
                        callback: function (value, validator, $field) {
                            //calc_product_count();
                            return Number(value) >= 7;
                        }
                    }
                }

            }
        }
    }).on('success.form.fv', function(e) {

        e.preventDefault();
        var $form = $(e.target);        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
        $(".alert-info").hide();

        if (update_user_setting($form.serialize())) {

            $("#loading_overlay").fadeOut(300);

            $(".alert-info").slideDown("400");
        }
        else{

        }
    });
});

function calc_product_count(){
    var republish_interval = $('#republish-interval-min').val();
    var publish_interval = $('#publish-interval').val();
    console.log(publish_interval);

    if (republish_interval == "" || republish_interval < 5) republish_interval = 5;
    if (publish_interval == "") publish_interval = 0;
    if (publish_interval == 0) publish_interval = 1;

    product_count = (Number(publish_interval)) * 24 * 60 / ( Number(republish_interval) + 2);
    product_count = Number((product_count).toFixed(0));

    product_min_count = $('#product_current_count').val();
    product_min_count = (product_min_count == 0)?1:product_min_count;
    product_min_count = (product_count < product_min_count)?product_count:product_min_count;
    product_min_count = Number(product_min_count) + 1;
    $('#countlabel').text('(範囲:'+product_min_count+'~'+product_count + ')');
}

function init_data(){
    $("#loading_overlay").show();
    $.ajax(
        {
            type: "POST",
            url: "index.php",
            data: "sp=user_setting&action=user_setting_init",
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                    $("#publish_start_time").val(data.msg['publish_time']);
                    $("#publish_interval").val(data.msg['publish_interval']);
                    $("#republish_interval").val(data.msg['republish_interval']);
                    //$("#product_count").val(data.msg['product_count']);
                }else if(data.result == "error"){
                    is_success = false;
                    alert(data.msg);
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}

function checkDay(obj){
    obj.value = obj.value.replace(/[^0-9.]/g, '');
    obj.value = obj.value.replace(/(\..*)\./g, '$1');

    if (Number(obj.value) > 31)
        obj.value = "31";

    if (Number(obj.value) < 0)
        obj.value = "0";
}

function checkNumber(obj){
    obj.value = obj.value.replace(/[^0-9.]/g, '');
    obj.value = obj.value.replace(/(\..*)\./g, '$1');
}

function checktime(obj){
    obj.value = obj.value.replace(/[^0-9.]/g, '');
    obj.value = obj.value.replace(/(\..*)\./g, '$1');

    var time = obj.value;

    if (Number(obj.value) > 23)
        obj.value = "23";

    if (Number(obj.value) < 0)
        obj.value = "0";
}

function checkProductCount(obj){
    obj.value = obj.value.replace(/[^0-9.]/g, '');
    obj.value = obj.value.replace(/(\..*)\./g, '$1');

    if (Number(obj.value) < 1 && obj.value != "")
        obj.value = "1";
}

function update_user_setting(serialized_data){
    var is_success = false;
    $("#loading_overlay").show();
    console.log(serialized_data);
    $.ajax(
        {
            type: "POST",
            url: "/users/setting",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
                $(".alert-info").hide();
                $(".alert-warning").hide();
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                    $(".alert-info").slideDown("400");
                }else if(data.result == "error"){
                    is_success = false;
                    $(".alert-warning").html(data.msg);
                    $(".alert-warning").slideDown("400");
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}

