////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

$(document).ready(function() {

    $('#certification_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    },
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください。'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

        if (certificate_mercari($form.serialize())) {
            $("#loading_overlay").fadeOut(300);
            location.reload();
        }
        else{
            $(".alert-warning").slideDown("400");
        }
    });
});

function certificate_mercari(serialized_data){
    var is_success = false;
    $("#loading_overlay").show();
    $.ajax(
        {
            type: "POST",
            url: "/users/certification",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                //$("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                }else if(data.result == "error"){
                    is_success = false;
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}
