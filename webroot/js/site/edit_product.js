////////////////////////////////////////////////////      出品       /////////////////////////////////////////////

$(document).ready(function() {

    $('#product_publish_form').formValidation({
        framework: 'bootstrap',
        //excluded: [':disabled', ':hidden'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            category: {
                excluded: false,
                validators: {
                    callback: {
                        message: 'カテゴリーが必要になります。',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: '商品名が必要です。'
                    },
                    stringLength: {
                        max: 30,
                        message: '商品名は30文字未満になるべきです。'
                    }
                }
            },
            description: {
                validators: {
                    notEmpty: {
                        message: '説明が必要です。'
                    },
                    stringLength: {
                        max: 1000,
                        message: '説明は1000文字未満になるべきです。'
                    }
                }
            },
            price: {
                validators: {
                    notEmpty: {
                        message: '価格が要求されます。'
                    },
                    numeric: {
                        message: '価格は、数値でなければなりません。'
                    },
                    // between: {
                    //     min: 300,
                    //     max: 290000,
                    //     message: '価格は300と290000の間でなければなりません。'
                    // }
                }
            },
            // photo_1: {
            //     validators: {
            //         notEmpty: {
            //             message: '商品の写真が必要です。'
            //         },
            //         file: {
            //             extension: 'jpeg,png,jpg',
            //             type: 'image/jpeg,image/png',
            //             maxSize: 2097152,   // 2048 * 1024
            //             message: '選択したファイルが有効ではありません。商品画像の容量が2MBを超えています。'
            //         }
            //     }
            // },

        }
    }).on('success.form.fv', function(e) {
        // Retrieve instances
        var $form = $(e.target);
        if ($form.find("#product_preview_wrapper").is(":visible")){
            $('#loading_overlay').show();
            $("#product_publish_form").unbind('submit').submit();
        }
        else {
            // Prevent form submission
            //e.preventDefault();

            // Do whatever you want here ...

            //$form.submit();
            $("input[type=text]").each(function (e) {
                var val = $(this).val();
                var id = $(this).attr("id");
                var preview_id = id + "_preview";

                $("#" + preview_id).html(val);
            });

            $("select").each(function (e) {
                var text = $(this).find("option:selected").text();
                var id = $(this).attr("id");
                var preview_id = id + "_preview";

                $("#" + preview_id).html($.trim(text));
            })

            $("#description_preview").html($("#description").val().replace(/\n/g, '<br \\>'));

            $("#loading_overlay").show();
            $("#product_input_wrapper").fadeOut(500, function () {
                $("#product_preview_wrapper").fadeIn(500, function () {
                    $("#loading_overlay").hide()
                });
            })

            return false;
        }
    });

    $('#product_publish_form .product_photo').change(function(e){
        if( $('#product_publish_form').data('formValidation').isValidField($(this).attr("name")) ) {
            var preview_id = "#" + $(this).attr("name") + "_preview";
            var preview_preview_id = preview_id+"_preview";
            readURL(this, preview_id);
            readURL(this, preview_preview_id);

            var attr_id = $(this).attr("id");
            var attr = attr_id.split("_");

            var next_id = parseInt(attr[1], 10) + 1;
            $(".photo_"+next_id).slideDown(300);
        }
    });

});
function saveProduct() {
    $('#product_publish_form').submit();
}

function readURL(input, display_id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(display_id).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}



