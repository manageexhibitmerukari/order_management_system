$(document).ready(function(){

    $('#datepicker-from').datepicker({
        autoclose: true,
        todayHighlight: true,
        language: 'ja'

    });
    $('#datepicker-to').datepicker({
        autoclose: true,
        todayHighlight: true,
        language: 'ja'

    });
});

/**
 * Created by root on 4/5/17.
 */

var tbl_data = [];
var main_Table;
var currentpage = 0;

var element_total_price = $('#total_price');
var element_total_profit = $('#total_profit');
var element_percent = $('#percent');
var element_total_product = $('#total_product');
var element_total_funds = $('#total_funds');
var element_total_web = $('#total_web');
var element_total_ship = $('#total_ship');

$(document).ready(function () {
    make_init_data();
    total_profit();
    $(document).on('keyup', '#main_tbl tbody input[type="number"]', function(){
        total_funds();
    });

});

function total_funds() {

    $('#main_tbl tbody input[type="number"]').each(function () {
       console.log($(this).val());
    });
}
function total_profit() {
    if (data_product) {

        var total_price = 0; //tong gia san pham
        var total_profit = 0; // tong loi nhuan
        var percent = 0; // ty le gia tong gia san pham / loi nhuan
        var total_product = 0; // tong so san pham
        var total_funds = 0; // tong so tien von (chi phi bo ra)
        var total_web = 10000; // tong so tien thue web
        var total_ship = 0; // tong so tien ship

        var arr_order = [];
        var i = 0;
        var lenght = data_product.length;
        for(i ; i < lenght; i ++  ) {
            order = [];
            total_price += data_product[i].amount * data_product[i].price;
            total_product += data_product[i].amount;

            order[data_product[i].order.id] = data_product[i].order.price_ship;
            arr_order.push(order);
        }

        element_total_price.html(total_price);
        element_total_profit.html(total_profit);
        element_percent.html(percent);
        element_total_product.html(total_product);
        element_total_funds.html(total_funds);
        element_total_web.html(total_web);
        element_total_ship.html(total_ship);

        //console.log(arr_order);
        console.log(data_product);
    }
}
function isset(variable) {
    return typeof variable !== typeof undefined ? true : false;
}

function make_init_data(){
    // call back ajax function
    if($("#loading_overlay").not(":visible"))
        $("#loading_overlay").fadeIn(300);

    //console.log(data_product);
    init_tbl_data(data_product);
    make_list(tbl_data);
    $("#loading_overlay").fadeOut(300);

}
function make_list(input_data) {
    var main_tbl_wrap = $('#main_tbl_wrap');
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover" style="width: 100%;">';
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);
    if (input_data != null){
        main_Table = $('#main_tbl').DataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": false,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            data : input_data,
            columns : [
                {
                    "data" : 'product_photo1',
                    "title": "説明写真",
                    "width": '130px'
                },
                {
                    "data" : 'product_title',
                    "title": "商品名",
                    "width": '500px'
                },
                {
                    "data" : '',
                    "title": "属性"
                },
                {
                    "data" : 'price',
                    "title": "単価"
                },
                {
                    "data" : 'amount',
                    "title": "数量"
                },
                {
                    "data" : '',
                    "title": "仕入れ",
                    "width": '100px'

                },
                {
                    "data" : '',
                    "title": "売上金額"
                },
                {
                    "data" : '',
                    "title": "利益"
                },
            ],
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [0,3]
            } ],
            createdRow : function(row, data, dataIndex)
            {
                var timestamp = Number(new Date());
                var imgLink = '<a href="javascript:void(0)" target="_blank">' +
                    '<img src="' + data.product_photo1 + '?'+ timestamp +'" style="width:101px; height: 101px;">' +
                    '</a>';
                $(row).children('td').eq(0).html(imgLink);

                var sizeColor = data.sub_product.size.sizeName + '/' + data.sub_product.color;
                $(row).children('td').eq(2).html(sizeColor);

                var amout = '<span id="data">'

                var funds = '<input type="number" id ="' + data.sub_product.id +'" name="' + data.sub_product.id +'" class="form-control" min="0" style="width: 100px;">';
                $(row).children('td').eq(5).html(funds);

                var total_price = data.price * data.amount;
                $(row).children('td').eq(6).html(total_price);

            }
        });
    }
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));