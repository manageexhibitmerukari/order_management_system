////////////////////////////////////////////////////      出品       /////////////////////////////////////////////

$(document).ready(function() {
    /*$("#category").change(function(e){
        var category_id = parseInt($(this).val(),10);
        changeSize(category_id);
    })*/

    $('.select2').select2({
        // closeOnSelect:false
    });

    $("#brand_initial").change(function(e){
        var category_id = $("#category").val();
        var brand_initial_id = $("#brand_initial").val();

        $.ajax({
                type: "POST",
                url: "/user-products/change-brand-initial",
                data: "sp=product_mng&category_id="+category_id + "&brand_initial=" + brand_initial_id,
                contentType: "application/x-www-form-urlencoded;",
                dataType: "json",
                beforeSend: function() {
                    $("#loading_overlay").fadeIn(300);
                },
                error: function(data, status, errThrown){
                    $("#loading_overlay").fadeOut(300);
                },
                success: function(data){
                    if(data.result == "success"){
                        update_brand_list(data.brandList);
                    }else if(data.result == "error"){
                        alert(data.msg);
                    }
                    $("#loading_overlay").fadeOut(300);
                },
                statusCode: {
                    404: function() {
                        alert('page not found');
                        $("#loading_overlay").fadeOut(300);
                    }
                }
            });
    });


    $("#branch_id, #account_number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }

        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });

    $('#register_bank_form').formValidation({
        framework: 'bootstrap',
        //excluded: [':disabled', ':hidden'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            bank_id: {
                excluded: false,
                validators: {
                    callback: {
                        message: 'カテゴリーが必要になります。',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            kind: {
                validators: {
                    notEmpty: {
                        message: '商品名が必要です。'
                    },
                    /*stringLength: {
                        max: 40,
                        message: '商品名は40文字未満になるべきです。'
                    }*/
                }
            },
            branch_id: {
                validators: {
                    notEmpty: {
                        message: '説明が必要です。'
                    },
                    numeric: {
                            message: '価格は、数値でなければなりません。'
                    }
                }
            },
            account_number: {
                validators: {
                    notEmpty: {
                        message: '価格が要求されます。'
                    },
                    numeric: {
                        message: '価格は、数値でなければなりません。'
                    },
                    // between: {
                    //     min: 300,
                    //     max: 290000,
                    //     message: '価格は300と290000の間でなければなりません。'
                    // }
                }
            },
            family_name: {
                validators: {
                    notEmpty: {
                        message: '商品の写真が必要です。'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: '商品の写真が必要です。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        // Retrieve instances
        var $form = $(e.target);        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance
             if (registerBank($form.serialize())) {
            $("#loading_overlay").fadeOut(300);
            location.href = '/users/currentSales';
        }
        else{
            $(".alert-warning").slideDown("400");
        }
    });


    $('#secret_code_form_user').formValidation({
        framework: 'bootstrap',
        //excluded: [':disabled', ':hidden'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {

            }

    }).on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        // Retrieve instances
        var $form = $(e.target);        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
        if (checkcode($form.serialize())) {
            $("#loading_overlay").fadeOut(300);
        }
        else{
            $(".alert-warning").slideDown("400");
        }
    });

    function checkcode(serialized_data) {
        $.ajax(
            {
                type: "POST",
                url: "/users/checkCode",
                data: serialized_data,
                contentType: "application/x-www-form-urlencoded;",
                dataType: "json",
                async: false,
                beforeSend: function() {
                    //$("#loading_overlay").fadeIn(300);
                },
                error: function(data, status, errThrown){
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                },
                success: function(data){
                    console.log(data);
                  if(data.result == "succes"){
                       $('#showcontent').show();
                      $('#register_bank_form .select2 ').css('width','100%');
                      $('#checkcode').hide();
                  }
                    else{
                      alert('現在の暗証番号のが正しくありません。');
                  }
                    $("#loading_overlay").fadeOut(300);
                },
                statusCode: {
                    404: function() {
                        alert('page not found');
                        $("#loading_overlay").fadeOut(300);
                        is_success = false;
                    }
                }
            });
    }    
    
     function registerBank(serialized_data){
        var is_success = false;

        $("#loading_overlay").show();
        $.ajax(
            {
                type: "POST",
                url: "/users/registerBank",
                data: serialized_data,
                contentType: "application/x-www-form-urlencoded;",
                dataType: "json",
                async: false,
                beforeSend: function() {
                    //$("#loading_overlay").fadeIn(300);
                },
                error: function(data, status, errThrown){
                    $("#loading_overlay").fadeOut(300);
                    is_success = false;
                },
                success: function(data){
                    if(data.result == "error")
                        alert(data.msg);
                    else
                    {
                        if (data.result.result.toLowerCase() == "ok") {
                            is_success = true;
                        }else if(data.result.result == "error"){
                            alert(data.result.errors[0].message);
                            is_success = false;
                        }
                    }

                    $("#loading_overlay").fadeOut(300);
                },
                statusCode: {
                    404: function() {
                        alert('page not found');
                        $("#loading_overlay").fadeOut(300);
                        is_success = false;
                    }
                }
            });
        return is_success;
    }




    $("#price, #reservation_hour, #reservation_minute").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }

        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });


    $("#publishOptionCheck").click(function(){
        if($('#publishOptionCheck').is(':checked')){
            $("#publishOptionWrapper").slideDown(300);
            $("#publishOptionWrapperPreview").slideDown(300);
        }else{
            $("#publishOptionWrapper").slideUp(300);
            $("#publishOptionWrapperPreview").slideUp(300);
        }
    });


});


function update_size_list(size){
    $('#size').html("");
    if(size.length > 0) {
        for (var i = 0; i < size.length; i++) {
            $('#size').append($("<option/>", {
                value: size[i].id,
                text: size[i].sizeName
            }));
        }
    }
    else
        $('#size').html("");
}

function update_brand_list(brand){
    $('#brand').html("");
    if(brand.length > 0) {
        for (var i = 0; i < brand.length; i++) {
            $('#brand').append($("<option/>", {
                value: brand[i].id,
                text: brand[i].brandName
            }));
        }
    }
    else
        $('#brand').html("");
}

function update_shipping_method_list(method){
    $("#shipping_method").html("");
    for (var i = 0; i < method.length; i++) {
        $('#shipping_method').append($("<option/>", {
            value: method[i].shippingValue,
            text: method[i].shippingMethod
        }));
    }
}

function saveProduct(){
    $("#register_bank_form").submit();
}

function selectCategory(category_id, deep, obj){
    $("#deep_"+deep+"_category").val($(obj).html());
    if(deep == 2){
        changeSize(category_id);
        var btn_html = "";
        var final_category = $("#deep_0_category").val()+" &rarr; "+$("#deep_1_category").val()+" &rarr; "+$("#deep_2_category").val();
        //btn_html += "<button type='button' class='btn btn-success ladda-button btn-sm' style='margin-right:3px; margin-bottom:3px;' >"+$(obj).html()+"</button>";
        btn_html += final_category;
        btn_html += "<button type='button' class='btn btn-default ladda-button btn-sm' style='margin-left:10px; margin-right:3px; margin-bottom:3px;' onclick='javascript:cancelCategory(3)'>キャンセル</button>";

        $("#category").val(category_id);
        $("#deep_3_categories").html(btn_html);
        $("#deep_2_categories").fadeOut(300, function(){$("#deep_3_categories").fadeIn(300);});

        $("#category_preview").html(final_category);

        $('#product_publish_form')
            .find('[name="category"]')
            .end()
            // Revalidate the field manually
            .formValidation('revalidateField', 'category');

    }
    else {

        $.ajax({
                type: "POST",
                url: "/categories/getChildCategories",
                data: "sp=product_mng&action=get_child_categories&parent_id=" + category_id,
                contentType: "application/x-www-form-urlencoded;",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#loading_overlay").fadeIn(300);
                },
                error: function (data, status, errThrown) {
                    $("#loading_overlay").fadeOut(300);
                },
                success: function (data) {
                    if (data.result == "success") {
                        if (data.msg.length <= 0)
                        {
                            $("#category").val(category_id);
                            $('#product_publish_form')
                                .find('[name="category"]')
                                .end()
                                // Revalidate the field manually
                                .formValidation('revalidateField', 'category');
                        }
                        update_category_list(category_id, data.msg, deep);
                    } else if (data.result == "error") {
                        alert(data.msg);
                    }
                    $("#loading_overlay").fadeOut(300);
                },
                statusCode: {
                    404: function () {
                        alert('page not found');
                        $("#loading_overlay").fadeOut(300);
                    }
                }
            });
    }

    return;
}

function changeSize(category_id){
    $.ajax(
        {
            type: "POST",
            url: "/categories/changeCategory",
            data: "sp=product_mng&action=change_category&category_id="+category_id,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            beforeSend: function() {
               // $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                //$("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                if(data.result == "success"){
                    update_size_list(data.size_list);
                    update_brand_list(data.brandList);
                    $( "#brand_initial" ).val("");
                }else if(data.result == "error"){
                    alert(data.msg);
                }
                //$("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    //$("#loading_overlay").fadeOut(300);
                }
            }
        });
    return;
}

function update_category_list(category_id, categories, deep){
//    $("#deep_"+deep+"_categories").fadeOut(100);

    var next_deep = deep + 1;
    var category_html = "";

    if(next_deep == 2)
        button_class = "btn-success";
    else
        button_class = "btn-primary";

    if (categories.length <= 0)
    {
        var final_category = $("#deep_0_category").val()+" &rarr; "+$("#deep_1_category").val();
        category_html = final_category;
        $("#category").val(category_id);

        $("#category_preview").html(final_category);

        changeSize(category_id);

        $('#product_publish_form')
            .find('[name="category"]')
            .end()
            // Revalidate the field manually
            .formValidation('revalidateField', 'category');
    }

    for(var i=0; i<categories.length; i++) {
        category_html += "<button type='button' class='btn "+button_class+" ladda-button btn-sm' style='margin-right:3px; margin-bottom:3px;' onclick='javascript:selectCategory("+categories[i].id+","+next_deep+", this)'>"+categories[i].categoryName+"</button>";
    }

    category_html += "<button type='button' class='btn btn-default ladda-button btn-sm' style='margin-right:3px; margin-bottom:3px;' onclick='javascript:cancelCategory("+next_deep+")'>キャンセル</button>";

    $("#deep_"+next_deep+"_categories").html(category_html);
    $("#deep_"+deep+"_categories").fadeOut(300, function(){$("#deep_"+next_deep+"_categories").fadeIn(300);});
}


function cancelCategory(deep){
    var before_deep = deep - 1;

    $("#category").val("");

    $("#deep_"+deep+"_categories").fadeOut(300, function(){$("#deep_"+before_deep+"_categories").fadeIn(300)});

    $('#product_publish_form')
        .find('[name="category"]')
        .end()
        // Revalidate the field manually
        .formValidation('revalidateField', 'category');
}