/**
 * Created by root on 4/11/17.
 */

var tbl_data = [];
var currentpage = 0;
var firstload = true;
var main_Table;
$(document).ready(function() {
    var status = $('#status').val();

    make_init_data(status);

    $(document).on('click', '#request-ship', function() {
        var id = $(this).attr('item-id');
        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm({
            message: "この商品の配送を中止しましたか。",
            callback: function (result) {
                if (result) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "/client-products/remove-request",
                        //contentType: "application/x-www-form-urlencoded;",
                        dataType: "json",
                        data : {
                            id : id
                        },
                        beforeSend: function() {
                            $("#loading_overlay").fadeIn(300);
                        },
                        error: function(data, status, errThrown){
                            $("#loading_overlay").fadeOut(300);
                            console.log(errThrown);
                        },
                        success: function(data){
                            console.log(data);
                            if (data.result == 'success') {
                                alert('商品配送が中止されまれした。');
                                $('.tbl-manager-shipping #request-ship').each(function () {
                                    if ($(this).attr('item-id') == id) {
                                        var row = $(this).parents('tr');
                                        main_Table.row(row).remove().draw(false);
                                    }
                                });
                            }
                            $("#loading_overlay").fadeOut(300);
                        },
                        statusCode: {
                            404: function() {
                                alert('page not found');
                                $("#loading_overlay1").hide();
                            }
                        }
                    });
                }
            }
        });
    });
    check_active(status);

});

function check_active(status) {
    if (status == 'new'){
        $('#request li').each(function(){
            $(this).removeClass('active');
        });
        $('#new').addClass('active');
    } else {
        $('#request li').each(function(){
            $(this).removeClass('active');
        });
        $('#sent').addClass('active');
    }
}
function make_init_data(status){
    // call back ajax function
    $.ajax(
        {
            type: "POST",
            url: "/client-products/manager-shipping",
            //contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            data : {
                status: status
            },
            beforeSend: function() {
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                console.log(errThrown);
            },
            success: function(data){
                console.log(data);
                init_tbl_data(data.products);
                make_list(tbl_data, status);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
    return;
}

function make_list(input_data, status) {
    if (input_data != null){
        main_Table = $('.tbl-manager-shipping').DataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            data : input_data,
            columns : [
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": "項番"
                },
                {
                    "data" : 'product_title',
                    "defaultContent" : '',
                    "title": "商品名"
                },
                {
                    "data" : 'product_photo1',
                    "title": "画像"
                },
                {
                    "data" : 'price',
                    "title": "商品価格"
                },
                {
                    "data" : 'amount',
                    "title": "在庫数"
                },
                {
                    "data" : 'sizeName',
                    "title": "サイズ"
                },
                {
                    "data" : 'color',
                    "title": "色"
                },
                {
                    "data" : null,
                    "defaultContent" : '',
                    "title": ""
                }
            ],
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [0,1,2,3,4,5,6,7]
            } ],
            createdRow : function(row, data, dataIndex)
            {
                var no = dataIndex + 1;
                $(row).children('td').eq(0).html(no);

                var img = '<img src="'+ data.product_photo1 +'" width="120px" height="120px">';
                $(row).children('td').eq(2).html(img);

                if (status == 'new'){
                    var icon = '<button class="btn btn-primary" id="request-ship" item-id="'+ data.id +'">配送中止</button>';
                    $(row).children('td').eq(7).html(icon);
                }
            },
        });
    }
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

function init_tbl_data(input_data){
    tbl_data = input_data;
    return;
}

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));