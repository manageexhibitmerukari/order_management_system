$(document).ready(function() {
    $("#side").hide();

    total_all();

    $('.quantity').each(function ()
    {
        $(this).focusout(function () {
            if ($(this).val() <= 0) {
                $(this).val(1);
            }
        });
    });

    $('.update-single-cart').click(function ()
    {
        var amount = $(this).parent().parent().find('.quantity').val();
        var sku = $(this).parent().parent().find('.sku').val();
        var cartId = $(this).attr('cart-id');
        var input_amount = $(this).parent().parent().find('.quantity');

        $.ajax({
            type: "POST",
            url: "/user-products/update-single-product",
            data: {
                cart_id : cartId,
                amount: amount,
                sku: sku,
            },
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
            },
            success: function(data)
            {


                if (data.error) {
                    console.log(data.amount_default);
                    input_amount.val(data.amount_default);
                    bootbox.alert(data.error);
                    return;
                }
                total_all();

                bootbox.alert('商品の数量が変更されました');
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                }
            }
        });
    });

    $('.delete-cart').click(function ()
    {
        var deleteButton = $(this);
        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm( {
            message:"本当に削除しますか。",
            callback:  function(result) {
                if(result == true)
                {
                    var cartId = deleteButton.attr('cart-id');

                    $.ajax({
                        type: "POST",
                        url: "/user-products/delete-cart",
                        data: {
                            cart_id : cartId
                        },
                        contentType: "application/x-www-form-urlencoded;",
                        dataType: "json",
                        async: false,
                        beforeSend: function() {
                        },
                        error: function(data, status, errThrown){
                            console.log(errThrown);
                        },
                        success: function(data)
                        {
                            deleteButton.parents('.pc-product-detail').remove();
                            total_all();

                            $(".num-cart").text(data.countProduct)
                        },
                        statusCode: {
                            404: function() {
                                alert('page not found');
                            }
                        }
                    });
                }
            }
        });
    });

    $('#buy').click(function() {
        var buyButton = $(this);
        var payment_method = $('.payment_method').val();
        //console.log(payment_method);
        if(!payment_method) {
            $('.error_payment').show();
            return false;
        } else {

            $('.error_payment').hide()
        }

        if ($('#cart_container').find('.pc-product-detail').length > 0)
        {
            bootbox.confirm({
                message: "ご注文しましたら注文内容が修正できません。<br>注文してよろしいですか。",
                buttons: {
                    confirm: {
                        label: 'はい',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'いいえ',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: "/user-products/buy",
                            contentType: "application/x-www-form-urlencoded;",
                            dataType: "json",
                            data: {
                                payment_method : $('.payment_method').val(),
                            },
                            async: false,
                            beforeSend: function () {
                                buyButton.prop('disabled', true);
                            },
                            error: function (data, status, errThrown) {
                                bootbox.alert("注文する時、エラーが発生しました。もう一度やり直してください。");
                                buyButton.prop('disabled', false);
                                // console.log(errThrown);
                            },
                            success: function (data) {
                                if (data.result == 'success') {
                                    $('.pc-product-detail').remove();
                                    total_all();
                                    $(".num-cart").text(0);

                                    $("#submit_cart_success_message").show();
                                    /*bootbox.alert({
                                        message: '注文を完了しました',
                                        callback: function () {
                                            // location.reload();
                                            $('.pc-product-detail').remove();

                                            total_all();
                                            $(".num-cart").text(0)
                                        }
                                    });*/
                                } else if (data.result == 'error') {
                                    bootbox.alert(data.msg);
                                }

                                buyButton.prop('disabled', false);
                            },
                            statusCode: {
                                404: function () {
                                    alert('page not found');
                                }
                            }
                        });

                    }
                }
        });


        } else{
            alert('カートの商品数量は１以上にしてください。');
        }
    });
});


function changeQuantity(obj,cart_id) {
    value = parseInt($(obj).val());
    quantity_max = parseInt($("#quantity_max_"+cart_id).val());

    console.log(quantity_max);

    if(value >=1 && value <= quantity_max){
        $("#error_"+cart_id).html("");
        $("#update-single-cart_"+cart_id).removeClass("disabled");
    }else {
        $("#error_"+cart_id).html("在庫を超えました。在庫量は" +quantity_max + "です。");
        $("#update-single-cart_"+cart_id).addClass("disabled");
    }
}

function total_all() {
    var sumPrice = 0;
    var tax = 0;
    var totalPrice = 0;
    var fee_up = parseInt($('#fee_up').val());
    var price_order = parseInt($('#price_order').val());
    var fee_default = parseInt($('#fee_default').val());
    var fee_ship = 0;



    $('.detail').each(function () {
        var price = $(this).find('.price').val();
        var quantity = $(this).find('.quantity').val();

        sumPrice += price * quantity;
    });

    if (sumPrice > 0)
    {
        tax = Math.floor(sumPrice * 0.08);
        totalPrice = sumPrice + tax;
    }

    if (sumPrice >= price_order) {
        fee_ship = fee_up;
        if(fee_ship == 0 && price_order == 0) {
            fee_ship = fee_default;
        }
    } else {
        fee_ship = fee_default;
    }

    $('#ship_price').html(fee_ship);

    totalPrice += fee_ship;



    $('#sum_price').text(sumPrice.toLocaleString());
    $('#tax').text(tax.toLocaleString());
    $('#total_price').text(totalPrice.toLocaleString());



    if(sumPrice == 0) {
        $('#buy').addClass('disabled');
    } else {
        $('#buy').removeClass('disabled');
    }

}

function getShippingFee(zipcode, price) {
    $.ajax(
        {
            type: "POST",
            url: "/user-products/ajax-get-shipping-fee",
            data: {
                zipcode: zipcode,
                price: price
            },
            dataType: "json",

            beforeSend: function() {
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
            },
            success: function(data){
                if(data.status == 'success') {
                    $('#ship_price').html(data.fee_ship);
                } else {
                    console.log('get shipping price error');
                }
            },
            statusCode: {
                404: function() {
                    console.log('get shipping price error');
                }
            }
        });


}