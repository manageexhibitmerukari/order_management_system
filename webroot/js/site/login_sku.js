/**
 * Created by root on 4/13/17.
 */

$(document).ready(function () {
    $('#btn-login-sku').click(function () {
        var username = $('#login-sku .username').val();
        var password = $('#login-sku .password').val();
        $.ajax({
            type: "POST",
            url: "/users/login-sku",
            contentType: "application/x-www-form-urlencoded;",
            data : {
                username : username,
                password : password
            },
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
                $("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                console.log(data);
                if (data.result == 'success') {
                    $('#login-sku form').css({'display': 'none'});
                    $('#login-sku .logout').css({'display': 'block'});
                    $('#login-sku .welcome').text('現在のユーザーは ' + username + 'です');
                } else {
                    $('.error-login-sku').text(data.msg);
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
    });
    $('#btn-register-sku').click(function () {
        var username = $('#login-sku .username').val();
        var password = $('#login-sku .password').val();
        $.ajax({
            type: "POST",
            url: "/users/register-sku",
            contentType: "application/x-www-form-urlencoded;",
            data : {
                username : username,
                password : password
            },
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
                $("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                if (data.result == 'success') {
                    $('#login-sku form').css({'display': 'none'});
                    $('#login-sku .logout').css({'display': 'block'});
                    $('#login-sku .welcome').text('現在のユーザーは ' + username + 'です');
                } else {
                    $('.error-login-sku').html(data.msg);
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
    });
    $('#login-sku .logout').click(function() {
       $('#login-sku').modal('hide');
        $('#login-sku form').css({'display': 'block'});
        $('#login-sku .logout').css({'display': 'none'});
        $('#login-sku .welcome').text('');
    });
});