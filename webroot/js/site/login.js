$(document).ready(function() {

    $('#signup_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            username: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'ユーザーIDが必要です。'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    }
                }
            },
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    },
                    callback: {
                        message: 'パスワードの確認が違います。',
                        callback: function (value, validator, $field) {
                            var item_value = $('#password').val();
                            return value == item_value;
                        }
                    }
                }
            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    },
                    /*emailAddress: {
                     message: '有効なメールアドレスを入力してください。'
                     }*/
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください'
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        // Prevent form submission
        //e.preventDefault();

        // Retrieve instances
        //var $form = $(e.target),        // The form instance
         //   fv = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

    });

    $('#forgotpassword_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    },
                    /*emailAddress: {
                     message: '有効なメールアドレスを入力してください。'
                     }*/
                    regexp: {
                        regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                        message: '有効なメールアドレスを入力してください'
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        // Prevent form submission
        //e.preventDefault();

        // Retrieve instances
        //var $form = $(e.target),        // The form instance
        //   fv = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

    });
})


$("#captcha_refresh_btn").click(function () {
    var l = Ladda.create(this);
    l.start();
    $("#captcha_img_tag").attr("src", "captcha.php?_=" + ((new Date()).getTime()));
    l.stop();
});

function check_data() {
    if ($('#username').val() == "" && $('#password').val() == "") {
        alert('ユーザー名とパスワードを入力してください。');
        $('#username').focus();
        return false;
    }
    else if ($('#username').val() == "") {
        alert('ユーザー名を入力してください。');
        $('#username').focus();
        return false;

    }
    else if ($('#password').val() == "") {
        alert('パスワードを入力してください。');
        $('#password').focus();
        return false;
    }
    else if ($('#captcha').val() == "") {
        alert('確認コードを入力してください。');
        $('#captcha').focus();
        return false;
    }
    return true;
}