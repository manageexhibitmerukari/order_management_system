/**
 * Created by root on 4/7/17.
 */
/**
 * Created by root on 4/5/17.
 */

var tbl_data = [];
var currentpage = 0;
var firstload = true;
var allchecked = false;
var main_Table;
var statusDelete = 0;
$(document).ready(function() {
    make_init_data();
    $(document).on('click', '.change-status', function() {
       var id = $(this).attr('id-item');
        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm({
            message: "本当に注文を差戻しますか。",
            callback: function (result) {
                if (result) {
                    $.ajax({
                        type: "POST",
                        url: "/user-products/change-status-order",
                        contentType: "application/x-www-form-urlencoded;",
                        dataType: "json",
                        data: {
                            id: id
                        },
                        beforeSend: function () {
                            $("#loading_overlay").fadeIn(300);
                        },
                        error: function (data, status, errThrown) {
                            console.log(errThrown);
                            $("#loading_overlay").fadeOut(300);
                        },
                        success: function (data) {
                            console.log(data);
                            if (data.result == 'success') {
                                alert('状態を更新しました');
                                $('.tbl-order tr td img').each(function () {
                                   if ($(this).attr('id-item') == id) {
                                       $(this).parent().parent().find('td:eq(3)').text('キャンセル');
                                       $(this).parent().parent().find('td:eq(5)').empty();
                                   }
                                });
                            }
                            $("#loading_overlay").fadeOut(300);
                        },
                        statusCode: {
                            404: function () {
                                alert('page not found');
                                $("#loading_overlay1").hide();
                            }
                        }
                    });

                }
            }
        });
    });
});
function make_init_data(){
    // call back ajax function
    $.ajax(
        {
            type: "POST",
            url: "/user-products/order-user",
            //contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                console.log(errThrown);
            },
            success: function(data){
                console.log(data);
                init_tbl_data(data.listOrder);
                make_list(tbl_data);
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
}

function make_list(input_data) {
    if (input_data != null){
        main_Table = $('.tbl-order').DataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [],  // sort desc
            "order": [[ 0, "desc" ]],  //first sort field
            "bInfo": true,
            "bAutoWidth": true,
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            data : input_data,
            columns : [
                {
                    "data" : 'id',
                    "title": "注文番号"
                },
                {
                    "data" : 'date',
                    "title": "受注日"
                },
                {
                    "data" : 'total_price',
                    "title": "受注合計"
                },
                {
                    "data" : 'status',
                    "title": "注文の状態"
                },
                {
                    "data" : null,
                    "title": "明細"
                },
                // {
                //     "data" : null,
                //     "title": ""
                // }
            ],
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [4] // 5
            } ],
            createdRow : function(row, data, dataIndex)
            {
                var no = dataIndex + 1;
                $(row).children('td').eq(0).html(no);
                var detail = '<a href="/user-products/manager-product?id=' + data.id + '">明細</a>';
                $(row).children('td').eq(4).html(detail);
                var status = '';

                if (data.status == 'wait_payment') {
                    status = '未決済';
                } else if (data.status == 'done') {
                    status = '完了済';
                } else if (data.status == 'reject') {
                    status = 'キャンセル';
                }
                $(row).children('td').eq(3).html(status);

                // if (data.status != 'done' && data.status != 'reject') {
                //     var icon = '<img src="/img/icon/close-box-red.png" class="change-status" style="width: 16px; height: 16px;" id-item="' + data.id +'">';
                //     $(row).children('td').eq(5).html(icon);
                // } else {
                //     $(row).children('td').eq(5).html('');
                // }
            }
        });
    }
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

function init_tbl_data(input_data){
    tbl_data = input_data;
    return;
}

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));