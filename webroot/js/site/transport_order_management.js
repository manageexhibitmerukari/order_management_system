////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

var tbl_data = [];
var all_checked = false;
var selected_orders= [];
var main_Table;


$(document).ready(function() {
    make_init_data();

    $('#order_detail_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {

        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission

        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

        update_order_data($form.serialize());

        $('#order_detail_modal').modal('hide');
    });
});


function make_init_data(){
    // call back ajax function
    $.ajax({
        type: "POST",
        url: "/orders/transport-order-management",
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
            // console.log(errThrown);
        },
        success: function(data){
            init_tbl_data(data.orderArr);
            make_list(tbl_data);

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").hide();
            }
        }
    });
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

function make_list(input_data){
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    if(input_data.length <= 0){
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th>注文番号</th>';
        inner_html += '<th>注文の状態</th>';
        inner_html += '<th>更新者</th>';
        inner_html += '<th>変更時間</th>';
        inner_html += '<th>発送サービス</th>';
        inner_html += '<th>発送ID</th>';
        // inner_html += '<th>order_detail</th>';
        inner_html += '<th></th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        inner_html += '<tr>';
        inner_html += '<td style="text-align:center;vertical-align:middle;padding-top:20px;" colspan=8>';
        inner_html += '<div class="alert alert-warning alert-dismissable">';
        inner_html += '<h4><i class="icon fa fa-warning"></i> 警告!</h4>';
        inner_html += 'データはありません。';
        inner_html += '</div>';
        inner_html += '</td>';
        inner_html += '</tr>';
        inner_html += '</tbody>';
    }
    else
    {
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th style="min-width:30px !important;">注文番号</th>';
        inner_html += '<th style="min-width:30px !important;">注文の状態</th>';
        inner_html += '<th style="min-width:80px !important;">更新者</th>';
        inner_html += '<th style="min-width:80px !important;">変更時間</th>';
        inner_html += '<th style="min-width:80px !important;">発送サービス</th>';
        inner_html += '<th style="min-width:30px !important;">発送ID</th>';
        // inner_html += '<th style="min-width:30px !important;">order_detail</th>';
        inner_html += '<th style="min-width:30px !important;"></th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        for(i=0; i<input_data.length; i++) {
            inner_html += '<tr>';
            for (key in input_data[i]) {
                if (input_data[i][key] == null) input_data[i][key] = "";
            }

            inner_html += '<td>';
            inner_html += input_data[i].id;
            inner_html += '</td>';

            var transport_status = '';
            if (input_data[i].status == 'new')
                transport_status = '依頼済';
            else if (input_data[i].status == 'booked_on_alibaba')
                transport_status = '注文済';
            else if (input_data[i].status == 'in_warehouse_china')
                transport_status = '中国入庫済';
            else if (input_data[i].status == 'arrived_in_japan')
                transport_status = '日本へ発送済';
            else if (input_data[i].status == 'in_warehouse_japan')
                transport_status = '日本入庫済';

            inner_html += '<td>' ;
            inner_html += transport_status;
            inner_html += '</td>';

            inner_html += '<td>' ;
            inner_html += input_data[i].last_user_update;
            inner_html += '</td>';

            inner_html += '<td>';
            inner_html += input_data[i].last_time_update;
            inner_html += '</td>';

            inner_html += '<td>';
            inner_html += input_data[i].ship_id;
            inner_html += '</td>';

            inner_html += '<td>' ;
            inner_html += input_data[i].ship_service;
            inner_html += '</td>';

            // inner_html += '<td>' ;
            // inner_html += '<a href = "/orders/transport-order-detail/' + input_data[i].id + '">明細</a>';
            // inner_html += '</td>';

            inner_html +=  '<td>' ;
            inner_html += '<button onclick="displayUpdateUI(this);" id="' + input_data[i]['id'] + '" class="btn btn-primary btn-sm" style="display:block; margin-bottom:5px;width: 100px;"><i class="fa fa-edit"></i>&nbsp;更新</button>';
            inner_html += '<a class="btn btn-info btn-sm" href = "/orders/transport-order-detail/' + input_data[i].id + '" style="width:100px">明細</a>';
            inner_html +=  '</td>';

            inner_html += '</tr>';
        }
        inner_html += '</tbody>';
    }
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);


    if(input_data.length > 0) {
        main_Table = $('#main_tbl').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [[0, 'desc']],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "iDisplayLength": 20,
            "aLengthMenu": [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "order": [[ 0, "desc" ]],  //first sort field,
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [6]
            } ]
        });
    }
}

function displayUpdateUI(obj){
    var edit_order_id = parseInt(obj.id, 10);

    tbl_data_pos=tbl_data.map(function (element) { return element.id;}).indexOf(edit_order_id);

    $('#order_detail_modal #ship_id').val(tbl_data[tbl_data_pos].ship_id);
    $('#order_detail_modal #ship_service').val(tbl_data[tbl_data_pos].ship_service);
    $('#order_detail_modal #status').val(tbl_data[tbl_data_pos].status);
    $('#order_detail_modal #order_id').val(tbl_data[tbl_data_pos].id);

    $('#order_detail_modal').modal();
}

function update_order_data(serialized_data)
{
    $("#loading_overlay1").show();
    $.ajax({
        type: "POST",
        url: "/orders/update-transport-order",
        data: serialized_data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay1").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay1").fadeOut(300);
            is_success = false;
        },
        success: function(data){
            make_init_data();
            $("#loading_overlay1").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").fadeOut(300);
                is_success = false;
            }
        }
    });
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));