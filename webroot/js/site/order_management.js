////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

var tbl_data = [];
var all_checked = false;
var selected_orders= [];
var main_Table;


$(document).ready(function() {
    var status = $('#status').val();
    make_init_data(status);
});


function make_init_data(status){
    // call back ajax function
    $.ajax({
        type: "POST",
        url: "/orders/management",
        data: {
            status: status
        },
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
            // console.log(errThrown);
        },
        success: function(data){
            init_tbl_data(data.ordersArr);
            make_list(tbl_data, status);

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").hide();
            }
        }
    });
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

function make_list(input_data, status){
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    if(input_data.length <= 0){
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th>注文番号</th>';
        inner_html += '<th>顧客名</th>';
        inner_html += '<th>受注日</th>';
        inner_html += '<th>受注合計</th>';
        inner_html += '<th>明細</th>';
        if (status == 'wait_payment')
        {
            //inner_html += '<th>発送状態</th>';
            inner_html += '<th>支払方法</th>';
            inner_html += '<th>期限日数</th>';
            inner_html += '<th></th>';
        }
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        inner_html += '<tr>';
        inner_html += '<td style="text-align:center;vertical-align:middle;padding-top:20px;" colspan=8>';
        inner_html += '<div class="alert alert-warning alert-dismissable">';
        inner_html += '<h4><i class="icon fa fa-warning"></i> 警告!</h4>';
        inner_html += 'データはありません。';
        inner_html += '</div>';
        inner_html += '</td>';
        inner_html += '</tr>';
        inner_html += '</tbody>';
    }
    else
    {
        inner_html += '<thead>';
        inner_html += '<tr>';
        if (status == 'wait_payment')
            inner_html += '<th><input type =checkbox id="flowcheckall" onchange="checkAll(this)" '+((all_checked)?'checked':'')+'></th>';
        inner_html += '<th style="min-width:30px !important;">注文番号</th>';
        inner_html += '<th style="min-width:80px !important;">顧客名</th>';
        inner_html += '<th style="min-width:80px !important;">受注日</th>';
        inner_html += '<th style="min-width:80px !important;">受注合計</th>';
        inner_html += '<th style="max-width:80px !important;">支払方法</th>';
        inner_html += '<th style="min-width:30px !important;">明細</th>';
        if (status == 'wait_payment')
        {
            //inner_html += '<th style="max-width:80px !important;">発送状態</th>';
            inner_html += '<th style="max-width:80px !important;">期限日数</th>';
            inner_html += '<th style="max-width:80px !important;"></th>';
        }
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        for(i=0; i<input_data.length; i++) {
            inner_html += '<tr>';
            for (key in input_data[i]) {
                if (input_data[i][key] == null) input_data[i][key] = "";
            }

            if (status == 'wait_payment')
            {
                inner_html += '<td>';
                inner_html += '<input onchange = "selectOrder(this)" type = checkbox id="check' + input_data[i]['id'] + '" >';
                inner_html += '</td>';
            }

            inner_html += '<td>';
            inner_html += input_data[i].id;
            inner_html += '</td>';

            inner_html +=  '<td>' ;
            inner_html +=  input_data[i].username;
            inner_html +=  '</td>';

            inner_html +=  '<td>' ;
            inner_html +=  input_data[i].buy_date;
            inner_html +=  '</td>';

            inner_html += '<td>';
            inner_html += input_data[i].total_price;
            inner_html += '</td>';

            var payment_method = '';
            if (input_data[i].status == 'reject') {
                payment_method = "キャンセル";
            } else {
                if (input_data[i].payment_method == 'transfer_bank') {
                    payment_method = '銀行振込';
                } else if (input_data[i].payment_method == 'card_bank') {
                    payment_method = 'カード支払い';
                }
            }

            inner_html += '<td>';
            inner_html += payment_method;
            inner_html += '</td>';

            inner_html +=  '<td>' ;
            inner_html +=   '<a href = "/orders/detail_order/' + input_data[i].id + '">明細</a>';
            inner_html +=  '</td>';

            if (status == 'wait_payment')
            {

                inner_html += '<td>';
                inner_html += input_data[i].notify;
                inner_html += '</td>';
            }


            inner_html += '<td>';
            // if (status == 'new')
            //     // inner_html += '<button class="btn btn-primary btn-sm" style="display:block; margin-top:5px; width: 85px;" onclick="approveOrder(' + input_data[i].id + ')">承認</button>';
            // if (status == 'wait_payment')
            //     inner_html += '<button class="btn btn-primary btn-sm" style="display:block; margin-top:5px; width: 85px;" onclick="finishOrder(' + input_data[i].id + ')">承認</button>';
            if(input_data[i].status !='reject')
            inner_html += '<button class="btn btn-danger btn-sm " style="display:block; margin-top:5px;width: 85px;" onclick="rejectOrder(' + input_data[i].id + ')">キャンセル </button>';

            inner_html += '</td>';


            inner_html += '</tr>';
        }
        inner_html += '</tbody>';
    }
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);

    var disableSortColumns = [4];
    var defaultSortColumn = 0;
    if (status == 'wait_payment')
    {
        disableSortColumns = [0, 5, 8];
        defaultSortColumn = 1;
    }


    if(input_data.length > 0) {
        main_Table = $('#main_tbl').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [[defaultSortColumn, 'desc']],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "iDisplayLength": 20,
            "aLengthMenu": [[ 20, 40, 60, 80, 100, -1], [ 20, 40, 60, 80, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "order": [[ defaultSortColumn, "desc" ]],  //first sort field,
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : disableSortColumns
            } ],
            "fnDrawCallback": function()
            {
                var status = $('#status').val();
                if (status == 'wait_payment')
                {
                    var checkboxes = document.getElementsByTagName('input');
                    var check_all =  document.getElementById("flowcheckall");

                    if (check_all.checked) {
                        for (i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = true;
                            }
                        }
                    } else {
                        for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                var order_id = checkboxes[i].id.replace('check','');
                                checkboxes[i].checked = (selected_orders[order_id] == 1);
                            }
                        }
                    }
                }


                // if (!firstload)
                // {
                //     currentpage = this.fnPagingInfo().iPage;
                // }

                // $("#main_tbl thead th:first").removeClass("sorting_desc").removeClass("sorting_asc").addClass("sorting_disabled");
            }
        });
    }
}

function approveOrder(order_id)
{
    sendRequest(order_id, "/orders/approve-order");
}

function rejectOrder(order_id)
{
    bootbox.setDefaults({locale:"ja"});
    bootbox.confirm ({
        message: '本当にキャンセルしますか。',
        callback: function (result) {
            if (result) {
                sendRequest(order_id, "/orders/reject-order");
            }
        }
    });

}

function finishOrder(order_id)
{
    sendRequest(order_id, "/orders/finish-order");
}

function sendRequest(order_id, url)
{
    // call back ajax function
    $.ajax({
        type: "POST",
        url: url,
        data: {
            order_id: order_id
        },
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
            // console.log(errThrown);
        },
        success: function(data)
        {
            if (data.result == 'error')
                bootbox.alert(data.message);

            if (data.isReload)
            {
                var status = $('#status').val();
                make_init_data(status);
            }

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
}

$('#saveNumberOfDays').click(function ()
{
    var numberOfDays = $('#numberOfDays').val();

    $.ajax({
        type: "POST",
        url: "/orders/change-number-of-days-limit",
        data: {
            numberOfDays: numberOfDays
        },
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data)
        {
            if (data.result == "success")
                location.reload();

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
});

function createTransportOrder()
{
    var orderIds = [];
    for (var index in selected_orders){
        if (selected_orders[index] == 1)
        {
            orderIds.push(index);
        }
    }

    if (orderIds.length <= 0)
    {
        bootbox.alert("there are no orders selected。");
        return;
    }

    $.ajax({
        type: "POST",
        url: "/orders/create-transport-order",
        dataType: "json",
        data: {
            order_ids: orderIds
        },
        beforeSend: function () {
            $("#loading_overlay").fadeIn(300);
        },
        error: function (data, status, errThrown) {
            console.log(errThrown);
            $("#loading_overlay").fadeOut(300);
        },
        success: function (data) {
            console.log(data);
            if (data.result == 'success')
            {
                selected_orders = [];
                var status = $('#status').val();
                make_init_data(status);
            }
            else
            {
                bootbox.alert(data.message);
            }
            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function () {
                alert('page not found');
                $("#loading_overlay1").hide();
            }
        }
    });
}

function checkAll(ele) {
    for (var index in tbl_data) {
        var order_id = tbl_data[index]['id'];
        selected_orders[order_id] = ele.checked ? 1: 0;
    }
    all_checked = ele.checked;
    main_Table.fnStandingRedraw();
}

function selectOrder(obj)
{
    var order_id = obj.id.replace('check','');

    for (var index in tbl_data)
    {
        if(order_id == tbl_data[index]['id'])
        {
            selected_orders[order_id] = ( obj.checked )?1:0;
            break;
        }
    }

    all_checked = false;
    document.getElementById('flowcheckall').checked = false;
}


$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));