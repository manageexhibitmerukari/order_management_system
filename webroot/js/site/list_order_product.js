var tblData = [];
var listProductDataTable = '';
var allchecked = false;

$(document).ready(function ()
{
    var status = $('#status').val();
    getContent(status);

    $(document).on('click', '#mainTable tbody input[type="checkbox"]', function()
    {
        allchecked = false;
        $('#checkAll').prop('checked', false);
    });

    $(document).on('click', '#checkAll', function()
    {
        if ($(this).is(':checked'))
        {
            $('#mainTable tbody input[type="checkbox"]').each(function ()
            {
                $(this).prop('checked', true);
                allchecked = true;
            });
        }
        else
        {
            $('#mainTable tbody input[type="checkbox"]').each(function ()
            {
                $(this).prop('checked', false);
                allchecked = false;
            });
            item_id = [];
        }

    });

    $(document).on('click', '.paginate_button ', function()
    {
        if (allchecked == true)
        {
            $('#mainTable tbody input[type="checkbox"]').each(function ()
            {
                $(this).prop('checked', true);
            });
        }
    });
});

function getContent(status)
{
    $.ajax({
        type: "POST",
        url: "/orders/list-order-product",
        data: {
            status: status
        },
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data)
        {
            initTblData(data.data);
            makeTable(tblData);

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
}

function initTblData(listProduct)
{
    tblData = listProduct;
}

function makeTable(listProduct)
{
    if(listProductDataTable)
        listProductDataTable.fnDestroy();

    listUserDataTable = $('#mainTable').dataTable({
        "bPaginate" : true,
        "bLengthChange" : true,
        "bFilter" : true,
        "bSort" : true,
        "aaSorting" : [],
        "bInfo" : true,
        "bAutoWidth" : true,
        "iDisplayLength" : 20,
        "aLengthMenu" : [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
        "language" : {
            "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
        },
        data: listProduct,
        columns : [
            {
                "title": "<input type='checkbox' id='checkAll' "+ ((allchecked) ? 'checked' : '') + ">",
                "data" : null,
                "defaultContent" : '',
                "class" : "col-lg-1"
            },
            {
                "title" : "画像",
                "data" : null,
                "defaultContent" : '',
                "class" : "col-lg-2"
            },
            {
                "title" : "商品名",
                "data" : "title"
            },
            {
                "title" : "オプション",
                "data" : null,
                "defaultContent" : '',
                "class" : "col-lg-1"
            },
            {
                "title" : "単価",
                "data" : "price",
                "class" : "col-lg-1"
            },
            {
                "title" : "数量",
                "data" : "amount",
                "class" : "col-lg-1"
            },
            {
                "title" : "顧客名",
                "data" : "username",
                "class" : "col-lg-1"
            }
        ],
        createdRow : function (row, data, dataIndex)
        {
            var htmlCol0 = '<input type="checkbox" id="check" class="call-checkbox" item-id="' + data.userProductId + '">';
            $(row).children('td').eq(0).html(htmlCol0);

            var htmlCol1 = '<img src="' + data.photo + '" style="width: 100px">';
            $(row).children('td').eq(1).append(htmlCol1);

            var htmlCol3 = data.color + '/' + data.size;
            $(row).children('td').eq(3).append(htmlCol3);
        },
        "aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [0, 1, 3]
        }]
    });
}

$('#exportCsv').click(function ()
{
    var checkAll = false;
    var userProductIdList = [];
    var status = $('#status').val();

    if ($('#checkAll').is(':checked'))
    {
        checkAll = true;
    }

    if (checkAll == false)
    {
        $('#mainTable tbody input[type=checkbox]').each(function () {
            if ($(this).is(':checked'))
                userProductIdList.push($(this).attr('item-id'));
        })
    }

    if (checkAll == false && userProductIdList.length == 0)
    {
        alert('選択されたデータがありません。データを選択してください。');
        return;
    }

    $.ajax({
        type: "POST",
        url: "/orders/export-csv-list-order-product",
        dataType: "json",
        data : {
            checkAll : checkAll,
            status : status,
            userProductIdList : userProductIdList
        },
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data){
            if (data.result == 'success') {
                window.location = location.origin + '/files/csv/listProduct/item.csv';
            }
            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
});