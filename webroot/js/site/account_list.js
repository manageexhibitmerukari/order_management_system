////////////////////////////////////////////////////      メルカリアカウント管理       /////////////////////////////////////////////

var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;
var gol_msg2 = "";
var gol_msg3 = "";

$(document).ready(function() {
    make_init_data();

    $('#account_detail_modal').on('hidden.bs.modal', function() {
        $('#account_detail_form').formValidation('resetForm', true);
        $('#account_detail_modal #current_user').val("0");
        if(refresh_list)
            make_init_data();
    });

    $('#account_delete_modal').on('hidden.bs.modal', function() {
        $('#account_delete_modal #delete_account_id').val("0");
        if(refresh_list)
            make_init_data();
    });


    $('#account_detail_modal, #account_delete_modal').on('shown.bs.modal', function() {
        refresh_list = false;
    });

    $('#account_detail_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            password: {
                validators: {
                    notEmpty: {
                        message: 'パスワードが必要です。'
                    }
                }
            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'メールアドレスは必須です。'
                    }
                }
            },
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission

        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request
        add_account($form.serialize());

        // add_buy_account($form.serialize());

    });
});


$('#account_detail_form_furiru').formValidation({
    framework: 'bootstrap',
    excluded: [':disabled'],
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err: {
        // You can set it to popover
        // The message then will be shown in Bootstrap popover
        container: 'tooltip'
    },
    fields: {
        password_furiru: {
            validators: {
                notEmpty: {
                    message: 'パスワードが必要です。'
                }
            }
        },
        email_furiru: {
            verbose: false,
            validators: {
                notEmpty: {
                    message: 'メールアドレスは必須です。'
                }
            }
        }
    }
}).on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    // Retrieve instances
    var $form = $(e.target),        // The form instance
        fv    = $(e.target).data('formValidation'); // FormValidation instance
        add_furiru_account($form.serialize());
});



function add_furiru_account(serialized_data){

    $("#loading_overlay").show();
    $.ajax(
        {
            type: "POST",
            url: "/accounts/addFuriruAccount",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
            },
            success: function(data){
                if(data.result == "success"){
                    // alert('success');
                    location.reload();
                }else if(data.result == "error" ){
                    alert(data.msg);
                }
                else if(data.result == "errors" ){
                    alert(data.msg);
                }
                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay").fadeOut(300);
                }
            }
        });
}

function add_account(serialized_data){
    // console.log(serialized_data);return;
    $("#loading_overlay1").show();
    $.ajax(
    {
        type: "POST",
        url: "/accounts/add_account",
        data: serialized_data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay1").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay1").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay1").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").fadeOut(300);
            }
        }
    });
}

function addUnactiveAccount(){
    idAccount=$('#unactiveBoughtAccount').val();

    $.ajax({
        type: "POST",
        url: "/accounts/add-unactive-account",
        data: "idAccount=" + idAccount,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay1").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay1").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay1").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay1").fadeOut(300);
            }
        }
    });
}

function make_init_data(){
    // call back ajax function
    $.ajax(
        {
            type: "POST",
            url: "/accounts/get_list_accounts",
            //contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            beforeSend: function() {
                $("#loading_overlay").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay").fadeOut(300);
                // console.log(errThrown);
            },
            success: function(data){
                console.log(data);
                if(data.result == "success"){
                    $("#btn_add_user").attr("disabled", false);
                    init_tbl_data(data.msg, data.msg2,data.msg3);
                    make_list(tbl_data);
                }else if(data.result == "error"){
                    alert(data.msg);
                }

                $("#loading_overlay").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").hide();
                }
            }
        });
    return;
}

function init_tbl_data(input_data, msg2,msg3){
    tbl_data = input_data;
    gol_msg2 = msg2;
    gol_msg3 = msg3;
    return;
}

function make_list(input_data){
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    if(input_data.length <= 0){
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th>No</th>';
        inner_html += '<th>メルカリアカウントID</th>';
        inner_html += '<th>フリルID</th>';
        inner_html += '<th>ニックネーム</th>';
        inner_html += '<th>変更</th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        inner_html += '<tr>';
        inner_html += '<td style="text-align:center;vertical-align:middle;padding-top:20px;" colspan=7>';
        inner_html += '<div class="alert alert-warning alert-dismissable">';
        inner_html += '<h4><i class="icon fa fa-warning"></i> 警告!</h4>';
        inner_html += 'データはありません。';
        inner_html += '</div>';
        inner_html += '</td>';
        inner_html += '</tr>';
        inner_html += '</tbody>';
    }
    else{
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th style="min-width:30px !important;">No</th>';
        inner_html += '<th style="min-width:80px !important;">メルカリアカウントID</th>';
        inner_html += '<th style="min-width:80px !important;">フリルID</th>';
        inner_html += '<th style="min-width:80px !important;">ニックネーム</th>';
        inner_html += '<th style="min-width:30px !important;">変更</th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        for(i=0; i<input_data.length; i++) {
            inner_html += '<tr id="' + input_data[i].id + '" name="' + input_data[i].id + '">';
            for (key in input_data[i]) {
                if (input_data[i][key] == null) input_data[i][key] = "";
            }
            //No
            inner_html += '<td>';
            input_data[i].No = (input_data.length - i);
            inner_html += input_data[i].No;
            inner_html += '</td>';

            inner_html +=  '<td>' ;
            inner_html +=  input_data[i].email;
            inner_html +=  '</td>';

            inner_html +=  '<td>' ;
            inner_html +=  input_data[i].furiru_email;
            inner_html +=  '</td>';

            if(input_data[i].name != '') {
                inner_html += '<td>';
                inner_html += input_data[i].name;
                inner_html += '</td>';
            }
            else{
                inner_html += '<td>';
                inner_html += input_data[i].screen_name_furiru;
                inner_html += '</td>';
            }

            // inner_html +=  '<td>' ;
            // inner_html +=
            // inner_html +=  '</td>';

            var currentTime = new Date();
            var tokenExpireDate = new Date(input_data[i]['token_expire_date']);
            inner_html +=  '<td>' ;
            if(tokenExpireDate.getTime() <= currentTime.getTime())
            {
                inner_html +=  '<button onclick="getAccessToken(this);" id="' + input_data[i]['id'] + '" class="btn btn-success btn-sm" style="display:block">再認識</button>';
                inner_html +=  '<p style="color: #dd0000;">認識が切れたので、再認識してください</p>';
            }
            if(input_data[i]['is_block_mercari'])
            {
                inner_html +=  '<button onclick="displayInsertUI(' + input_data[i]['id'] + ');" id="' + input_data[i]['id'] + '" class="btn btn-warning btn-sm" style="display:block; margin-bottom:5px;">重複する。</button>';
                inner_html +=  '<button onclick="displayDeleteUI(this);" id="' + input_data[i]['id'] + '" class="btn btn-danger btn-sm" style="display:block;margin-bottom:5px;"><i class="fa fa-trash-o"></i>&nbsp;削除</button>';
            }
            else
            {
                inner_html +=  '<input type="file" id="file-input-ava-' + input_data[i]['id'] + '" style="display: none;" accept="image/*" onchange="changeAvatar(this);">';
                inner_html +=  '<button onclick="uploadAvatar(this);" id="' + input_data[i]['id'] + '" class="btn btn-primary btn-sm" style="display:block; margin-bottom:5px;">プロフィール変更</button>';
                inner_html +=  '<button onclick="displayDeleteUI(this);" id="' + input_data[i]['id'] + '" class="btn btn-danger btn-sm" style="display:block;margin-bottom:5px;"><i class="fa fa-trash-o"></i>&nbsp;削除</button>';


            }
            if(input_data[i].is_bought_account == 1)
                inner_html +=  '<button onclick="displaySaleComplete(' + input_data[i]['id'] + ');" id="' + input_data[i]['id'] + '" class="btn btn-primary btn-sm" style="display:block">&nbsp;販売完了</button>';

            if(parseInt(gol_msg2) < input_data[i].user_profile.furiru_max_account  && input_data[i].furiru_email == '' ){
                id = input_data[i]["id"];
                inner_html +=  '<button onclick="displayInsertUIfuriru(this,id); " id="' + input_data[i]['id'] + '" class="btn btn-primary btn-sm" style="display:block; margin-top:5px;">フリルアカウント追加</button>';
            }
            if(parseInt(gol_msg3) < input_data[i].user_profile.max_account  && input_data[i].email == '' ){
                id = input_data[i]["id"];
                inner_html +=  '<button onclick="displayInsertUI(this,id); " id="' + input_data[i]['id'] + '" class="btn btn-primary btn-sm" style="display:block; margin-top:5px;">メルカリアカウントを追加</button>';
            }
            inner_html +=  '</td>';

            inner_html += '</tr>';
        }
        inner_html += '</tbody>';
    }
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);
    if(input_data.length > 0) {
        $('#main_tbl').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "aaSorting": [[0, 'desc']],  // sort desc
            "bInfo": true,
            "bAutoWidth": true,
            "iDisplayLength": 20,
            "aLengthMenu": [[20, 25, 50, 100, -1], [20, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets' : [3]
            } ],
            "order": [[ 0, "desc" ]]  //first sort field
        });
    }
}

function displayInsertUI(duplicateAccountId,id){
    $('#id_account_mercari').val(id);
    $('#cm_no').text(tbl_data.length+1);
    tbl_data_pos = -1;
    $('#account_detail_modal #duplicate_account').val(duplicateAccountId);
    $('#account_detail_modal #email').prop("readonly", false);
    $('#account_detail_modal').modal();
}

function displaySaleComplete(account_id){
    $.ajax(
        {
            type: "POST",
            url: "/accounts/sale-complete",
            data: "account_id=" + account_id,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay1").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay1").fadeOut(300);
            },
            success: function(data){
                if(data.result =true ){
                    location.reload();
                }else if(data.result == "error"){
                    alert(data.msg);
                }
                $("#loading_overlay1").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").fadeOut(300);
                }
            }
        });
}

function displayInsertUIfuriru(duplicateAccountId_furiru,id){
    $('#id_account').val(id);
    $('#cm_no_furiru').text(tbl_data.length+1);
    tbl_data_pos = -1;
    $('#account_detail_modal_furiru #duplicate_account_furiru').val(duplicateAccountId_furiru);
    $('#account_detail_modal_furiru #email_furiru').prop("readonly", false);
    $('#account_detail_modal_furiru').modal();
}
function displayDeleteUI(obj){
    var delete_account_id = parseInt(obj.id, 10);
    $("#delete_account_id").val(delete_account_id);
    $("#account_delete_modal").modal();
}

function deleteAccount(){
    var delete_account_id = $("#delete_account_id").val();
    $.ajax(

    {
        type: "POST",
        url: "/accounts/delete",
        data: "account_id=" + delete_account_id,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
            $("#loading_overlay2").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay2").fadeOut(300);
        },
        success: function(data){
            if(data.result == "success"){
                location.reload();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay2").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay2").fadeOut(300);
            }
        }
    });
}

function uploadAvatar(obj){
    var edit_account_id = parseInt(obj.id, 10);
    $('#file-input-ava-'+edit_account_id).click();
}

function changeAvatar(obj)
{
    var account_id = obj.id;
    var file = $(obj).prop("files")[0];

    var formdata = new FormData();
    formdata.append("image", file);
    formdata.append("account_id", account_id);
    $.ajax({
        url: "/accounts/changeAvatar",
        method : "POST",
        data : formdata,
        dataType : "json",
        contentType : false,
        processData : false,
        maxTimeout : 4000,
        beforeSend: function() {
            $("#loading_overlay2").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay2").fadeOut(300);
        },
        success : function(data) {
            location.reload();
        },
        complete : function() {


        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay2").hide();
            }
        }
    });
}

function getAccessToken(obj)
{
    var account_id = obj.id;
    $("#loading_overlay").show();
    $.ajax({
        url: "/accounts/getAccessToken",
        method : "POST",
        data : 'account_id='+account_id,
        dataType : "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success : function(data) {
            if(data.result == "success"){
                make_init_data();
            }else if(data.result == "error"){
                alert(data.msg);
            }
            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
}