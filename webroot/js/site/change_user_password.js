
$(document).ready( function () {

    $('#change_user_password').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],

        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            current_pass: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: '現在のパスワードが必要です。'
                    }
                }
            },
            new_pass: {
                validators: {
                    notEmpty: {
                        message: '新しいパスワードが必要です。'
                    }
                }
            },
            re_new_pass: {
                validators: {
                    notEmpty: {
                        message: '確認新しいパスワード要です。'
                    },
                    callback: {
                        message: 'パスワードの確認が違います。',
                        callback: function (value, validator, $field) {
                            var item_value = $('#new_pass').val();
                            return value == item_value;
                        }
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        // Prevent form submission
        //e.preventDefault();

        // Retrieve instances
        //var $form = $(e.target),        // The form instance
        //   fv = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request
    });
});