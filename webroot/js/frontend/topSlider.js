// JavaScript Document
(function($){
$(function(){
	var setId = '#sliderBox'; //コンテンツ全体
	var setMainId = '#sliderMain'; //メインビジュアル
    var setThumbId = '#sliderThumb'; //サムネイル
	var setMainImg = setMainId + ' ul li#viewImg';
	var setThumbImg = setThumbId + ' .thumbImg ul li#thumb';
	var setThumTxt = setThumbId + ' .thumbText ul li#thumbTex';
	var fadeTime = 500; //フェード時間
	var delayTime = 3000; //ディレイ時間
	
	//liタグの数分処理(メインビジュアル)
	$(setMainId + '  ul li').each(function(i){
		$(this).attr('id','viewImg' + (i + 1).toString()); //liタグにIDを振る
		$(setMainId + ' ul li').css({zIndex:'98',opacity:'0'}); //メインビジュアル全ての透過率を0にする
		$(setMainId + ' ul li:first').css({zIndex:'99',opacity:'1'}); //1枚目のメインビジュアルを1番上に表示する
	});
	
	//liタグの数分処理(サムネイル)
	$(setThumbId + ' .thumbImg  ul li').each(function(i){
		$(this).attr('id','thumb' + (i + 1).toString()); //liタグにIDを振る
	});
	$(setThumbId + ' .thumbImg ul li:first').addClass('active'); //サムネイルの最初のliタグにクラスをつける
	
	//liタグの数分処理
	$(setThumbId + ' .thumbText ul li').each(function(i){
		$(this).attr('id','thumbTex' + (i + 1).toString()); //liタグにIDを振る
		$(setThumbId + ' .thumbText ul li').css({display:'none'}); //サムネイルテキストを全て非表示にする
		$(setThumbId + ' .thumbText ul li:first').css({display:'block'}); //1番最初のサムネイルテキストを表示する
	});

	//サムネイルhover時処理
	$(setThumbId + ' .thumbImg ul li').hover(
		function(){
			clearInterval(setTimer); //timer()関数を止める
			
				var listLengh = 0;
				var listIndex = 0;
				var listCount = 0;
				var connectCont = 0;
				var showCont = 0;
				listLengh = $(setThumbId + ' .thumbImg ul li').length;
				listIndex = $(setThumbId + ' .thumbImg ul li').index(this);
				listCount = listIndex+1;
				connectCont = listIndex;
				showCont = connectCont+1;
				
				//hoverに対応した画像に切り替わる
				$(setMainImg + (showCont)).siblings().stop().animate({opacity:'0'},fadeTime,function(){$(setMainImg + (showCont)).siblings().css({zIndex:'98'})});
				$(setMainImg + (showCont)).stop().animate({opacity:'1'},fadeTime,function(){$(setMainImg + (showCont)).css({zIndex:'99'})});
				//hoverしているサムネイルのliタグにactiveクラスをつけそれ以外を削除する
				$(setThumbImg + (showCont)).addClass('active');
				$(setThumbImg + (showCont)).siblings().removeClass('active');
				//hoverしているサムネイルに対応したテキストを表示させ、それ以外を非表示にする
				$(setThumTxt + (showCont)).css({display:'block'});
				$(setThumTxt + (showCont)).siblings().css({display:'none'});
							
		},
		function(){

			timer(); //timer()関数を開始する
		}
	);

	// hoverでスライド動作ストップ
	$(setMainId).mouseover(function(){
		clearInterval(setTimer); //timer()関数を止める
	}).mousemove(function(){
		clearInterval(setTimer); //timer()関数を止める
	}).mouseout(function(){
		timer(); //timer()開始する
	});
	
	timer();

	function timer() {
		setTimer = setInterval(function(){
			$(setThumbId + ' .thumbImg ul li.active').each(function(){
				var listLengh = 0;
				var listIndex = 0;
				var listCount = 0;
				var connectCont = 0;
				var showCont = 0;
				listLengh = $(setThumbId + ' .thumbImg ul li').length;
				listIndex = $(setThumbId + ' .thumbImg ul li').index(this);
				listCount = listIndex+1;
				connectCont = listIndex;
				showCont = connectCont+1;
				
				if(listLengh == listCount){
					//最後のナビから最初に戻る処理
					//最初の画像に切り替わる
					$(setMainImg + 1).siblings().stop().animate({opacity:'0'},fadeTime,function(){$(this).css({zIndex:'98'})});
					$(setMainImg + 1).stop().animate({opacity:'1'},fadeTime,function(){$(this).css({zIndex:'99'})});
					//最初のサムネイルのliタグにactiveクラスをつけそれ以外を削除する
					$(setThumbImg + 1).addClass('active');
					$(setThumbImg + 1).siblings().removeClass('active');
					
					//最初のサムネイルに対応したテキストを表示させ、それ以外を非表示にする
					$(setThumTxt + 1).css({display:'block'});
					$(setThumTxt + 1).siblings().css({display:'none'});
				
				} else {
					//最後のナビ以外の時の処理
					//次の画像に切り替わる
					$(setMainImg + (showCont)).next('li').siblings().stop().animate({opacity:'0'},fadeTime,function(){$(this).css({zIndex:'98'})});
					$(setMainImg + (showCont)).next('li').stop().animate({opacity:'1'},fadeTime,function(){$(this).css({zIndex:'99'})});
					//次のサムネイルのliタグにactiveクラスをつけそれ以外を削除する
					$(setThumbImg + (showCont)).next('li').addClass('active');
					$(setThumbImg + (showCont)).next('li').siblings().removeClass('active');
					//次のサムネイルに対応したテキストを表示させ、それ以外を非表示にする
					$(setThumTxt + (showCont)).next('li').css({display:'block'});
					$(setThumTxt + (showCont)).next('li').siblings().css({display:'none'});
					
				};
			});
		},delayTime); //ディレイ時間
	};

});
})(jQuery);
