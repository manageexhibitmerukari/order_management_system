/**
 * Created by root on 3/29/17.
 */
$(function () {
    var pageTop = $('#scrolltotop');
    pageTop.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            pageTop.fadeIn();
        } else {
            pageTop.fadeOut();
        }
    });
    pageTop.click(function () {
        $('html, body').animate({scrollTop: 0}, 500, 'swing');
        return false;
    });
});

$(document).ready(function () {
    $(document).bind('cbox_open', function () {
        $('html, body').css({overflowY: 'hidden'});
    }).bind('cbox_closed', function () {
        $('html, body').css({overflowY: ''});
    });
});

$(window).on('orientationchange resize', function () {
    var w = $(window).width(), h = $(window).height(), boxw, boxh;
    boxw = (w * 0.98) > 802 ? '802px' : '98%';
    boxh = (h * 0.98) > 600 ? '600px' : '98%';
    $.colorbox.resize({width: boxw, height: boxh});
});

var popped = ('state' in window.history && window.history.state !== null), ajaxLoad = 0, exrate = 20.71, sitetype = 1, sp = '', ep = '', sellerid = '', sellertype = '', url, params = {};

window.onpopstate = function (event) {
    if (!popped) {
        popped = true;
        return false;
    }
    if (event.state && ajaxLoad > 0 && !event.state.ext) {
        location.href = location.pathname + location.search;
    }
}

window.onbeforeunload = function (e) {
    params['ext'] = 1;
    window.history.replaceState(params, null, url);
};

$(function () {
    getPage(1, -1, 1, 0, 0, '');
    setValues();
    setHorizontalScroll();
    activateNarrowMenu();
    breakColspan();
});



function setValues() {
    var sitetypeval, spval, epval, priceval;
    $("input[name='src_sitetype']").each(function () {
        if ($(this).is(':checked')) {
            sitetypeval = $(this).parent().text();
            return false;
        }
    });
    $('#src_sitetype_value').html(sitetypeval);

    spval = $('#src_sp').val();
    epval = $('#src_ep').val();
    if (spval == '' && epval == '') {
        priceval = '全ての価格';
    } else {
        priceval = numberFormat(spval) + '～' + numberFormat(epval) + ' <span class="crc_key_label">元</span>';
    }
    $('#src_price_value').html(priceval);
}

function calJpPrice() {
    var price_cny_sp = $('#src_sp').val();
    var price_cny_ep = $('#src_ep').val();
    if (price_cny_sp != '' || price_cny_ep != '') {
        var price_jpy_sp, price_jpy_ep;
        if (!$('#price_jp').size()) {
            $('#price_wrap').append('<div id="price_jp" class="mt5">約<span id="price_jp_sp"> - </span>～<span id="price_jp_ep"> - </span>円</div>');
        }
        if (price_cny_sp.match(/[^0-9]+/)) {
            price_cny_sp = convertNum(price_cny_sp);
            $('#src_sp').val(price_cny_sp);
        }
        if (price_cny_sp.match(/^[0-9]+$/)) {
            price_jpy_sp = Math.round(price_cny_sp * exrate);
            $('#price_jp_sp').html(numberFormat(price_jpy_sp));
        } else {
            $('#price_jp_sp').html('-');
        }
        if (price_cny_ep.match(/[^0-9]+/)) {
            price_cny_ep = convertNum(price_cny_ep);
            $('#src_ep').val(price_cny_ep);
        }
        if (price_cny_ep.match(/^[0-9]+$/)) {
            price_jpy_ep = Math.round(price_cny_ep * exrate);
            $('#price_jp_ep').html(numberFormat(price_jpy_ep));
        } else {
            $('#price_jp_ep').html('-');
        }
    } else {
        if ($('#price_jp').size()) {
            $('#price_jp').remove();
        }
    }
}

function getPage(reset, tcnt, page, sort, offset, offsetvals) {
    var error = 0, errormsg;
    if (typeof page == 'undefined') {
        page = 1;
    }
    if (typeof sort == 'undefined') {
        sort = -1;
    }
    if (typeof offset == 'undefined') {
        offset = 0;
    }
    if (typeof tcnt == 'undefined') {
        tcnt = -1;
    }
    if (typeof offsetvals == 'undefined') {
        offsetvals = '';
    }
    if (reset == 1) {
        $("input[name='src_sitetype']").each(function () {
            if ($(this).is(':checked')) {
                sitetype = $(this).val();
                return false;
            }
        });
        sp = $('#src_sp').val();
        ep = $('#src_ep').val();

        if (sp != '' || ep != '') {
            if (sp == '') {
                error = 1;
                errormsg = '下限価格を入力してください。';
            } else if (sp == 0) {
                error = 1;
                errormsg = '下限価格は0.01以上の金額を入力してください。';
            } else if (!sp.match(/^\d{1,7}(\.\d{1,2})?$/)) {
                error = 1;
                errormsg = '下限価格は7桁までの半角数字で入力してください。';
            } else if (ep == '') {
                error = 1;
                errormsg = '上限価格を入力してください。';
            } else if (!ep.match(/^\d{1,7}(\.\d{1,2})?$/)) {
                error = 1;
                errormsg = '上限価格は7桁までの半角数字で入力してください。';
            } else if (parseFloat(sp) >= parseFloat(ep)) {
                error = 1;
                errormsg = '上限価格は下限価格よりも大きい値を入力してください。';
            }
        }
    }
    if (error) {
        window.alert(errormsg);
    } else {
        url = 'http://chinamart.jp/feature/drone_accessory';
        var path = [];
        path.push('page=' + page);
        if (page > 1 || offset > 0) {
            path.push('tcnt=' + tcnt);
        }
        if (offset > 0) {
            path.push('os=' + offset);
        }
        if (offsetvals != '') {
            path.push('osv=' + offsetvals);
        }
        if (sort != 0) {
            path.push('sort=' + sort);
        }
        if (sitetype != 0) {
            path.push('sitetype=' + sitetype);
        }
        if (sp) {
            path.push('sp=' + sp);
        }
        if (ep) {
            path.push('ep=' + ep);
        }
        if (path.length > 0) {
            if (url.match(/[&\?]/)) {
                url += '&';
            } else {
                url += '?';
            }
            url += path.join('&');
        }
        $('.narrow_value').removeClass('hover');
        params['mode'] = 'feature';
        params['nocache'] = 0;
        params['page'] = page;
        params['os'] = offset;
        params['tcnt'] = tcnt;
        params['osv'] = encodeURIComponent(offsetvals);
        params['sort'] = sort;
        params['sitetype'] = sitetype;
        params['sp'] = sp;
        params['ep'] = ep;
        params['catid'] = '';
        params['q_to'] = encodeURIComponent('无人机 配件');
        params['sellerid'] = sellerid;
        params['sellertype'] = sellertype;
        params['imgupdate'] = '';
        $.ajax({
            type: 'POST',
            url: 'http://chinamart.jp/taobao_list_call.php',
            data: params,
            timeout: 60000,
            success: function (data) {
                $('#outerajax').html(data);
                $('img.listimg').lazyload();
                if (typeof(window.history.pushState) == 'function') {
                    if (ajaxLoad == 0) {
                        window.history.replaceState(params, null, url);
                    } else {
                        window.history.pushState(params, null, url);
                    }
                } else {
                    window.location.hash = '#!' + path;
                }
                setItemlist();
                ajaxLoad++;
            },
            error: function () {
                $('#innerajax').html('<div class="rbox x">読み込み中にエラーが発生しました。大変申し訳ありませんが、しばらく経った後に再度アクセスしてください。</div>');
            },
            beforeSend: function (thisXHR) {
                if (page > 1 || offset > 0) {
                    scrollToElement('listtop');
                }
                $('#innerajax').html('<div style="margin:150px 0"><div class="cent">ただいま情報を取得しています。<span id="ajax_progress"></span></div><div class="cent mt10"><img src="//chinamart.jp/img/img_loadingbar.gif" width="220" height="19"></div></div>');
            }
        });
    }
}

function toggleSubcategory(sub_hiddennum) {
    if ($('#subcategory_hidden').is(':visible')) {
        $('#subcategory_hidden').css('display', 'none');
        $('#subcategory_btn').html('《更に' + sub_hiddennum + '件のサブカテゴリ》');
    } else {
        $('#subcategory_hidden').css('display', 'inline');
        $('#subcategory_btn').html('《一部を隠す》');
    }
}