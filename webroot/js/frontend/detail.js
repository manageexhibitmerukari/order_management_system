/**
 * Created by root on 3/30/17.
 */
var old_quantity = 0;
$(document).ready(function () {

    $(document).on('click', '.owl-dots .owl-dot', function () {
            $('.owl-dots .owl-dot').each(function () {
                $(this).removeClass('active');
            });
            $(this).addClass('active');
            $('.owl-item').find('img').attr('src', $(this).find('img').attr('src'));
        });

    $('.pc-header .pc-header-inner .icon-search').click(function () {
        $(this).parent('.pc-header-form').submit();
    });

    if ($('#status-update').val() === 'success') {
        alert('カートから商品の数量が更新されました。');
        window.location.href = location.origin + '/user-products/show-cart';
    }

    $('.item-buy-btn').click(function () {
        var sku = $('#sku').val().trim();
        var color = $('#color').val().trim();
        var size = $('#size').val().trim();
        var amount = parseInt($('#amount').val().trim());
        var amount_default = parseInt($('#amount_default').val().trim());
        var price = $('.item-price .price').text().trim();
        var name = $('main .item-box-container h2.item-name').text().trim();

        if (color === ''){
            alert('商品の色を選択してください');
            return false;
        }
        if (amount < amount_default) {
            alert('販売下限数量は'+amount_default+'です。 '+amount_default+'以上入力してください。');
            document.getElementById("amount").focus();
            document.getElementById("amount").value=amount_default;
            return false;
        }
        if (size === '') {
            alert('商品のサイズを選択して下さい');
            return false;
        }
        var data = {
            'sku' : sku,
            'name' : name,
            'color' : color,
            'amount' : amount,
            'size' : size,
            'price' : price
        };
        $.ajax(
        {
            type: "POST",
            url: "/user-products/add-product",
            data: data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
            },
            success: function(data){
                bootbox.alert(data.msg);
                $('.num-cart').html(data.countProduct);

            },
            statusCode: {
                404: function() {
                    alert('page not found');
                }
            }
        });
    });

    $('#size').change(function () {
       var parent = $('#sku').val().trim();
       var size = $(this).val().trim();
       var color = $('#color').val().trim();
       if (size != '') {
           getColor(parent, size);
       }
    });

    $('#color').change(function () {
        var parent = $('#sku').val().trim();
        var color = $(this).val().trim();
        var size = $('#size').val().trim();
        if (color !== '') {
            if (size !== '')
                getPrice(parent, size, color);
            else
                getSize(parent, color);
        }
    });


    if ($('#container').attr('error') == 'error-login') {
        $('#login').ready(function() {
            $('#login').show();
            $('#login').addClass('in');
        });
    }


    $('.modal-login').click(function () {
       $('.box-error').empty();
    });

    $('.login-user').click(function () {
        $.ajax(
            {
                type: "POST",
                url: "/users/login-user",
                contentType: "application/x-www-form-urlencoded;",
                dataType: "json",
                data : {
                    username : $('#username').val(),
                    password : $('#password').val()
                },
                async: false,
                beforeSend: function() {
                },
                error: function(data, status, errThrown){
                    console.log(errThrown);
                },
                success: function(data){
                    console.log(data);
                    if (data.result == 'success') {
                        window.location.href = location.origin + location.pathname;
                    } else {
                        var form = $('form > .has-feedback .box-error');
                        var html = "<div class='alert alert-error'><p style='text-align:center;'>" + "" + data.msg + "" + "</p></div>";
                        form.prepend(html);
                    }
                },
                statusCode: {
                    404: function() {
                        alert('page not found');
                    }
                }
            });
    });

});


function getPrice(parent, size, color) {
    var data = {
        'parent_sku' : parent,
        'size_id' : size,
        'color' : color
    };
    $.ajax(
        {
            type: "POST",
            url: "/user-products/get-price",
            data: data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
            },
            success: function(data){
                console.log(data);
                if(data.amount == 0) {
                    $('#sold-out').removeClass('hidden');
                } else {
                    $('#sold-out').addClass('hidden');
                }
                $('.owl-dots').empty();
                var html = '';
                var active = '';
                var timestamp = Number(new Date());
                $.each(data.img, function (key, value) {
                    active = '';
                    if (key == 0) {
                        $('.owl-item').find('img[name="img-fisrt"]').attr('src', value + '?' + timestamp);
                        active = 'active';
                    }
                    html += '<div class="owl-dot ' + active + '">' +
                                '<span></span>' +
                                '<div class="owl-dot-inner">' +
                                '<img src="' + value + '?'+ timestamp +'" style="height: 75px;">' +
                                '</div>' +
                            '</div>';
                });
                $('.owl-dots').append(html);
                $('.item-price').html('¥ ' + data.price);
                $('#due_time').html(data.due_time);
                if ( data.buy_term ) {
                    $('#amount').val(data.buy_term);
                    $('#amount_default').val(data.buy_term);
                } else {
                    $('#amount').val(1);
                    $('#amount_default').val(1);
                }

                if (parseInt(data.amount) == 0) {
                    $('#remain_amount').html("<span style='color:red'>売り切れ</span>");
                } else {
                    $('#remain_amount').html(data.amount);
                }

            },
            statusCode: {
                404: function() {
                    alert('page not found');
                }
            }
        });
}

function getColor(parent_sku, id) {
    var data = {
        'parent_sku' : parent_sku,
        'size_id' : id
    };
    $.ajax(
    {
        type: "POST",
        url: "/user-products/get-color",
        data: data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        async: false,
        beforeSend: function() {
        },
        error: function(data, status, errThrown){
            console.log(errThrown);
        },
        success: function(data){
            var html;
            var color;

            if (data.color.length == 1 && data.color[0]['color'] == "無" ) {

                getPrice(data.color[0]['parent_sku'], data.color[0]['size_id'], data.color[0]['id'])
                html += '<option value="'+ data.color[0]['id'] +'">' + data.color[0]['color'] + '</option>';
            }
            else {
                html = '<option value="">すべて</option>';
                $.each(data.color, function(key, value) {
                    html += '<option value="'+ value['id'] +'">' + value['color'] + '</option>';
                });

            }
            $('#color').html(html);

        },
        statusCode: {
            404: function() {
                alert('page not found');
            }
        }
    });
}

function getSize(parent_sku, color) {
    var data = {
        'parent_sku' : parent_sku,
        'color' : color
    };
    $.ajax(
        {
            type: "POST",
            url: "/user-products/get-size",
            data: data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
            },
            error: function(data, status, errThrown){
                console.log(errThrown);
            },
            success: function(data){
                var html = '<option value="">すべて</option>';
                $.each(data.sizes, function(key, value) {
                    html += '<option value="'+ value['id'] +'">' + value['sizeName'] + '</option>';
                });
                $('#size').html(html);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                }
            }
        });
}

function changeQuantity(obj) {
    value = parseInt($(obj).val());
    //quantity_max = parseInt($("#quantity_max_"+cart_id).val());

    //console.log(quantity_max);

    if(value >=1 ){
        $(".item-buy-btn").removeClass("disabled");
    }else {

        $(".item-buy-btn").addClass("disabled");
    }
}