var baseurl = "//chinamart.jp/";
var rooturl = "http://chinamart.jp/";
var rooturls = "https://chinamart.jp/";

var hostName = document.location.hostname;
if(hostName == "localhost" || hostName == "127.0.0.1" ){
	baseurl = "//localhost/chinamart/";
	rooturl = "http://localhost/chinamart/";
	rooturls = "https://localhost/chinamart/";
}else{
	window.onerror = function(text, page, line) {
		$.get(
			+'error_javascript.php', {page:encodeURIComponent(page),line:line,text:encodeURIComponent(text)});
		return true;
	}
}

function startNarrow(code) {
	if (code == 13) {
		getPage(-1, 1);
	}
}

function activateNarrowMenu(){
	$('.narrow_value_current').bind('touchstart', function(e){
		var windowwidth = $(window).width();
		$('.narrow_value.hover').removeClass('hover');
		$(this).parent('.narrow_value').addClass('hover');
		var elm = $(this).parents('.narrow_each');
		var elm_left = elm.offset().left;
		var elm_width = elm.outerWidth();
		var elm_right = windowwidth - elm_left - elm_width;
		if(elm_left > elm_right){
			$(this).siblings('.narrow_options').css({left:'auto',right:'0'});
		}else{
			$(this).siblings('.narrow_options').css({left:'0',right:'auto'});
		}
	});
	$('.narrow_options_close').bind('touchstart', function(e){
		$(this).parents('.narrow_value').removeClass('hover');
	});
}

function activateOverflowPulldown(){
	var windowwidth = $(window).width();
	var menuwidth, otheridx;
	if(windowwidth > 774) {
		$('.overflowpulldown').each(function(){
			menuwidth = 0;
			otheridx = 0;
			$(this).find('li').each(function(i){
				menuwidth += $(this).outerWidth();
				if(menuwidth > 690){
					otheridx = i;
					return false;
				}
			});
			$(this).find('li:gt('+(otheridx-1)+')').wrapAll('<li class="tabmenu_other"></li>').wrapAll('<ul class="tabmenu_other_wrap"></ul>').parent().before('<a>その他</a>');
			$(this).parent('.slide_wrap').css('overflow','visible');
		});
	}else{
		$('.overflowpulldown').each(function(){
			menuwidth = 0;
			$(this).find('li').each(function(i){
				menuwidth += $(this).outerWidth()+4;
			});
			menuwidth += 6;
			$(this).css('width', menuwidth+'px');
		});

	}
}

function setSlideHeight(){
	var h = $('#slide img:eq(0)').height();
	$('#slide').animate({height:h+'px'},'fast');
}

function activateExpand(h){
	var elmclone, wrapheight;
	if(typeof h == 'undefined'){
		h = 600;
	}
	$('.expand').not('.active').each(function(){
		if($(this).is(':visible')){
			wrapheight = $(this).outerHeight();
		}else{
			elmclone = $(this).clone().css({position:'absolute',visibility:'hidden',display:'block'}).appendTo("body");
			wrapheight = elmclone.outerHeight();
			elmclone.remove();
		}
		if(wrapheight > h){
			$(this).addClass('active').css('height',h+'px');
			$(this).append('<div class="expandbtn" onclick="toggleExpand(this)">全てを表示</div>');
		}
	});
}

function toggleExpand(btn){
	if($(btn).parent('.expand').hasClass('shown')){
		$(btn).parent('.expand').removeClass('shown');
		$(btn).html('全てを表示');
	}else{
		$(btn).parent('.expand').addClass('shown');
		$(btn).html('一部を隠す');
	}
}

function setHorizontalScroll(){
	var w = $(window).width(),scroll = {};
	if(w <= 600){
		$('.slide_wrap').each(function(i){
			if($(this).find('table').size()){
				scroll[i] = new iScroll(this, {hScrollbar: true, vScrollbar: false});
			}else {
				scroll[i] = new iScroll(this, {hScrollbar: false, vScrollbar: false});
			}
			scroll[i].scrollToElement(".shown");
		});
	}else{
		$('.slide_wrap').children('.wd_narrow, .wd_middle').removeClass('wd_narrow wd_middle');
	}
}

function setItemlist(f){
	var windowwidth = $(window).width();
	if(windowwidth <= 774 || f == 1){
		var w = $('.list_wrap').not('.exclude').width();
		var each = 152;
		var d = parseInt(w / each);
		var neww = d * each;
		$('.list_wrap').not('.exclude').find('.list_inner').css('width', neww + 'px');
	}else{
		$('.list_inner').css('width', '100%');
	}
}

function setRanklist(){
	var w = $('.rank_wrap').width();
	var each = 94;
	var d = parseInt(w / each);
	var neww = d * each;
	$('.rank_inner').css('width',neww+'px');
}

function setFeaturelist(){
	var winw = $(window).width(),row,col,clone,colspan;
	var each;
	if(winw <= 774) {
		each = 150;
	}else{
		each = 254;
	}
	var w = $('.ftr_wrap').width();
	var d = parseInt(w / each);
	var neww = d * each;
	$('.ftr_inner').css('width',neww+'px');
}

function breakColspan(){
	var w = $(window).width(),row,col,clone,colspan;
	if(w <= 600){
		$('.break_colspan>tbody>tr').each(function(){
			if($(this).children('th').size() > 1){
				row = $(this);
				$(this).children('th').each(function(i){
					if(i > 0){
						col = $(this).next('td').andSelf();
						clone = col.clone(true).wrapAll('<tr></tr>').parent('tr').addClass('temprow');
						row.after(clone);
						col.remove();
						row = clone;
					}
				});
			}
			$(this).children('td').each(function(){
				colspan = $(this).attr('colspan');
				if( typeof colspan !== 'undefined' && colspan !== false ){
					$(this).removeAttr('colspan').addClass('widecol');
				}
			});
		});
	}else{
		$('.break_colspan>tbody>tr').each(function(){
			if($(this).hasClass('temprow')){
				$(this).children().appendTo(row);
				$(this).remove();
			}else{
				row = $(this);
			}
			$(this).children('td.widecol').each(function(){
				$(this).attr('colspan','3');
			});
		});
	}
}

function showTooltip(obj){
	var obj = $(obj),pos,w,str = obj.attr('data-tooltip'),
	dw = $('body').outerWidth(), dh = $('body').outerHeight();
	var offtop = obj.offset().top, offleft = obj.offset().left;
	$('.tooltip_wrap').remove();
	if(offleft < (dw/2)){
		if(offtop < 150){
			pos = 'lt';
		} else {
			pos = 'lb';
		}
		if((dw - offleft) < 200){
			w = dw - offleft;
		}else{
			w = 200;	
		}
	} else {
		if(offtop < 150){
			pos = 'rt';
		} else {
			pos = 'rb';
		}
		if(offleft < 200){
			w = offleft;
		}else{
			w = 200;
		}
	}
	$('body').append('<div class="tooltip_wrap" style="left:'+offleft+'px;top:'+offtop+'px"><div class="tooltip_inner '+pos+'" style="width:'+w+'px">'+str+'</div></div>');
	$('.tooltip_wrap').fadeIn(200);
}

function closeColorbox(){
	try {
		window.parent.$.colorbox.close();
	}catch(e) {
		$('#cboxOverlay, #colorbox').css('visibility','hidden');
	}
}

function hideTooltip(){
	$('.tooltip_wrap').not('.tooltip_fix').remove();
}

function fixTooltip(){
	$('.tooltip_wrap').addClass('tooltip_fix');
}

function activateToggle(){
	$('li.head').click(function(){
		var params = {height:'toggle', opacity:'toggle'}; 
		if($(this).hasClass('shown')){
			$(this).next().animate(params,'fast');
			$(this).removeClass('shown');
		}else{
			$(this).siblings('.body:visible').animate(params,'fast');
			$(this).siblings('.head').removeClass('shown');
			$(this).addClass('shown');
			$(this).next().animate(params,'fast');
		}
	}); 
}

function checkLogout(){
	if(window.confirm('ログアウトしてよろしいですか。')){
		window.location.href = rooturl+"logout";
	}
}

function cal_shippriceprice(){
	var weight = $('#cal_weight').val();
	var plan = $('#cal_plan').val();
	var shipper = $('#cal_shipper').val();
	var country = $('#cal_country').val();
	if(!weight.match(/^\d{1,2}(\.\d)?$/)){
		window.alert('重量は小数点1桁までの半角数字で入力してください。');
	} else if (weight > 30){
		window.alert('重量は30kg以下で入力してください。');
	} else {
		$.ajax({
			type: "GET",
			url: rooturl+"shiprate.php",
			data: {weight:weight,plan:plan,shipper:shipper,country:country},
			success: function(data){
				$('#cal_result').html(data);
				$('#cal_button').removeAttr('disabled');
				$('#cal_button').css('cursor','pointer');
			},
			beforeSend: function(){
				$('#cal_button').prop('disabled', true);
				$('#cal_button').css('cursor','default');
			}
		});
	}
}

function getCalender(cy,cn){
	$.ajax({
		type: "GET",
		url: rooturl+"calendar.php",
		data: {cy:cy,cn:cn},
		success: function(data){
			$('#calendar').html(data);
		},
		beforeSend: function(){
			$('#calendar').html('<img src="'+rooturl+'img/img_loading.gif" width="16" height="16" style="margin:92px 126px">');
		}
	});	
}

function scrollToElement(element){
	var pageElement = document.getElementById(element);
	var positionX = 0,         
	positionY = 0;    
	
	while(pageElement != null){
		positionX += pageElement.offsetLeft;
		positionY += pageElement.offsetTop;
		pageElement = pageElement.offsetParent;
		window.scrollTo(positionX, positionY);
	}
}

function setMaxLength(obj,maxlen){
	var str = $(obj).val();
	var len = str.length;
	if(len > maxlen){
		str = str.slice(0,maxlen);
		$(obj).val(str);
	}
}

function getCategorySuggest(code,site){
	if($('#textsuggest').size() == 0){
		$('#textsugget_wrap').append('<div id="textsuggest"></div>');
	}
	if(13 === code){
		var q = $('#src_category').val();
		q = q.replace(/^\s+|\s+$/g,'');
		if(q.length){
			$.ajax({
				type: 'GET',
				url: rooturl+'category_suggest.php',
				data: {q:encodeURIComponent(q),site:site},
				success: function(data){
					$('#textsuggest').html(data);
				},
				beforeSend: function(){
					$('#textsuggest').html('<img src="'+rooturl+'img/img_loading.gif" width="16" height="16" class="mt5 mb5">');
				}
			});
		}else{
			$('#textsuggest').html('<div><b>キーワードを入力してください。</b></div>');
		}
	}else{
		$('#textsuggest').html('<div><em>エンターキーで検索を開始します。</em></div>');
	}
}

function hideCategorySuggest(){
	if($('#textsuggest').size()){
		$('#textsuggest').css('display','none');
	}
}

function showCategorySuggest(){
	if($('#textsuggest').size()){
		$('#textsuggest').css('display','block');
	}
}

function addCart(){
	var site = $('#itm_site').val();
	var itmitem = $('#itm_item').val();
	var name = $('#itm_name').val();
	var imgurl = $('#itm_imgurl').val();
	var price = $('#itm_price').val();
	var seller = $('#itm_seller').val();
	var secret = $('#itm_secret').val();
	var num = convertNum($('#itm_num').val());
	var minnum = $('#itm_minnum').val();
	var maxnum = $('#itm_maxnum').val();
	var edtmin = $('#itm_edtmin').val();
	var edtmax = $('#itm_edtmax').val();
	var skunum = $('.itm_sku_hdn').length;
	var sku = '',skue;
	if(maxnum == 0){
		$('#cartadd_result').css('display','block').html('<div id="cartadd_result_inner" class="noresult">在庫がありません</div>');
		$('#cartadd_result_inner').delay(2000).fadeOut('slow');
		return false;
	}

	for(var i = 0; i < skunum; i++){
		if(i != 0){sku += ' / ';}
		skue = $('#itm_sku'+i).val();
		if(skue == ''){
			$('#cartadd_result').css('display','block').html('<div id="cartadd_result_inner" class="noresult">オプションを選択してください</div>');
			$('#cartadd_result_inner').delay(2000).fadeOut('slow');
			return false;
		}else{
			sku += skue;
		}
	}

	var numchk=/^[0-9]{1,5}$/i;
	if(!num.match(numchk) || num == 0){
		$('#cartadd_result').css('display','block').html('<div id="cartadd_result_inner" class="noresult">数量を半角数字で入力してください</div>');
		$('#cartadd_result_inner').delay(2000).fadeOut('slow');
		return false;
	}else if(parseInt(num) < parseInt(minnum)){
		$('#cartadd_result').css('display','block').html('<div id="cartadd_result_inner" class="noresult">数量は'+minnum+'個以上にしてください</div>');
		$('#cartadd_result_inner').delay(2000).fadeOut('slow');
		return false;
	}else if(parseInt(num) > parseInt(maxnum)){
		$('#cartadd_result').css('display','block').html('<div id="cartadd_result_inner" class="noresult">数量は'+maxnum+'個以下にしてください</div>');
		$('#cartadd_result_inner').delay(2000).fadeOut('slow');
		return false;
	}else{
		 $.ajax({
			type: "POST",
			dataType: 'json',
			url: rooturl+"cart_add.php",
			data: {site:site,itmitem:itmitem,name:encodeURIComponent(name),price:price,seller:encodeURIComponent(seller),num:num,edtmin:edtmin,edtmax:edtmax,sku:encodeURIComponent(sku),imgurl:imgurl,secret:secret},
			success: function(data){
				if(data.status == 'success'){
					$('#cartadd_result').css('display','block').html('<div id="cartadd_result_inner" class="result" onclick="removeCartaddResult()">追加しました。<a onclick="openColorbox(\''+rooturl+'cart?mode=popup\')">確認</a></div>');
					if(data.exemode == 'add') {
						if ($('#cartnum').size()) {
							var cartnum = $('#cartnum').text();
							cartnum = parseInt(cartnum) + 1;
							$('#cartnum').html(cartnum);
						} else {
							$('#cartnum_wrap').html('<span id="cartnum">1</span>');
						}
					}
				}else{
					window.alert('カートへの追加に失敗しました。大変申し訳ありませんが、しばらく経ってから再度お試しください。');
				}
				$('#cartadd_button').removeClass('progressbtn');
			},
			beforeSend: function(){
				$('#cartadd_result').css('display','block').html('<div class="progress">只今、追加しています</div>');
				$('#cartadd_button').addClass('progressbtn');
				
			}
		});	
	}
}

function removeCartaddResult(){
	$('#cartadd_result_inner').fadeOut('slow');
}

function addFavorite(btn){
	var itm_site = $('#itm_site').val(),itm_item = $('#itm_item').val(),itm_name = $('#itm_name').val(),itm_img = $('#itm_imgurl').val(),itm_price = $('#itm_price').val();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: rooturl+'mp/favorite_add.php',
		data: {itm_site:itm_site,itm_item:itm_item,itm_name:encodeURIComponent(itm_name),itm_price:itm_price,itm_img:encodeURIComponent(itm_img)},
		success: function(data){
			if(data.status == 'login'){
				$(btn).find('.bubble').removeClass('d').addClass('i').html('会員ログインが必要です');
			}else if(data.status == 'success'){
				$(btn).attr('title','既に追加済みです').find('.bubble').removeClass('d').html('追加しました');
			}else if(data.status == 'exist'){
				$(btn).attr('title','既に追加済みです').find('.bubble').removeClass('d').addClass('i').html('既に追加済みです');
			}else{
				$(btn).find('.bubble').removeClass('d').addClass('x').html('追加に失敗しました');
			}
		},
		complete: function(){
			$(btn).find('.bubble_wrap').delay(5000).fadeOut('slow',function(){$(this).remove();});
		},
		beforeSend: function(){
			$(btn).addClass('d').append('<div class=\"bubble_wrap high\"><div class=\"bubble d\">お待ちください</div></div>');
		}
	});
}

function addFavoriteList(btn,itm_site,itm_item,itm_name,itm_img,itm_price){
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: rooturl+'mp/favorite_add.php',
		data: {itm_site:itm_site,itm_item:itm_item,itm_name:encodeURIComponent(itm_name),itm_price:itm_price,itm_img:encodeURIComponent(itm_img)},
		success: function(data){
			if(data.status == 'login'){
				$(btn).find('.bubble').removeClass('d').addClass('i').html('会員ログインが必要です');
			}else if(data.status == 'success'){
				$(btn).attr('title','既に追加済みです').find('.bubble').removeClass('d').html('追加しました');
			}else if(data.status == 'exist'){
				$(btn).attr('title','既に追加済みです').find('.bubble').removeClass('d').addClass('i').html('既に追加済みです');
			}else{
				$(btn).find('.bubble').removeClass('d').addClass('x').html('追加に失敗しました');
			}
		},
		complete: function(){
			$(btn).find('.bubble_wrap').delay(5000).fadeOut('slow',function(){$(this).remove();});
		},
		beforeSend: function(){
			$(btn).parent().css('display','block');
			$(btn).addClass('d').append('<div class=\"bubble_wrap\"><div class=\"bubble d\">お待ちください</div></div>');
		}
	});
}

function addFavoriteSeller(btn,site,seller,sellerid){
	$.ajax({
		type: "POST",
		dataType: 'json',
		url: rooturl+"mp/favorite_seller_add.php",
		data: {site:site,seller:encodeURIComponent(seller),sellerid:encodeURIComponent(sellerid)},
		success: function(data){
			if(data.status == 'login'){
				$(btn).find('.bubble').removeClass('d').addClass('i').html('会員ログインが必要です');
			}else if(data.status == 'success'){
				$(btn).attr('title','既に追加済みです').find('.bubble').removeClass('d').html('追加しました');
			}else if(data.status == 'exist'){
				$(btn).attr('title','既に追加済みです').find('.bubble').removeClass('d').addClass('i').html('既に追加済みです');
			}else{
				$(btn).find('.bubble').removeClass('d').addClass('x').html('追加に失敗しました');
			}
		},
		complete: function(){
			$(btn).find('.bubble_wrap').delay(5000).fadeOut('slow',function(){$(this).remove();});
		},
		beforeSend: function(){
			$(btn).addClass('d').append('<div class=\"bubble_wrap high\"><div class=\"bubble d\">お待ちください</div></div>');
		}
	});	
}

function setSku(type,num,prop){
	$('#cartadd_result').css('display','none');
	var skuobj = $('.item_skus:eq('+type+') .item_sku_each:eq('+num+')');
	if(skuobj.hasClass('shown')){
		delete propvalue[type];
		skuobj.removeClass('shown');
		$('#itm_sku'+type).val('');
		$(".item_skus").not(':eq('+type+')').find('.disabled').not('.nostock').removeClass('disabled');
		$('#sku_stock').css('display','block').html('');
	}else{
		if(skuobj.hasClass('disabled')){
			return false;
		}

		$('.item_skus:eq('+type+') .item_sku_each').removeClass('shown');
		skuobj.addClass('shown');
		var skuvalue = skuobj.attr('name');
		$('#itm_sku'+type).val(skuvalue);
		
		if(skuobj.find('img').size()){
			var thumburl = skuobj.find('img').attr('src');
			thumburl = thumburl.replace('_30x30.jpg','_310x310.jpg');
			$('#item_img').css("background-image","url('"+thumburl+"')");
			$('.item_thumb_each').removeClass('shown');
		}
		
		propvalue[type] = prop;
		var propvalue_copy = propvalue.slice(0);
		var propstr = propvalue_copy.sort().join(';');
		var stockstr = '';
		if(skustock[propstr] != undefined && skuprice[propstr] != undefined){
			if(skustock[propstr] == 0){
				stockstr += '<span class="bold clr_red">';
			}else{
				stockstr += '<span class="bold clr_green">';
			}
			stockstr += '在庫:'+skustock[propstr];
			$('#itm_maxnum').val(skustock[propstr]);
			$('#itm_edtmax').val(skustock[propstr]);
			
			var this_skuid = skuid[propstr];
			var this_skuprice = skuprice[propstr];

			stockstr += ' / 価格:';

			if((this_skuid in promotion) && promotion[this_skuid] != this_skuprice){
				stockstr += '<span class=\"del\"><span>'+numberFormat(skuprice[propstr])+'元</span></span><b class=\"ml5\">'+numberFormat(promotion[this_skuid])+'元</b>';
				this_skuprice = promotion[this_skuid];
			}else{
				stockstr += numberFormat(this_skuprice)+'元';
			}			
			$('#itm_price').val(this_skuprice);
			stockstr += '</span>';
			$('#sku_stock').html(stockstr);
		}
		
		var thisprice = $('#itm_price').val();
		checkOutStock(type);
		
		var skunum = $('.itm_sku_hdn').length;
		var sku = '';
		for(var i = 0; i < skunum; i++){
			if(i != 0){sku += ' / ';}
			var skue = $('#itm_sku'+i).val();
			sku += skue;
		}
		var secret = sku+thisprice;
		$('#itm_secret').val($.md5(secret));
	}
}

function setPriceAli(){
	var thisprice = priceranges[0][1],thisedtmin = priceranges[0][0],thisedtmax = priceranges[1][0]-1,num = $('#itm_num').val();

	if(typeof priceranges == 'undefined'){
		return false;
	}

	for(var i = (priceranges.length-1); i >= 0; i--){
		if(parseInt(num) >= parseInt(priceranges[i][0])){
			thisprice = priceranges[i][1];
			thisedtmin = priceranges[i][0];
			if(typeof priceranges[i+1] != 'undefined') {
				thisedtmax = priceranges[i + 1][0] - 1;
			}else{
				thisedtmax = maxnum;
			}
			break;
		}
	}
	$('#itm_price').val(thisprice);
	$('#itm_edtmin').val(thisedtmin);
	$('#itm_edtmax').val(thisedtmax);
	
	var skunum = $('.itm_sku_hdn').length;
	var sku = '';
	for(var i = 0; i < skunum; i++){
		if(i != 0){sku += ' / ';}
		var skue = $('#itm_sku'+i).val();
		sku += skue;
	}
	var secret = sku+thisprice;
	$('#itm_secret').val($.md5(secret));
}
function setSkuAli(type,num,prop){
	$('#cartadd_result').css('display','none');
	var skuobj = $('.item_skus:eq('+type+') .item_sku_each:eq('+num+')');
	if(skuobj.hasClass('shown')){
		delete propvalue[type];
		skuobj.removeClass('shown');
		$('#itm_sku'+type).val('');
		$(".item_skus").not(':eq('+type+')').find('.disabled').not('.nostock').removeClass('disabled');
		$('#sku_stock').css('display','block').html('');
	}else{
		if(skuobj.hasClass('disabled')){
			return false;
		}

		$('.item_skus:eq('+type+') .item_sku_each').removeClass('shown');
		skuobj.addClass('shown');
		var skuvalue = skuobj.attr('name');
		$('#itm_sku'+type).val(skuvalue);
		
		if(skuobj.find('img').size()){
			var thumburl = skuobj.find('img').attr('src');
			thumburl = thumburl.replace('_30x30.jpg','_310x310.jpg');
			$('#item_img').css("background-image","url('"+thumburl+"')");
			$('.item_thumb_each').removeClass('shown');
		}
		
		propvalue[type] = prop;
		var propvalue_copy = propvalue.slice(0);
		var propstr = propvalue_copy.sort(function(a,b){
			return a-b;
		}).join(';');
		var stockstr = '';
		if(skustock[propstr] != undefined && skuprice[propstr] != undefined){
			if(skustock[propstr] == 0){
				stockstr += '<span class="bold clr_red">';
			}else{
				stockstr += '<span class="bold clr_green">';
			}
			stockstr += '在庫:'+skustock[propstr];
			$('#itm_maxnum').val(skustock[propstr]);
			$('#itm_edtmax').val(skustock[propstr]);
			maxnum = skustock[propstr];
			
			var this_skuid = skuid[propstr];
			var this_skuprice = skuprice[propstr];
			
			if(this_skuprice != ''){
				stockstr += ' / 価格:'+numberFormat(this_skuprice)+'元';
				$('#itm_price').val(this_skuprice);
			}
			stockstr += '</span>';
			$('#sku_stock').html(stockstr);
		}
		
		var thisprice = $('#itm_price').val();
		checkOutStock(type);
		
		var skunum = $('.itm_sku_hdn').length;
		var sku = '';
		for(var i = 0; i < skunum; i++){
			if(i != 0){sku += ' / ';}
			var skue = $('#itm_sku'+i).val();
			sku += skue;
		}
		var secret = sku+thisprice;
		$('#itm_secret').val($.md5(secret));
	}
}

function checkOutStock(type){
		var skuval = propvalue[type];
		$(".item_skus").not(':eq('+type+')').find('.disabled').not('.nostock').removeClass('disabled');
		for(var skupair in skuoutstock){
			var skuary = skupair.split(';');
			if(skuary.some(function(v){ return v == skuval})){
				for(var j = 0; j < skuary.length; j++){
					if(skuary[j] != skuval){
						var $dsku = $(".item_sku_each[name='"+skuary[j]+"']");
						if($dsku.hasClass('shown')){
							$(".itm_sku_hdn[value='"+skuary[j]+"']"+j).val('');
							$dsku.removeClass('shown').addClass('disabled');
						}else{
							$dsku.addClass('disabled');
						}
					}
				}
			}
		}
}

function swapImg(num){
	var thumbdiv = $('.item_thumb_each').eq(num);
	var thumburl = thumbdiv.find('img').attr('src');
	$('#item_img').css("background-image","url('"+thumburl+"')");
	$('.item_thumb_each').removeClass('shown');
	thumbdiv.addClass('shown');
}

function isNumberKey(evt){
	evt = evt || window.event;
	var key = evt.keyCode || evt.which;
	if(key == 13){
		true;
	}else{
		key = String.fromCharCode(key);
		regex = /[0-9]|\./;
		if( !regex.test(key) ) {
			evt.returnValue = false;
			if(evt.preventDefault) evt.preventDefault();
		}
	}
}

function convertNum(str){
	var zenkaku=new Array('１','２','３','４','５','６','７','８','９','０','．'),hankaku=new Array(1,2,3,4,5,6,7,8,9,0,'.'),count;
    while(str.match(/[０-９．]/)){
    	for(count=0; count < zenkaku.length; count++){
        	str=str.replace(zenkaku[count], hankaku[count]);
        }
    }
    return str;
}

function numberFormat(n) {
	var l, m='';
	var mark = (n < 0) ? '-' : '';
	var flt = '';
	n = Math.abs(n);
	if (n % 1) {
		flt  = n + '';
		flt = flt.substr(flt.indexOf('.'));
	}
	n = Math.floor(n) + '';
	while ( (l = n.length) >3 ) {
		m = ',' + n.substr( l - 3, 3 ) + m;
		n = n.substr( 0, l - 3 );
	}
	return mark + n + m + flt;
}



function openColorboxImg(openurl){
	try {
		$.colorbox({href:openurl});
	}catch(e) {
		window.open(openurl);
	}
}

function openColorbox(openurl){
	if(openurl.match(/\?/)){
		openurl += '&popupmode=popup';
	}else{
		openurl += '?popupmode=popup';
	}

	try {
		$.colorbox();
	}catch(e) {
		window.open(openurl);
	}
}

function openColorboxModal(openurl){
	if(openurl.match(/\?/)){
		openurl += '&popupmode=popup';
	}else{
		openurl += '?popupmode=popup';
	}

	try {
		$.colorbox({opacity: 0.7, fixed:true, iframe:true, width:'98%', height:'98%', maxWidth: '802px', maxHeight: '600px', href:openurl, overlayClose: false});
	}catch(e) {
		window.open(openurl);
	}
}

function openColorboxRel(openurl){
	if(openurl.match(/\?/)){
		openurl += '&popupmode=popup';
	}else{
		openurl += '?popupmode=popup';
	}

	try {
		$.colorbox({opacity: 0.7, fixed:true, href:openurl, rel: 'imggroup'});
	}catch(e) {
		window.open(openurl);
	}
}

function toggleSearch(){
	if($('#hd_src_wrapper').hasClass('shown')){
		$('#hd_src_wrapper').animate({height:'0px',opacity:0},'fast',function(){
			$(this).css({'height':'','opacity':''}).removeClass('shown');
		});
	}else{
		$('#hd_src_wrapper').css({'display':'block','height':'0px','opacity':'0'}).animate({height:'104px',opacity:1},'fast',function(){
			$(this).css({'display':'','height':'','opacity':''}).addClass('shown');
		});
	}
}

function setLang(lang){
	var topcat;
	var site = $('#src_site').val();
	if(lang == 't'){
		$('#search_t').addClass('shown');
		$('#search_nt, #search_seller, #search_url').removeClass('shown');
		$('#src_lang').val('t');
		if(site == 'taobao'){
			topcat = $('#src_taobao_topcat').val();
		}else{
			topcat = $('#src_alibaba_topcat').val();
		}
		$('#hd_src_category, #hd_src_keyword').removeClass('sellerurl');
		if(topcat == 0){
			$('#src_keyword').removeClass('long short').addClass('middle').attr('placeholder',"検索ワードをスペースで区切って入力してください。");
			$('#src_history').removeClass('long short').addClass('middle');
		}else{
			$('#src_keyword').removeClass('long middle').addClass('short').attr('placeholder',"検索ワードをスペースで区切って入力してください。");
			$('#src_history').removeClass('long middle').addClass('short');
		}
	} else if(lang == 'nt'){
		$('#search_nt').addClass('shown');
		$('#search_t, #search_seller, #search_url').removeClass('shown');
		$('#src_lang').val('nt');
		if(site == 'taobao'){
			topcat = $('#src_taobao_topcat').val();
		}else{
			topcat = $('#src_alibaba_topcat').val();
		}
		$('#hd_src_category, #hd_src_keyword').removeClass('sellerurl');
		if(topcat == 0){
			$('#src_keyword').removeClass('long short').addClass('middle').attr('placeholder',"検索ワードをスペースで区切って入力してください。");
			$('#src_history').removeClass('long short').addClass('middle');
		}else{
			$('#src_keyword').removeClass('long middle').addClass('short').attr('placeholder',"検索ワードをスペースで区切って入力してください。");
			$('#src_history').removeClass('long middle').addClass('short');

		}
	} else if(lang == 'seller'){
		$('#search_seller').addClass('shown');
		$('#search_t, #search_nt, #search_url').removeClass('shown');
		$('#src_lang').val('seller');
		$('#hd_src_category, #hd_src_keyword').addClass('sellerurl');
		$('#src_keyword').removeClass('middle short').addClass('long').attr('placeholder',"出品者の名前を正確に入力してください。");
		$('#src_history').removeClass('middle short').addClass('long');
	} else if(lang == 'url'){
		$('#search_url').addClass('shown');
		$('#search_t, #search_nt, #search_seller').removeClass('shown');
		$('#src_lang').val('url');
		$('#hd_src_category, #hd_src_keyword').addClass('sellerurl');
		if(site == 'taobao'){
			$('#src_keyword').removeClass('middle short').addClass('long').attr('placeholder',"淘宝・天猫の商品詳細ページURLまたは商品IDを入力してください。");
		}else{
			$('#src_keyword').removeClass('middle short').addClass('long').attr('placeholder',"アリババの商品詳細ページURLまたは商品IDを入力してください。");
		}
		$('#src_history').removeClass('middle short').addClass('long');
	}
}

function changeSite(type){
	var lang = $('#src_lang').val(),topcat;
	if(type=='taobao'){
		$('#search_btn_taobao').removeClass('d');
		$('#search_btn_alibaba').addClass('d');
		$('#search_taobao').addClass('shown');
		$('#search_alibaba').removeClass('shown');
		if(lang == 't' || lang == 'nt'){
			topcat = $('#src_taobao_topcat').val();
			if(topcat == 0){
				$('#src_keyword, #src_history').removeClass('short long').addClass('middle');
			}else{
				$('#src_keyword, #src_history').removeClass('middle long').addClass('short');
			}
		}else{
			$('#src_keyword, #src_history').removeClass('short middle').addClass('long');
		}
		$('#src_site').val('taobao');
		if($('#search_url').hasClass('shown')){
			$('#src_keyword').attr('placeholder','淘宝または天猫の商品詳細ページURLを入力してください。');
		}
	}else{
		$('#search_btn_taobao').addClass('d');
		$('#search_btn_alibaba').removeClass('d');
		$('#search_taobao').removeClass('shown');
		$('#search_alibaba').addClass('shown');
		if(lang == 't' || lang == 'nt'){
			topcat = $('#src_alibaba_topcat').val();
			if(topcat == 0){
				$('#src_keyword, #src_history').removeClass('short long').addClass('middle');
			}else{
				$('#src_keyword, #src_history').removeClass('middle long').addClass('short');
			}
		}else{
			$('#src_keyword, #src_history').removeClass('short middle').addClass('long');
		}
		$('#src_site').val('alibaba');
		if($('#search_url').hasClass('shown')){
			$('#src_keyword').attr('placeholder','アリババの商品詳細ページURLを入力してください。');
		}
	}
}

function changeTopCategory(top_category){
	var site = $('#src_site').val();
	if(top_category == 0){
		$('#src_keyword, #src_history').removeClass('short').addClass('middle');
		$('#hd_src_catdiv_'+site).removeClass('shown').addClass('default').html('<select><option>-</option></select>');
	}else{
		$.ajax({
			type: 'POST',
			url: '/user-products/category_search',
			data: {id: top_category},
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
			success: function (data) {
				var html = '<select name="category-child">';
				html += '<option value="">全カテゴリ</option>'
				$.each(data.child, function(index, value) {
					html += '<option value="' + value['id'] + '">' + value['categoryName'] + '</option>';
				});
				html += '</select>';
                $('#src_keyword, #src_history').removeClass('middle').addClass('short');
				$('#hd_src_catdiv_'+site).html(data);
                $('#hd_src_catdiv_'+site).addClass('shown').html(html);
			}
		});
	}
}


function itemSearch(){
	// var iid;
	// var site = $('#src_site').val();
	// var q = $('#src_keyword').val().replace(/(^\s+)|(\s+$)/g, "");
	// var lang = $('#src_lang').val();
	// if(q.match(/^http/)){
	// 	lang = 'url';
	// }
	// if(lang == 'url'){
	// 	if(site == 'taobao'){
	// 		if(q.match(/(taobao\.com|tmall\.com)/)){
	// 			var reg = new RegExp("(id=|\/i|item\/)([0-9]{5,})");
	// 			var m;
	// 			if(m = q.match(reg)) {
	// 				iid = m[2];
	// 			}else{
	// 				window.alert('このURLからは商品情報が取得できません。');
	// 				return false;
	// 			}
	// 		}else{
	// 			iid = q;
	// 		}
	// 		if(iid.match(/[0-9]{5,}/)) {
	// 			window.location.href = rooturl+"taobao_item?iid="+iid+"&key="+$.md5(iid);
	// 		}else{
	// 			window.alert('淘宝・天猫の商品詳細ページURLまたは商品IDを入力してください。');
	// 		}
	// 	}else{
	// 		if(q.match(/(1688\.com)/)){
	// 			var reg = new RegExp("offer\/([0-9]{5,})\.html");
	// 			var m;
	// 			if(m = q.match(reg)){
	// 				iid = m[1];
	// 			}else{
	// 				window.alert('このURLからは商品情報が取得できません。');
	// 			}
	// 		}else{
	// 			iid = q;
	// 		}
	// 		if(iid.match(/[0-9]{5,}/)) {
	// 			window.location.href = rooturl+"alibaba_item?iid="+iid+"&key="+$.md5(iid);
	// 		}else{
	// 			window.alert('アリババの商品詳細ページURLまたは商品IDを入力してください。');
	// 		}
    //
	// 	}
	// } else if(lang == 'seller'){
	// 	if(q == ''){
	// 		window.alert('出品者の名前を正確に入力してください。');
	// 	} else {
	// 		if(site == 'taobao'){
	// 			window.location.href = rooturl+"taobao_list?seller="+encodeURIComponent(q);
	// 		}else{
	// 			window.location.href = rooturl+"alibaba_list?seller="+encodeURIComponent(q);
	// 		}
	// 	}
	// } else {
	// 	var topcat = $('#src_'+site+'_topcat').val();
	// 	var catkey = $('#src_'+site+'_category').val();
	// 	if((typeof catkey == 'undefined' || catkey == "") && q == ''){
	// 		window.alert('検索キーワードを入力してください。');
	// 	}else if(q.length > 30){
	// 		window.alert('検索キーワードは30文字以内で入力してください。');
	// 	}else{
	// 		var hrefstr;
	// 		if(site == 'taobao'){
	// 			hrefstr = rooturl+"taobao_list";
	// 		}else{
	// 			hrefstr = rooturl+"alibaba_list";
	// 		}
	// 		if(typeof catkey != 'undefined' && catkey != ""){
	// 			hrefstr += "/"+catkey;
	// 		}
	// 		if(q != ''){
	// 			hrefstr += "?q="+encodeURIComponent(q)+"&lang="+lang;
	// 		}
	// 		window.location.href = hrefstr;
	// 	}
	// }
	$('#search-top').submit();
}

function itemSearchHistory(q_from,q_to){
	var site = $('#src_site').val();
	var topcat = $('#src_'+site+'_topcat').val();
	var catkey = $('#src_'+site+'_category').val();
	var hrefstr;
	if(site == 'taobao'){
		hrefstr = rooturl+"taobao_list";
	}else{
		hrefstr = rooturl+"alibaba_list";
	}
	if(typeof catkey != 'undefined'){
		hrefstr += "/"+catkey;
	}
	hrefstr += "?q_from="+encodeURIComponent(q_from)+"&q_to="+encodeURIComponent(q_to);
	window.location.href = hrefstr;
}

function showHistoryKeyword(){
	var lang = $('#src_lang').val();
	if(lang == "t" || lang == "nt"){
		$('#src_history').fadeIn(300);
	}
}

function hideHistoryKeyword(){
	$('#src_history').fadeOut(300);
}

function startItemSearch(code){
	if(13 === code){
		itemSearch();
	}
}

function setCategorylist(){
	var windowwidth = $(window).width();
	if(windowwidth <= 774) {
		var w = $('.category_wrap').width();
		var each = 150;
		var d = parseInt(w / each);
		var neww = d * each;
		$('.category_inner').css('width', neww + 'px');
	}else{
		$('.category_inner').css('width', '100%');
	}
}

function titleTranslateAzure(accesstoken) {
	var title = $('#item_title').val();
	var fname = $.md5(title) + '.cache';

	return $.ajax({
		type: "GET",
		dataType: 'json',
		url: rooturl + 'translatecache_chk',
		data: {file: fname, type: 'title'},
		success: function (data) {
			if (data.status != 'nodata') {
				$('#item_title_ja').html(data.response);
			} else {
				var p = {};
				p.appid = 'Bearer ' + accesstoken;
				p.from = 'zh-CHS';
				p.to = 'ja';
				p.text = title;
				return $.ajax({
					url: 'http://api.microsofttranslator.com/V2/Ajax.svc/Translate',
					data: p,
					dataType: 'jsonp',
					jsonp: 'oncomplete',
					success: function (data) {
						$('#item_title_ja').html(data);
						$.post(rooturl + 'translatecache_save.php', {fname: fname, ftype: 'title', fcont: data});
					}
				});
			}
		}
	})
}

function skuTranslateAzure(accesstoken) {
	var skustr = "[";
	$('.item_sku_each div.original').each(function (i) {
		if (i != 0) {
			skustr += ",";
		}
		skustr += "\"" + $(this).html() + "\"";
	});
	skustr += "]";
	var fname = $.md5(skustr) + '.cache';
	return $.ajax({
		type: "GET",
		dataType: 'json',
		url: rooturl + 'translatecache_chk',
		data: {file: fname, type: 'sku'},
		success: function (data) {
			if (data.status != 'nodata') {
				var res = data.response;
				var skueach = res.split('|');
				$('.item_sku_each div.original').each(function (i) {
					$(this).after('<div class="translate">' + skueach[i] + '</div>');
				});
			} else {
				var p = {};
				p.appid = 'Bearer ' + accesstoken;
				p.from = 'zh-CHS';
				p.to = 'ja';
				p.texts = skustr;
				return $.ajax({
					url: 'http://api.microsofttranslator.com/V2/Ajax.svc/TranslateArray',
					data: p,
					dataType: 'jsonp',
					jsonp: 'oncomplete',
					success: function (data) {
						var rststr = "";
						$('.item_sku_each div.original').each(function (i) {
							if (i != 0) {
								rststr += "|";
							}
							rststr += data[i].TranslatedText;
							$(this).after('<div class="translate">' + data[i].TranslatedText + '</div>');
						});
						$.post(rooturl + 'translatecache_save.php', {fname: fname, ftype: 'sku', fcont: rststr});
					}
				});
			}
		}
	})
}

/*
function titleTranslateMicrosoft(accesstoken) {
	var title = $('#item_title').val();
	var fname = $.md5(title)+'.cache';

	$.ajax({
		type: "GET",
		dataType: 'json',
		url: rooturl+'translatecache_chk',
		data: {file:fname,type:'title'},
		success: function(data){
			if(data.status != 'nodata'){
				$('#item_title_ja').html(data.response);
			}else{
				var p = {};
				p.appid = 'Bearer '+accesstoken;
				p.from = 'zh-CHS';
				p.to = 'ja';
				p.text = title;
				trantitle = $.ajax({
					url: 'http://api.microsofttranslator.com/V2/Ajax.svc/Translate',
					data: p,
					dataType: 'jsonp',
					jsonp: 'oncomplete',
					success: function(data){
						$('#item_title_ja').html(data);
						$.post(rooturl+'translatecache_save.php', { fname: fname, ftype: 'title', fcont: data });
					}
				});
			}
		}
	})
}

function skuTranslateMicrosoft(accesstoken){
	var skustr = "[";
	$('.item_sku_each div.original').each(function(i){
		if(i != 0){skustr += ",";}
		skustr += "\""+$(this).html()+"\"";
	});
	skustr += "]";
	var fname = $.md5(skustr)+'.cache';
	$.ajax({
		type: "GET",
		dataType: 'json',
		url: rooturl+'translatecache_chk',
		data: {file:fname,type:'sku'},
		success: function(data){
			if(data.status != 'nodata'){
				var res =  data.response;
				var skueach = res.split('|');
				$('.item_sku_each div.original').each(function(i){
					$(this).after('<div class="translate">'+skueach[i]+'</div>');
				});
			}else{
				var p = {};
				p.appid = 'Bearer '+accesstoken;
				p.from = 'zh-CHS';
				p.to = 'ja';
				p.texts = skustr;
				transku = $.ajax({
					url: 'http://api.microsofttranslator.com/V2/Ajax.svc/TranslateArray',
					data: p,
					dataType: 'jsonp',
					jsonp: 'oncomplete',
					success: function(data){
						var rststr = "";
						$('.item_sku_each div.original').each(function(i){
							if(i != 0){rststr += "|";}
							rststr += data[i].TranslatedText;
							$(this).after('<div class="translate">'+data[i].TranslatedText+'</div>');
						});
						$.post(rooturl+'translatecache_save.php', { fname: fname, ftype: 'sku', fcont: rststr });
					}
				});
			}
		}
	})
}
*/
function checkDate(strDate){
	if(!strDate.match(/^\d{4}\/\d{1,2}\/\d{1,2}$/)){
		return false;
	}

	var dateArr = strDate.split("/");
	if(dateArr.length < 3){
		return false;
	}

	var year = Number(dateArr[0]);
	var month = Number(dateArr[1] - 1);
	var day = Number(dateArr[2]);

	if(year >= 0 && month >= 0 && month <= 11 && day >= 1 && day <= 31){
		var date = new Date(year, month, day);
		if(isNaN(date)){
			return false;
		}else if(date.getFullYear() == year && date.getMonth() == month && date.getDate() == day){
			return true;
		}
	}
	return false;
}

/*button back to top */

jQuery(document).ready(function($){

    if($(".btn-top").length > 0){
        $(window).scroll(function () {
            var e = $(window).scrollTop();
            if (e > 100) {
                $(".btn-top").fadeIn()
            } else {
                $(".btn-top").fadeOut()
            }
        });
        $(".btn-top").click(function () {
            $('body,html').animate({
                scrollTop: 0
            })
        })
    }

	$("#btn-top").click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 300)
	})






});
