<?= $this->extend('/Layout/main') ?>


<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="alert alert-success col-xs-7" style="float: none; margin: 0 auto 0;">
                        <p style="font-size: 16px; text-align: center;">
                            以下の明細は、<?php echo date('Y/m/d H:i', $order->created->getTimestamp()) ?>まで受注されました。
                        </p>
                    </div>
                </div>


                <div class="box-body table-responsive  with-border" id="main_tbl_wrap">
                    <table id="main_tbl" class="table table-hover dataTable no-footer" role="grid" aria-describedby="main_tbl_info">
                        <thead>
                            <tr role="row">
                                <th aria-controls="main_tbl" rowspan="1" colspan="1" aria-sort="descending" aria-label="項番: この列の昇順に">
                                    画像
                                </th>
                                <th aria-controls="main_tbl" rowspan="1" colspan="1" aria-label="顧客名: この列の昇順に">
                                    商品名
                                </th>
                                <th aria-controls="main_tbl" rowspan="1" colspan="1" aria-label="受注日: この列の昇順に">
                                    オプション
                                </th>
                                <th rowspan="1" colspan="1" aria-label="受注合計">
                                    単価
                                </th>
                                <th aria-controls="main_tbl" rowspan="1" colspan="1" aria-label="明細: この列の昇順に">
                                    数量
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($orderProducts as $orderProduct) : ?>
                                <tr role="row" class="odd">
                                    <td><img src="<?= $orderProduct->product_photo1 ?>" style="width:100px;"></td>
                                    <td><?= $orderProduct->product_title ?></td>
                                    <td><?= $orderProduct->color ?>/<?= $orderProduct->size->sizeName ?></td>
                                    <td><?= $orderProduct->price ?></td>
                                    <td><?= $orderProduct->amount ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>
