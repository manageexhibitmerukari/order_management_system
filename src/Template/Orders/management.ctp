<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/order_management.js?updated=1504149147', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        受注一覧
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-list"></i> <b>受注一覧</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">受注一覧</h3>
                    <input type="hidden" id="status" value="<?=$status?>">
                </div>

                <div class="row">
                    <ul class="nav nav-tabs" style="padding-bottom: 10px;">
<!--                        --><?php //if ($status == 'wait_payment') : ?>
<!--                            <li class="active"><a href="#" style="background-color: #3D8DBC; color: white">処理中</a></li>-->
<!--                        --><?php //else : ?>
<!--                            <li><a href="/orders/management?status=wait_payment">処理中</a></li>-->
<!--                        --><?php //endif; ?>

                        <!--<?php if ($status == 'reject') : ?>-->
                            <!--<li class="active"><a href="#" style="background-color: #3D8DBC; color: white">キャンセル</a></li>-->
                        <!--<?php else : ?>-->
                            <!--<li><a href="/orders/management?status=reject">キャンセル</a></li>-->
                        <!--<?php endif; ?>-->

<!--                        --><?php //if ($status == 'done') : ?>
<!--                            <li class="active"><a href="#" style="background-color: #3D8DBC; color: white">完了済</a></li>-->
<!--                        --><?php //else : ?>
<!--                            <li><a href="/orders/management?status=done">完了済</a></li>-->
<!--                        --><?php //endif; ?>
                        <li class="pull-right">
                            <span style="padding-right: 10px;">期限日数: <input id="numberOfDays" type="number" min="1" max="30" step="1" value="<?= $orderProcessDayLimit ?>" style="width: 60px;"></span>
                            <button id="saveNumberOfDays" type="button" class="btn btn-primary btn-sm" style="margin-right: 25px;">保存</button>
                        </li>
                    </ul>

                </div>

                <div class="box-body table-responsive  with-border" id="main_tbl_wrap">
                </div>

                <?php if ($status == 'wait_payment') : ?>
                    <button onclick="createTransportOrder()" class="btn btn-primary" style="margin: 25px;">発送依頼追加</button>
                <?php endif; ?>


                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>
