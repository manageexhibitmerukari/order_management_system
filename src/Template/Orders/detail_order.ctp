<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/detail_order.js?update=1504149147', ['block' => 'script']) ?>

<?php //dd($orderProducts)?>
<!-- Main content -->
<section class="content">
    <input type="hidden" value="<?php echo $order->id?>" id="order-id">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="alert alert-success col-xs-7" style="float: none; margin: 0 auto 0;">
                        <p style="font-size: 16px; text-align: center;">
                            以下の明細は、<?php echo date('Y/m/d H:i', $order->created->getTimestamp()) ?>まで受注されました。
                        </p>
                    </div>
                </div>


                <div class="box-body table-responsive  with-border" id="main_tbl_wrap">

                </div>

                <button class="btn btn-danger remove-checked" disabled style="margin: 25px;">キャンセル</button>

                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>
