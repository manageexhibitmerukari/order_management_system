<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/transport_order_management.js?updated=1501733352', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        中国調達管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-list"></i> <b>中国調達管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">中国調達管理</h3>
                </div>

                <div class="box-body table-responsive  with-border" id="main_tbl_wrap">
                </div>

                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="order_detail_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(null, ['url' => 'editTransportOrder', 'id' =>'order_detail_form', 'class' => 'form-horizontal' ]) ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i>  注文変更</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">発送ID </label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control input-sm" name="ship_id" id="ship_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">発送サービ </label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control input-sm" name="ship_service" id="ship_service" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-4 control-label">注文の状態 </label>
                        <div class="col-xs-7">
                            <select name="status" id="status">
                                <option value="new">依頼済</option>
                                <option value="booked_on_alibaba">注文済</option>
                                <option value="in_warehouse_china">中国入庫済</option>
                                <option value="arrived_in_japan">日本へ発送済</option>
                                <option value="in_warehouse_japan">日本入庫済</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer clearfix">
                        <button type="submit"  id="change_record" name="change_record" data-style="zoom-in" class="btn btn-success ladda-button pull-center"><i class="fa fa-save"></i> セーブ</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> キャンセル</button>
                        <input type="hidden" id="order_id" value="0" name="order_id" />
                    </div>
                </div>
            <?= $this->Form->end() ?>

            <div class="overlay-wrapper">
                <div id="loading_overlay1" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>
