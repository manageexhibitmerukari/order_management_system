<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/list_order_product.js', ['block' => 'script']) ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        発注管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-list"></i> <b>発注管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">発注管理</h3>
                    <input type="hidden" id="status" value="<?=$status?>">
                </div>

                <div class="col-lg-12">
                    <ul class="nav nav-tabs">
                        <?php if ($status == 'wait_payment') : ?>
                            <li class="active"><a href="#" style="background-color: #3D8DBC; color: white">処理中</a></li>
                        <?php else : ?>
                            <li><a href="/orders/list-order-product?status=wait_payment">処理中</a></li>
                        <?php endif; ?>

                        <?php if ($status == 'done') : ?>
                            <li class="active"><a href="#" style="background-color: #3D8DBC; color: white">完了済</a></li>
                        <?php else : ?>
                            <li><a href="/orders/list-order-product?status=done">完了済</a></li>
                        <?php endif; ?>

                        <li class="pull-right">
                            <button id="exportCsv" type="button" class="btn btn-primary btn-sm">CSVファイル出力</button>
                        </li>
                    </ul>

                </div>

                <div class="box-body">
                    <br><br><br>
                    <div class="col-lg-12">
                        <table class="table table-responsive table-hover with-border" id="mainTable"></table>
                    </div>
                </div>

                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>
