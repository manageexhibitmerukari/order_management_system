<header class="sp-header visible-sp">

    <div class="sp-header-inner clearfix">
        <h1>
            <a href="https://www.mercari.com/jp/">
                <img src="/img/search/logo.svg" alt="mercari" width="148" height="40">
            </a>
        </h1>

        <div class="sp-header-user-nav clearfix">
            <a href="https://www.mercari.com/jp/mypage/" class="sp-header-user-icon">
                <figure>
                    <div>
                        <img src="/img/search/member_photo_noimage_thumb.png" alt="" width="32">
                    </div>
                    <figcaption>
                        58
                    </figcaption>
                </figure>
            </a>

            <form action="search" class="sp-header-form">
                <input type="search" name="keyword" value="" placeholder="キーワードから探す"
                       class="sp-header-search input-default">
                <i class="icon-search"></i>
            </form>

        </div>
    </div>


    <nav class="sp-header-nav">
        <ul>
            <li>
                <a href="https://www.mercari.com/jp/category/1/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_レディース">レディース</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/brand/658/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_シャネル">シャネル</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/category/2/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_メンズ">メンズ</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/brand/857/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_ナイキ">ナイキ</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/category/3/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_ベビー・キッズ">ベビー・キッズ</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/brand/1326/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_ルイ ヴィトン">ルイ ヴィトン</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/category/6/" class="" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_コスメ・香水・美容">コスメ・香水・美容</a>
            </li>
            <li>
                <a href="https://www.mercari.com/jp/brand/" class="active-category" data-ga="element"
                   data-ga-category="NAVIGATION" data-ga-label="sp_header_nav_ブランド一覧">ブランド一覧</a>
            </li>
        </ul>
    </nav>

</header>

<header class="pc-header visible-pc">
    <div class="pc-header-inner">
        <div class="clearfix">
            <h1 class="l-left">
                <a href="https://www.mercari.com/jp/">
                    <img src="/img/search/logo.svg" alt="mercari" width="134" height="36">
                </a>
            </h1>

            <form action="search" class="pc-header-form l-right" method="GET">
                <input type="search" name="keyword" value="" placeholder="キーワードから探す" class="input-default">
                <i class="icon-search"></i>
            </form>
        </div>

        <div class="pc-header-nav-box clearfix">

            <nav class="l-left">
                <ul class="pc-header-nav">
                    <li>
                        <h2>
                            <a href="javascript:void(0);" class="pc-header-nav-root">カテゴリー</a>
                        </h2>
                        <ul class="pc-header-nav-parent-wrap" data-mega="1">
                            <?php foreach ($parent_category as $row) {?>
                            <li class="pc-header-nav-parent">
                                <a href="/user-products/search?category=<?php echo $row['id']; ?>"><?php echo $row['categoryName']; ?></a>
                                <?php  if(isset($row['child']))  {?>
                                <ul class="pc-header-nav-child-wrap" data-mega="2">
                                    <?php foreach ($row['child'] as $value) { ?>
                                    <li class="pc-header-nav-child">
                                        <a href="/user-products/search?category=<?php echo $row['id']; ?>"><?php echo $value['categoryName']?></a>
                                        <?php if(isset($value['child'])) { ?>
                                        <ul class="pc-header-nav-grand-child-wrap" data-mega="3">
                                            <?php foreach ($value['child'] as $val) { ?>
                                            <li class="pc-header-nav-grand-child">
                                                <a href="/user-products/search?category=<?php echo $row['id']; ?>"><?php echo $val['categoryName'];?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <?php } ?>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <?php  } ?>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </nav>


            <div class="l-right">

                <ul class="pc-header-user-nav">
                    <li>
                        <a href="https://www.mercari.com/jp/mypage/" class="sp-header-user-icon pc-header-nav-root">
                            <figure>
                                <div>
                                    <img src="/img/search/member_photo_noimage_thumb.png" alt=""
                                         width="32">
                                </div>
                            </figure>
                            <div>マイページ</div>
                        </a>

                        <div class="pc-header-user-box pc-header-mypage-box">
                            <div class="pc-header-mypage-state text-center">
                                <figure>
                                    <div>
                                        <img src="/img/search/member_photo_noimage_thumb.png"
                                             alt="" width="60">
                                    </div>
                                    <figcaption>えりりん❤︎</figcaption>
                                </figure>
                                <ul class="pc-header-mypage-review-listing">
                                    <li><a href="https://www.mercari.com/jp/mypage/review/history/">評価: 4</a></li>
                                    <li><a href="https://www.mercari.com/jp/mypage/listings/listing/">出品数: 4</a>
                                    </li>
                                </ul>
                                <ul class="pc-header-mypage-sales-point">
                                    <li>
                                        <a href="https://www.mercari.com/jp/mypage/sales/" class="clearfix">
                                            <div class="l-left">売上金</div>
                                            <div class="l-right">¥ 0<i class="icon-arrow-right"></i></div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.mercari.com/jp/mypage/point/" class="clearfix">
                                            <div class="l-left">ポイント</div>
                                            <div class="l-right">P 0<i class="icon-arrow-right"></i></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <ul class="mypage-nav-list">
                                <li>
                                    <a href="https://www.mercari.com/jp/mypage/like/history/"
                                       class="mypage-nav-list-item">
                                        いいね！一覧
                                        <i class="icon-arrow-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</header>

<a href="javascript:void(0);" class="campaign-banner campaign-banner-lottery">
    <img src="/img/search/banner.jpg" alt="3月ドコモキャンペーン">
</a>


<main class="search-container l-container clearfix">
    <div class="l-content">
        <div class="hidden-large search-nav clearfix">
            <div>
                <i class="icon-sort"></i>
                <div class="search-nav-sort">並び替え</div>
            </div>
            <div data-dropdown="nav" data-dropdown-nav-id="extend">
                <i class="icon-search-detail"></i>
                <div>詳細検索</div>
            </div>
        </div>

        <section>
            <section class="items-box-container clearfix">
                <h2 class="search-result-head">
                    検索結果 <?php echo (isset($products)) ? count($products) : 0;?>件
                </h2>
                <?php
                    if (isset($products) && count($products)) {?>
                        <?php foreach ($products as $row) {?>
                        <section class="items-box">
                            <a href="/user-products/detail-product?id=<?php echo $row['id'];?>">
                                <figure class="items-box-photo">
                                    <img data-src="https://static-mercari-jp-web-imgtr.akamaized.net/thumb/photos/m38907597051_1.jpg?1490188274"
                                         class=" lazyloaded" alt="<?php echo $row['product_title']; ?>"
                                         src="<?php echo $row['photo']; ?>">
                                </figure>
                                <div class="items-box-body">
                                    <h3 class="items-box-name font-2"><?php echo $row['product_title']; ?></h3>
                                    <div class="items-box-num clearfix">
                                        <div class="items-box-price font-5">¥ <?php echo $row['price']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </section>
                        <?php } ?>
                    <div class="paginator">
                        <?php
                                echo $this->Paginator->numbers();
                        ?>
                    </div>
                <?php  } else { ?>
                    <p class="search-result-description">
                        該当する商品が見つかりません。商品は毎日増えていますので、これからの出品に期待してください。
                    </p>
                <?php } ?>
                <h3 class="items-box-head">
                    新着商品
                </h3>

                <div class="items-box-content clearfix">
                    <?php foreach ($product_new as $row) {?>
                        <section class="items-box">
                            <a href="/user-products/detail-product?id=<?php echo $row['id'];?>">
                                <figure class="items-box-photo">
                                    <img data-src="https://static-mercari-jp-web-imgtr.akamaized.net/thumb/photos/m38907597051_1.jpg?1490188274"
                                         class=" lazyloaded" alt="<?php echo $row['product_title']; ?>"
                                         src="<?php echo $row['photo']; ?>">
                                </figure>
                                <div class="items-box-body">
                                    <h3 class="items-box-name font-2"><?php echo $row['product_title']; ?></h3>
                                    <div class="items-box-num clearfix">
                                        <div class="items-box-price font-5">¥ <?php echo $row['price']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </section>
                    <?php } ?>
                    <div style="clear:both;"></div>

                </div>
            </section>
        </section>

    </div>

    <div class="l-side">
        <form action="search" novalidate="novalidate"
              class="search-extend-container search-extend-form" data-ga="element" data-ga-category="SEARCH"
              data-ga-label="extend_search">
            <div class="search-dropdown-content search-extend" data-dropdown="content"
                 data-dropdown-content-id="extend">

                <h3 class="visible-large">詳細検索</h3>

                <div class="form-group">
                    <label>
                        <i class="icon-plus"></i>
                        <span>キーワードを追加する</span>
                    </label>
                    <input type="text" value="" name="keyword" placeholder="例）値下げ" class="input-default">
                </div>

                <div class="form-group">
                    <label>
                        <i class="icon-list"></i>
                        <span>カテゴリーを選択する</span>
                    </label>
                    <div class="select-wrap" data-search="parent">
                        <select name="category_root" class="select-default">
                            <option value="">すべて</option>
                            <?php foreach ($parent_category as $row ) {?>
                                <option value="<?php echo $row['id']; ?>" data-root-id="<?php echo $row['id']; ?>"><?php echo $row['categoryName'] ;?></option>
                            <?php } ?>
                        </select>
                        <i class="icon-arrow-bottom"></i>
                    </div>
                    <div data-search="child">
                        <?php foreach ($parent_category as $row) { ?>
                            <div class="select-wrap select-category-child" data-root-id="<?php echo $row['id']; ?>">
                                <select name="category_child" class="select-default" disabled="">
                                    <option value="">すべて</option>
                                    <?php foreach ($row['child'] as $value) { ?>
                                    <option value="<?php echo $value['id'];?>" data-child-id="<?php echo $value['id'];?>"><?php echo $value['categoryName']; ?></option>
                                    <?php } ?>
                                </select>
                            <i class="icon-arrow-bottom"></i>
                        </div>
                        <?php } ?>
                    </div>
                    <div data-search="grand-child">
                        <?php foreach ($parent_category as $row) { ?>
                            <?php foreach ($row['child'] as $value) { ?>
                                <div class="select-category-grand-child" data-child-id="<?php echo $value['id'];?>">
                                    <?php foreach ($value['child'] as $val) { ?>
                                        <div class="checkbox-default">
                                            <input type="checkbox" name="category_grand_child[<?php echo $val['id']; ?>]"
                                                   id="category_grand_child[<?php echo $val['id']; ?>]" value="1">
                                            <i class="icon-check"></i>
                                            <label for="category_grand_child[<?php echo $val['id']; ?>]"><?php echo $val['categoryName'];?></label>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label>
                        <i class="icon-size"></i>
                        <span>サイズを指定する</span>
                    </label>
                    <div class="select-wrap" data-search="parent">
                        <select name="size_group" class="select-default">
                            <option value="">すべて</option>
                            <?php foreach($size as $row) {?>
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['sizeName']; ?></option>
                            <?php } ?>
                        </select>
                        <i class="icon-arrow-bottom"></i>
                    </div>
                    <div data-search="child">
                        <div class="select-category-child" data-root-id="1">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[1]" id="size_id[1]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[1]">XS以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[2]" id="size_id[2]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[2]">S</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[3]" id="size_id[3]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[3]">M</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[4]" id="size_id[4]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[4]">L</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[5]" id="size_id[5]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[5]">XL</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[6]" id="size_id[6]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[6]">XL以上</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[7]" id="size_id[7]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[7]">FREE SIZE</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="2">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[8]" id="size_id[8]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[8]">25.5cm以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[9]" id="size_id[9]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[9]">26cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[10]" id="size_id[10]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[10]">26.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[11]" id="size_id[11]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[11]">27cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[12]" id="size_id[12]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[12]">27.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[13]" id="size_id[13]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[13]">28cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[14]" id="size_id[14]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[14]">28.5cm以上</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="3">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[15]" id="size_id[15]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[15]">22.0cm以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[16]" id="size_id[16]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[16]">22.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[17]" id="size_id[17]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[17]">23.0cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[18]" id="size_id[18]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[18]">23.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[19]" id="size_id[19]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[19]">24.0cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[20]" id="size_id[20]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[20]">24.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[21]" id="size_id[21]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[21]">25.0cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[22]" id="size_id[22]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[22]">25.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[23]" id="size_id[23]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[23]">26.0cm以上</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="6">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[34]" id="size_id[34]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[34]">60cm以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[35]" id="size_id[35]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[35]">~70cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[36]" id="size_id[36]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[36]">~80cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[37]" id="size_id[37]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[37]">~90cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[38]" id="size_id[38]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[38]">90cm以上</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="7">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[40]" id="size_id[40]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[40]">100cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[41]" id="size_id[41]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[41]">110cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[42]" id="size_id[42]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[42]">120cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[43]" id="size_id[43]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[43]">130cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[44]" id="size_id[44]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[44]">140cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[45]" id="size_id[45]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[45]">150cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[55]" id="size_id[55]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[55]">160cm</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="8">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[47]" id="size_id[47]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[47]">10.5cm以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[48]" id="size_id[48]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[48]">11cm・11.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[49]" id="size_id[49]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[49]">12cm・12.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[50]" id="size_id[50]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[50]">13cm・13.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[51]" id="size_id[51]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[51]">14cm・14.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[52]" id="size_id[52]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[52]">15cm・15.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[53]" id="size_id[53]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[53]">16cm・16.5cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[54]" id="size_id[54]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[54]">17cm以上</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="9">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[56]" id="size_id[56]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[56]">60cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[57]" id="size_id[57]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[57]">70cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[58]" id="size_id[58]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[58]">80cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[59]" id="size_id[59]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[59]">90cm</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[60]" id="size_id[60]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[60]">95cm</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="14">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[90]" id="size_id[90]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[90]">～20インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[91]" id="size_id[91]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[91]">20～26インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[92]" id="size_id[92]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[92]">26～32インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[93]" id="size_id[93]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[93]">32～37インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[94]" id="size_id[94]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[94]">37～40インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[95]" id="size_id[95]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[95]">40～42インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[96]" id="size_id[96]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[96]">42～46インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[97]" id="size_id[97]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[97]">46～52インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[98]" id="size_id[98]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[98]">52～60インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[99]" id="size_id[99]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[99]">60インチ～</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="17">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[114]" id="size_id[114]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[114]">ニコンFマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[115]" id="size_id[115]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[115]">キヤノンEFマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[116]" id="size_id[116]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[116]">ペンタックスKマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[117]" id="size_id[117]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[117]">ペンタックスQマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[118]" id="size_id[118]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[118]">フォーサーズマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[119]" id="size_id[119]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[119]">マイクロフォーサーズマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[120]" id="size_id[120]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[120]">α Aマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[121]" id="size_id[121]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[121]">α Eマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[122]" id="size_id[122]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[122]">ニコン1マウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[123]" id="size_id[123]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[123]">キヤノンEF-Mマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[124]" id="size_id[124]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[124]">Xマウント</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[125]" id="size_id[125]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[125]">シグマSAマウント</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="12">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[76]" id="size_id[76]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[76]">50cc以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[77]" id="size_id[77]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[77]">51cc-125cc</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[78]" id="size_id[78]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[78]">126cc-250cc</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[79]" id="size_id[79]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[79]">251cc-400cc</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[80]" id="size_id[80]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[80]">401cc-750cc</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[81]" id="size_id[81]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[81]">751cc以上</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="13">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[82]" id="size_id[82]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[82]">XSサイズ以下</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[83]" id="size_id[83]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[83]">Sサイズ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[84]" id="size_id[84]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[84]">Mサイズ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[85]" id="size_id[85]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[85]">Lサイズ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[86]" id="size_id[86]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[86]">XLサイズ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[87]" id="size_id[87]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[87]">XXLサイズ以上</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[88]" id="size_id[88]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[88]">フリーサイズ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[89]" id="size_id[89]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[89]">子ども用</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="11">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[67]" id="size_id[67]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[67]">13インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[68]" id="size_id[68]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[68]">14インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[69]" id="size_id[69]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[69]">15インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[70]" id="size_id[70]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[70]">16インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[71]" id="size_id[71]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[71]">17インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[72]" id="size_id[72]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[72]">18インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[73]" id="size_id[73]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[73]">19インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[74]" id="size_id[74]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[74]">20インチ</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[75]" id="size_id[75]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[75]">21インチ</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="18">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[126]" id="size_id[126]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[126]">140cm～</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[127]" id="size_id[127]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[127]">150cm～</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[128]" id="size_id[128]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[128]">160cm～</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[129]" id="size_id[129]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[129]">170cm～</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[130]" id="size_id[130]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[130]">スキーボード</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[131]" id="size_id[131]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[131]">子ども用</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[132]" id="size_id[132]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[132]">その他</label>
                            </div>

                        </div>
                        <div class="select-category-child" data-root-id="15">
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[100]" id="size_id[100]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[100]">135cm-140cm未満</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[101]" id="size_id[101]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[101]">140cm-145cm未満</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[102]" id="size_id[102]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[102]">145cm-150cm未満</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[103]" id="size_id[103]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[103]">150cm-155cm未満</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[104]" id="size_id[104]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[104]">155cm-160cm未満</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[105]" id="size_id[105]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[105]">160cm-165cm未満</label>
                            </div>
                            <div class="checkbox-default">
                                <input type="checkbox" name="size_id[106]" id="size_id[106]" value="1">
                                <i class="icon-check"></i>
                                <label for="size_id[106]">165cm-170cm未満</label>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="search-extend-btn-empty"></div>
                <div class="search-extend-btn clearfix">
                    <div data-search="reset" class="btn-default btn-gray">クリア</div>
                    <button type="submit" class="btn-default btn-red">完了</button>
                </div>
            </div>
        </form>
        <div class="l-visible-large">
            <a href="https://itunes.apple.com/jp/app/id667861049?l=ja&amp;mt=8" target="_blank" class="side-banner">
                <img src="/img/search/side_banner.jpg" alt="Download Mercari Now">
            </a>
        </div>
    </div>
</main>


<div class="overlay"></div>