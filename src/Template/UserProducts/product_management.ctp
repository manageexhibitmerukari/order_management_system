<?= $this->Html->script('site/product_management.js', ['block' => 'script']) ?>

<div id="main">
    <div class="l-content">
        <section>
            <div class="row" style="margin: 0;">
                <a href="/user-products/manager-order" class="col-sm-3 btn-menu-link">注文履歴</a>
                <a href="#" class="col-sm-3 btn-menu-link-active">商品リスト</a>
                <a href="/users/address-management" class="col-sm-3 btn-menu-link">お届け先リスト</a>
                <a href="/users/update-info" class="col-sm-3 btn-menu-link">会員情報管理</a>
                <a href="/users/change-user-password" class="col-sm-3 btn-menu-link">パスワード変更</a>
            </div>

            <hr>

            <h3 >商品リスト</h3>
            <table class="tbl-order table table-bordered" id="main_table">
            </table>
        </section>
    </div>
</div>