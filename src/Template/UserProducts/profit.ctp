<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/profit.js?updated=1501130856', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        利益管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-money"></i> <b>利益管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container" name="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-money"></i>
                    <h3 class="box-title">利益管理</h3>

                </div>

                <div class="form-inline" style="margin:15px 0 15px 25px">
                    <div class="form-group">
                        <label for="title_search">計算期間:</label>
                        <div class="input-group date" >
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" value="<?php echo (isset($last_one_month)) ? date('Y/m/d', strtotime($last_one_month)) : ''?>" class="form-control" id="datepicker-from">

                        </div>

                    </div>
                    <div class="form-group" style="margin-left: 20px">
                        <label for="status_search">から: </label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" value="<?php echo (isset($current_month)) ? date('Y/m/d', strtotime($current_month)) : ''?>" class="form-control" id="datepicker-to">

                        </div>
                    </div>
                    <div class="form-group" style="margin-left: 20px">
                        <input type="button" name="get-data" id="get-data" class="btn btn-primary" value="表示" >
                    </div>

                    <div class="form-group" style="margin-left: 20px">
                        <input type="button" name="save-data" id="save-data" class="btn btn-primary" value="保存" >
                    </div>


                    <script>
                        var data_product = <?php echo (isset($dataProducts)) ? $dataProducts : '' ?>;
                        //console.log(data_product);
                    </script>
                </div>

                <div class="box-body table-responsive with-border" id="profit-table" name="profit-table" style="padding:0 25px;margin-bottom:25px;">
                    <table class="table table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th>売上金額</th>
                                <th>利益</th>
                                <th>利益率</th>
                                <th>点数</th>
                                <th>仕入れ金額</th>
                                <th>サイト手数料</th>
                                <th>配送料</th>

                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td id="total_price"></td>
                                <td id="total_profit"></td>
                                <td id="percent"></td>
                                <td id="total_product"></td>
                                <td id="total_funds"></td>
                                <td id="total_web"></td>
                                <td id="total_ship"></td>

                            </tr>

                        </tbody>
                    </table>
                </div>



                <div class="box-body table-responsive with-border" id="main_tbl_wrap" name="main_tbl_wrap">
                </div>

                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>