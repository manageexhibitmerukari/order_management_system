<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/product_upload.js', ['block' => 'script']) ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        商品一括登録
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-upload"></i> <b>商品一括登録</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container" name="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="modal-body">
                    <form class="form-horizontal col-xs-11" style="float:none; margin: 0 auto"  >
                        <div class="form-group">
                            <div class="col-xs-5">
                                <a href="/files/download/template.zip" class="btn btn-success" >
                                    テンプレート出力
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-upload"></i>
                    <h3 class="box-title">商品データのアップロード</h3>
                </div>

                <div class="box-body table-responsive  with-border" id="main_wrap" >
                    <div class="alert alert-warning col-xs-7" style="float: none; margin: 0 auto 20px; display: none;">
                        <!-- <h3 style="margin-top: 0px; ">ごめんなさい!</h3>-->

                    </div>

                    <div class="alert alert-info col-xs-7" style="float: none; margin: 0 auto 20px; display: none;">
                        <!-- <h3 style="margin-top: 0px; ">ごめんなさい!</h3>-->
                        <p style="font-size: 16px;">
                            商品が追加成功
                        </p>
                    </div>

                    <form class="form-horizontal col-xs-11" style="float:none; margin: 0 auto" id="product_upload_form" method="post" enctype="multipart/form-data">
                        <div id="product_input_wrapper">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">商品ファイル(zip形式) </label>
                                <div class="col-xs-8 control-label">
                                    <input type="file" class="form-control input-sm" name="product_file" id="product_file"/>
                                </div>
                            </div>

                            <div class="form-group" style="text-align:center;">
                                <button type="button" onclick="javascript:uploadProduct()" id="publish_upload" name="publish_upload" class="btn btn-success" style="">商品アップ</button>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal col-xs-11" style="float:none; margin: 0 auto" id="product_upload_form_sub" method="post" enctype="multipart/form-data">
                        <div id="product_input_wrapper1">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">商品ファイル(zip形式) </label>
                                <div class="col-xs-8 control-label">
                                    <input type="file" class="form-control input-sm" name="product_file_sub" id="product_file_sub"/>
                                </div>
                            </div>

                            <div class="form-group" style="text-align:center;">
                                <button type="button" onclick="javascript:uploadProductSub()" id="publish_upload_sub" name="publish_upload_sub" class="btn btn-success" style="">子商品アップ</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <!--                        <img class="fa" src="./images/lib/ajax-loader1.gif">-->
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>

        </div>
    </div>
</section>
