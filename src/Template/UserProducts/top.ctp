<div id="main">

    <div id="slider-home">
        <?php if (isset($banners) && $banners) : ?>

        <ul class="pgwSlider">

            <?php foreach ( $banners as $banner) : ?>
                <li>
                    <a href="<?php echo $banner['link']?>" title="<?php echo $banner['title'] ?>" target="_blank">
                        <img src="<?php echo $banner['image'] ?>" data-description="<?php echo $banner['description']?>">
                        <span><?php echo $banner['title'] ?></span>
                    </a>
                </li>
            <?php endforeach;?>

        </ul>
        <?php endif ?>


        <div class="clearfix"> </div>

    </div>

    <script>
        $(document).ready(function() {
            $('.pgwSlider').pgwSlider();
        });
    </script>

    <?php if (isset($endBreadcrume) && $endBreadcrume != '') { ?>
        <div id="breadnavi">
            <?php if (isset($breadcrume) && isset($breadcrume[0]) && $breadcrume[0] != '') { ?>
                <?php foreach($breadcrume as $row) { ?>
                    <span itemscope="">
                        <a href="javascript:void(0);" itemprop="url">
                            <span itemprop="title"><?php echo $row ;?></span>
                        </a>
                    </span>  &gt;
                <?php } ?>
            <?php } ?>
            <strong><?php echo $endBreadcrume; ?></strong>
        </div>
    <?php } ?>

    <section>
        <div id="listtop"></div>
        <div id="outerajax">
            <div class="clearfix mb10">
                <div class="list_tcnt bold clr_green"><?php echo $count; ?> 件の商品がみつかりました。</div>
                <!--<div class="list_sort">-->
                    <!--<ul class="sortmenu">-->
                        <!--<li class="shown">-->
                            <!--<a onclick="getPage(0,0,1,0)">お薦め順</a>-->
                        <!--</li>-->
                        <!--<li><a onclick="getPage(0,0,1,1)">人気順</a></li>-->
                    <!--</ul>-->
                <!--</div>-->
            </div>
            <div id="innerajax">
                <div class="row list_product">

                        <?php

                        $i = 0;

                        foreach ($products as $row) {

                            $soid_out = $row['sub_product_amount'] > 0 || (strtotime($row['max_exhibition_time']) > strtotime(date('Y/m/d H:i:s')));
                            $comming_soon = (strtotime($row['max_exhibition_time']) > strtotime(date('Y/m/d H:i:s')));


                            if($i % 4 == 0) echo "</div> <div class='row list_product' >";

                            ?>
                            <div class="col-md-3 product_detail">
                                <a href="/user-products/detail-product?id=<?php echo $row['id'];?>"
                                   rel="nofollow" class="clearfix">
                                    <img class="img-responsive" src="<?php echo $row['photo'].'?'.strtotime(date('c')); ?>" alt="" >
                                    <img src="/img/sold-out.png" class="<?php echo ($soid_out) ?  'hidden' : ''?>" style="position:absolute;height:100px;top:-80px;left:-80px;">
                                    <img src="/img/comming-soon.png"  class="<?php echo ($comming_soon) ? '' : 'hidden'?>" style="opacity:0.7;height: 170px;">

                                </a>

                                <div class="list_product_name">
                                    <?php echo $row['product_title']; ?>
                                </div>
                                <div class="list_category">
                                    <?php echo $row['category']['categoryName']; ?>
                                </div>
                                <div class="list_pricejp">
                                    ¥ <?php echo number_format($row['price']); ?> <span>(税抜)<span>
                                </div>

                            </div>

                            <?php

                            ?>
                        <?php
                            $i++;
                        } ?>


                </div>
            </div>
            <div class="paginator">
                <?php
                    echo $this->Paginator->numbers();
                ?>
            </div>
    </section>
</div>