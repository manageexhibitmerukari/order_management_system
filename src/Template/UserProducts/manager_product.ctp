<div id="main">
    <div class="l-content">
        <h3 class="time-click">以下の明細は、<?php echo date('Y/m/d H:i:s');?>まで受注されました。</h3>
        <section class="item-box-container page-cart">
            <table class="tbl-order-product table table-bordered">

            </table>
        </section>
    </div>
    <div id="login" class="modal fade" role="dialog">
        <div class="login-box-body">
            <div class="login-logo">
                <a href="javascript:void(0)">ログイン</a>
            </div>
            <form method="post" accept-charset="utf-8" action="/users/loginUser"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>            <!-- error message -->
                <div class="form-group has-feedback">
                    <?php if (isset($error)) {?>
                    <div class="alert alert-error">
                        <p style="text-align:center;"><?php echo $error; ?></p>
                    </div>
                    <?php } ?>
                    <!-- END error message -->

                    <div class="form-group has-feedback">
                        <div class="input-group_nouse">
                            <div class="input text"><input type="text" name="username" class="form-control" placeholder="ユーザーIDを入力してください。" id="username"></div>                </div>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="input-group_nouse">
                            <div class="input password"><input type="password" name="password" class="form-control" placeholder="パスワードを入力してください。" id="password"></div>                </div>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="row" style="margin:0px;">
                        <div class="col-xs-5 fix-format" style="width:100%">
                            <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">
                                ログイン&nbsp;<i class="fa fa-sign-in"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript"
        src="/js/site/manager_product.js?updated=1501574595"></script>
