<?= $this->Html->script('site/show_cart_user.js?update=1506652012', ['block' => 'script']) ?>


<div id="main" style="float: none;">
    <div class="l-content">
        <section class="page-cart">

            <div class="row">
                <div class="col-sm-9">
                    <div class="pc-main">
                        <div class="pc-main-tile">
                            <p>仕入れカート</p>
                        </div>
                    </div>
                    <div class="pc-product" id="cart_container">
                        <div id="submit_cart_success_message" style="text-align: center; font-weight: bold; font-size: 20px; display: none;">
                            <span>ご注文を完了しました。</span><br>
                            <span>ありがとうございます。</span>
                        </div>

                        <?php foreach($products as $key => $row): ?>
                            <div class="pc-product-detail clearfix">
                                <div class="col-xs-3">
                                    <img src="<?php echo $row['product_photo1'].'?'.time(); ?>" alt="<?php echo $row['name']; ?>" name="image" class="img-responsive">
                                </div>

                                <div class="col-xs-4 info">
                                    <a href="/user-products/detail-product?id=<?= $row['main_product_id'] ?>" target="_blank"><?php echo $row['name']; ?></a>
                                    <p>色: <?php echo $row['color']; ?></p>
                                    <p>サイズ: <?php echo $row['sizeName']; ?></p>
                                </div>

                                <div class="col-xs-3 detail">
                                    <div class="price-container">
                                        <?php echo number_format($row['price']); ?>
                                        <input type="hidden" class="price" value="<?= $row['price'] ?>">
                                    </div>
                                    <div class="multipli">x</div>
                                    <div class="quantity-container">
                                        <input type="number" onkeyup="changeQuantity(this,<?= $row['cart_id'] ?>)" value="<?php echo $row['quantity']; ?>" class="form-control quantity" min="1" max="<?= $row['amount'] ?>">


                                        <input type="hidden" id="quantity_max_<?= $row['cart_id'] ?>" value="<?= $row['amount'] ?>" >
                                        <input type="hidden" class="sku" value="<?php echo $row['sku'] ?>">
                                    </div>
                                    <span class="help-block" id="error_<?= $row['cart_id'] ?>" style="    text-align: center; color:red"></span>
                                </div>

                                <div class="col-xs-2 action">
                                    <button cart-id="<?= $row['cart_id'] ?>" class="btn btn-default update-single-cart disabled" id="update-single-cart_<?= $row['cart_id'] ?>"> 更新</button>
                                    <button cart-id="<?= $row['cart_id'] ?>" class="btn btn-default delete-cart" ><span class="glyphicon glyphicon-trash"></span> 削除</button>


                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="pc-side">
                        <div class="pc-side-total">
                            <p class="clearfix">
                                <span class="pull-left"> 小計</span>
                                <span class="pull-right">&#165;<span id="sum_price"></span></span>
                            </p>
                            <p class="clearfix">
                                <span class="pull-left"> 消費税</span>
                                <span class="pull-right">&#165;<span id="tax"></span></span>
                            </p>

                            <p class="clearfix">
                                <span class="pull-left"> 送料 </span>
                                <span class="pull-right">&#165;<span id="ship_price"></span></span>
                            </p>

                            <hr>
                            <p class="clearfix">
                                <span class="pull-left"> 合計</span>
                                <span class="pull-right" >&#165;<span id="total_price"></span>(税込)</span>
                            </p>
                            <div class="select-wrap payment">
                                <select class="payment_method">
                                    <option value="">支払い方法選択ください </option>
                                    <option value="transfer_bank">銀行振込</option>
                                    <option value="card_bank">カード支払い</option>
                                </select>

                                <i class="icon-arrow-bottom"></i>
                            </div>
                            <span class="error_payment" style="display: none; color: red;font-size: 11px;">
                                    支払い方法を選択してください
                                </span>

                            <button class="btn btn-menu-link <?php echo (count($products) <=0) ? 'disabled' : ''   ?>" id="buy"> 注文する</button>




                        </div>
                        <div class="pc-side-address" style="word-wrap: break-word;">
                            <p>お届け先住所 <a href="/users/address-management"> 変更 </a> </p>
                            <p><?= $address->name ?></p>
                            <?= nl2br($address->full_address) ?>
                        </div>
                        <input type="hidden" id="zip-code" value="<?php echo $address->zip_code?>">
                        <input type="hidden" id="fee_up" value="<?php echo $fee['fee_up']?>">
                        <input type="hidden" id="price_order" value="<?php echo $fee['price_order']?>">
                        <input type="hidden" id="fee_default" value="<?php echo $fee['fee_default']?>">
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>