<?= $this->extend('/Layout/main') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        商品詳細
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-camera"></i> <b>商品詳細</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container" name="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-camera"></i>
                    <h3 class="box-title">商品詳細</h3>
                </div>
                <div class="box-body table-responsive  with-border" id="main_wrap" >
                    <form class="form-horizontal col-xs-11" style="float:none; margin: 0 auto" id="product_publish_form" method="post" enctype="multipart/form-data">
                        <div id="product_input_wrapper">
                            <div class="form-group">
                                <div class="form-group photo_1">
                                    <label class="col-xs-3 control-label">商品画像 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-3">
                                        <img src="<?php echo $product['photo'].'?'.strtotime(date('c'));?>" name="photo_1_preview" id="photo_1_preview" style="width:100%; border:1px solid #d2d6de"/>
                                        <img src="/img/no_product.png" style="display: none" id="default_photo" />
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="file" accept="image/jpeg|image/png" class="form-control input-sm product_photo" name="photo_1" id="photo_1"/>
                                    </div>
                                </div>

                                <div class="form-group photo_2">
                                    <label class="col-xs-3 control-label"></label>
                                    <div class="col-xs-3">
                                        <img src="<?php echo $product['photo_2'].'?'.strtotime(date('c'));?>" name="photo_1_preview" id="photo_2_preview" style="width:100%; border:1px solid #d2d6de"/>
                                        <img src="/img/no_product.png" style="display: none" id="default_photo" />
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="file" accept="image/jpeg|image/png" class="form-control input-sm product_photo" name="photo_2" id="photo_2"/>
                                    </div>
                                </div>
                                <div class="form-group photo_3">
                                    <label class="col-xs-3 control-label"></label>
                                    <div class="col-xs-3">
                                        <img src="<?php echo $product['photo_3'].'?'.strtotime(date('c'));?>" name="photo_1_preview" id="photo_3_preview" style="width:100%; border:1px solid #d2d6de"/>
                                        <img src="/img/no_product.png" style="display: none" id="default_photo" />
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="file" accept="image/jpeg|image/png" class="form-control input-sm product_photo" name="photo_3" id="photo_3"/>
                                    </div>
                                </div>
                                <div class="form-group photo_4">
                                    <label class="col-xs-3 control-label"></label>
                                    <div class="col-xs-3">
                                        <img src="<?php echo $product['photo_4'].'?'.strtotime(date('c'));?>" name="photo_4_preview" id="photo_4_preview" style="width:100%; border:1px solid #d2d6de"/>
                                        <img src="/img/no_product.png" style="display: none" id="default_photo" />
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="file" accept="image/jpeg|image/png" class="form-control input-sm product_photo" name="photo_4" id="photo_4"/>
                                    </div>
                                </div>

                                <div class="form-group" style="display: none">
                                    <label class="col-xs-3 control-label">商品名 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control input-sm" name="id" value="<?php if(isset($product['id'])) echo $product['id']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">商品名 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control input-sm" name="name" value="<?php if(isset($product['product_title'])) echo $product['product_title']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">備考 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <textarea class="form-control" name="description" rows="5"><?php if (isset($product['description'])) echo $product['description']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">価格 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control input-sm" name="price" value="<?php if (isset($product['price'])) echo $product['price']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">カテゴリ : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <select class="form-control" name="category">
                                            <?php foreach ($categories as $row) {
                                                $selected = '';
                                                if ($row['id'] == $product['category_id']) {
                                                    $selected = 'selected';
                                                }
                                                echo "<option  value='".$row["id"]."' $selected>". $row['categoryName'] ."</option>";
                                             } ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Publish Preview Wrapper -->
                        <div id="product_preview_wrapper">
                            <div class="form-group" style="text-align:center;">
                                <button type="button" onclick="javascript:saveProduct()" id="publish_product" name="publish_product" class="btn btn-success" style=""><i class="fa fa-camera"></i> 保存</button>
                                <a href="/user-products/product-list" name="cancel_preview" class="btn btn-primary" style=""><i class="fa fa-times"></i> リセット</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <!--                        <img class="fa" src="./images/lib/ajax-loader1.gif">-->
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>


<?= $this->Html->script('site/edit_product.js', ['block' => 'script']) ?>
