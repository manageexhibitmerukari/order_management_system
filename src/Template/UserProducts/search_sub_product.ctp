<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/search_sub_product.js?updated=1501574595', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        商品状態管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-list"></i> <b>商品状態管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container" name="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">商品リスト</h3>

                </div>

                <?php
                $title_search = $this->request->query('title_search');
                $status_search = $this->request->query('status_search');

                ?>
                <div class="form-inline" style="margin:15px 0 15px 25px">
                    <div class="form-group">
                        <label for="title_search">商品名:</label>
                        <input type="text" name="title_search" class="form-control" id="title_search" value="<?= ($title_search) ? $title_search : '' ?>">
                    </div>
                    <div class="form-group" style="margin-left: 20px">
                        <label for="status_search">状態: </label>
                        <select name="status_search" class="form-control" id="status_search">
                            <option value=""> ----Select----</option>
                            <option value="has_exhibition" <?= ($status_search == 'has_exhibition') ? 'selected' : '' ?>> 予約商品</option>
                            <option value="has_product" <?= ($status_search == 'has_product') ? 'selected' : '' ?>>在庫有り</option>
                            <option value="out_product" <?= ($status_search == 'out_product') ? 'selected' : '' ?>>売り切れ</option>
                            <option value="expired" <?= ($status_search == 'expired') ? 'selected' : '' ?>> 販売限定期間切れ</option>

                        </select>
                    </div>

                    <input type="button" class="btn btn-primary" id="button_search" value="検索" onclick="make_init_data()" style="margin-left: 20px">
                </div>




                <div class="box-body table-responsive  with-border" id="main_tbl_wrap" name="main_tbl_wrap">
                </div>
                <button class="btn btn-danger remove-checked" style="margin: 25px;">削除</button>
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>