
<div id="main">
        <section class="item-box-container">
            <h2 class="item-name" style="text-align: left;"><?php echo $product['product_title']; ?></h2>

            <div class="item-main-content clearfix">
                <?php
                // Count sub product
                $count = 0;

                if (isset($product['sub_products']) && $product['sub_products']) {
                    $max_exhibition_time = 0;
                    foreach ($product['sub_products'] as $sub_product) {

                            $time = strtotime($sub_product->selling_check_time) + $sub_product->selling_term * 3600;
                            //dd($time > strtotime(date('Y-m-d')) );
                            if(
                                //check thời gian bán sản phẩm trong bao nhiêu tiếng
                                ( ($time > strtotime(date('Y-m-d H:i:s')) ) || $sub_product->selling_check_time == null )
                                &&
                                // check có set thời gian bán hay không
                                ( ($sub_product->selling_term > 0) || $sub_product->selling_term == null )
                            ) {
                                $count += $sub_product->amount;
                            }
                            if($max_exhibition_time < strtotime($sub_product->exhibition_time)) {                                               $max_exhibition_time = strtotime($sub_product->exhibition_time);
                            }

                    }

                    $comming_soon = $max_exhibition_time > strtotime(date('Y/m/d H:i:s'));

                    //dd($comming_soon);
                }

                ?>
                <div class="item-photo">
                    <img src="/img/sold-out.png" class="<?php echo ($count > 0 || $comming_soon) ?  'hidden' : ''?>" style="position:absolute; height: 140px; z-index: 1000" id="sold-out">

                    <img src="/img/comming-soon.png"  class="<?php echo ($comming_soon) ? '' : 'hidden'?>" style="margin: auto; position: absolute; top: -95px; left: 0; right: 0; bottom: 0; max-height: 300px; z-index: 1000; opacity: 0.7;" id="comming-soon">
                    <div class="owl-carousel owl-loaded owl-drag">

                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="left: 0px; width: 1200px;">
                                <div class="owl-item active">
                                    <img
                                        alt="【春　注目】　フラワーブラウス　ボトルネック　M" class="owl-lazy"
                                        src="<?php if (isset($product['photo'])) echo $product['photo'].'?'.strtotime(date('c')) ; ?>"
                                        name="img-fisrt"
                                        >

                                </div>

                            </div>
                        </div>
                        <div class="owl-nav disabled">
                            <div class="owl-prev">prev</div>
                            <div class="owl-next">next</div>
                        </div>

                        <div class="owl-dots">
                            <?php if (isset($product['photo'])) : ?>
                                <div class="owl-dot active">
                                    <span></span>
                                    <div class="owl-dot-inner">
                                        <img src="<?php echo $product['photo'].'?'.strtotime(date('c')) ; ?>">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (isset($product['photo_2'])) : ?>
                                <div class="owl-dot">
                                    <span></span>
                                    <div class="owl-dot-inner">
                                        <img src="<?php echo $product['photo_2'].'?'.strtotime(date('c')) ; ?>" style="height: 75px;">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (isset($product['photo_3'])) : ?>
                                <div class="owl-dot">
                                    <span></span>
                                    <div class="owl-dot-inner">
                                        <img src="<?php echo $product['photo_3'].'?'.strtotime(date('c')) ; ?>" style="height: 75px;">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (isset($product['photo_4'])) : ?>
                                <div class="owl-dot">
                                    <span></span>
                                    <div class="owl-dot-inner">
                                        <img src="<?php echo $product['photo_4'].'?'.strtotime(date('c')) ; ?>" style="height: 75px;">
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>


                    </div>
                </div>

                <table class="item-detail-table">
                    <tbody>
                    <tr>
                        <th>カテゴリー</th>
                        <td>
                                <div><?php echo $product['categoryName']; ?></div>
                            <input type="text" value="<?php echo $product['sku']; ?>" hidden id="sku"/>
                        </td>
                    </tr>
                    <tr>
                        <th>商品のサイズ</th>
                        <td>
                            <div class="select-wrap">
                                <select name="size" class="select-default" id="size">
                                    <option value>すべて</option>
                                    <?php foreach ($product['sizeName'] as $key=>$row) {
                                                echo '<option value="'. $key .'">'. $row .'</option>';
                                    } ?>
                                </select>
                                <i class="icon-arrow-bottom"></i>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>色</th>
                        <td>
                            <div class="select-wrap">
                                <select name="color" class="select-default" id="color">
                                    <option value>すべて</option>
                                </select>
                                <i class="icon-arrow-bottom"></i>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>購入数量</th>
                        <td>
                            <div class="select-wrap">
                                <input type="number" value="1" name="amount" class="form-control" id="amount" min="1" onkeyup="changeQuantity(this)">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>在庫数量</th>
                        <td>
                            <div>
                                <span id="remain_amount"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>期間限定</th>
                        <td>
                            <div>
                                <span id="due_time"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="item-price-box text-center">
                <span class="item-price bold">¥
                    <span class="price"><?php echo $product['price']; ?></span>
                </span>
                <span style="font-size: 20px">（税抜）</span>
            </div>

            <input type="hidden" id="amount_default">
            <?php if(!$comming_soon):?>
            <a href="javascript:void(0);" class="item-buy-btn f18-24"
               data-client="hidden" id-item="<?php echo $product['id']; ?>" >カートに入れる</a>
            <?php endif;?>

            <div class="item-description f14">
                <?php echo nl2br($product['description']); ?>
            </div>

        </section>
    </div>
<!--</main>-->

<div class="overlay"></div>