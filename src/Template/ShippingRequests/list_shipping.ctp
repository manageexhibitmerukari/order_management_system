<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/shipping_request.js?updated=1504149147', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        配送待ち中の商品管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-list"></i> <b>発送管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container" name="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">商品発送管理</h3>
<!--                    <button class="btn btn-primary export-csv pull-right" style="margin-right: 15px;"> CSVエクスポート</button>-->
                    <div style="display: inline-block; margin-left: 10px;" class="pull-right">
                        <p id="account_status">

                        </p>
                    </div>
                </div>


                <div class="box-body table-responsive  with-border" id="main_tbl_wrap" name="main_tbl_wrap">
                </div>
                <!--<button class="btn btn-danger remove-checked" style="margin: 25px;">削除</button>-->
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>