<!DOCTYPE html>
<!-- saved from url=(0043)https://www.mercari.com/jp/search/?keyword= -->
<html lang="ja-JP">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>メルカリ スマホでかんたん フリマアプリ</title>
    <meta name="description"
          content="CMで話題！フリマアプリ「メルカリ」は、スマホから誰でも簡単に売り買いが楽しめるフリマアプリです。購入時はクレジットカード・キャリア決済・コンビニ・銀行ATMで支払いでき、品物が届いてから出品者に入金される独自システムで安心です。">

    <link href="/css/app.jp.css" rel="stylesheet">



</head>

<body class="">

<div class="default-container ">
    <?= $this->fetch('content') ?>
</div>

<script src="/js/frontend/app.js?1501105979"></script>

</body>
</html>