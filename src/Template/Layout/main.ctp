<?= $this->start('header') ?>
<header class="main-header">
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>M</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
                <b>ナックルメロン管理画面</b>
            </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a class="sidebar-toggle" data-toggle="offcanvas" role="button"></a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="user user-menu" style="padding: 14px">
                    <div style="display: inline;">
                    </div>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<?= $this->end() ?>

<?= $this->start('aside') ?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">&nbsp;</li>
            <?php if ($currentUser['level'] == 5) { ?>

                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-cog"></i>&nbsp;<span>各種設定</span>',
                        ['controller' => 'shippingFee', 'action' => 'index'],
                        ['escapeTitle' => false]) ?>
                </li>

                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-shopping-cart"></i>&nbsp;<span>出品管理</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li style="display: none">
                            <?= $this->Html->link(
                            '&nbsp;&nbsp;<i class="fa fa-upload"></i>&nbsp;<span>商品一括登録</span>',
                            ['controller' => 'userProducts', 'action' => 'upload'],
                            ['escapeTitle' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link(
                            '&nbsp;&nbsp;<i class="fa fa-list"></i>&nbsp;<span>商品出品設定</span>',
                            ['controller' => 'userProducts', 'action' => 'product-list'],
                            ['escapeTitle' => false]) ?>
                        </li>

                        <li>
                            <?= $this->Html->link(
                                '&nbsp;&nbsp;<i class="fa fa-list"></i>&nbsp;<span>商品状態管理</span>',
                                ['controller' => 'userProducts', 'action' => 'search-sub-product'],
                                ['escapeTitle' => false]) ?>
                        </li>

                    </ul>
                </li>
                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-list"></i>&nbsp;<span>受注一覧</span>',
                        ['controller' => 'orders', 'action' => 'management'],
                        ['escapeTitle' => false]) ?>
                </li>
                <!-- <li>
                    <?= $this->Html->link(
                    '<i class="fa fa-list"></i>&nbsp;<span>発注管理</span>',
                    ['controller' => 'Orders', 'action' => 'listOrderProduct'],
                    ['escapeTitle' => false]) ?>
                </li> -->
                <li>
                    <?= $this->Html->link(
                    '<i class="fa fa-list"></i>&nbsp;<span>発送管理</span>',
                    ['controller' => 'shippingRequests', 'action' => 'list-shipping'],
                    ['escapeTitle' => false]) ?>
                </li>

                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-file-image-o"></i>&nbsp;<span>バナー管理</span>',
                        ['controller' => 'banners', 'action' => 'index'],
                        ['escapeTitle' => false]) ?>
                </li>

                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-users"></i>&nbsp;<span>ユーザー管理</span>',
                        ['controller' => 'users', 'action' => 'retails-list'],
                        ['escapeTitle' => false]) ?>
                </li>

                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-users"></i>&nbsp;<span>代行者管理</span>',
                        ['controller' => 'users', 'action' => 'index'],
                        ['escapeTitle' => false]) ?>
                </li>

            <?php } ?>
            <li>
                <?= $this->Html->link(
                    '<i class="fa fa-list"></i>&nbsp;<span>中国調達管理</span>',
                    ['controller' => 'Orders', 'action' => 'transportOrderManagement'],
                    ['escapeTitle' => false]) ?>
            </li>
            <li>
                <?= $this->Html->link(
                    '<i class="fa fa-power-off"></i>&nbsp;<span>ログアウト</span>',
                    ['controller' => 'users', 'action' => 'logout'],
                    ['escapeTitle' => false]) ?>
            </li>

        </ul>
    </section>
</aside>
<?= $this->end() ?>

<!-- Main content -->

<div class="content-wrapper">
    <?= $this->fetch('content') ?>
</div>

<?= $this->start('footer') ?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>バージョン</b> 1.0
    </div>
</footer>
<?= $this->end() ?>