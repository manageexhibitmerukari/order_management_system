<!DOCTYPE html>
<!-- saved from url=(0061)http://chinamart.jp/feature/drone_accessory?page=1&sitetype=1 -->
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title><?php echo (isset($pageName) && $pageName) ? $pageName. ' - ' : '' ?>ナックルメロンサイト </title>

    <link rel="apple-touch-icon" href="/img/myPage_melon.png">
    <link rel="shortcut icon" href="/img/myPage_melon.png">
    <link rel="stylesheet" href="/css/style.css" type="text/css"
          media="all">
    <link href="/css/app.jp.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/skin/bootstrap/css/bootstrap.min.css" />
    <?= $this->Html->css('plugins/datatables/dataTables.bootstrap.css') ?>
    <link rel="stylesheet" href="/css/skin/dist/css/AdminLTE.css" />
    <link rel="stylesheet" href="/css/fb.css" />
    <?= $this->Html->css('plugins/validation/formValidation.min.css') ?>

    <!--[if lt ie 8]>
    <script type="text/javascript">
        alert('当サイトの全ての機能をご利用いただくには最新のブラウザにアップデートしてください。');
    </script>


    <!--<script src="/js/plugins/jQuery/jquery-2.2.1.min.js"></script>-->

    <![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="//chinamart.jp/lib/js/html5.js"></script>
    <![endif]-->
    <script src="/js/frontend/numeral.min.js"></script>
    <script src="/js/frontend/jquery.min.js"></script>

    <script type="text/javascript" src="/js/frontend/library.js"></script>
    <script type="text/javascript" src="/js/frontend/common.js?updated=1501130856"></script>
    <script type="text/javascript" src="/js/frontend/lazyload.js"></script>

    <?= $this->html->script('plugins/datatables/jquery.dataTables.min.js') ?>
    <?= $this->html->script('plugins/bootbox/bootbox.js') ?>

    <?= $this->html->script('skin/bootstrap/js/bootstrap.min.js') ?>
    <?= $this->html->script('plugins/datatables/dataTables.bootstrap.min.js') ?>
    <script src="/js/frontend/detail.js?updated=1504149147"></script>
    <script src="/js/site/login_sku.js?updated=1501130856"></script>
    <?= $this->html->script('plugins/validation/formValidation.min.js') ?>
    <?= $this->html->script('plugins/validation/framework/bootstrap.min.js') ?>
    <?= $this->html->script('site/detail_address.js?updated=1501130856') ?>

    <?= $this->fetch('script') ?>

    <?= $this->fetch('css') ?>
    <!-- Top slider -->
    <link rel="stylesheet" href="/css/pgwslider.min.css" type="text/css" media="all">
    <script src="/js/frontend/pgwslider.min.js"></script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3048701-6']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
<body>
    <div id="container" error="<?php if(!empty($this->request->session()->read('err_login'))) echo 'error-login'; ?>">
        <noscript>&lt;div id="maintenance"&gt;当サイトをご利用いただくにはJavascriptを有効にしてください。&lt;/div&gt;</noscript>
        <div id="top_wrap">
            <div id="top">
                <div class="row">
                    <div class="col-md-4 top-right">
                        <a href="/">
<!--                            <img src="/img/logo_pc_header_2.png">-->
                            KNUCKLE MELON
                        </a>
                    </div>
                    <div class="col-md-5 top-left">
                        <span>卸サイト【KNUCKLE MELON】ナックルメロン</span>
                    </div>
                    <div class="col-md-3">
                        <?php if(isset($currentUser)) :  ?>
                            <ul class="current-user">
                                <li>会員ID <b><?php echo $currentUser->username ?></b> さん</li>
                                <li><a href="/users/logout"> ログアウト </a></li>

                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div>
                    <?php if(isset($countProduct) && $countProduct) :
                        //dd($countProduct);
                        ?>
                    <?php endif;?>
                </div>
                <ul id="top_menu">

                </ul>
            </div>
        </div>
        <div id="header">
            <div id="header_logo">
                <a href="/">
                    <img src="/img/oder.png">
                </a>
            </div>
            <div id="hd_src_wrapper">
                <ul id="hd_src_lang">
<!--                    <li class="shown" id="search_t"><a onclick="setLang('t')">日本語で検索</a></li>-->
                    <!--<li id="search_nt"><a onclick="setLang('nt')">中国語で検索</a></li>-->
                    <!--<li id="search_seller"><a onclick="setLang('seller')">出品者から検索</a></li>-->
                    <!--<li id="search_url"><a onclick="setLang('url')">URL/IDから検索</a></li>-->
                </ul>
                <input type="hidden" id="src_lang" value="t">
                <div id="hd_src">
                    <form action="/user-products/top" method="get" id="search-top">
                        <div class="hd_src_row">
                            <div id="hd_src_category">
                                <div id="search_taobao" class="hd_src_category_each shown">
                                    <div class="hd_src_pardiv">
                                        <select id="src_taobao_topcat" onchange="changeTopCategory(this.value)" name="category-parent">
                                            <option value="0">全カテゴリ</option>
                                            <?php
                                                foreach ($categories as $row) {
                                                    $selected = '';
                                                    if (isset($parentCategoryId) && $parentCategoryId == $row['id'])
                                                        $selected = 'selected';
                                                    echo '<option value="' . $row['id'] . '" '. $selected.'>' . $row["categoryName"] . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="hd_src_catdiv_taobao" class="hd_src_catdiv <?= (isset($childCategories) && count($childCategories) > 0) ? 'shown' : 'default' ?>">
                                        <?php if (isset($childCategories) && count($childCategories) > 0) { ?>
                                            <select name="category-child">
                                                <option value="">全カテゴリ</option>
                                                <?php
                                                    foreach ($childCategories as $row) {
                                                        $selected = '';
                                                        if ($childCategoryId == $row['id'])
                                                            $selected = 'selected';
                                                        echo '<option value="' . $row['id'] . '" '. $selected.'>' . $row["categoryName"] . '</option>';
                                                    }
                                                ?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div id="search_alibaba" class="hd_src_category_each">
                                    <div class="hd_src_pardiv">
                                        <select id="src_alibaba_topcat" onchange="changeT
                                        opCategory(this.value)">
                                            <option value="0" selected="">全カテゴリ</option>
                                            <option value="1">アパレル</option>
                                            <option value="2">美容/育児</option>
                                            <option value="3">生活/住宅</option>
                                            <option value="4">家電/情報</option>
                                            <option value="5">趣味/娯楽</option>
                                            <option value="6">事務/商業</option>
                                            <option value="7">機械/工具</option>
                                            <option value="8">材料/部品</option>
                                        </select>
                                    </div>
                                    <div id="hd_src_catdiv_alibaba" class="hd_src_catdiv default">
                                        <select>
                                            <option>-</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hd_src_row">
                            <div id="hd_src_keyword">
                                <input type="text" id="src_keyword" value="<?= (isset($keyword) ? $keyword : '') ?>" placeholder="検索ワードをスペースで区切って入力してください。"
                                       class="<?= (isset($childCategories) && count($childCategories) > 0) ? 'short' : 'middle' ?>" onblur="hideHistoryKeyword()" onfocus="showHistoryKeyword()" name="keyword"
                                       onkeypress="startItemSearch(event.keyCode)">
                            </div>
                            <div id="hd_src_btn">
                                <input type="hidden" id="src_site" value="taobao">
                                <a onclick="itemSearch()" class="searchbtn" title="検索する">
                                    <span class="menumark searchstart"></span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="header_right">
                <span id="header_right_hidden">
                    <a onclick="toggleSearch()" class="headerbtn" title="商品検索">
<!--                        <i class="glyphicon glyphicon-log-in"></i>-->
<!--                        <span>ログイン</span>-->
                    </a>
                    <a href="https://chinamart.jp/login" class="headerbtn" title="会員ログイン">
<!--                        <i class="glyphicon glyphicon-shopping-cart"></i>-->
<!--                        <span>ログイン</span>-->

                    </a>
                </span>
                <?php if(isset($currentUser)) { ?>
                    <a href="/user-products/manager-order" class="headerbtn" title="受注一覧" style="padding: 0 20px; height: 57px;" >
                        <span><img src="/img/myPage_melon.png" style="width: 20px; height: 20px;"></span>
                        <span>マイページ</span>
                    </a>
                <?php } else { ?>
                    <a href="/users/login-user" class="headerbtn" title="受注一覧" style="height: 57px;">
                        <span class="glyphicon glyphicon-log-in"></span>
                        <span>ログイン</span>
                    </a>
                <?php } ?>
                <a href="/user-products/show-cart" class="headerbtn" title="カートを確認" style="height: 57px;">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <span>仕入れカート</span>

                        <?php if (isset($countProduct)) : ?>
                        <span class="num-cart"><?php echo ($countProduct) ? $countProduct : 0 ?> </span>
                        <?php endif;?>


                </a>
            </div>

        </div>
        <div id="inner">
            <?= $this->fetch('content') ?>

            <div id="side">

                <div class="history_product">
                    <div class="hp_top">
                        <span>新規出品情報</span> <span>!</span>
                    </div>

                    <div class="ht_content">

                        <div class="hp_detail clearfix">
                            <div class="hp_detail_left">
                                <span>本日の新着</span>
                                <span><?= $lastProductCreatedTime ?> 更新</span>
                            </div>
                            <div class="hp_detail_right">
                                <span><?= $todayProductCount ?></span>
                                <span>商品</span>
                            </div>

                        </div>

                        <div class="hp_detail hp_detail_1 clearfix">
                            <div class="hp_detail_left">
                                <span><?= $oneDayAgoDate ?> 追加分 </span>
                            </div>
                            <div class="hp_detail_right">
                                <span><?= $oneDayAgoProductCount ?></span>
                                <span>商品</span>
                            </div>
                        </div>

                        <div class="hp_detail hp_detail_1 clearfix">
                            <div class="hp_detail_left">
                                <span><?= $twoDaysAgoDate ?> 追加分 </span>
                            </div>
                            <div class="hp_detail_right">
                                <span><?= $twoDaysAgoProductCount ?></span>
                                <span>商品</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="side-link">
                    <?php if(isset($bannerSides) && $bannerSides) : ?>

                        <?php foreach ($bannerSides as $bannerSide): //dd($bannerSide)?>

                            <div class="box-link">
                                <a href="<?php echo $bannerSide['link'] ?>" target="_blank" title="<?php echo $bannerSide['title'] ?>">
                                    <img src="<?php echo $bannerSide['image'] ?>" class="img-responsive">
                                    <span><?php echo $bannerSide['title'] ?></span>
                                </a>
                            </div>

                        <?php endforeach;?>
                    <?php endif ?>

                </div>


            </div>
            <div class="clear"></div>
        </div>
        <footer id="footer">
            <div class="info_footer">
                <p class="if_title">KNUCKLE MELON（ナックルメロン）とは</p>
                <p class="if_detail">KNUCKLE MELONはレディースからメンズ・キッズまで、幅広いジャンルのファストファッションアイテムを中心に、まとめて購入できる卸サイトです。 最新のファストファッションアイテムを取り揃え、仕入れに欠かせない情報も商品ページに掲載してあります。
                    お得なイベントも開催しますので、是非、卸サイトKNUCKLE MELONでのお買い物をお楽しみください！
                </p>
            </div>

            <div class="menu_footer">
                <ul>
                    <li><a id="btn-top" href="javascript:void(0)">トップに戻る</a> </li>
                    <li><a href="/guide">ご利用ガイド</a></li>
                    <li><a href="/term-of-service">利用規約</a></li>
                    <li><a href="/term-of-service2">特定商取引法に基づく表記</a></li>
                    <li><a href="/fqa">よくある質問</a></li>
                    <li><a href="/contact">お問い合わせ</a></li>
                </ul>
            </div>

            <a class="btn-top" href="javascript:void(0);" title="Top" >
                <span class="glyphicon glyphicon-arrow-up"> </span>
            </a>
        </footer>
        <div id="scrolltotop" class="menumark scroll" title="ページ上部に戻る" style="display: none;"></div>
    </div>
    <div id="login" class="modal fade" role="dialog">
        <div class="login-box-body">
            <div class="login-logo">
                <a href="javascript:void(0)">ログイン</a>
            </div>
            <form method="post" accept-charset="utf-8" action="/users/loginUser"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>            <!-- error message -->
                <div class="form-group has-feedback">
                    <!-- END error message -->
                    <div class='box-error'></div>
                    <div class="form-group has-feedback">
                        <div class="input-group_nouse">
                            <div class="input text"><input type="text" name="username" class="form-control" placeholder="ユーザーIDを入力してください。" id="username"></div>                </div>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="input-group_nouse">
                            <div class="input password"><input type="password" name="password" class="form-control" placeholder="パスワードを入力してください。" id="password"></div>                </div>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="row" style="margin:0px;">
                        <div class="col-xs-5 fix-format" style="width:100%">
                            <button type="button" name="submit" class="btn btn-primary btn-block btn-flat login-user">
                                ログイン&nbsp;<i class="fa fa-sign-in"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="cboxOverlay" style="display: none;"></div>
    <div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
        <div id="cboxWrapper">
            <div>
                <div id="cboxTopLeft" style="float: left;"></div>
                <div id="cboxTopCenter" style="float: left;"></div>
                <div id="cboxTopRight" style="float: left;"></div>
            </div>
            <div style="clear: left;">
                <div id="cboxMiddleLeft" style="float: left;"></div>
                <div id="cboxContent" style="float: left;">
                    <table class="tbl-cart">
                        <tr>
                            <th class="name">商品</th>
                            <th class="quantity">数量</th>
                            <th class="price">価格</th>
                            <th class="delete"></th>
                        </tr>
                        <tr>
                            <td colspan="4" id="subtotal"><span id="">小計: ￥ 25,094</span></td>
                        </tr>
                    </table>
                </div>
                <div id="cboxMiddleRight" style="float: left;"></div>
            </div>
            <div style="clear: left;">
                <div id="cboxBottomLeft" style="float: left;"></div>
                <div id="cboxBottomCenter" style="float: left;"></div>
                <div id="cboxBottomRight" style="float: left;"></div>
            </div>
        </div>
        <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>

    </div>

    <div id="login-sku" class="modal fade" role="dialog">
        <div class="login-box-body">
            <form method="post" accept-charset="utf-8" class="form-horizontal"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>            <!-- error message -->
                    <p class="error-login-sku"></p>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="username">ユーザー名</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control username" name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="username">パスワード</label>
                        <div class="col-sm-8">
                            <input type="password" name="password" class="form-control password" >
                        </div>
                    </div>

                    <div class="row" style="margin:0px;">
                        <div class="col-xs-5 fix-format" style="width:100%; padding:0">
                            <button type="button" id="btn-login-sku" class="btn btn-primary btn-block btn-flat">
                                ログイン&nbsp;<i class="fa fa-sign-in"></i>
                            </button>
                            <button type="button" id="btn-register-sku" class="btn btn-primary btn-block btn-flat">
                                登録&nbsp;<i class="fa fa-sign-in"></i>
                            </button>
                        </div>
                    </div>
            </form>
            <div class="form-group logout">
                <label class="welcome"></label>
                <button class="btn btn-primary btn-block btn-flat logout">ログアウト</button>
            </div>
        </div>
    </div>
    <div id="cboxOverlay" style="display: none;"></div>
    <div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
        <div id="cboxWrapper">
            <div>
                <div id="cboxTopLeft" style="float: left;"></div>
                <div id="cboxTopCenter" style="float: left;"></div>
                <div id="cboxTopRight" style="float: left;"></div>
            </div>
            <div style="clear: left;">
                <div id="cboxMiddleLeft" style="float: left;"></div>
                <div id="cboxContent" style="float: left;">
                    <table class="tbl-cart">
                        <tr>
                            <th class="name">商品</th>
                            <th class="quantity">数量</th>
                            <th class="price">価格</th>
                            <th class="delete"></th>
                        </tr>
                        <tr>
                            <td colspan="4" id="subtotal"><span id="">小計: ￥ 25,094</span></td>
                        </tr>
                    </table>
                </div>
                <div id="cboxMiddleRight" style="float: left;"></div>
            </div>
            <div style="clear: left;">
                <div id="cboxBottomLeft" style="float: left;"></div>
                <div id="cboxBottomCenter" style="float: left;"></div>
                <div id="cboxBottomRight" style="float: left;"></div>
            </div>
        </div>
        <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>
    </div>
</body>
</html>