<!doctype html>
<title>Site Maintenance</title>
<style>
    body { text-align: center; padding: 150px; }
    h1 { font-size: 50px; }
    body { font: 20px Helvetica, sans-serif; color: #333; }
    article { display: block; text-align: left; width: 650px; margin: 0 auto; }
    a { color: #dc8100; text-decoration: none; }
    a:hover { color: #333; text-decoration: none; }
    .wrapper {background-color: #ffffff !important;}
</style>

<article>
    <h1>システムをアップデートしています。!</h1>
    <div>
        <p>30分ほどを待ちください。!</p>
        <p></p>
    </div>
</article>