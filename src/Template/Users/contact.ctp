<?= $this->Html->script('site/contact.js', ['block' => 'script']) ?>


<div id="main">

    <section>

        <div class="row">

            <div class="col-md-12">

                <div class="contact">

                    <div class="contact-title">
                        <p>お問い合わせ</p>
                    </div>

                    <div class="contact-form">

                            <?= $this->Form->create(null, ['id' => 'contact_form', 'class' => 'form-horizontal']) ?>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="issue">件名 </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="issue" >
                                    <option value="配送について">配送について</option>
                                    <option value="返品依頼について">返品依頼について</option>
                                    <option value="ログインについて">ログインについて</option>
                                    <option value="会員登録について">会員登録について</option>
                                    <option value="お支払について">お支払について</option>
                                    <option value="注文変更・キャンセルについて">注文変更・キャンセルについて</option>
                                    <option value="注文について">注文について</option>
                                    <option value="退会依頼について">退会依頼について</option>
                                    <option value="その他の質問">その他の質問</option>
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="firstname">お名前 <span class="require">(必須)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="firstname">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="lastname">フリガナ <span class="require">(必須)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="lastname">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">メールアドレス <span class="require">(必須)</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="email" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="address">住所</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="address">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="phone">電話番号</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="phone">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="content">問い合わせ内容<span class="require">(必須)</label>
                            <div class="col-sm-8">
                                <textarea rows="6" class="form-control" name="content"></textarea>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <?php echo $this->Flash->render() ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-6 col-sm-6">
                                <button type="submit" class="btn btn-success">お問い合わせ</button>
                            </div>
                        </div>


                        <?= $this->Form->end() ?>

                    </div>
                </div>

            </div>

        </div>

    </section>

</div>