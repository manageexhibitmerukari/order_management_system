<?= $this->Html->script('site/register.js', ['block' => 'script']) ?>

<section class="content">
    <div class="login-box">
        <!-- BEGIN Login Form -->
        <div class="login-box-body">
            <div class="login-logo">
                <a href="javascript:void(0)"><?php echo "会員登録"; ?></a>
            </div><!-- /.login-logo -->

            <?= $this->Flash->render('auth') ?>
            <?= $this->Form->create(null,['id'=>'signup_form']) ?>
            <!-- error message -->
            <div class="form-group has-feedback">
                <?php
			            if(!is_null($error)){
			                echo '<div class="alert alert-error">';
                echo '<p style="text-align:center;">'.$error.'</p>';
                echo '</div>';
            }
            ?>
            </div>
            <!-- END error message -->

            <div class="form-group has-feedback">
                <div class="input-group_nouse">
                    <?= $this->Form->input('username', ['label' => false, 'class' => 'form-control', 'placeholder' => 'ユーザー名']) ?>
                </div>
                <!--<span class="glyphicon glyphicon-user form-control-feedback"></span>-->
            </div>

            <div class="form-group has-feedback">
                <div class="input-group_nouse">
                    <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control', 'placeholder' => 'メールアドレス']) ?>
                </div>
                <!--<span class="glyphicon glyphicon-lock form-control-feedback"></span>-->
            </div>
            <div class="form-group has-feedback">
                <div class="input-group_nouse">
                    <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control', 'placeholder' => 'パスワード']) ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <div class="input-group_nouse">
                    <?= $this->Form->input('confirm_password', ['label' => false, 'type'=> 'password', 'class' => 'form-control', 'placeholder' => 'パスワード（確認）']) ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <div class="input-group_nouse">
                    <textarea class="form-control" name="note" rows="5"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-5" style ="width:100%">
                    <button type="submit" name="submitBtn" class="btn btn-primary btn-block btn-flat">会員登録&nbsp;<i class="fa fa-sign-in"></i>
                    </button>

                </div>
            </div>
            <!--<div class="row text-center" style="padding-top: 10px;">-->
                <!--<a href="/users/terms-of-service">利用規約</a>-->
            <!--</div>-->
        </div>
        </form>
    </div>
    <style>
        .input-group-addon{font-size: 12px;}
        .form-control{font-size: 12px;}
        .btn{font-size: 12px}
        .login-logo, .register-logo{font-size:30px;}
        .pull-right{float: right;}
        .login-box-body .form-control-feedback{
            color:#a94442;
        }
        .has-success .control-label,
        .has-success .help-block,
        .has-success .form-control-feedback {
            color: #3c763d;
        }
    </style>
    <!-- END Login Form -->
    </div><!-- /.login-box -->
</section>