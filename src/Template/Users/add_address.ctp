<div id="main">
    <section>
        <h3>新しい住所を追加</h3>
        <?= $this->Form->create(null, ['id' =>'detail_address_form', 'class' => 'form-horizontal' ]) ?>
        <div class="modal-body">
            <div class="form-group">
                <label class="col-xs-3 control-label" for="name">氏名 </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="name" id="name" required />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="zip_code">郵便番号 </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="zip_code" id="zip_code" required />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="city">都道府県 </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="city" id="city" required />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="address1">住所１ </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="address1" id="address1" required />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="address2">住所2 </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="address2" id="address2" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="company_name">会社名 </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="company_name" id="company_name" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="phone_number">電話番号 </label>
                <div class="col-xs-7">
                    <input type="text" class="form-control input-sm" name="phone_number" id="phone_number" required />
                </div>
            </div>

            <div style="text-align: center">
                <button type="submit" class="btn btn-menu-link">保存する</button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </section>
</div>
