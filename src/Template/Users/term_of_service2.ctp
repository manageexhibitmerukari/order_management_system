<div id="main">

    <section>

        <div class="row">

            <div class="col-md-12">

                <div class="term-service-2">

                    <p class="ts2-title">特定商取引に関する法律に基づく表示
                    </p>

                    <div class="ts2-content">
                        <div class="ts2-detail">
                            <p>&diams; 販売業者 : <span> 株式会社ナックルメロン </span> </p>

                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 運営統括責任者 : <span>品川　雄大 </span> </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 所在地 : <span>〒496-0856　愛知県津島市瑠璃小路町1-63</span> </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 電話番号 : <span>0567-58-5805 </span> </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 商品代金以外の必要料金 </span> </p>
                            <p>
                                - 銀行振込でご購入の際の振込手数料 <br>
                                - 商品発送の際の送料
                            </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; ご発注の有効期限 : <span>請求日から3営業日 </span> </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 販売数量 : <span>特に指定はございません。（商品によって設定あり）</span> </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 引き渡し時期 : <span>原則として、ご入金確認後、2営業日以内に商品を出荷します。</span> </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; お支払い方法 </p>
                            <p>
                                銀行振込 <br>
                                クレジットカード支払い
                            </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; お支払い期限 </p>
                            <p>ご請求日より3営業日以内に弊社の指定口座へお振込ください。<br>
                                3営業日を超えてご入金が無い場合には、キャンセル扱いとさせていただきます。<br>
                                再度、御注文いただきますようお願いします。<br>
                            </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 返品期限 </p>
                            <p>商品の返品・交換には初期不良の場合以外では応じられません。<br>
                                商品到着日から8日間以内に生じました初期不良に付きましては交換対応をいたします。<br>

                            </p>
                        </div>

                        <div class="ts2-detail">
                            <p>&diams; 返品送料 </p>
                            <p>不良品、発送商品間違いの場合、弊社着払いにて対応いたします。<br>
                            </p>
                        </div>




                    </div>

                </div>

            </div>

        </div>

    </section>

</div>