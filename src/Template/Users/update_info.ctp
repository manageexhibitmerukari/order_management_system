<?= $this->Html->script('site/register.js', ['block' => 'script']) ?>


<div id="main">
    <section>
        <div class="row" style="margin: 0;">
            <a href="/user-products/manager-order" class="col-sm-3 btn-menu-link">注文履歴</a>
<!--            <a href="/user-products/product-management" class="col-sm-3 btn-menu-link">商品リスト</a>-->
            <a href="/users/address-management" class="col-sm-3 btn-menu-link">お届け先リスト</a>
            <a href="/users/update-info" class="col-sm-3 btn-menu-link-active">会員情報管理</a>
            <a href="/users/change-user-password" class="col-sm-3 btn-menu-link">パスワード変更</a>
        </div>
        <hr>

        <div class="row">

                <div class="bl-head">
                    <h3>会員情報管理</h3>
                </div>

                <?php if (isset($result) && $result == 'success') { ?>
                    <div class="alert alert-success col-xs-10" style="float: none; margin: 0 auto 20px;">
                        <p style="font-size: 16px;">
                            会員情報を更新しました。
                        </p>
                    </div>
                <?php } else if (isset($result) && $result == 'error') { ?>
                    <div class="alert alert-error col-xs-10" style="float: none; margin: 0 auto 20px;">
                        <p style="font-size: 16px;">
                            <?= $error; ?>
                        </p>
                    </div>
                <?php } ?>

                <?= $this->Form->create($user, ['id' => 'register_form', 'class' => 'form-horizontal']) ?>
                    <div class="form-group">
                        <label for="username" class="col-xs-4 control-label"> ログイン ID </label>
                        <?= $this->Form->input('username', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-xs-4 control-label">メールアドレス</label>
                        <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-xs-4 control-label">お名前</label>
                        <?= $this->Form->input('name', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="payment_name" class="col-xs-4 control-label">ご請求先名  <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="ご登録名義（個人名）ではなく会社名で請求書が必要な場合は、こちらへ会社名を必ずご入力下さいませ。"></span></label>
                        <?= $this->Form->input('payment_name', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

            <script>
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>

                    <div class="form-group">
                        <label for="name_kana" class="col-xs-4 control-label">フリガナ</label>
                        <?= $this->Form->input('name_kana', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="phone_number" class="col-xs-4 control-label">電話番号</label>
                        <?= $this->Form->input('phone_number', ['maxlength' => false, 'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-xs-4 control-label">会社名</label>
                        <?= $this->Form->input('company', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="postal" class="col-sm-4 control-label">郵便番号</label>
                        <?= $this->Form->input('postal', ['maxlength' => false,'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-5">{{content}}</div>']]) ?>
                        <button type="button" class="btn btn-sm btn-primary" onclick="checkZipCode($('#postal').val())">検索</button>
                    </div>

                    <div class="form-group">
                        <label for="state" class="col-sm-4 control-label">都道府県</label>
                        <?= $this->Form->input('state', ['readonly' => true, 'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="town" class="col-sm-4 control-label">市町村</label>
                        <?= $this->Form->input('town', ['readonly' => true, 'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="house_number" class="col-xs-4 control-label">番地</label>
                        <?= $this->Form->input('house_number', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="building" class="col-xs-4 control-label">建物名等</label>
                        <?= $this->Form->input('building', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                <div class="form-group">
                    <button type="submit" class="col-sm-2 col-sm-offset-5 btn-menu-link" ><i class="fa fa-save (alias)"></i> 保存</button>
                </div>
                <?= $this->Form->end() ?>

        </div>
    </section>
</div>