<?= $this->Html->script('site/change_user_password.js', ['block' => 'script']) ?>


<div id="main">
    <section>
        <div class="row" style="margin: 0;">
            <a href="/user-products/manager-order" class="col-sm-3 btn-menu-link">注文履歴</a>
<!--            <a href="/user-products/product-management" class="col-sm-3 btn-menu-link">商品リスト</a>-->
            <a href="/users/address-management" class="col-sm-3 btn-menu-link">お届け先リスト</a>
            <a href="/users/update-info" class="col-sm-3 btn-menu-link">会員情報管理</a>
            <a href="/users/change-user-password" class="col-sm-3 btn-menu-link-active">パスワード変更</a>

        </div>
        <hr>

        <h3>パスワード変更</h3>

        <?= $this->Form->create(null, ['id' => 'change_user_password', 'class' => 'form-horizontal']) ?>

            <div class="form-group">
                <label class="control-label col-sm-4" for="current_pass">現在のパスワード</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="current_pass" >
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="new_pass">新しいパスワード</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="new_pass" id="new_pass">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="re_new_pass">新しいパスワード （確認）</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="re_new_pass">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-5">
                    <?php echo $this->Flash->render() ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-2">
                    <button type="submit" class="btn btn-change-pw">更新する</button>
                </div>
            </div>

        <?= $this->Form->end() ?>

    </section>
</div>