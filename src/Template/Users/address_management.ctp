<?= $this->Html->script('site/address_management.js', ['block' => 'script']) ?>


<div id="main">
    <section>
        <div class="row" style="margin: 0;">
            <a href="/user-products/manager-order" class="col-sm-3 btn-menu-link">注文履歴</a>
<!--            <a href="/user-products/product-management" class="col-sm-3 btn-menu-link">商品リスト</a>-->
            <a href="/users/address-management" class="col-sm-3 btn-menu-link-active">お届け先リスト</a>
            <a href="/users/update-info" class="col-sm-3 btn-menu-link">会員情報管理</a>
            <a href="/users/change-user-password" class="col-sm-3 btn-menu-link">パスワード変更</a>
        </div>
        <hr>

        <h3>お届け先住所の選択</h3>
        <a href="/users/add-address" class="btn btn-success btn-sm">
            <i class="fa fa-plus"></i> 新しい住所を追加
        </a>
        <br>
        <br>

        <div class="ftr_wrap">
            <div class="ftr_inner clearfix" style="width: 762px;">
                <?php foreach ($addresses as $address) { ?>
                    <div class="ftr_each " style="width: 245px; text-align: left; word-wrap: break-word;">
                        <p><strong><?= $address->name ?></strong></p>
                        <div style="min-height: 120px;"><?= $address->full_address ?></div>
                        <br>

                        <div style="text-align: center">
                            <a href="/users/change-address/<?= $address->id ?>" class="col-sm-10 col-sm-offset-1 btn btn-warning btn-sm <?= ($address->id == $currentAddressId) ? 'disabled' : '' ?>" style="text-decoration: none;"><strong>この住所を使う</strong></a>
                            <br>
                            <br>
                            <a href="/users/edit-address/<?= $address->id ?>" class="col-sm-4 col-sm-offset-1 btn btn-default btn-sm" style="text-decoration: none;"><strong>編集</strong></a>
                            <button onclick="deleteAddress(<?= $address->id ?>)" class="col-sm-4 col-sm-offset-2 btn btn-default btn-sm <?= ($address->id != $currentAddressId && count($addresses) > 0) ? '' : 'disabled' ?>"><strong>削除</strong></button>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
</div>