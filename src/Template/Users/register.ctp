<?= $this->Html->script('site/register.js', ['block' => 'script']) ?>


<div id="main">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="bl-title">
                    <p>会員情報入力</p>
                </div>
            </div>
            <?php if (!$success) { ?>

                    <?= $this->Form->create(null, ['id' => 'register_form', 'class' => 'form-horizontal']) ?>
                    <div class="form-group">
                        <label for="username" class="col-sm-4 control-label"> ログイン ID </label>
                        <?= $this->Form->input('username', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="password" class="col-sm-4 control-label">パスワード</label>
                        <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="confirm_password" class="col-sm-4 control-label">パースワード確認用</label>
                        <?= $this->Form->input('confirm_password', ['type' => 'password', 'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">メールアドレス</label>
                        <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">お名前</label>
                        <?= $this->Form->input('name', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>



                    <div class="form-group">
                        <label for="payment_name" class="col-sm-4 control-label">ご請求先名  <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="ご登録名義（個人名）ではなく会社名で請求書が必要な場合は、こちらへ会社名を必ずご入力下さいませ。"></span> </label>
                        <?= $this->Form->input('payment_name', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <script>
                        $(document).ready(function(){
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    </script>

                    <div class="form-group">
                        <label for="name_kana" class="col-sm-4 control-label">フリガナ</label>
                        <?= $this->Form->input('name_kana', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="phone_number" class="col-sm-4 control-label">電話番号</label>
                        <?= $this->Form->input('phone_number', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-4 control-label">会社名</label>
                        <?= $this->Form->input('company', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="postal" class="col-sm-4 control-label">郵便番号</label>
                        <?= $this->Form->input('postal', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-5">{{content}}</div>']]) ?>
                        <button type="button" class="btn btn-sm btn-primary" onclick="checkZipCode($('#postal').val())">検索</button>
                    </div>

                    <div class="form-group">
                        <label for="state" class="col-sm-4 control-label">都道府県</label>
                        <?= $this->Form->input('state', ['readonly' => true, 'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="town" class="col-sm-4 control-label">市町村</label>
                        <?= $this->Form->input('town', ['readonly' => true, 'label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="house_number" class="col-sm-4 control-label">番地</label>
                        <?= $this->Form->input('house_number', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <div class="form-group">
                        <label for="building" class="col-sm-4 control-label">建物名等</label>
                        <?= $this->Form->input('building', ['label' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="col-sm-6">{{content}}</div>']]) ?>
                    </div>

                    <?php if (isset($error)) { ?>
                        <div class="col-sm-offset-4 col-sm-6" style="color:#a94442">
                            <p>
                                <?= $error; ?>
                            </p>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <button type="submit" class="col-sm-4 col-sm-offset-5 btn-menu-link" ><i class="fa fa-save (alias)"></i> 登録</button>
                    </div>
                    <?= $this->Form->end() ?>

                    <?php } else { ?>
                    <div class="col-sm-offset-2 col-sm-8" style="">
                        <p>
                            仮登録完了いたしました。<br><br>

                            ご入力いただいたメールアドレス宛に内容確認メールを送付いたしましたので、ご確認の上、本登録手続きをお進みください。<br>
                            確認のメールが届かない場合は、「迷惑メール」フォルダなどをご確認ください。
                        </p>
                    </div>
                <?php } ?>
        </div>
    </section>
</div>