<div id="main">

    <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="reset-pw">

                    <div class="reset-pw-title">
                        <p>パスワード再設定</p>
                    </div>

                    <div class="reset-pw-des">
                        <p>会員登録したときの、メールアドレスと電話番号を入力して「パスワード再設定」ボタンを押してください。
                            入力頂いたメールアドレスに、新しいパスワードをお送りいたします。
                            ご登録アドレスをすでに変更している場合は確認ができないため、新規会員登録をお願いします。</p>
                    </div>

                    <div class="reset-pw-form">
                        <div class="col-sm-offset-2 col-sm-8">
                            <?= $this->Form->create(null, ['id' => 'register_form']) ?>

                                <div class="form-group">
                                    <label for="email">メールアドレス:</label>
                                    <input type="email" class="form-control" name="email">
                                </div>

                            <div>
                                <?php echo $this->Flash->render() ?>
                            </div>

                                <button type="submit" class="btn btn-menu-link col-sm-offset-4">パスワード再設定</button>
                            <?= $this->Form->end()?>
                            <div class="reset-pw-form-des">

                                <p >「迷惑メール対策」を設定されてる方や、ドメイン指定受信の設定をされている方は、
                                「knucklemelon.com」ドメインからのメールが受信できるように、予め設定の変更をお願い致します。</p>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
</div>

<?= $this->Html->script('site/register.js', ['block' => 'script']) ?>