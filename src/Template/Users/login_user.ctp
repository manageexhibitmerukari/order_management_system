<?= $this->Html->script('site/login_user.js', ['block' => 'script']) ?>


<div id="main">
    <section>

        <div class="row">
            <div class="col-md-12">
                <div class="bl-title">
                    <p>ログイン</p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box-login">
                    <div class="bl-head">
                        <p>すでに会員のお客様</p>
                    </div>

                    <div class="bl-content">
                        <p class ="blc-title">KnuckleMelon IDでログイン </p>

                        <?= $this->Form->create() ?>

                        <div class="form-group has-feedback">

                            <div class="form-group has-feedback">
                                <label for="username"> ログイン </label>
                                <div class="input-group_nouse">
                                    <?= $this->Form->input('username', ['label' => false, 'class' => 'form-control']) ?>
                                </div>

                            </div>

                            <div class="form-group has-feedback">
                                <label for="password">パスワード</label>
                                <div class="input-group_nouse">
                                    <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control']) ?>
                                </div>

                            </div>

                            <!-- error message -->
                            <?php
                            if(isset($error)){
                                echo '<p style="text-align:center; color: red">'.$error.'</p>';
                            }
                            ?>
                            <!-- END error message -->

                            <button type="submit" onclick="return check_data();" name="submit" class="btn btn-primary btn-block btn-flat ">
                                ログイン&nbsp;<i class="fa fa-sign-in"></i>
                            </button>

                        </div>
                        <?= $this->Form->end() ?>

                        <div class="link-reset-pw">
                            <a href="/users/reset-pass" claas=""> パスワードを忘れた方</a>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-6">
                <div class="box-register">
                    <div class="br-head">
                        <p>はじめてのお客様</p>
                    </div>
                    <div class="br-content">
                        <p class="brc-title"> 新規会員登録</p>
                        <p>
                        レディースからメンズ・キッズまで、
                        多様なジャンルのファストファッ
                        ションアイテムがまとめて買え
                        るショッピングサイトKNUCKLE
                        MELON <br><br>


                        メールアドレス、電話番号で会員
                        登録可能です。
                        </p> <br><br>

                        <a href="/users/register"> 新規会員登録 </a>

                    </div>
                </div>
            </div>

        </div>
    </section>
</div>