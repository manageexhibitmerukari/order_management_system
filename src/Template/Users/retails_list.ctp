<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/retails_list.js?updated=1501733352', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        ユーザー管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="glyphicon glyphicon-user"></i> <b>ユーザー管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">ユーザー管理</h3>

                    <button onclick="displayInsertUI();" class="btn btn-success btn-sm pull-right" id = "btn_add_user" name="btn_add_user">
                        <i class="fa fa-plus"></i> ユーザーを追加
                    </button>
                </div>


                <div class="box-body table-responsive  with-border" id="main_tbl_wrap">
                </div>

                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="user_detail_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create('Users', ['url' => 'add', 'id' =>'user_detail_form', 'class' => 'form-horizontal' ]) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user-plus"></i>  新規ユーザー</h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="username">ログインID </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="username" id="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="password">パスワード </label>
                    <div class="col-xs-7">
                        <input type="password" class="form-control input-sm" name="password" id="password" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="email">メルアド </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="email" id="email" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="name">お名前 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="name" id="name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="phone_number">電話番号 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="phone_number" id="phone_number"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="company">会社名 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="company" id="company"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="postal">郵便番号 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="postal" id="postal"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="state">都道府県 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="state" id="state"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="town">市町村 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="town" id="town"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="house_number">番地 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="house_number" id="house_number"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="building">建物名等 </label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="building" id="building"/>
                    </div>
                </div>
            </div>

            <div class="modal-footer clearfix">
                <button type="submit"  id="change_record" name="change_record" data-style="zoom-in" class="btn btn-success ladda-button pull-center"><i class="fa fa-save"></i> セーブ</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> キャンセル</button>
                <input type="hidden" id="current_user" value="0" name="current_user" />
            </div>
            <?= $this->Form->end() ?>

            <div class="overlay-wrapper">
                <div id="loading_overlay1" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="user_delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content alert">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title text-red"><i class="icon fa fa-warning"></i>  警告</h3>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <h4 class="box-title text-red">
                        あなたは本当にこのユーザを削除したいですか。
                    </h4>
                </div>
            </div>

            <div class="modal-footer clearfix">
                <button type="button"  onclick="deleteUser();" id="delete_record" name="delete_record" data-style="zoom-in" class="btn btn-success ladda-button pull-center"><i class="fa fa-check-square"></i> はい</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> キャンセル</button>
                <input type="hidden" id="delete_user_id" value="0" name="delete_user_id" />
            </div>

            <div class="overlay-wrapper">
                <div id="loading_overlay2" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>
