<div id="main">
    <section>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="bl-head">
                    <h4 style="text-align: center;"><?= $message ?></h4>
                </div>
            </div>
        </div>
    </section>
</div>