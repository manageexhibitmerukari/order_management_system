<div id="main">
    <div class="l-content">
        <input type="hidden" id="status" value="<?=$status?>">
        <ul class="nav nav-tabs" id="request">
            <li class="active" id="new"><a href="/client-products/manager_shipping?status=new">依頼済み</a></li>
            <li id="sent"><a href="/client-products/manager_shipping?status=sent">発送済み</a></li>
        </ul>
        <table class="tbl-manager-shipping table table-bordered">

        </table>
    </div>
</div>
<script type="text/javascript" src="/js/site/manager_shipping.js"></script>