<?= $this->extend('/Layout/main') ?>

<?= $this->Html->css('shipping_fee.css') ?>

<?= $this->Html->script('site/shipping_fee.js', ['block' => 'script']) ?>



<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        各種設定
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-truck"></i> <b>各種設定</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-truck"></i>
                    <h3 class="box-title">送料設定</h3>

                </div>


                <div class="box-body">
                    <div class="shipping-fee">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="fee_default_all" style="margin-right: 30px;">全地域の発送料は</label>
                                <input type="number" class="form-control i-n" id="fee_default_all" value="<?php if(isset($shippingFeeAll) && $shippingFeeAll): echo $shippingFeeAll->fee_default; else: echo 0; endif?>">
                                <label for="fee_default_all">円です。売上額は</label>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control i-n" id="price_order_all" value="<?php if(isset($shippingFeeAll) && $shippingFeeAll): echo $shippingFeeAll->price_order; endif?>">
                                <label for="price_order_all">円以上になったら</label>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control i-n" id="fee_up_all" value="<?php if(isset($shippingFeeAll) && $shippingFeeAll): echo $shippingFeeAll->fee_up; endif?>">
                                <label for="fee_up_all">円になります。</label>
                            </div>

                            <input type="hidden" id="id_fee_all" value="<?php if(isset($shippingFeeAll) && $shippingFeeAll): echo $shippingFeeAll->id; endif?>">
                        </form>

                    </div>

                    <div class="shipping-fee-area">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="fee_default_area" >以下の地域の発送料は</label>
                                <input type="number" class="form-control i-n" id="fee_default_area">
                                <label for="price_order_all">円です。売上額は</label>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control i-n" id="price_order_area">
                                <label for="price_order_area">円以上になったら</label>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control i-n" id="fee_up">
                                <label for="fee_up">円になります。</label>
                            </div>
                            <div class="form-group" id="post_code">
                                <input type="text" id="input_post_code" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-primary" id="add_fee_area" style="width: 70px">↓ 追加</button>
                            </div>

                            <div class="error"></div>
                        </form>

                        <div class="shipping-fee">
                            <table class="table table-responsive table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="40%" class="text-center">地域</th>
                                        <th class="text-center">送料（円）</th>
                                        <th class="text-center">売上額（円）</th>
                                        <th class="text-center">お得送料（円）</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody id="data">
                                    <?php
                                    if($shippingFeeArea && count($shippingFeeArea->toArray()) > 0):
                                        foreach ($shippingFeeArea as $feeArea) :?>
                                            <tr>
                                                <td class="area"><?php echo $feeArea->area?></td>
                                                <td class="fee_default"><?php echo $feeArea->fee_default?></td>
                                                <td class="price_order"><?php echo $feeArea->price_order?></td>
                                                <td class="fee_up"><?php echo $feeArea->fee_up?></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger delete-fee" id="<?php echo $feeArea->id ?>" onclick="deleteFee(this)">削除</button>
                                                    <input type="hidden" class="id_fee_area" value="<?php echo $feeArea->id ?>">
                                                </td>


                                            </tr>
                                    <?php
                                        endforeach;
                                    endif?>

                                </tbody>
                            </table>
                        </div>

                    </div>

                    <input type="hidden" id="data_fee_area" value="">
                    <button type="button" class="btn btn-primary" id="save_shipping_fee" style="width: 70px">保存</button>
                    <div class="error2"></div>

                </div>

            </div>
        </div>
    </div>
</section>