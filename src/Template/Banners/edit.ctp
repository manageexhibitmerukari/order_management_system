<?= $this->extend('/Layout/main') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        バナー変更
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-plus"></i> <b>バナー変更</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container" name="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-plus"></i>
                    <h3 class="box-title">バナー変更</h3>
                </div>

                <div class="box-body table-responsive  with-border" id="main_wrap" >
                    <?php if (isset($banner) && $banner) : ?>

                    <form class="form-horizontal col-xs-11" style="float:none; margin: 0 auto" id="banner_publish_form" method="post" enctype="multipart/form-data">

                        <div id="banner_input_wrapper">

                            <div class="form-group">

                                <div class="form-group photo_1">
                                    <label class="col-xs-3 control-label">写真 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-3">
                                        <img src="<?php echo $banner->image.'?'.strtotime(date('c'));?>" name="photo_1_preview" id="photo_1_preview" style="width:100%; border:1px solid #d2d6de"/>
                                        <img src="/img/no_product.jpg" style="display: none" id="default_photo" />
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="file" accept="image/jpeg|image/png" class="form-control input-sm product_photo" name="photo_1" id="photo_1" v/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-3 control-label">タイトル : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control input-sm" name="title" value="<?php echo $banner->title?>"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-3 control-label">バナーリンク : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control input-sm" name="link" value="<?php echo $banner->link?>"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-3 control-label">バナー類 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <select class="form-control" name="type">
                                            <option value="main_banner" <?php echo ($banner->type == 'main_banner') ? 'selected' : ''?>">横バナー</option>
                                            <option value="side_banner" <?php echo ($banner->type == 'side_banner') ? 'selected' : ''?>>縦バナー</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-3 control-label">表示順番 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <input type="number" class="form-control input-sm" name="position" value="<?php echo $banner->position?>" max="999"/>
                                    </div>
                                </div>

                                <div class="form-group">

                                    <label class="col-xs-3 control-label">説明 : <br/><span style="font-size: 10px; color:gray;"></span></label>
                                    <div class="col-xs-5">
                                        <textarea class="form-control" name="description" rows="5"><?php echo $banner->description ?></textarea>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Publish Preview Wrapper -->
                        <div id="product_preview_wrapper">
                            <div class="form-group" style="text-align:center;">
                                <button type="submit" id="publish_product" name="publish_product" class="btn btn-success" style=""><i class="fa fa-camera"></i> 保存</button>
                                <a href="/banners" name="cancel_preview" class="btn btn-primary" style=""><i class="fa fa-times"></i> リセット</a>
                            </div>
                        </div>

                    </form>

                    <?php endif ?>

                </div>
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <!--                        <img class="fa" src="./images/lib/ajax-loader1.gif">-->
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>

<?= $this->Html->script('site/edit_banner.js?update=1505302411', ['block' => 'script']) ?>




