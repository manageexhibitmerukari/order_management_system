<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/banner_list.js?updated=1501130856', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        バナー管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="fa fa-file-image-o"></i> <b>バナー管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">バナー一覧</h3>

                    <div style="display: inline-block; margin:10px 10px 0 0;" class="pull-right">

                        <a href="/banners/add" class="btn btn-primary" >バナー追加 </a>
                    </div>
                </div>


                <div class="box-body table-responsive  with-border" id="main_tbl_wrap">

                </div>
                <button class="btn btn-danger remove-checked" style="margin: 25px;">削除</button>
                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>