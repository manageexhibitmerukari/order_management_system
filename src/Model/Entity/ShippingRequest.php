<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ShippingRequest Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $buyer_address
 * @property int $client_product_id
 * @property string $status
 * @property string $note
 * @property string $item_code
 * @property int $num_export
 * @property \Cake\I18n\Time $last_export
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ClientProduct $client_product
 */
class ShippingRequest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
