<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MainProduct Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $sku
 * @property string $product_title
 * @property string $description
 * @property int $category_id
 * @property string $photo
 * @property float $price
 * @property int $meruakri
 * @property int $furiru
 * @property int $rakuma
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\SubProduct[] $sub_products
 */
class MainProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
