<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Security;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $phone_number
 * @property int $level
 * @property string $note
 * @property \Cake\I18n\Time $reg_date
 * @property \Cake\I18n\Time $last_login
 * @property int $parent_user
 * @property int $order_process_day_limit
 * @property bool $created_by_admin
 * @property string $name
 * @property string $company
 * @property string $postal
 * @property string $state
 * @property string $town
 * @property string $building
 * @property int $address_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\ClientProduct[] $client_products
 * @property \App\Model\Entity\MainProduct[] $main_products
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\ShippingRequest[] $shipping_requests
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\Address $address
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password)
    {
        return Security::hash($password, 'md5', true);
    }
}
