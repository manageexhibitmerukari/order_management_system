<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserProduct Entity
 *
 * @property int $id
 * @property int $order_id
 * @property string $product_title
 * @property string $description
 * @property string $sku
 * @property string $product_photo1
 * @property string $product_photo2
 * @property string $product_photo3
 * @property string $product_photo4
 * @property float $price
 * @property int $amount
 * @property int $size_id
 * @property string $color
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Size $size
 */
class UserProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
