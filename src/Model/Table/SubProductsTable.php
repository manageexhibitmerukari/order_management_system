<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SubProducts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Sizes
 *
 * @method \App\Model\Entity\SubProduct get($primaryKey, $options = [])
 * @method \App\Model\Entity\SubProduct newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SubProduct[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SubProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SubProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SubProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SubProduct findOrCreate($search, callable $callback = null, $options = [])
 */
class SubProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sub_products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Sizes', [
            'foreignKey' => 'size_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('MainProducts', [
            'foreignKey' => 'parent_sku',
            'bindingKey' => 'sku',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('UserProducts', [
            'foreignKey' => 'sku',
            'bindingKey' => 'sku',
        ]);

        $this->hasMany('Carts', [
            'foreignKey' => 'sub_product_id',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('sku', 'create')
            ->notEmpty('sku');

        $validator
            ->requirePresence('parent_sku', 'create')
            ->notEmpty('parent_sku');

        $validator
            ->requirePresence('product_photo1', 'create')
            ->notEmpty('product_photo1');
//
//        $validator
//            ->requirePresence('product_photo2', 'create')
//            ->notEmpty('product_photo2');
//
//        $validator
//            ->requirePresence('product_photo3', 'create')
//            ->notEmpty('product_photo3');
//
//        $validator
//            ->requirePresence('product_photo4', 'create')
//            ->notEmpty('product_photo4');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->integer('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->integer('selling_term')
            ->allowEmpty('selling_term');

        $validator
            ->dateTime('selling_check_time')
            ->allowEmpty('selling_check_time');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['size_id'], 'Sizes'));

        return $rules;
    }
}
