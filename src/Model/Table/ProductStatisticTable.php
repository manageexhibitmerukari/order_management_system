<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductStatistic Model
 *
 * @method \App\Model\Entity\ProductStatistic get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProductStatistic newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProductStatistic[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProductStatistic|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductStatistic patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProductStatistic[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProductStatistic findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductStatisticTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('product_statistic');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->integer('product_count')
            ->requirePresence('product_count', 'create')
            ->notEmpty('product_count');

        return $validator;
    }
}
