<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MfCloud Model
 *
 * @method \App\Model\Entity\MfCloud get($primaryKey, $options = [])
 * @method \App\Model\Entity\MfCloud newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MfCloud[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MfCloud|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MfCloud patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MfCloud[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MfCloud findOrCreate($search, callable $callback = null, $options = [])
 */
class MfCloudTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('mf_cloud');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('access_token', 'create')
            ->notEmpty('access_token');

        $validator
            ->requirePresence('refresh_token', 'create')
            ->notEmpty('refresh_token');

        $validator
            ->dateTime('expire_date')
            ->requirePresence('expire_date', 'create')
            ->notEmpty('expire_date');

        return $validator;
    }
}
