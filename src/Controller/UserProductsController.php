<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/23/17
 * Time: 9:01 AM
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use ParseCSV\parseCSV;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\AddressesTable $Addresses
 * @property \App\Model\Table\ClientProductsTable $ClientProducts
 * @property \App\Model\Table\MainProductsTable $MainProducts
 *
 * @property \App\Controller\Component\EmailComponent $Email
 * @property \App\Controller\Component\MFCloudComponent $MFCloud
 */

class UserProductsController extends AppController
{
    public function isAuthorized($user) {
        if(in_array($this->request->action, ['showCart', 'managerOrder'])) {
            if(isset($user['created_by_admin']) && $user['created_by_admin']) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public $helpers = [ 'Paginator' => ['templates' => 'paginator-templates'] ];

    public $paginate = [];
    public function upload()
    {
        if ($this->request->is('ajax'))
        {
            $tableMain = TableRegistry::get('mainProducts');
            $this->loadModel('Users');
            $userId = $this->Auth->user('id');

            $target_dir = WWW_ROOT."files/upload/product/main/";
            if (!is_dir($target_dir)) {
                mkdir($target_dir, 0777,true);
            }

            $target_file = $target_dir . $_FILES["product_file"]["name"];

            $type = pathinfo($target_file,PATHINFO_EXTENSION);
            $target_file = $target_dir . $userId .'.'.$type;

            $result = "error";
            $msg = "";

            if (move_uploaded_file($_FILES["product_file"]["tmp_name"], $target_file))
            {

                $result = "success";
                $target_product = WWW_ROOT."files/upload/product/".$userId."/main/unzip/";

                if (!is_dir($target_product)) {
                    mkdir($target_product, 0777,true);
                }

                $zip = new \ZipArchive();

                $res = $zip->open($target_file);
                $this->deleteDir($target_product);
                if ($res == true)
                {
                    $zip->extractTo($target_product);
                    $zip->close();
                    $csvfile = Common::findCsvFilePath($target_product);
                    if (is_null($csvfile))
                    {
                        $result = "error";
                        $msg = "item.csv　ファイルは存在しない";
                    }
                    else
                    {
                        $target_product = dirname($csvfile).'/';
                        $encodeFile = $target_product.'encode.txt';
                        shell_exec('nkf -w "'.$csvfile.'" > "'.$encodeFile.'"');
                        $csv = new parseCSV();

                        $delimiter = $csv->auto($encodeFile);
                        $csv->delimiter = $delimiter;
                        $csv->parse($encodeFile);
                        $products = $csv->data;
                        if(count($products) == 0)
                        {
                            $csv = new parseCSV();
                            $csv->delimiter = ",";
                            $csv->parse($csvfile);

                            $products = $csv->data;
                        }
                        if (count($products) == 0) {
                            $result = "error";
                            $msg = "item.csv　ファイルは正しくない";

                        } else {
                            $items = $this->loadModel('ProductItems')->find();
                            $product_list = array();
                            foreach ($products as $key => $row)
                            {
                                $product_detail_path = WWW_ROOT . "img/upload/".$userId."/main/";
                                if ( !is_dir($product_detail_path) )
                                {
                                    mkdir($product_detail_path, 0777, true);
                                }
                                $photo = '';
                                $sku = '';
                                $description = '';
                                $price = '';
//                                $category = '';
                                $productName = '';

//                                $furiruKey = ['furiru_category_id', 'furiru_size_group_id', 'furiru_brand_id', 'furiru_shipping_charge_id', 'furiru_shipping_method_id', 'furiru_city_id', 'furiru_product_status_id', 'furiru_shipping_duration_id', 'furiru_request_required'];
//                                $mercariKey = ['categoryid', 'brandid', 'sizeid', 'status', 'carriageid', 'delivery_method', 'delivery_area', 'delivery_date'];
//                                $rakumaKey = ['rakuma_category', 'rakuma_size', 'rakuma_brand', 'rakuma_condition_type', 'rakuma_postage_type', 'rakuma_delivery_method', 'rakuma_prefecture_code', 'rakuma_delivery_term'];

                                $furiruCategory = null;
                                $furiruBrand = null;
                                $furiruProductStatus = null;
                                $furiruShippingCharge = null;
                                $furiruShippingMethod = null;
                                $furiruShippingDuration = null;
                                $furiruCity = null;
                                $furiruRequestRequired = null;

                                $category = null;
                                $itemCondition = null;
                                $shippingCharge = null;
                                $shippingMethod = null;
                                $senderAddress = null;
                                $shippingDuration = null;
                                $brand = null;

                                $rakumaCategory = null;
                                $rakumaBrand = null;
                                $rakumaConditionType = null;
                                $rakumaPostageType = null;
                                $rakumaDeliveryMethod = null;
                                $rakumaPrefectureCode = null;
                                $rakumaDeliveryTerm = null;


                                foreach ($items as $index => $item){
                                    $name = $item['itemname'];
                                    $field = $item['item'];
                                    if ($field == 'imageurl1') {
                                        if (isset($row[$name])) {
                                            if ( $row[$name] != "") {
                                                $photo = $row[$name];
                                            } else {
                                                $result = 'error';
                                                $msg = '行に商品の'. $name .'が空にしない';
                                                break;
                                            }
                                            if ( ! preg_match('/(\.jpg|\.png|\.jpeg|\.bmp)$/', strtolower($photo))) {
                                                $result = 'error';
                                                $msg = $name .' 写真のファイル名は正しくはありません。';
                                                break;
                                            }

                                            if (!is_file($target_product.$photo)){
                                                $result = "error";
                                                $msg = "(".$photo.") ファイルは存在しない";
                                                break;
                                            }

                                            if($result == "error")
                                            {
                                                break;
                                            }
                                        } else{
                                            $result = "error";
                                            $msg =  $name . 'カラムが存在しません';
                                            break;
                                        }

                                    }

                                    if ($field == 'title')
                                    {
                                        if (isset($row[$name])) {
                                            $productName = trim($row[$name]);

                                            $len = mb_strlen($productName, 'utf8');
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }

                                        if ($len > 40){
                                            $result = "error";
                                            $msg = ($key + 2)." 行に商品名が４０文字以上";
                                            break;
                                        }
                                        else if ($productName == ""){
                                            $result = "error";
                                            $msg = ($key + 2)." 行に商品名が空にしない";
                                            break;
                                        }
                                    }

                                    if ($field == 'detail')
                                    {
                                        if (isset($row[$name])) {
                                            $description = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg =  $name . 'カラムが存在しません';
                                            break;
                                        }
                                        if (mb_strlen($description,'utf8') > 1000)
                                        {
                                            $result = "error";
                                            $msg = ($key + 2)." 行に商品の説明が１０００文字以上しない";
                                            break;
                                        }
                                        else if ($description == ""){
                                            $result = "error";
                                            $msg = ($key + 2) . ' 行に商品の説明が空にしない';
                                            break;
                                        }
                                    }

                                    if ( $field == 'categoryid')
                                    {
                                        if (isset($row[$name])) {
                                            $category = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }

                                        if($category != "") {
                                            $ret = $this->loadModel('Categories')->exists(['id' => intval($category)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . ' 行目のカテゴリが正しくありません。';
                                                break;
                                            }
                                        } else {
                                            $result = "error";
                                            $msg = ($key + 2) . ' 行目のカテゴリが正しくありません。';
                                            break;
                                        }
                                    }

                                    if ($field == 'sell_price')
                                    {
                                        $price = $row[$name];
                                        if ($price <= 0 || $price == "" || !is_numeric($price))
                                        {
                                            $result = "error";
                                            $msg = ($key + 2)."行目の商品価格情報が正しくありません。";
                                            break;
                                        }
                                    }

                                    //check brand id...
                                    if ($field == "brandid") {
                                        $brand = $row[$name];

                                        if ($brand != "") {
                                            $ret = $this->loadModel('Brands')->exists(['id' => intval($brand)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のブランド情報が正しくありません。";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "status") {
                                        $itemCondition = $row[$name];

                                        if ($itemCondition != "") {
                                            $ret = $this->loadModel('ProductItemConditions')->exists(['id' => intval($itemCondition)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目の商品状能情報が正しくありません。";
                                                break;
                                            }
                                        } else {
                                            $result = "error";
                                            $msg = ($key + 2) . "行目の商品状能情報が正しくありません。";
                                            break;
                                        }
                                    }

                                    if ($field == "carriageid") {
                                        $shippingCharge = $row[$name];

                                        if ($shippingCharge != "") {
                                            $ret = $this->loadModel('ShippingCharges')->exists(['id' => intval($shippingCharge)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目の配送料負担情報が正しくありません。";
                                                break;
                                            }
                                        } else {
                                            $result = "error";
                                            $msg = ($key + 2) . "行目の商品状能情報が正しくありません。";
                                            break;
                                        }
                                    }

                                    if ($field == "delivery_method") {
                                        $shippingMethod = $row[$name];

                                        if ($shippingMethod != "") {
                                            $ret = $this->loadModel('ShippingMethods')->exists(['id' => intval($shippingMethod)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目の配送方法情報が正しくありません。";
                                                break;
                                            }
                                        } else {
                                            $result = "error";
                                            $msg = ($key + 2) . "行目の商品状能情報が正しくありません。";
                                            break;
                                        }
                                    }

                                    if ($field == "delivery_area") {
                                        $senderAddress = $row[$name];

                                        if ($senderAddress != "") {
                                            $ret = $this->loadModel('Cities')->exists(['id' => intval($senderAddress)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目の発送元の地域情報が正しくありません。";
                                                break;
                                            }
                                        } else {
                                            $result = "error";
                                            $msg = ($key + 2) . "行目の発送元の地域情報が正しくありません。";
                                            break;
                                        }
                                    }

                                    if ($field == "delivery_date") {
                                        $shippingDuration = $row[$name];

                                        if ($shippingDuration != "") {
                                            $ret = $this->loadModel('ShippingDurations')->exists(['id' => intval($shippingDuration)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目の発送までの日付情報が正しくありません。";
                                                break;
                                            }
                                        } else {
                                            $result = "error";
                                            $msg = ($key + 2) . "行目の発送までの日付情報が正しくありません。";
                                            break;
                                        }
                                    }

                                    if ($field == "furiru_category_id") {
                                        $furiruCategory = $row[$name];

                                        if ($furiruCategory != "") {
                                            $ret = $this->loadModel('FuriruCategories')->exists(['id' => intval($furiruCategory)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行のカテゴリが正しくありません。(フリル)";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "furiru_brand_id") {
                                        $furiruBrand = $row[$name];

                                        if ($furiruBrand != "") {
                                            $ret = $this->loadModel('FuriruBrands')->exists(['id' => intval($furiruBrand)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行のブランド情報が正しくありません。(フリル)";
                                                break;
                                            }
                                        }
                                    }


                                    if ($field == "furiru_product_status_id") {
                                        $furiruProductStatus = $row[$name];

                                        if ($furiruProductStatus != "") {
                                            $ret = $this->loadModel('FuriruProductStatus')->exists(['id' => intval($furiruProductStatus)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行の商品状能情報が正しくありません。(フリル)";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "furiru_shipping_charge_id") {
                                        $furiruShippingCharge = $row[$name];

                                        if ($furiruShippingCharge != "") {
                                            $ret = $this->loadModel('FuriruShippingCharges')->exists(['id' => intval($furiruShippingCharge)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行の配送料負担情報が正しくありません。(フリル)";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "furiru_shipping_method_id") {
                                        $furiruShippingMethod = $row[$name];

                                        if ($furiruShippingMethod != "") {
                                            $ret = $this->loadModel('FuriruShippingMethods')->exists(['shippingValue' => intval($furiruShippingMethod)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行の配送方法情報が正しくありません。(フリル)";
                                                break;
                                            }
                                        }
                                    }


                                    if ($field == "furiru_shipping_duration_id") {
                                        $furiruShippingDuration = $row[$name];

                                        if ($furiruShippingDuration != "") {
                                            $ret = $this->loadModel('FuriruShippingDurations')->exists(['id' => intval($furiruShippingDuration)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行の発送までの日付情報が正しくありません。(フリル)";
                                                break;
                                            }
                                        }
                                    }


                                    if ($field == "furiru_city_id") {
                                        $furiruCity = $row[$name];

                                        if ($furiruCity != "") {
                                            $ret = $this->loadModel('FuriruCities')->exists(['cityValue' => intval($furiruCity)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行の発送元の地域情報が正しくありません。";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "furiru_request_required")
                                    {
                                        $furiruRequestRequired = $row[$name];

                                        if ($furiruCity != "") {
                                            $ret = $this->loadModel('FuriruRequestRequires')->exists(['value' => intval($furiruRequestRequired)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 1) . "行の購入申請情報が正しくありません。";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_category")
                                    {
                                        $rakumaCategory = $row[$name];
                                        if ($rakumaCategory != "") {
                                            $ret = $this->loadModel('RakumaCategories')->exists(['id' => intval($rakumaCategory)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマカテゴリが正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_brand")
                                    {
                                        $rakumaBrand = $row[$name];
                                        if ($rakumaBrand != "") {
                                            $ret = $this->loadModel('RakumaBrand')->exists(['brand_id' => intval($rakumaBrand)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマブランド情報が正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_condition_type")
                                    {
                                        $rakumaConditionType = $row[$name];
                                        if ($rakumaConditionType != "") {
                                            $ret = $this->loadModel('RakumaProductStatus')->exists(['id' => intval($rakumaConditionType)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマ商品状能情報が正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_postage_type")
                                    {
                                        $rakumaPostageType = $row[$name];
                                        if ($rakumaPostageType != "") {
                                            $ret = $this->loadModel('RakumaShippingCharges')->exists(['id' => intval($rakumaPostageType)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマ配送料負担情報が正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_delivery_method")
                                    {
                                        $rakumaDeliveryMethod = $row[$name];
                                        if ($rakumaDeliveryMethod != "") {
                                            $ret = $this->loadModel('RakumaShippingMethods')->exists(['id' => intval($rakumaDeliveryMethod)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマ配送方法情報が正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_prefecture_code")
                                    {
                                        $rakumaPrefectureCode = $row[$name];
                                        if ($rakumaPrefectureCode != "") {
                                            $ret = $this->loadModel('RakumaCities')->exists(['cityValue' => intval($rakumaPrefectureCode)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマ発送元の地域情報が正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == "rakuma_delivery_term")
                                    {
                                        $rakumaDeliveryTerm = $row[$name];
                                        if ($rakumaDeliveryTerm != "") {
                                            $ret = $this->loadModel('RakumaShippingDurations')->exists(['id' => intval($rakumaDeliveryTerm)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のラクマ発送までの日付情報が正しくありません";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == 'sku')
                                    {
                                        $sku = trim($row[$name]);
                                        if (empty($sku))
                                        {
                                            $result = "error";
                                            $msg = ($key + 2) . '行に商品のSKUが空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'mercari')
                                    {
                                        $mercari = $row[$name];
                                    }

                                    if ($field == 'furiru')
                                    {
                                        $furiru = $row[$name];
                                    }

                                    if ($field == 'rakuma')
                                    {
                                        $rakuma = $row[$name];
                                    }
                                }

                                if ($result == "error") {
                                    break;
                                }

                                $publishRecord = array(
                                    'sku' => $sku,
                                    'user_id' => $userId,
                                    'product_title' => $productName,
                                    'description' => $description,
                                    'category_id' => $category,
                                    'photo' => $photo,
                                    'price' => $price,
                                    'merukari' => $mercari,
                                    'furiru' => $furiru,
                                    'rakuma' => $rakuma,
                                    'itemCondition' => (isset($itemCondition)) ? $itemCondition : 1,
                                    'shippingCharge' => (isset($shippingCharge)) ? $shippingCharge : 1,
                                    'shippingMethod' => (isset($shippingMethod)) ? $shippingMethod : 1,
                                    'senderAddress' => (isset($senderAddress)) ? $senderAddress : 1,
                                    'shippingDuration' => (isset($shippingDuration)) ? $shippingDuration : 1,
                                    'brand' => (isset($brand)) ? $brand : null,
                                    'furiru_category_id' => $furiruCategory,
                                    'furiru_brand_id' => $furiruBrand,
                                    'furiru_product_status_id' => $furiruProductStatus,
                                    'furiru_shipping_charge_id' => $furiruShippingCharge,
                                    'furiru_shipping_method_id' => $furiruShippingMethod,
                                    'furiru_shipping_duration_id' => $furiruShippingDuration,
                                    'furiru_city_id' => $furiruCity,
                                    'furiru_request_required' => $furiruRequestRequired,
                                    'rakuma_category' => $rakumaCategory,
                                    'rakuma_brand' => $rakumaBrand,
                                    'rakuma_condition_type' => $rakumaConditionType,
                                    'rakuma_postage_type' => $rakumaPostageType,
                                    'rakuma_delivery_method' => $rakumaDeliveryMethod,
                                    'rakuma_prefecture_code' => $rakumaPrefectureCode,
                                    'rakuma_delivery_term' => $rakumaDeliveryTerm,
                                );
                                $product_list =  array_merge($product_list, array($publishRecord));

                            }

                            if ($result != "error")
                            {
                                foreach ($product_list as $key=> $product)
                                {
                                    if ($tableMain->exists(['sku' => $product['sku']]))
                                        $this->updateMainProduct($product, $target_product);
                                    else
                                        $this->insertProductMain($product, $target_product);
                                }

                                $msg = "商品が追加成功";
                            }
                        }
                    }
                } else {
                    $result = 'error';
                    $msg = '';

                    switch ($res)
                    {
                        case \ZipArchive::ER_EXISTS:
                            $msg = 'File alrealy exists';
                            break;

                        case \ZipArchive::ER_INCONS:
                            $msg = 'Zip archive inconsistent';
                            break;

                        case \ZipArchive::ER_INVAL:
                            $msg = 'Invalid argument';
                            break;

                        case \ZipArchive::ER_NOENT:
                            $msg = 'No such file.';
                            break;

                        case \ZipArchive::ER_NOZIP:
                            $msg = 'アップしたファイルのフォーマットがZipじゃない';
                            break;
                    }
                }
            }  else {
                $msg = "商品ファイルがアップできない";
            }

            $this->set(compact('result','msg'));
            $this->set('_serialize', ['result','msg']);
        }
    }

    public function deleteProduct() {
        if ($this->request->is('ajax')) {
            $result = 'error';
            $tableMain = TableRegistry::get('mainProducts');
            $tableSub = TableRegistry::get('subProducts');
            $id = $this->request->data('id');
            $user_id = $this->Auth->user('id');
            $all = isset($this->request->data['all']) ? $this->request->data('all') : '';
            $msg = '';
            if ($all == true) {
                $where = ['user_id ' => $user_id];
                $msg = 'all';
            } else {
                $where = ['id IN ' => $id];
            }
            $result_main = $tableMain->find()->where($where)->toArray();

            $array_sku = [];
            $array_imagemain = [];

            foreach ($result_main as $row) {
                $array_imagemain[] = $row['photo'];
                $array_sku[] = $row['sku'];
            }

            $result_sub = $tableSub->find()
                ->where(['parent_sku IN' => $array_sku])
                ->toArray();
            $array_imagesub = [];

            foreach ($result_sub as $key=>$row ) {
                $array_imagesub[$key]['product_photo1'] = $row['product_photo1'];
                $array_imagesub[$key]['product_photo2'] = $row['product_photo2'];
                $array_imagesub[$key]['product_photo3'] = $row['product_photo3'];
                $array_imagesub[$key]['product_photo4'] = $row['product_photo4'];

                $tableSub->delete($row);
            }
            foreach ($array_imagesub as $row) {

                $this->check_same_image($row['product_photo1'], $tableSub, 'product_photo1');
                $this->check_same_image($row['product_photo2'], $tableSub, 'product_photo2');
                $this->check_same_image($row['product_photo3'], $tableSub, 'product_photo3');
                $this->check_same_image($row['product_photo4'], $tableSub, 'product_photo4');
            }

            foreach ($array_imagemain as $row) {
                if (!empty($row) && file_exists(WWW_ROOT . $row)) {
                    unlink(WWW_ROOT . $row);
                }
            }

            foreach ($result_main as $mainProduct)
                $tableMain->delete($mainProduct);

            $result = 'success';
            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result','msg']);
        }
    }

    public function deleteSubProduct()
    {
        if ($this->request->is('ajax'))
        {
            $result = 'error';
            $tableSub = TableRegistry::get('subProducts');
            $tableMain = TableRegistry::get('mainProducts');
            $id = $this->request->data('id');
            $all = isset($this->request->data['all']) ? $this->request->data('all') : '';
            $msg = '';

            if ($all == true) {
                $id_main = $this->request->data('id_parent');
                $parent_sku = $tableMain->find()->select(['sku'])->where(['id' => $id_main])->first();
                $where = ['parent_sku' => $parent_sku['sku']];
                $msg = 'all';
            } else {
                $where = ['id IN ' => $id];
            }

            $subProducts = $tableSub->find()->where($where);
            foreach ($subProducts as $row)
            {
                if ($tableSub->delete($row))
                {
                    $this->check_same_image($row['product_photo1'], $tableSub, 'product_photo1');
                    $this->check_same_image($row['product_photo2'], $tableSub, 'product_photo2');
                    $this->check_same_image($row['product_photo3'], $tableSub, 'product_photo3');
                    $this->check_same_image($row['product_photo4'], $tableSub, 'product_photo4');
                }
            }
            $result = 'success';

            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result','msg']);
        }
    }

    public function check_same_image($image, $table, $field) {
        if (!empty($image) && file_exists(WWW_ROOT . $image)){
            $count = $table->find()->where([$field => $image])->count();
            if ($count == 0 ){
                unlink(WWW_ROOT . $image);
            }
        }
    }

    public function uploadSubProduct()
    {
        if ($this->request->is('ajax'))
        {
            $tableMain = TableRegistry::get('mainProducts');
            $tableSub = TableRegistry::get('subProducts');
            $this->loadModel('Users');
            $userId = $this->Auth->user('id');

            $target_dir = WWW_ROOT."files/upload/product/sub/";
            if (!is_dir($target_dir)) {
                mkdir($target_dir, 0777,true);
            }

            $target_file = $target_dir . $_FILES["product_file_sub"]["name"];
            $type = pathinfo($target_file,PATHINFO_EXTENSION);
            $target_file = $target_dir .$userId .'.'.$type;

            $result = "error";
            $msg = "";

            if (move_uploaded_file($_FILES["product_file_sub"]["tmp_name"], $target_file)) {

                $result = "success";
                $target_product = WWW_ROOT . "files/upload/product/" . $userId . "/sub/unzip/";

                if (!is_dir($target_product)) {
                    mkdir($target_product, 0777, true);
                }

                $zip = new \ZipArchive();
                $res = $zip->open($target_file);
                $this->deleteDir($target_product);
                if ($res === true)
                {
                    $zip->extractTo($target_product);
                    $zip->close();
                    $csvfile = Common::findCsvFilePath($target_product);
                    if (is_null($csvfile)) {
                        $result = 'error';
                        $msg = 'item.csv　ファイルは存在しない';
                    } else {
                        $target_product = dirname($csvfile) . '/';
                        $encodeFile = $target_product.'encode.txt';
                        shell_exec('nkf -w "' . $csvfile . '" > "' . $encodeFile . '"');

                        $csv = new parseCSV();
                        $delimiter = $csv->auto($encodeFile);
                        $csv->delimiter = $delimiter;
                        $csv->parse($encodeFile);

                        $products = $csv->data;
                        if (count($products) == 0) {
                            $csv = new parseCSV();
                            $csv->delimiter = ",";
                            $csv->parse($csvfile);
                            $products = $csv->data;
                        }
                        if (count($products) == 0) {
                            $result = 'error';
                            $msg = 'item.csv　ファイルは正しくない';
                        } else {
                            $items = $this->loadModel("ProductItems")->find('all');
                            $product_list = array();

                            foreach ($products as $key => $row) {
                                $product_detail_path = WWW_ROOT."img/upload/".$userId."/sub/";
                                if (!is_dir($product_detail_path)) {
                                    mkdir($product_detail_path, 0777, true);
                                }
                                $imageurl_array = array();
                                $parent_sku = '';
                                $sku = '';
                                $color = '';
                                $quantity = 0;
                                $size = '';
                                $furiruSize = '';
                                $rakumaSize = '';
                                $price = '';

                                foreach ($items as $idx => $item) {
                                    $name = $item['itemname'];
                                    $field = $item['item'];

                                    if ($field == 'imageurl1') {
                                        if (isset($row[$name]) && $row[$name] != '') {
                                            if ( ! preg_match('/(\.jpg|\.png|\.jpeg|\.bmp)$/', strtolower($row[$name]))) {
                                                $result = 'error';
                                                $msg = $name .' 写真のファイル名は正しくはありません。';
                                                break;
                                            }
                                            $imageurl_array = array_merge($imageurl_array, array($row[$name]));
                                        } else {
                                            $result = 'error';
                                            $msg = '行に商品の'. $name .'が空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'imageurl2') {
                                        if (isset($row[$name]) && $row[$name] != '') {
                                            if ( ! preg_match('/(\.jpg|\.png|\.jpeg|\.bmp)$/', strtolower($row[$name]))) {
                                                $result = 'error';
                                                $msg = $name .' 写真のファイル名は正しくはありません。';
                                                break;
                                            }
                                            $imageurl_array = array_merge($imageurl_array, array($row[$name]));
                                        }
                                    }

                                    if ($field == 'imageurl3') {
                                        if (isset($row[$name]) && $row[$name] != '') {
                                            if ( ! preg_match('/(\.jpg|\.png|\.jpeg|\.bmp)$/', strtolower($row[$name]))) {
                                                $result = 'error';
                                                $msg = $name .' 写真のファイル名は正しくはありません。';
                                                break;
                                            }
                                            $imageurl_array = array_merge($imageurl_array, array($row[$name]));
                                        }
                                    }

                                    if ($field == 'imageurl4') {
                                        if (isset($row[$name]) && $row[$name] != '') {
                                            if ( ! preg_match('/(\.jpg|\.png|\.jpeg|\.bmp)$/', strtolower($row[$name]))) {
                                                $result = 'error';
                                                $msg = $name .' 写真のファイル名は正しくはありません。';
                                                break;
                                            }
                                            $imageurl_array = array_merge($imageurl_array, array($row[$name]));
                                        }

                                        if ( count($imageurl_array) <= 0 || count($imageurl_array) > 4) {
                                            $result = 'error';
                                            $msg = ($key + 2)." 行に写真数が正しくない";
                                            break;
                                        }

                                        for ($i = 0; $i < count($imageurl_array) ; $i++) {
                                            if ($imageurl_array[$i] == "")
                                                continue;

                                            if (!is_file($target_product.$imageurl_array[$i])) {
                                                $result = 'error';
                                                $msg = ($key + 2)." 行に".($i + 1)." 目の写真が存在しない";
                                                break;
                                            }
                                        }
                                        if ($result == 'error')
                                            break;
                                    }
                                    if ($field == 'sku') {

                                        if (isset($row[$name])) {
                                            $sku = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }

                                        if ($sku == '') {
                                            $result = 'error';
                                            $msg = ($key + 2) . ' 行に商品のSKUが空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'parent_sku') {

                                        if (isset($row[$name])) {
                                            $parent_sku = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }
                                        if ( ! $tableMain->exists(['sku' => $parent_sku])) {
                                            $result = 'error';
                                            $msg = ($key + 2) . '行に親SKU が存在しない';
                                            break;
                                        }
                                        if ($parent_sku == '') {
                                            $result = 'error';
                                            $msg = ($key + 2) . ' 行に親SKU が空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'sell_price') {

                                        if (isset($row[$name])) {
                                            $price = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }
                                        if ($price == '') {
                                            $result == 'error';
                                            $msg = ($key + 2) . ' 行に値段が空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'quantity') {

                                        if (isset($row[$name])) {
                                            $quantity = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }
                                        if ($quantity == '') {
                                            $result = 'error';
                                            $msg = ($key + 2) . ' 行に商品の数が空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'sizeid') {

                                        if (isset($row[$name])) {
                                            $size = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }
                                        if ($size == '') {
                                            $result = 'error';
                                            $msg = ($key + 2) . ' 行にサイズが空 にしない';
                                            break;
                                        }
                                        else
                                        {
                                            $ret = $this->loadModel('Sizes')->exists(['id' => intval($size)]);
                                            if (!$ret) {
                                                $result = "error";
                                                $msg = ($key + 2) . "行目のサイズ情報が正しくありません。";
                                                break;
                                            }
                                        }
                                    }

                                    if ($field == 'furiru_size_group_id') {

                                        if (isset($row[$name])) {
                                            $furiruSize = $row[$name];

                                            if ($furiruSize != '')
                                            {
                                                $ret = $this->loadModel('FuriruSizeGroups')->exists(['size_id' => intval($furiruSize)]);
                                                if (!$ret) {
                                                    $result = "error";
                                                    $msg = ($key + 1) . "行のサイズ情報が正しくありません。(フリル)";
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if ($field == 'rakuma_size') {

                                        if (isset($row[$name])) {
                                            $rakumaSize = $row[$name];

                                            if ($rakumaSize != '')
                                            {
                                                $ret = $this->loadModel('RakumaSize')->exists(['size_id' => intval($rakumaSize)]);
                                                if (!$ret) {
                                                    $result = "error";
                                                    $msg = ($key + 2) . "行目のラクマサイズ情報が正しくありません";
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if ($field == 'color') {
                                        if (isset($row[$name])) {
                                            $color = $row[$name];
                                        } else{
                                            $result = "error";
                                            $msg = $name . 'カラムが存在しません';
                                            break;
                                        }
                                        if ($color == '') {
                                            $result = 'error';
                                            $msg = ($key + 2) . ' 行に商品の色が空にしない';
                                            break;
                                        }
                                    }

                                    if ($field == 'selling_term')
                                    {
                                        if (isset($row[$name]))
                                            $sellingTerm = $row[$name];
                                    }
                                }

                                $record = array(
                                    'sku' => $sku,
                                    'parent_sku' => $parent_sku,
                                    'product_photo1' => isset($imageurl_array[0]) ? $imageurl_array[0] : '',
                                    'product_photo2' => isset($imageurl_array[1]) ? $imageurl_array[1] : '',
                                    'product_photo3' => isset($imageurl_array[2]) ? $imageurl_array[2] : '',
                                    'product_photo4' => isset($imageurl_array[3]) ? $imageurl_array[3] : '',
                                    'size_id' => $size,
                                    'furiru_size_id' => $furiruSize,
                                    'rakuma_size' => $rakumaSize,
                                    'price' => $price,
                                    'amount' => $quantity,
                                    'color' => $color,
                                    'selling_term' => (isset($sellingTerm)) ? $sellingTerm : 0
                                );

                                $product_list = array_merge($product_list, array($record));
                            }
                            if ($result != "error")
                            {
                                foreach ($product_list as $key => $product)
                                {
                                    if ($tableSub->exists(['sku' => $product['sku']]))
                                        $this->updateSubProduct($product, $target_product);
                                    else
                                        $this->insertProductSub($product, $target_product);
                                }

                                $msg = "商品が追加成功";
                            }
                        }
                    }
                } else {
                    $result = 'error';
                    $msg = '';
                    switch ($res)
                    {
                        case \ZipArchive::ER_EXISTS:
                            $msg = 'File alrealy exists';
                            break;

                        case \ZipArchive::ER_INCONS:
                            $msg = 'Zip archive inconsistent';
                            break;

                        case \ZipArchive::ER_INVAL:
                            $msg = 'Invalid argument';
                            break;

                        case \ZipArchive::ER_NOENT:
                            $msg = 'No such file.';
                            break;

                        case \ZipArchive::ER_NOZIP:
                            $msg = 'アップしたファイルのフォーマットがZipじゃない';
                            break;
                    }
                }

            } else {
                $msg = "商品ファイルがアップできない";
            }
            $this->set(compact('result','msg'));
            $this->set('_serialize', ['result','msg']);
        }
    }
    public function addMainProduct()
    {
        $cloneProductId = $this->request->query('clone_product_id');

        $tableMainProduct = TableRegistry::get('mainProducts');

        $mainProduct = null;

        if ($cloneProductId)
        {
            $mainProduct = $tableMainProduct->find()->where(['id' => $cloneProductId, 'user_id' => $this->Auth->user('id')])->first();

            if(is_null($mainProduct))
                $this->redirect(['action' => 'product-list']);
            else {
                $cloneImgUrls = [
                    $mainProduct->photo,
                    $mainProduct->photo_2,
                    $mainProduct->photo_3,
                    $mainProduct->photo_4,
                ];
            }

        }
        //dd($cloneImgUrls);

        if ($this->request->is('post'))
        {
            $mainProduct = $tableMainProduct->newEntity();

            $mainProduct->product_title = trim(mb_convert_kana($this->request->data('name'), "s"));
            $mainProduct->description = trim(mb_convert_kana($this->request->data('description'), "s"));
            $mainProduct->price = trim(mb_convert_kana($this->request->data('price'), "s"));
            $mainProduct->category_id = $this->request->data('category');
            $mainProduct->sku = $this->genererSku();

            $user_id = $this->Auth->user('id');
            $mainProduct->user_id = $user_id;
            $mainProduct->photo = '';
            if ($tableMainProduct->save($mainProduct))
            {
                $imgDir = 'img/upload/'.$user_id.'/main/';
                if (!is_dir(WWW_ROOT.$imgDir))
                    mkdir($imgDir, 0777, true);

                $id = $mainProduct->id;

                if (!empty($_FILES['photo_1']['tmp_name']) && $_FILES['photo_1']['tmp_name'] )
                {
                    $img = $_FILES['photo_1']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $mainProduct->photo = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (is_file(WWW_ROOT.$cloneImgUrls[0]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[0], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[0], WWW_ROOT.$newImgUrl))
                            $mainProduct->photo = '/'.$newImgUrl;
                    }
                }

                if (!empty($_FILES['photo_2']['tmp_name']) && $_FILES['photo_2']['tmp_name'] )
                {
                    $img = $_FILES['photo_2']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_2']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_2.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $mainProduct->photo_2 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[1]) && is_file(WWW_ROOT.$cloneImgUrls[1]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[1], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_2.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[1], WWW_ROOT.$newImgUrl))
                            $mainProduct->photo_2 = '/'.$newImgUrl;
                    }
                }

                if (!empty($_FILES['photo_3']['tmp_name']) && $_FILES['photo_3']['tmp_name'] )
                {
                    $img = $_FILES['photo_3']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_3']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_3.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $mainProduct->photo_3 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[2]) && is_file(WWW_ROOT.$cloneImgUrls[2]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[2], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_3.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[2], WWW_ROOT.$newImgUrl))
                            $mainProduct->photo_3 = '/'.$newImgUrl;
                    }
                }

                if (!empty($_FILES['photo_4']['tmp_name']) && $_FILES['photo_4']['tmp_name'] )
                {
                    $img = $_FILES['photo_4']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_4']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_4.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $mainProduct->photo_4 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[3]) && is_file(WWW_ROOT.$cloneImgUrls[3]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[3], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_4.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[3], WWW_ROOT.$newImgUrl))
                            $mainProduct->photo_4 = '/'.$newImgUrl;
                    }
                }

                $tableMainProduct->save($mainProduct); // Save path image
            }


            $this->redirect(['action' => 'product-list']);

        }

        $tableCategory = TableRegistry::get('Categories');


        $categories = $tableCategory->find()->toArray();

        $this->set(compact('categories', 'mainProduct'));
    }

    public function genererSku () {
        $tableMainProduct = TableRegistry::get('mainProducts');

        $skus = $tableMainProduct->find()->select(['sku'])->toArray();
        $arr_sku = [];
        foreach ($skus as $sku) {
            $arr_sku[] = $sku->sku;
        }

        $i = 1;
        $new_sku = '';
        while ( $i < 999999) {
            if ($i < 10) {
                $new_sku = 'SKU00000'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 100) {
                $new_sku = 'SKU0000'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 1000) {
                $new_sku = 'SKU000'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 10000) {
                $new_sku = 'SKU00'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 100000) {
                $new_sku = 'SKU0'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 1000000) {
                $new_sku = 'SKU'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            }

            $i++;
        }
        return $new_sku;
    }


    public function editMainProduct()
    {
        $tableMainProduct = TableRegistry::get('mainProducts');
        if ($this->request->is('post')) {


            $name = trim(mb_convert_kana($this->request->data('name'), "s"));
            $description = trim(mb_convert_kana($this->request->data('description'), "s"));
            $price = trim(mb_convert_kana($this->request->data('price'), "s"));
            $category = $this->request->data('category');
            $meruakri = trim(mb_convert_kana($this->request->data('merukari'), "s"));
            $furiru = trim(mb_convert_kana($this->request->data('furiru'), "s"));
            $rakuma = trim(mb_convert_kana($this->request->data('rakuma'), "s"));

            $id = $this->request->data('id');
            $user_id = $this->Auth->user('id');
            if (!empty($_FILES['photo_1']['tmp_name'])) {
                $img = $_FILES['photo_1']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/main/'.$id.'.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $data['photo'] = '/'.$img_url;
            }
            if (!empty($_FILES['photo_2']['tmp_name'])) {
                $img = $_FILES['photo_2']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_2']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/main/'.$id.'_2.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $data['photo_2'] = '/'.$img_url;
            }
            if (!empty($_FILES['photo_3']['tmp_name'])) {
                $img = $_FILES['photo_3']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/main/'.$id.'_3.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $data['photo_3'] = '/'.$img_url;
            }
            if (!empty($_FILES['photo_4']['tmp_name'])) {
                $img = $_FILES['photo_4']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/main/'.$id.'_4.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $data['photo_4'] = '/'.$img_url;
            }

            $data['product_title'] = $name;
            $data['description'] = $description;
            $data['price'] = $price;
            $data['category_id'] = $category;
            $data['merukari'] = $meruakri;
            $data['furiru'] = $furiru;
            $data['rakuma'] = $rakuma;

            $tableMainProduct->updateAll($data, ['id' => $id ]);
            $this->redirect(['action' => 'product-list']);

        }
        $id = $this->request->query('id');
        $product = $tableMainProduct->find()->where(['id' => $id])->first();
        $tableCategory = TableRegistry::get('Categories');

        $categories = $tableCategory->find()->toArray();

        $this->set(compact('product', 'categories'));
        $this->set('_serialize', ['product', 'categories']);
    }

    public function genererSkusub ($parent_sku = null) {

        if (!$parent_sku) {
            return false;
        }

        $tableMainProduct = TableRegistry::get('subProducts');

        $skus = $tableMainProduct->find()->select(['sku'])->where(['parent_sku' => $parent_sku])->toArray();
        $arr_sku = [];
        foreach ($skus as $sku) {
            $arr_sku[] = $sku->sku;
        }

        $i = 1;
        $new_sku = '';
        while ( $i < 999) {
            if ($i < 10) {
                $new_sku = 'SUB00'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 100) {
                $new_sku = 'SUB0'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            } elseif($i < 1000) {
                $new_sku = 'SUB'. $i;
                if (!in_array($new_sku, $arr_sku)) break;
            }

            $i++;
        }

        return $new_sku;
    }

    public function addSubProduct()
    {
        $cloneProductId = $this->request->query('clone_product_id');
        $mainProductId = $this->request->query('main_product_id');

        $this->loadModel('MainProducts');
        $userId = $this->Auth->user('id');
        if (is_null($mainProductId) || !$this->MainProducts->exists(['id' => $mainProductId, 'user_id' => $userId]))
            $this->redirect(['action' => 'product-list']);

        $mainProduct = $this->MainProducts->get($mainProductId);

        $subProductsTable = TableRegistry::get('SubProducts');

        $subProduct = null;

        if ($cloneProductId)
        {
            $subProduct = $subProductsTable->find()->where(['SubProducts.id' => $cloneProductId])->contain(['MainProducts' => function (Query $query) use ($userId) {
                return $query->where(['user_id' => $userId]);
            }])->first();

            if(is_null($subProduct))
                $this->redirect(['action' => 'product-sub-list', 'id' => $mainProductId]);
            else
                $cloneImgUrls = [
                    $subProduct->product_photo1,
                    $subProduct->product_photo2,
                    $subProduct->product_photo3,
                    $subProduct->product_photo4,
                ];
        }

        $error = null;

        if ($this->request->is('post'))
        {


            $exhibition_time = null;
            $date = $this->request->data('date');
            $time = $this->request->data('time');
            if ($date) {
                $time = strtotime($date.$time);
                $exhibition_time = date('Y-m-d H:i:s', $time);
            }
//            echo $exhibition_time; echo '<br>';
//            echo Time::now();
//            dd($exhibition_time > Time::now() );
            $subProduct = $subProductsTable->newEntity();

            echo
            $price = trim(mb_convert_kana($this->request->data('price'), "s"));
            $quantity = trim(mb_convert_kana($this->request->data('quantity'), "s"));
            $color = trim(mb_convert_kana($this->request->data('color'), "s"));
            $size = trim(mb_convert_kana($this->request->data('size'), "s"));
            $sellingTerm = trim(mb_convert_kana($this->request->data('selling_term'), "s"));
            $sku = $this->genererSkusub($mainProduct->sku);
            $buy_term = trim(mb_convert_kana($this->request->data('buy_term'), "s"));;

            $subProduct->exhibition_time = $exhibition_time;

            $color = ($color) ? $color : '無';

            if ($sellingTerm == '') {
                $subProduct->selling_term = null;
                $subProduct->selling_check_time = null;
            }
            elseif ($sellingTerm == 0)
            {
                $subProduct->exhibition_time = date('Y/m/d H:i:s', strtotime('2000/1/1'));
                $subProduct->selling_term = 0;
                $subProduct->selling_check_time = Time::now();


            } else
            {
                $subProduct->selling_term = $sellingTerm;
                if (strtotime($exhibition_time) > strtotime(Time::now())) {
                    $subProduct->selling_check_time = $exhibition_time;
                } else {
                    $subProduct->selling_check_time = Time::now();
                }
            }

            $subProductsTable->patchEntity($subProduct, [
                'sku' => $sku,
                'parent_sku' => $mainProduct->sku,
                'price' => $price,
                'amount'=> $quantity,
                'size_id' => $size,
                'color' => $color,
                //'selling_term' => $sellingTerm,
                //'selling_check_time' => (!empty($sellingTerm)) ? Time::now() : null,
                'product_photo1' => 'photo1',
                'buy_term' => $buy_term
            ]);

            if ($subProductsTable->save($subProduct))
            {
                $imgDir = 'img/upload/'.$userId.'/sub/';
                if (!is_dir(WWW_ROOT.$imgDir))
                    mkdir($imgDir, 0777, true);

                $id = $subProduct->id;

                $subProduct->sku = $sku. $id;

                if (!empty($_FILES['photo_1']['tmp_name']) && $_FILES['photo_1']['tmp_name'] )
                {
                    $img = $_FILES['photo_1']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_1.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $subProduct->product_photo1 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[0]) && is_file(WWW_ROOT.$cloneImgUrls[0]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[0], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_1.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[0], WWW_ROOT.$newImgUrl))
                            $subProduct->product_photo1 = '/'.$newImgUrl;
                    }
                }
                
                if (!empty($_FILES['photo_2']['tmp_name']) && $_FILES['photo_2']['tmp_name'] )
                {
                    $img = $_FILES['photo_2']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_2']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_2.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $subProduct->product_photo2 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[1]) && is_file(WWW_ROOT.$cloneImgUrls[1]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[1], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_2.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[1], WWW_ROOT.$newImgUrl))
                            $subProduct->product_photo2 = '/'.$newImgUrl;
                    }
                }
                
                if (!empty($_FILES['photo_3']['tmp_name']) && $_FILES['photo_3']['tmp_name'] )
                {
                    $img = $_FILES['photo_3']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_3']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_3.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $subProduct->product_photo3 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[2]) && is_file(WWW_ROOT.$cloneImgUrls[2]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[2], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_3.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[2], WWW_ROOT.$newImgUrl))
                            $subProduct->product_photo3 = '/'.$newImgUrl;
                    }
                }
                
                if (!empty($_FILES['photo_4']['tmp_name']) && $_FILES['photo_4']['tmp_name'] )
                {
                    $img = $_FILES['photo_4']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_4']['name'], PATHINFO_EXTENSION));
                    $img_url = $imgDir.$id.'_4.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $subProduct->product_photo4 = '/'.$img_url;
                }
                else if (isset($cloneImgUrls))
                {
                    if (!empty(WWW_ROOT.$cloneImgUrls[3]) && is_file(WWW_ROOT.$cloneImgUrls[3]))
                    {
                        $type = strtolower(pathinfo(WWW_ROOT.$cloneImgUrls[3], PATHINFO_EXTENSION));
                        $newImgUrl = $imgDir.$id.'_4.'.$type;
                        if (copy(WWW_ROOT.$cloneImgUrls[3], WWW_ROOT.$newImgUrl))
                            $subProduct->product_photo4 = '/'.$newImgUrl;
                    }
                }

                $subProductsTable->save($subProduct); // Save path image
            }

            $this->redirect(['action' => 'product-sub-list', 'id' => $mainProductId]);

        }

        $sizes = $this->loadModel('Sizes')->find()->toArray();

        $this->set(compact('sizes', 'subProduct', 'error'));
    }

    public function editSubProduct() {

        $tableSubProduct = TableRegistry::get('subProducts');
        $tableMainProduct = TableRegistry::get('mainProducts');
        $tableSize = TableRegistry::get('sizes');
        $user_id = $this->Auth->user('id');

        $exhibition_time = null;
        $date = $this->request->data('date');
        $time = $this->request->data('time');

        $search = $this->request->query('search');
        $title_search = $this->request->query('title_search');
        $status_search = $this->request->query('status_search');

       // dd($search);
        if ($date) {
            $time = strtotime($date.$time);
            $exhibition_time = date('Y-m-d H:i:s', $time);
        }

        if ($this->request->is('post')) {

            $price = trim(mb_convert_kana($this->request->data('price'), "s"));
            $quantity = trim(mb_convert_kana($this->request->data('quantity'), "s"));
            $color = trim(mb_convert_kana($this->request->data('color'), "s"));
            $size = trim(mb_convert_kana($this->request->data('size'), "s"));
            $id = $this->request->data('id');
            $sellingTerm = trim(mb_convert_kana($this->request->data('selling_term'), "s"));
            $buy_term = trim(mb_convert_kana($this->request->data('buy_term'), "s"));


            $color = ($color) ? $color : '無';

            if (!empty($_FILES['photo_1']['tmp_name'])) {
                $img = $_FILES['photo_1']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/sub/'.$id.'_1.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $where['product_photo1'] = '/'.$img_url;
            }
            if (!empty($_FILES['photo_2']['tmp_name'])) {
                $img = $_FILES['photo_2']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_2']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/sub/'.$id.'_2.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $where['product_photo2'] = '/'.$img_url;
            }
            if (!empty($_FILES['photo_3']['tmp_name'])) {
                $img = $_FILES['photo_3']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_3']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/sub/'.$id.'_3.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $where['product_photo3'] = '/'.$img_url;
            }
            if (!empty($_FILES['photo_4']['tmp_name'])) {
                $img = $_FILES['photo_4']['tmp_name'];
                $type = strtolower(pathinfo($_FILES['photo_4']['name'], PATHINFO_EXTENSION));
                $img_url = 'img/upload/'.$user_id.'/sub/'.$id.'_4.'.$type;
                file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                $where['product_photo4'] = '/'.$img_url;
            }
            $where['price'] = $price;
            $where['color'] = $color;
            $where['amount'] = $quantity;
            $where['size_id'] = $size;

//            $tableSubProduct->updateAll($where, ['id' => $id ]);

            $subProduct = $tableSubProduct->get($id);
            $tableSubProduct->patchEntity($subProduct, [
                'price' => $price,
                'amount'=> $quantity,
                'size_id' => $size,
                'color' => $color
            ]);

            $subProduct->exhibition_time = $exhibition_time;

            //dd(strtotime($exhibition_time) > strtotime(Time::now()));
            if ($sellingTerm == '') {
                $subProduct->selling_term = null;
                $subProduct->selling_check_time = null;
            }
            elseif ($sellingTerm == 0)
            {
                $subProduct->selling_term = 0;
                $subProduct->selling_check_time = Time::now();
                $subProduct->exhibition_time = date('Y/m/d H:i:s', strtotime('2000/1/1'));

            } else
            {
                $subProduct->selling_term = $sellingTerm;
                if (strtotime($exhibition_time) > strtotime(Time::now())) {
                    $subProduct->selling_check_time = $exhibition_time;
                } else {
                    $subProduct->selling_check_time = Time::now();
                }
            }

            $subProduct->buy_term = $buy_term;
            if (isset($where['product_photo1']))
                $subProduct->product_photo1 = $where['product_photo1'];
            if (isset($where['product_photo2']))
                $subProduct->product_photo2 = $where['product_photo2'];
            if (isset($where['product_photo3']))
                $subProduct->product_photo3 = $where['product_photo3'];
            if (isset($where['product_photo4']))
                $subProduct->product_photo4 = $where['product_photo4'];

            $tableSubProduct->save($subProduct);

            $parent_sku = $tableSubProduct->find()->select(['parent_sku'])->where(['id' => $id])->first();
            $productMain = $tableMainProduct->find()->select(['id'])->where(['sku' => $parent_sku['parent_sku']])->first();

            if ($search) {
                $this->redirect(['action' => 'search-sub-product', 'title_search' => $title_search, 'status_search' => $status_search]);
            } else {
                $this->redirect(['action' => 'product-sub-list', 'id' => $productMain['id']]);
            }



        }
        $id = $this->request->query('id');
        $product = $tableSubProduct->find()->where(['id' => $id])->first();
        $sizes = $tableSize->find()->toArray();
        $this->set(compact('product', 'sizes'));
        $this->set('_serialize', ['product', 'sizes']);
    }

    public function productList()
    {
        $msg = '';
        if ($this->request->is('ajax')) {
            $tableMainProduct = TableRegistry::get('mainProducts');
            $tableSubProduct = TableRegistry::get('subProducts');
            $products = [];
            $user_id = $this->Auth->user('id');
            $result = $tableMainProduct->find()->where(['user_id' => $user_id])->toArray();

            foreach ($result as $row) {
                $subProducts = $tableSubProduct->find()
                    ->select(['color', 'amount'])
                    ->where(['parent_sku' => $row['sku']])
                    ->toArray();
                $sumProduct = 0;
                $color = [];
                foreach ($subProducts as $value) {
                    $sumProduct+= $value['amount'];
                    $i = 0;
                    foreach ($color as $row1) {
                        if (strtolower($row1) == strtolower($value['color'])) {
                            $i = 1;
                        }
                    }
                    if ($i == 0)
                        $color[] = $value['color'];
                }


                $product = array(
                    'id' => $row['id'],
                    'SKU' => $row['sku'],
                    'photo' => $row['photo'],
                    'name' => $row['product_title'],
                    'color' => $color,
                    'price' => $row['price'],
                    'merukari' => $row['merukari'],
                    'furiru' => $row['furiru'],
                    'rakuma' => $row['rakuma'],
                    'amount' => $sumProduct
                );
                array_push($products, $product);
            }
            $this->set(compact('products'));
            $this->set('_serialize', ['products']);
        }
        $this->set(compact('msg'));
    }

    public function productSubList() {

        if ($this->request->is('ajax')) {
            $id = $this->request->data('id');
            $tableMain = TableRegistry::get('mainProducts');
            $tableSize = TableRegistry::get('sizes');
            $product = $tableMain->find()->where(['id' => $id])->first();
            $sku = $product['sku'];
            $tableSub = TableRegistry::get('subProducts');
            $subs = $tableSub->find()->where(['parent_sku' => $sku])->toArray();
            $products = [];

            foreach ($subs as $row)
            {
                $size = $tableSize->find()->where(['id' => $row['size_id']])->first();
                $row['sizeName'] = $size['sizeName'];
                $hours = 0;
                if ($row->exhibition_time && strtotime($row->exhibition_time) != strtotime('2000/1/1')) {
                    $row['exhibition_time'] = (string)date('Y/m/d H:i:s', strtotime($row->exhibition_time));
                } else {
                    $row['exhibition_time'] = '';
                }


                if (isset($row->selling_check_time) && isset($row->selling_term))
                {

                    $modifiedTime = $row->selling_check_time;
                    $expiredTime = $modifiedTime->modify('+' . $row->selling_term . ' hours');
                    if (strtotime($row->selling_check_time) > strtotime(Time::now())) {
                        $diffTime = date_diff($row->selling_check_time, $expiredTime);
                    } else {
                        $diffTime = date_diff(Time::now(), $expiredTime);
                    }


                    if ($diffTime->invert)
                        $row->remainingTime = 'あと0';
                    else
                    {
//                        $hours = $diffTime->h + ($diffTime->d * 24) + ($diffTime->m * 30 * 24) + ($diffTime->y * 365 * 24);
                        $hours = $diffTime->h + ($diffTime->days * 24);
                        $row->remainingTime = "あと".$hours . ":";
                        if ($diffTime->i >= 0 && $diffTime->i < 10)
                            $row->remainingTime .= '0' . $diffTime->i;
                        else
                            $row->remainingTime .= $diffTime->i;
                    }
                }
                else
                    $row->remainingTime = '';

                $products[] = $row;
            }

            $mainProductId = $id;
            $this->set(compact('products', 'mainProductId'));
            $this->set('_serialize', ['products', 'mainProductId']);
        }
        else
        {
            $mainProductId = $this->request->query('id');
            $this->set(compact('mainProductId'));
            $this->set('_serialize', ['mainProductId']);
        }
    }

    public function top() {
        $this->viewBuilder()->layout('user');

        $tableCategory = TableRegistry::get('Categories');
        $tableMainProduct = TableRegistry::get('mainProducts');
        $tableSubProduct = TableRegistry::get('subProducts');
        $breadcrume = [];
        $keyword = null;
        $parentCategoryId = $this->request->query('category-parent');
        $childCategoryId = $this->request->query('category-child');

        $pageName = "ホーム";

        if (isset($parentCategoryId) && $parentCategoryId != 0)
            $childCategories = $tableCategory->find()->select(['id', 'categoryName', 'parentID'])->where(['parentID' => $parentCategoryId])->toArray();
        else
            $childCategories = [];

        if ($this->request->is('GET')) {

            $tableCategory = TableRegistry::get('Categories');
            $tableMain = TableRegistry::get('mainProducts');
            if (isset($this->request->query['keyword'])) {

                $pageName = "検索結果";

                $keyword = $this->request->query('keyword');
                if ($parentCategoryId === 0) {
                    $where = ['product_title like ' => '%' . $keyword . '%'];
                } else if ($childCategoryId == '') {
                    $array_id = [$parentCategoryId];
                    $child_id = [$parentCategoryId];
                    $i = 0;
                    do {
                        $child = [];
                        $result = $tableCategory->find()->select(['id'])->where(['parentID IN' => $child_id])->toArray();
                        $i++;
                        foreach ($result as $row) {
                            $array_id[] = $row['id'];
                            $child[] = $row['id'];
                        }
                        $child_id = $child;

                    } while (count($child) > 0);
                    $name_parent_category = $tableCategory->find()->where(['id' => $parentCategoryId])->first();
                    $breadcrume[] = $name_parent_category['categoryName'];
                    $where = [['product_title like ' => '%' . $keyword . '%'], ['category_id IN' => $array_id]];
                } else {
                    $name_parent_category = $tableCategory->find()->where(['id' => $parentCategoryId])->first();
                    $name_child_category = $tableCategory->find()->where(['id' => $childCategoryId])->first();
                    $breadcrume[] = $name_parent_category['categoryName'];
                    $breadcrume[] = $name_child_category['categoryName'];
                    $array_id = [$childCategoryId];
                    $child_id = [$childCategoryId];
                    $i = 0;
                    do {
                        $child = [];
                        $result = $tableCategory->find()->select(['id'])->where(['parentID IN' => $child_id])->toArray();
                        $i++;
                        foreach ($result as $row) {
                            $array_id[] = $row['id'];
                            $child[] = $row['id'];
                        }
                        $child_id = $child;

                    } while (count($child) > 0);
                    $where = [['product_title like ' => '%' . $keyword . '%'], ['category_id IN' => $array_id]];
                }
                $endBreadcrume = '「'. $keyword .'」の検索結果';
                $this->paginate = [
                    'limit' => 40
                ];

                $products = $tableMain->find()->where($where);
                $products->contain(['Categories'])

                    ->innerJoinWith('SubProducts', function (Query $query) {
                        return $query
                            ->where([
                                'OR' => [ ['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) >' => date('Y-m-d H:i:s')], ['selling_check_time is' => null]
                                ],
                            ])
                            ->andWhere([
                                'OR' => [ ['selling_term >' => 0], ['selling_term is' => null] ]
                            ]);

                    })
                    ->select([
                        'sub_product_amount' => $products->func()->sum('SubProducts.amount'),
                        'max_exhibition_time' => $products->func()->max('SubProducts.exhibition_time'),
                    ])
                    ->select($tableMainProduct)
                    ->select($tableCategory)
                    ->order(['sub_product_amount' => 'DESC'])
                    ->group('mainProducts.id');
               // dd($products->toArray());
                $count = count($products->toArray());
                $products = $this->paginate($products);
            } else {
                $this->paginate = [
                    'limit' => 40
                ];

                $products = $tableMainProduct->find();
                $products->contain(['Categories'])

                    ->innerJoinWith('SubProducts', function (Query $query) {
                        return $query
                            ->where([
                                'OR' => [ ['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) >' => date('Y-m-d H:i:s')], ['selling_check_time is' => null]
                                ],
                            ])
                            ->andWhere([
                                'OR' => [ ['selling_term >' => 0], ['selling_term is' => null] ]
                            ]);

                    })
                    ->select([
                        'sub_product_amount' => $products->func()->sum('SubProducts.amount'),
                        'max_exhibition_time' => $products->func()->max('SubProducts.exhibition_time'),
                    ])
                    ->select($tableMainProduct)
                    ->select($tableCategory)
                    ->order(['sub_product_amount' => 'DESC'])
                    ->group('mainProducts.id');
                //dd($products->toArray());
                $count = count($products->toArray());
                $products = $this->paginate($products);
            }

        }
        $categories = $tableCategory->find()->where(['parentId' => 0 ])->toArray();

        $tableBanners = TableRegistry::get('Banners');

        $data = $tableBanners->find()->where(['position IS NOT' => null, 'type' => 'main_banner'])->order(['position' => 'ASC']);

        $data2 = $tableBanners->find()->where(['position IS' => null, 'type' => 'main_banner' ])->order(['id' => 'DESC']);

        $banners = [];
        if($data) {
            foreach ($data as $row) {

                $banner = array(
                    'id' => $row->id,
                    'title' => $row->title,
                    'link' => $row->link,
                    'description' => $row->description,
                    'image' => $row->image,
                    'type' => $row->type,
                    'position' => $row->position
                );
                array_push($banners, $banner);
            }
        }

        if($data2) {
            foreach ($data2 as $row2) {

                $banner = array(
                    'id' => $row2->id,
                    'title' => $row2->title,
                    'link' => $row2->link,
                    'description' => $row2->description,
                    'image' => $row2->image,
                    'type' => $row2->type,
                    'position' => $row2->position
                );
                array_push($banners, $banner);
            }
        }

        $this->set(compact('categories', 'products_exhibition', 'products', 'count', 'breadcrume', 'endBreadcrume', 'keyword', 'parentCategoryId', 'childCategoryId', 'childCategories','pageName','banners'));
        $this->set('_serialize', ['categories', 'products_exhibition', 'products', 'count', 'breadcrume', 'endBreadcrume', 'keyword', 'parentCategoryId', 'childCategoryId', 'childCategories', 'pageName', 'banners']);
    }

    public function addProduct() {
        $result = '';
        $msg = '';
        if ($this->request->is('ajax'))
        {
            $userId = $this->Auth->user('id');

            if (!is_null($userId))
            {
                $tableSub = TableRegistry::get('subProducts');
                $tableCart = TableRegistry::get('carts');

                $color = $this->request->data('color');
                $size = $this->request->data('size');
                $amount = $this->request->data('amount');
                $parent_sku = $this->request->data('sku');
                $product_rest = 0;

                $product = $tableSub->find()->select(['id', 'sku', 'amount', 'price'])->where(['id' => trim($color), 'size_id' => $size, 'parent_sku' => $parent_sku])->first();
                $cartProduct = $tableCart->find()->where(['user_id' => $this->Auth->user('id'), 'sku' => $product['sku']])->first();

                if ($amount > $product['amount']) {
                    $result = 'error';
                    $product_rest = $product['amount'];
                } else {
                    if ($cartProduct) {
                        if (($amount + $cartProduct['amount']) > $product['amount']) {
                            $result = 'error';
                            $product_rest = $product['amount'] - $cartProduct['amount'];
                        }
                    }
                }

                if ($result == 'error') {
                    $msg = '入力可能数量は、' . $product_rest . '以下です。';
                } else {
                    $result = 'success';
                    $msg = 'カートに追加しました';

                    if ($cartProduct) {
                        $cartProduct['amount'] += $amount;
                        $tableCart->save($cartProduct);
                    } else {
                        $cartProduct = $tableCart->newEntity([
                            'user_id' => $this->Auth->user('id'),
                            'sub_product_id' => $product['id'],
                            'amount' => $amount,
                            'sell_price' => $product['price'],
                            'sku' => $product['sku'],
                        ]);

                        $tableCart->save($cartProduct);
                    }

                    $carts = $this->loadModel('Carts');
                    $countProduct = $carts->find()->where(['user_id' => $this->Auth->user('id')])->count();
                }
            }
            else
            {
                $result = 'error';

                $msg = '購入に進むにはログインが必要です。';
            }
        }
        $this->set(compact('result', 'msg', 'countProduct'));
        $this->set('_serialize', ['result', 'msg', 'countProduct']);
    }

    public function showCart() {

        $this->viewBuilder()->layout('user');
        $status = '';
        $tableSub = TableRegistry::get('subProducts');
        $tableMain = TableRegistry::get('mainProducts');
        $tableSize = TableRegistry::get('sizes');
        $tableCart = TableRegistry::get('carts');

        $pageName = "仕入れカート";
        $products = [];

        $carts = $tableCart->find()->where(['user_id' => $this->Auth->user('id')])->toArray();

        foreach ($carts as $row)
        {
            $product = $tableSub->find()->select(['id', 'product_photo1', 'price', 'color', 'size_id', 'sku', 'parent_sku','amount'])->where(['sku' => $row['sku']])->first();

            $product_main = $tableMain->find()->select(['id', 'product_title', 'description'])->where(['sku' => $product['parent_sku']])->first();

            $size = $tableSize->find()->select(['sizeName'])->where(['id' => $product['size_id']])->first();
            $product['quantity'] = $row['amount'];
            $product['sku'] = $row['sku'];
            $product['name'] = $product_main['product_title'];
            $product['sizeName'] = $size['sizeName'];
            $product['main_product_id'] = $product_main['id'];
            $product['cart_id'] = $row['id'];

            $products[] = $product;
        }
        $totalPrice = 0;

        foreach ($products as $item) {
            $totalPrice += $item->price * $item->quantity;
        }

        $totalPrice = round($totalPrice + $totalPrice * 0.08, 2);

        $this->loadModel('Addresses');
        $this->loadModel('Users');
        $user = $this->Users->get($this->Auth->user('id'));
        $address = $this->Addresses->get($user->address_id);


        $fee = $this->getShipping_fee($address->zip_code,$totalPrice);

        $this->set(compact('products', 'status', 'address', 'pageName', 'fee'));
        $this->set('_serialize', ['products', 'status', 'address', 'pageName', 'fee']);
    }


    public function getShipping_fee($zip_code, $price) {

        $fee_ship = 0;
        $fee_up = 0;
        $price_order = 0;
        $fee_default = 0;
        $zip_code = str_replace('-', '', $zip_code);
        $temp_zip_code = $zip_code;
        $tableShippingFee = TableRegistry::get('shippingFee');
        $shippingFeeAll = $tableShippingFee->find()->where(['type' => 'all'])->first();
        $shippingFeeArea = $tableShippingFee->find()->where(['type' => 'area']);

        /*
         * Lấy danh sach khu vực đã setting giá
         * Tìm kiếm xem zipcode truyền vào bằng zipcode của khu vực trong database hay không
         * Kiểm tra giá đơn hàng truyền vào:
         *      $price > $price_order : nếu lớn hơn giá đơn hàng trong database thì tính giá ship = $fee_up
         *      ngược lại tính giá ship = $fee_default
         *
         */
        if($shippingFeeAll) {

            $flag = true;

            if(count($shippingFeeArea->toArray()) > 0) {
                foreach ($shippingFeeArea as $fee_area) {
                    $list_area = explode(',', $fee_area->area);
                    $fee_default = (int)$fee_area->fee_default;
                    $price_order = (int)$fee_area->price_order;
                    $fee_up = (int)$fee_area->fee_up;

                    foreach ($list_area as $area) {
                        $zip_code = $temp_zip_code;
                        $length_area = strlen($area);
                        $zip_code = substr(trim($zip_code), 0,$length_area);

                        if($zip_code == $area) {

                            $flag = false;

                            if ($price >= $price_order) {
                                $fee_ship = $fee_up;

                                if($fee_ship == 0 && $price_order == 0) {
                                    $fee_ship = $fee_default;
                                }
                            } else {
                                $fee_ship = $fee_default;
                            }
                        }

                    }
                    if($fee_ship > 0 || !$flag) {
                        break;
                    }
                }
            }

            /**
             * Nếu sau khi tìm kiếm giá ship vẫn bằng không thì lấy giá ship chung của tất cả khu vực
             * */
            if($fee_ship == 0 && $flag) {
                $fee_default = (int)$shippingFeeAll->fee_default;
                $price_order = (int)$shippingFeeAll->price_order;
                $fee_up = (int)$shippingFeeAll->fee_up;

                if ($price >= $price_order) {

                    $fee_ship = $fee_up;

                    if($fee_ship == 0 && $price_order == 0 ) {
                        $fee_ship = $fee_default;
                    }
                } else {
                    $fee_ship = $fee_default;
                }
            }
        }

        $fee = [];
        $fee['fee_up'] = $fee_up;
        $fee['price_order'] = $price_order;
        $fee['fee_default'] = $fee_default;
        return $fee;

    }

    public function checkShipping_fee($zip_code, $price) {

        $fee_ship = 0;
        $zip_code = str_replace('-', '', $zip_code);
        $temp_zip_code = $zip_code;
        $tableShippingFee = TableRegistry::get('shippingFee');
        $shippingFeeAll = $tableShippingFee->find()->where(['type' => 'all'])->first();
        $shippingFeeArea = $tableShippingFee->find()->where(['type' => 'area']);

        /*
         * Lấy danh sach khu vực đã setting giá
         * Tìm kiếm xem zipcode truyền vào bằng zipcode của khu vực trong database hay không
         * Kiểm tra giá đơn hàng truyền vào:
         *      $price > $price_order : nếu lớn hơn giá đơn hàng trong database thì tính giá ship = $fee_up
         *      ngược lại tính giá ship = $fee_default
         *
         */
        if($shippingFeeAll) {

            $flag = true;

            if(count($shippingFeeArea->toArray()) > 0) {
                foreach ($shippingFeeArea as $fee_area) {
                    $list_area = explode(',', $fee_area->area);
                    $fee_default = (int)$fee_area->fee_default;
                    $price_order = (int)$fee_area->price_order;
                    $fee_up = (int)$fee_area->fee_up;

                    foreach ($list_area as $area) {
                        $zip_code = $temp_zip_code;
                        $length_area = strlen($area);
                        $zip_code = substr(trim($zip_code), 0,$length_area);

                        if($zip_code == $area) {

                            $flag = false;

                            if ($price >= $price_order) {
                                $fee_ship = $fee_up;

                                if($fee_ship == 0 && $price_order == 0) {
                                    $fee_ship = $fee_default;
                                }
                            } else {
                                $fee_ship = $fee_default;
                            }
                        }

                    }
                    if($fee_ship > 0 || !$flag) {
                        break;
                    }
                }
            }

            /**
             * Nếu sau khi tìm kiếm giá ship vẫn bằng không thì lấy giá ship chung của tất cả khu vực
             * */
            if($fee_ship == 0 && $flag) {
                $fee_default = (int)$shippingFeeAll->fee_default;
                $price_order = (int)$shippingFeeAll->price_order;
                $fee_up = (int)$shippingFeeAll->fee_up;

                if ($price >= $price_order) {

                    $fee_ship = $fee_up;

                    if($fee_ship == 0 && $price_order == 0 ) {
                        $fee_ship = $fee_default;
                    }
                } else {
                    $fee_ship = $fee_default;
                }
            }
        }

        return $fee_ship;

    }

    public function search() {
        $this->viewBuilder()->layout('search');
        $tableCategory = TableRegistry::get('Categories');
        $tableMainProduct = TableRegistry::get('mainProducts');
        $tableSubProduct = TableRegistry::get('subProducts');
        $tableSize = TableRegistry::get('sizes');

        if ($this->request->is('get')) {
            $keyword = isset($this->request->query['keyword']) ? $this->request->query['keyword'] : '';
            $category = isset( $this->request->query['category'] ) ? $this->request->query('category') : '';
            $size = isset($this->request->query['size_group']) ? $this->request->query['size_group'] : '';
            $this->paginate = [
                'limit' => 40
            ];
            if ($keyword != '' || $category != '' || $size != '') {
                $where = [];
                if ($category != '') {
                    $parent = [$category];
                    $array_id = $parent;
                    do {
                        $child = $tableCategory->find()->select(['id'])->where(['parentID IN ' => $parent])->toArray();
                        $parent = [];
                        foreach ($child as $row) {
                            $array_id[] = $row['id'];
                            $parent[] = $row['id'];
                        }

                    } while(count($child) > 0);

                    $where[] = ['category_id IN' => $array_id];
                }

                if ($keyword != '')
                    $where[] = ['product_title like' => '%' . $keyword . '%'];

                if ($size != '') {
                    $array_size = array();
                    $result = $tableSubProduct->find()->select(['parent_sku'])->where(['size_id' => $size])->toArray();

                    if ($result) {
                        foreach ($result as $row) {
                            $array_size[] = $row['parent_sku'];
                        }
                    }

                    if (empty($array_size))
                        $where = ['1=0'];
                    else
                        $where[] = ['sku IN' => $array_size];
                }
                $products = $tableMainProduct->find()->where($where);
                $products = $this->paginate($products);
            }
        }
        $categories = $tableCategory->find()->toArray();
        $parent_category = [];
        foreach ($categories as $row) {
            if ($row['parentID'] == 0)
                $parent_category[] = $row;
        }
        do {
            $tree_category = $this->findCategory($parent_category, $categories);
            $parent_category = $tree_category['parent'];
            $child = $tree_category['child'];
        } while(count($child) > 0);

        $size = $tableSize->find()->toArray();
        $product_new = $tableMainProduct->find()->order(['id' => 'ASC'])->limit(40);


        $this->set(compact('parent_category', 'products', 'size', 'product_new'));
        $this->set('_serialize', ['parent_category', 'products', 'size', 'product_new']);
    }

    public function deleteCart()
    {
        if ($this->request->is('ajax'))
        {
            $cartId = $this->request->data('cart_id');

            $cartsModel = $this->loadModel('Carts');

            $cartsModel->deleteAll(['user_id' => $this->Auth->user('id'), 'id' => $cartId]);

            $countProduct = $cartsModel->find()->where(['user_id' => $this->Auth->user('id')])->count();

            $this->set(compact('countProduct'));
            $this->set('_serialize', ['countProduct']);
        }
    }

    public function detailProduct() {
        $this->viewBuilder()->layout('user');

        $pageName = "商品詳細";

        $tableMainProduct = TableRegistry::get('mainProducts');
        $tableSubProduct = TableRegistry::get('subProducts');
        $tableCategory = TableRegistry::get('Categories');
        $tableSize = TableRegistry::get('sizes');

        $id = $this->request->query('id');

        $product = $tableMainProduct->find()
            ->where(['id' => $id])
            ->contain(['SubProducts'])
            ->first();
        $subProducts = $tableSubProduct->find()
            ->where([
                'OR' => [ ['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) >' => date('Y-m-d H:i:s')], ['selling_check_time is' => null]
                ],
            ])
            ->andWhere([
                'OR' => [ ['selling_term >' => 0], ['selling_term is' => null] ]
            ])
            ->andWhere(['parent_sku' => $product['sku']])
            ->toArray();
        //dd($subProducts);
        $size_id = [];
        $img_sub = [];
        foreach ($subProducts as $row) {
            $size_id[] = $row['size_id'];
            $img['image1'] = $row['product_photo1'];
            $img['image2'] = $row['product_photo2'];
            $img['image3'] = $row['product_photo3'];
            $img['image4'] = $row['product_photo4'];
            array_push($img_sub, $img);
        }
        $size = [];
        if ( !empty($size_id)) {
            $size = $tableSize->find()
                ->select(['id', 'sizeName'])
                ->where(['id IN' => $size_id])
                ->order(['id' => 'ASC'])
                ->toArray();
        }

        $category = $tableCategory->find()
            ->select(['categoryName'])
            ->where(['id' => $product['category_id']])
            ->first();
        $product['sizeName'] = [];
        foreach ($size as $row) {
            $product['sizeName'][$row['id']] = $row['sizeName'];
        }

        $product['img_sub'] = $img_sub;
        $product['categoryName'] = $category['categoryName'];

        $categories = $tableCategory->find()->toArray();
        $parent_category = [];
        foreach ($categories as $row) {
            if ($row['parentID'] == 0)
                $parent_category[] = $row;
        }
        do {
            $tree_category = $this->findCategory($parent_category, $categories);
            $parent_category = $tree_category['parent'];
            $child = $tree_category['child'];
        } while(count($child) > 0);
        $categories = $tableCategory->find()->where(['parentId' => 0 ])->toArray();
        $this->set(compact('product', 'parent_category', 'categories', 'pageName'));
        $this->set('_serialize', ['product', 'parent_category', 'categories', 'pageName']);
    }

    public function updateCart() {
        if ($this->request->is('post')) {
            $products = $this->request->data('product');

            foreach ($products as $product)
            {
                $productCart = $this->loadModel('Carts')->find()->where(['user_id' => $this->Auth->user('id'), 'sku' => $product['key']])->first();
                $productCart->amount = $product['quantity'];
                $this->loadModel('Carts')->save($productCart);
            }

            $this->redirect(['action' => 'show-cart', 'status' => 'success']);
        }
    }

    public function logoutUser() {
        $this->request->session()->destroy();
        $this->Auth->logout();
        $this->redirect('/');
    }

    function  findCategory($parent, $categories) {
        $child = [];
        foreach ($parent as $row) {
            if (isset($row['child'])) {
                $this->findCategory($row['child'], $categories);
            } else {
                $row['child'] = [];
                foreach ($categories as $value) {
                    if ($value['parentID'] == $row['id']) {
                        $row['child'][] = $value;
                        $child[] = $value;
                    }
                }
            }
        }
        $array = [];
        $array['child'] = $child;
        $array['parent'] = $parent;
        return $array;
    }

    function copyMain($product, $target_product) {
        $userId = $this->Auth->user('id');
        $new_name = '';
        if ($product->photo != ''){
            $arr = explode('/', $product->photo);
            $product_detail_path = WWW_ROOT . "img/upload/".$userId."/main/";
            $type = pathinfo(end($arr),PATHINFO_EXTENSION);
            $new_name = '/img/upload/'.$userId.'/main/'.$product->id.'.'.$type;
            copy($target_product.end($arr),$product_detail_path.$product->id.'.'.$type);
        }
        return $new_name;
    }
    function copySub($product, $image, $key, $target_product) {
        $userId = $this->Auth->user('id');
        $new_name = '';
        if ($image != ''){
            $arr = explode('/', $image);
            $product_detail_path = WWW_ROOT . "img/upload/".$userId."/sub/";
            $type = pathinfo(end($arr),PATHINFO_EXTENSION);
            $new_name = '/img/upload/'.$userId.'/sub/'.$product->id.'_'.$key.'.'.$type;
            copy($target_product.end($arr),$product_detail_path.$product->id.'_'.$key.'.'.$type);
        }
        return $new_name;
    }

    function insertProductSub($product, $target_product) {
        $ret = true;
        $this->loadModel('SubProducts');
        try
        {
            $skuProduct = $this->SubProducts->newEntity();
            $skuProduct = $this->SubProducts->patchEntity($skuProduct, [
                'sku' => $product['sku'],
                'parent_sku' => $product['parent_sku'],
                'product_photo1' => ($product['product_photo1'] != '') ? $product['product_photo1'] : '',
                'product_photo2' => ($product['product_photo2'] != '') ? $product['product_photo2'] : '',
                'product_photo3' => ($product['product_photo3'] != '') ? $product['product_photo3'] : '',
                'product_photo4' => ($product['product_photo4'] != '') ? $product['product_photo4'] : '',
                'price' => $product['price'],
                'size_id' => $product['size_id'],
                'furiru_size_id' => $product['furiru_size_id'],
                'rakuma_size' => $product['rakuma_size'],
                'amount' => $product['amount'],
                'color' => $product['color'],
                'selling_term' => $product['selling_term']
            ]);

            if ($product['selling_term'])
                $skuProduct->selling_check_time = Time::now();

            $this->SubProducts->save($skuProduct);
            $new_photo1 = $this->copySub($skuProduct, $skuProduct->product_photo1, 1, $target_product);
            $new_photo2 = $this->copySub($skuProduct, $skuProduct->product_photo2, 2, $target_product);
            $new_photo3 = $this->copySub($skuProduct, $skuProduct->product_photo3, 3, $target_product);
            $new_photo4 = $this->copySub($skuProduct, $skuProduct->product_photo4, 4, $target_product);


            $this->SubProducts->updateAll(
                [
                    'product_photo1' => $new_photo1,
                    'product_photo2' => $new_photo2,
                    'product_photo3' => $new_photo3,
                    'product_photo4' => $new_photo4
                ],
                [
                    'id' => $skuProduct->id
                ]);


        }
        catch(\Exception $e)
        {
            $ret = false;
        }

        return $ret;
    }

    function rename_sub_image($old_path, $skuProduct, $key) {
        $new_photo = '';
        if ($old_path != '') {
            $arr_photo = explode('.',$old_path);
            $new_photo = '/img/upload/'.$this->Auth->user('id').'/sub/'.$skuProduct->id.'_'.$key.'.'.$arr_photo[1];
            rename(WWW_ROOT.$old_path, WWW_ROOT.$new_photo);
        }
        return $new_photo;
    }

    function insertProductMain($product, $target_product)
    {
        $ret = true;
        $this->loadModel('MainProducts');
        try
        {
            $skuProduct = $this->MainProducts->newEntity();
            $skuProduct = $this->MainProducts->patchEntity($skuProduct, [
                'user_id' => $product['user_id'],
                'sku' => $product['sku'],
                'product_title' => $product['product_title'],
                'description' => $product['description'],
                'category_id' => $product['category_id'],
                'photo' => ($product['photo']),
                'price' => $product['price'],
                'merukari' => $product['merukari'],
                'furiru' => $product['furiru'],
                'rakuma' => $product['rakuma'],
                'product_item_condition_id' => $product['itemCondition'],
                'shipping_charge_id' => $product['shippingCharge'],
                'shipping_method_id' => $product['shippingMethod'],
                'sender_address' => $product['senderAddress'],
                'shipping_duration_id' => $product['shippingDuration'],
                'brand_id' => $product['brand'],
                'furiru_category_id' => $product['furiru_category_id'],
                'furiru_brand_id' => $product['furiru_brand_id'],
                'furiru_product_status_id' => $product['furiru_product_status_id'],
                'furiru_shipping_charge_id' => $product['furiru_shipping_charge_id'],
                'furiru_shipping_method_id' => $product['furiru_shipping_method_id'],
                'furiru_shipping_duration_id' => $product['furiru_shipping_duration_id'],
                'furiru_city_id' => $product['furiru_city_id'],
                'furiru_request_required' => $product['furiru_request_required'],
                'rakuma_category' => $product['rakuma_category'],
                'rakuma_brand' => $product['rakuma_brand'],
                'rakuma_condition_type' => $product['rakuma_condition_type'],
                'rakuma_postage_type' => $product['rakuma_postage_type'],
                'rakuma_delivery_method' => $product['rakuma_delivery_method'],
                'rakuma_prefecture_code' => $product['rakuma_prefecture_code'],
                'rakuma_delivery_term' => $product['rakuma_delivery_term'],
            ]);

            $this->MainProducts->save($skuProduct);
            $new_photo = $this->copyMain($skuProduct, $target_product);
            $this->MainProducts->updateAll(['photo' => $new_photo], ['id' => $skuProduct->id]);

        }
        catch(\Exception $e)
        {
            $ret = false;
        }

        return $ret;
    }

    function makeProductDir($parent){
        $startDir = 100000;
        while (is_dir($parent.$startDir))
        {
            $startDir = $startDir + 1;
        }
        return $parent.$startDir."/";
    }

    function unlinkMain($photo, $check_exist_file) {
        if (!empty($photo) && file_exists(WWW_ROOT . $photo) && $check_exist_file == 1)
            unlink(WWW_ROOT . $photo);
    }

    function deleteDir($path) {
        $files = array_diff(scandir($path), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$path/$file")) ? $this->deleteDir("$path/$file") : unlink("$path/$file");
        }
        return rmdir($path);
    }

    function unlinkSub($array, $check_exist_file, $array_key_img) {
        if ($check_exist_file == 1) {
            foreach ($array as $key=>$row) {
                if (!empty($row) && file_exists(WWW_ROOT . $row) && in_array($key, $array_key_img))
                    unlink(WWW_ROOT . $row);
            }
        }
    }

    public function getColor() {
        if ($this->request->is('ajax')) {
            $tableSub = TableRegistry::get('subProducts');
            $parent_sku = $this->request->data('parent_sku');
            $size_id = $this->request->data('size_id');

            $color = $tableSub->find()
                ->select(['id', 'parent_sku', 'size_id', 'color'])
                ->where([
                    'OR' => [ ['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) >' => date('Y-m-d H:i:s')], ['selling_check_time is' => null]
                    ],
                ])
                ->andWhere([
                    'OR' => [ ['selling_term >' => 0], ['selling_term is' => null] ]
                    ])
                ->andWhere(['parent_sku' => $parent_sku]);


            if ($size_id != '') {
                $color->andWhere(['size_id' => $size_id]);
            }
          // dd($color);
            $color->toArray();

            $this->set(compact('color'));
            $this->set('_serialize', ['color']);
        }
    }

    public function getPrice () {
        if ($this->request->is('ajax')) {
            $tableSub = TableRegistry::get('subProducts');
            $parent_sku = $this->request->data('parent_sku');
            $size_id = $this->request->data('size_id');
            $color = $this->request->data('color');

            $product = $tableSub->find()
                ->select()
                ->where([
                    'OR' => [ ['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) >' => date('Y-m-d H:i:s')], ['selling_check_time is' => null]
                    ],
                ])
                ->andWhere([
                    'OR' => [ ['selling_term >' => 0], ['selling_term is' => null] ]
                ])
                ->andWhere(['parent_sku' => $parent_sku])
                ->andWhere(['size_id' => $size_id])
                ->andWhere(['id' => trim($color)])
                ->first();



            $price = $product['price'];
            $img = [];

            if ($product['product_photo1'] != '')
                $img[] = $product['product_photo1'];

            if ($product['product_photo2'] != '')
                $img[] = $product['product_photo2'];

            if ($product['product_photo3'] != '')
                $img[] = $product['product_photo3'];

            if ($product['product_photo4'] != '')
                $img[] = $product['product_photo4'];

            $due_time = '';
            if (!is_null($product->selling_check_time))
            {
                $due_time = $product->selling_check_time->getTimestamp();
                $due_time += $product->selling_term*3600;
                $due_time = date('Y-m-d H:i', $due_time);
            }

            $amount = $product->amount;
            $buy_term = $product->buy_term;

            $this->set(compact('price', 'img', 'due_time', 'amount', 'buy_term'));
            $this->set('_serialize', ['price', 'img', 'due_time', 'amount', 'buy_term']);
        }
    }

    public function getSize() {
        if ($this->request->is('ajax')) {
            $tableSub = TableRegistry::get('subProducts');
            $tableSize = TableRegistry::get('sizes');
            $parent_sku = $this->request->data('parent_sku');
            $color = $this->request->data('color');
            $where = ['parent_sku' => $parent_sku, 'color' => trim($color)];
            $array_id = $tableSub->find()->select(['size_id'])->where($where)->toArray();
            $id = [];
            foreach ($array_id as $row) {
                $id[] = $row['size_id'];
            }

            $sizes = $tableSize->find()->select(['id', 'sizeName'])->where(['id IN' => $id])->order(['sizeName' => 'ASC'])->toArray();

            $this->set(compact('sizes'));
            $this->set('_serialize', ['sizes']);
        }
    }

    public function categorySearch() {
        if ($this->request->is('ajax')) {
            $id = $this->request->data('id');

            $tableCategory = TableRegistry::get('Categories');

            $child = $tableCategory->find()->select(['id', 'categoryName', 'parentID'])->where(['parentID' => $id])->toArray();

            $this->set(compact('child'));
            $this->set('_serialize', ['child']);
        }
    }

    public function buy() {
        if ($this->request->is('ajax')) {
            try {
                $result = 'success';
                $msg = '';
                $tableUser = TableRegistry::get('Users');
                $tableSub = TableRegistry::get('subProducts');
                $tableMain = TableRegistry::get('mainProducts');
                $tableUserProduct = TableRegistry::get('UserProducts');
                $tableCart = TableRegistry::get('Carts');
                $tableAddress = TableRegistry::get('addresses');

                $payment_method = $this->request->data('payment_method');

                $allProduct = $tableCart->find()->where(['user_id' => $this->Auth->user('id')])->toArray();

                if (count($allProduct) == 0)
                {
                    $result = '';
                    $this->set(compact('result', 'msg'));
                    $this->set('_serialize', ['result', 'msg']);
                    return;
                }

                $user = $tableUser->find()->where(['id' => $this->Auth->user('id')])->first();
                $orderInfo = array(
                    'user_id' => $user['id'],
                    'username' => $user['username'],
                    'status' => 'wait_payment',
                    'payment_method' => $payment_method,
                );
                $tableOrder = TableRegistry::get('orders');
                $order = $tableOrder->newEntity();
                $tableOrder->patchEntity($order, $orderInfo);
                if ($tableOrder->save($order)) {
                    $id_order = $order->id;
                }
                $productsInfo = [];
                if (isset($id_order))
                {
                    $i = 0;
                    $totalPrice = 0;
                    $totalPrice_1 = 0;
                    $productIds = '';

                    $createBillItems = [];

                    foreach ($allProduct as $row)
                    {
                        $sub = $tableSub->find()->where(['sku' => $row['sku']])->first();
                        $main = $tableMain->find()->where(['sku' => $sub['parent_sku']])->first();

                        $productsInfo[] = [
                            'parent_sku' => $sub['parent_sku'],
                            'sku' => $sub['sku'],
                            'product_title' => $main['product_title'],
                            'price' => $sub['price'],
                            'amount' => $row['amount'],
                        ];

                        if ($row['amount'] > $sub['amount'])
                        {
                            $result = 'error';
                            $msg = '商品の数量は'.$main['product_title'].'以下で変更してください。対象商品: '.$sub['amount'].' です。';
                            break;
                        }

                       // $unit_price = round($sub['price'] + $sub['price']*0.08, 2);

                        $totalPrice_1 += $sub['price'] * $row['amount'];

                        $createBillItems[] = [
                            'name' => $main['product_title'],
                            'quantity' => $row['amount'],
                            'unit_price' => $sub['price'],
                            'code' => $sub['parent_sku'] . "_" . $sub['sku']
                        ];
                    }

                    $tax = round( $totalPrice_1 * 0.08,2);

                    $createBillItems [] = [
                        'name' => '消費税（8%）',
                        'quantity' => 1,
                        'unit_price' => $tax,
                        'code' => 'ABC99998'
                    ];

                    $address = $tableAddress->find()->where(['id' => $user->address_id])->first();

                    $price_ship = $this->checkShipping_fee($address->zip_code, $totalPrice_1);

                    $createBillItems [] = [
                        'name' => '送料',
                        'quantity' => 1,
                        'unit_price' => $price_ship,
                        'code' => 'ABC99999'
                    ];

                    $order->price_ship = $price_ship;
                    $tableOrder->save($order);
                    if ($result == 'success')
                    {
                        $this->loadComponent('MFCloud');
                        $createBillResponse = $this->MFCloud->createBill($user->department_id, $createBillItems, $id_order);
Log::debug("created mf cloud: ".$user->department_id."   ".json_encode($createBillItems)."   ".$id_order);

                        if ($createBillResponse)
                        {
                            $createBillResponse = json_decode($createBillResponse);
                            if (isset($createBillResponse->errors))
                            {
                                $msg = $createBillResponse->errors[0]->message;
                                $result = 'error';
                            }
                            else
                            {
                                $order->bill_id = $createBillResponse->id;
                                $order->status = "done";
                                $tableOrder->save($order);
                            }
                        }
                        else
                        {
                            $result = 'error';

                        }
                    }

                    if ($result == 'success')
                    {
                        foreach ($allProduct as $row)
                        {
                            $i++;
                            $sub = $tableSub->find()->where(['sku' => $row['sku']])->first();
                            $main = $tableMain->find()->where(['sku' => $sub['parent_sku']])->first();
                            $photo1 = $this->copyImgOrder($sub['product_photo1'], $id_order . $i, 1);
                            $photo2 = $this->copyImgOrder($sub['product_photo2'], $id_order . $i, 2);
                            $photo3 = $this->copyImgOrder($sub['product_photo3'], $id_order . $i, 3);
                            $photo4 = $this->copyImgOrder($sub['product_photo4'], $id_order . $i, 4);
                            $data = array(
                                'order_id' => $id_order,
                                'product_title' => $main['product_title'],
                                'description' => $main['description'],
                                'sku' => $sub['sku'],
                                'parent_sku' => $sub['parent_sku'],
                                'product_photo1' => $photo1,
                                'product_photo2' => $photo2,
                                'product_photo3' => $photo3,
                                'product_photo4' => $photo4,
                                'price' => $sub['price'],
                                'size_id' => $sub['size_id'],
                                'color' => $sub['color'],
                                'amount' => $row['amount']
                            );
                            $products = $tableUserProduct->newEntity();
                            $tableUserProduct->patchEntity($products, $data);
                            $tableUserProduct->save($products);

                            $totalPrice += $sub['price'] * $row['amount'];
                            $productIds .= $sub->sku . ',';

                            $sub->amount = $sub['amount'] - $row['amount'];
                            $tableSub->save($sub);
                            $result = 'success';
                        }
                    }

                    if ($result == 'success')
                    {
                        $user = $this->loadModel('Users')->get($order->user_id);

                        $this->loadComponent('Email');

                        $receiveAddress = $this->loadModel('Addresses')->get($user->address_id);

                        $this->Email->afterSubmitCartNew($user['email'], $order->id, $productsInfo, $user, $receiveAddress, [Configure::read('Contact.receiveEmail')]);//sky_fly.ptit12@yahoo.com.vn

                        $tableCart->deleteAll(['user_id' => $this->Auth->user('id')]);
                    }
                    else
                    {
                        $tableOrder->delete($order);
                    }
                }
            } catch (\Exception $e) {
                $result = 'error';
                $msg = "注文する時、エラーが発生しました。もう一度やり直してください。";
                Log::error("error userproductsController buy".$e->getMessage());
            }
            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);
        }
    }


    function copyImgOrder($photo, $id, $key) {
        $folder = WWW_ROOT . 'img/order';
        if (!is_dir($folder) ){
            mkdir($folder, 0777,true);
        }
        if ($photo == '' || !file_exists(WWW_ROOT . $photo))
            return '';
        $arr = explode('.', $photo);
        $photo = str_replace('/img', 'img', $photo);
        copy(WWW_ROOT.$photo, $folder .'/' . $id .'_'.$key.'.'.$arr[1]);
        return '/img/order/'. $id.'_'.$key.'.'.$arr[1];
    }

    public function managerOrder() {
        $pageName = "マイページ";
        $this->viewBuilder()->layout('user');
        $tableCategory = TableRegistry::get('Categories');
        $categories = $tableCategory->find()->toArray();
        $parent_category = [];
        foreach ($categories as $row) {
            if ($row['parentID'] == 0)
                $parent_category[] = $row;
        }

        $categories = $parent_category;
        $this->set(compact('categories', 'pageName'));
        $this->set('_serialize', ['categories', 'pageName']);

    }

    public  function orderUser() {

        if ($this->request->is('ajax')) {

            $id = $this->Auth->user('id');
            $tableOrder = TableRegistry::get('orders');
            $tableProduct = TableRegistry::get('userProducts');
            $order = $tableOrder->find()->where(['user_id' => $id])->order(['created' => 'ASC'])->toArray();
            $listOrder = [];

            foreach ($order as $row) {
                $products = $tableProduct->find()->select(['amount', 'price','order_status'])->where(['order_id' => $row['id']])->toArray();
                $total = 0;
                if($row && $row->status != 'reject') {
                    foreach($products as $value) {
                        if($value && $value['order_status'] != 'reject')
                        $total+= $value['amount'] * $value['price'];
                    }
                }


                $listOrder[] = array(
                    'id' => $row['id'],
                    'date' => date_format($row['created'], 'Y/m/d H:i'),
                    'total_price' => number_format($total),
                    'status' => $row['status']
                );
            }

            $this->set(compact('listOrder'));
            $this->set('_serialize', ['listOrder']);

        }
    }

    public function ProductUser() {


        if ($this->request->is('ajax')) {
            $order_id = $this->request->data('id');
            $tableProduct = TableRegistry::get('userProducts');
            $tableSize = TableRegistry::get('Sizes');

            $listProducts = $tableProduct->find()->where(['order_id' => $order_id])->toArray();
            $products = [];
            foreach ($listProducts as $row) {
                $size = $tableSize->find()->where(['id' => $row['size_id']])->first();

                $product = array(
                    'id' => $row['id'],
                    'product_photo1' => $row['product_photo1'],
                    'product_title' => $row['product_title'],
                    'option' => $row['color'] . '/' . $size['sizeName'],
                    'price' => number_format($row['price']),
                    'amount' => $row['amount'],
                    'order_status' => $row['order_status']
                );
                $products[] = $product;
            }


            $this->set(compact('products'));
            $this->set('_serialize', ['products']);

        }
    }

    public function managerProduct() {
        $this->viewBuilder()->layout('user');
        $tableCategory = TableRegistry::get('Categories');
        $categories = $tableCategory->find()->toArray();
        $parent_category = [];
        foreach ($categories as $row) {
            if ($row['parentID'] == 0)
                $parent_category[] = $row;
        }

        $categories = $parent_category;
        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    public function deleteProductOrder () {
        if ($this->request->is('ajax')) {
            $result = '';
            $msg = '';
            $id = $this->request->data('id');
            $tableProduct = TableRegistry::get('userProducts');
            $product = $tableProduct->get($id);
            if ($tableProduct->delete($product)) {
                $result = 'success';
                $msg = '';
            }


            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);

        }
    }

    function checkAmount() {
        if ($this->request->is('ajax')) {
            $msg = '';
            $tableSub = TableRegistry::get('subProducts');

            $amount = $this->request->data('amount');
            $key = $this->request->data('sku');

            $product = $tableSub->find()->select(['amount'])->where(['sku' => $key])->first();

            if ($amount > $product['amount']) {
                $result = 'error';
                $msg = '入力可能数量は、'. $product['amount'] .'以下です。';
            } else {
                $result = 'success';
            }
            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);

        }
    }

    function changeStatusOrder() {
        if ($this->request->is('ajax')) {
            $id = $this->request->data('id');

            $tableOrder = TableRegistry::get('orders');
            $tableSub = TableRegistry::get('subProducts');
            $tableUserProduct = TableRegistry::get('UserProducts');

            $order = $tableOrder->updateAll(['status' => 'reject'], ['id' => $id]);

            $products = $tableUserProduct->find()->where(['order_id' => $id])->toArray();

            if ($order) {
                $result = 'success';

                foreach ($products as $row) {
                    $sub = $tableSub->find()->where(['sku' => $row['sku']])->first();

                    $sub->amount = $sub['amount'] + $row['amount'];
                    $tableSub->save($sub);
                }
            }
            else
                $result = 'error';
            $this->set(compact('result'));
            $this->set('_serialize', ['result']);
        }
    }

    function updateMainProduct($product, $target_product) {
        $tableMain = TableRegistry::get('mainProducts');
        $user_id = $this->Auth->user('id');
        $old_product = $tableMain->find()->where(['sku' => $product['sku']])->first();
        $type_new = strtolower(pathinfo($target_product.$product['photo'], PATHINFO_EXTENSION));
        $type_old = strtolower(pathinfo(WWW_ROOT.$old_product['photo'], PATHINFO_EXTENSION));
        $arr_img = explode('.', $old_product['photo']);
        $arr_img[0] = str_replace('/img', 'img', $arr_img[0]);
        if ($type_new === $type_old) {
            file_put_contents(WWW_ROOT . $old_product['photo'], file_get_contents($target_product . $product['photo']));
            $photo = $old_product['photo'];
        } else {
            copy($target_product . $product['photo'], WWW_ROOT . $arr_img[0] .'.'. $type_new);
            $photo = '/' . $arr_img[0] .'.'. $type_new;
        }

        $data = [
            'product_title' => $product['product_title'],
            'description' => $product['description'],
            'category_id' => $product['category_id'],
            'photo'  => $photo,
            'price' => $product['price'],
            'merukari' => $product['merukari'],
            'furiru' => $product['furiru'],
            'rakuma' => $product['rakuma'],
            'product_item_condition_id' => $product['itemCondition'],
            'shipping_charge_id' => $product['shippingCharge'],
            'shipping_method_id' => $product['shippingMethod'],
            'sender_address' => $product['senderAddress'],
            'shipping_duration_id' => $product['shippingDuration'],
            'brand_id' => $product['brand'],
            'furiru_category_id' => $product['furiru_category_id'],
            'furiru_brand_id' => $product['furiru_brand_id'],
            'furiru_product_status_id' => $product['furiru_product_status_id'],
            'furiru_shipping_charge_id' => $product['furiru_shipping_charge_id'],
            'furiru_shipping_method_id' => $product['furiru_shipping_method_id'],
            'furiru_shipping_duration_id' => $product['furiru_shipping_duration_id'],
            'furiru_city_id' => $product['furiru_city_id'],
            'furiru_request_required' => $product['furiru_request_required'],
            'rakuma_category' => $product['rakuma_category'],
            'rakuma_brand' => $product['rakuma_brand'],
            'rakuma_condition_type' => $product['rakuma_condition_type'],
            'rakuma_postage_type' => $product['rakuma_postage_type'],
            'rakuma_delivery_method' => $product['rakuma_delivery_method'],
            'rakuma_prefecture_code' => $product['rakuma_prefecture_code'],
            'rakuma_delivery_term' => $product['rakuma_delivery_term'],
        ];

        $tableMain->updateAll($data, ['user_id' => $user_id, 'sku' => $product['sku']]);
    }

    function updateSubProduct($product, $target_product) {
        $tableSub = TableRegistry::get('subProducts');
        $productSub = $tableSub->find()->where(['sku' => $product['sku']])->first();

        $product_photo1 = $this->changeContentImg($productSub['product_photo1'], $product['product_photo1'], $target_product);
        $product_photo2 = $this->changeContentImg($productSub['product_photo2'], $product['product_photo2'], $target_product);
        $product_photo3 = $this->changeContentImg($productSub['product_photo3'], $product['product_photo3'], $target_product);
        $product_photo4 = $this->changeContentImg($productSub['product_photo4'], $product['product_photo4'], $target_product);
        $data = [
            'parent_sku' => $product['parent_sku'],
            'product_photo1' => $product_photo1,
            'product_photo2' => $product_photo2,
            'product_photo3' => $product_photo3,
            'product_photo4' => $product_photo4,
            'color' => $product['color'],
            'size_id' => $product['size_id'],
            'furiru_size_id' => $product['furiru_size_id'],
            'rakuma_size' => $product['rakuma_size'],
            'price' => $product['price'],
            'amount' => $product['amount'] + $productSub['amount'],
            'selling_term' => $product['selling_term']
        ];

        if ($product['selling_term'])
            $data['selling_check_time'] = Time::now();

//        $tableSub->updateAll($data, ['sku' => $product['sku']]);

        $tableSub->patchEntity($productSub, $data);
        $tableSub->save($productSub);
    }

    function changeContentImg($photo, $photo_real, $target_product) {
        if ($photo == '' || $photo_real == '')
            return '';
        $type = strtolower(pathinfo($photo_real, PATHINFO_EXTENSION));
        $arr = explode('.', $photo);
        $photo = $arr[0] . '.' . $type;
        file_put_contents(WWW_ROOT.$photo, file_get_contents($target_product.$photo_real));
        return $photo;
    }

    public function updateSingleProduct()
    {
        if ($this->request->is('ajax'))
        {
            $cartId = $this->request->data('cart_id');
            $amount = $this->request->data('amount');
            $sku = $this->request->data('sku');


            $tableSub = TableRegistry::get('subProducts');
            $productSub = $tableSub->find()->where(['sku' => $sku])->first();
            $error = '';
            $cart = $this->loadModel('Carts')->find()->where(['user_id' => $this->Auth->user('id'), 'id' => $cartId])->first();
            $amount_default = $cart->amount;

            if ($productSub) {
                $buy_term = $productSub->buy_term;
                if ($amount < $buy_term) {
                    $error = "販売下限数量は". $buy_term. "です。 ".$buy_term."以上入力してください。";
                } else {

                    $cart->amount = $amount;
                    $this->loadModel('Carts')->save($cart);
                }
            }
            $this->set(compact( 'error', 'amount_default'));



        }
    }

    public function productManagement()
    {
        $this->viewBuilder()->layout('user');

        if ($this->request->is('ajax'))
        {
            $user = $this->Auth->user();

            $this->loadModel('ClientProducts');

            $products = $this->ClientProducts
                ->find()
                ->where(['user_id' => $user['id']])
                ->contain(['Sizes'])
                ->toArray();

            $data['products'] = $products;

            $this->set(compact('products'));
            $this->set('_serialize', ['products']);
        }
    }

    public function searchSubProduct() {


        if ($this->request->is('ajax')) {

            $tableSub = TableRegistry::get('subProducts');

            $title_search = $this->request->data('title_search');
            $status_search = $this->request->data('status_search');

            $products = [];

            if (!$title_search && !$status_search ) {
                $products = [];

            } elseif (!$title_search && $status_search) {
                $products = $tableSub->find()
                    ->select([
                        'subProducts.id',
                        'subProducts.sku',
                        'subProducts.exhibition_time',
                        'subProducts.selling_term',
                        'subProducts.selling_check_time',
                        'subProducts.sku',
                        'subProducts.product_photo1',
                        'subProducts.color',
                        'subProducts.price',
                        'subProducts.amount',
                        'sizes.sizeName',
                        'main_products.product_title'
                    ])
                    ->innerJoin('sizes', ['subProducts.size_id = sizes.id'])
                    ->innerJoin('main_products', ['subProducts.parent_sku = main_products.sku']);


                if ($status_search == 'has_exhibition') {
                    $products->where(['subProducts.exhibition_time is not' => null])
                                ->andWhere(['subProducts.exhibition_time !=' => '2000-01-01 00:00:00']);
                } elseif ($status_search =='has_product') {
                    $products->where(['subProducts.amount >= subProducts.buy_term'])
                                ->orWhere(['subProducts.amount >' => 0, 'subProducts.buy_term is' =>null]);

                } elseif ($status_search =='out_product') {
                    $products->where(['subProducts.amount < subProducts.buy_term'])
                                ->orWhere(['subProducts.amount' => 0]);
                } elseif ($status_search == 'expired') {
                    $products->where(['subProducts.selling_term' => 0 ])
                        ->orWhere(['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) <' => date('Y-m-d H:i:s')]);
                } else {
                    $products = [];
                }



            } else {

                $products = $tableSub->find()
                    ->select([
                        'subProducts.id',
                        'subProducts.sku',
                        'subProducts.exhibition_time',
                        'subProducts.selling_term',
                        'subProducts.selling_check_time',
                        'subProducts.sku',
                        'subProducts.product_photo1',
                        'subProducts.color',
                        'subProducts.price',
                        'subProducts.amount',
                        'sizes.sizeName',
                        'MainProducts.product_title'
                    ])
                    ->innerJoin('sizes', ['subProducts.size_id = sizes.id'])
                    ->innerJoinWith("MainProducts", function (Query $query) use ($title_search) {
                        return $query->where(['product_title like' => "%$title_search%"]);
                    });

                if ($status_search == 'has_exhibition') {
                    $products->where(['subProducts.exhibition_time is not' => null])
                        ->andWhere(['subProducts.exhibition_time !=' => '2000-01-01 00:00:00']);
                } elseif ($status_search =='has_product') {
                    $products->where(['subProducts.amount >= subProducts.buy_term'])
                        ->orWhere(['subProducts.amount >' => 0, 'subProducts.buy_term is' =>null]);

                } elseif ($status_search =='out_product') {
                    $products->where(['subProducts.amount < subProducts.buy_term'])
                        ->orWhere(['subProducts.amount' => 0]);
                } elseif ($status_search == 'expired') {
                    $products->where(['subProducts.selling_term' => 0 ])
                        ->orWhere(['DATE_ADD(selling_check_time, INTERVAL selling_term HOUR) <' => date('Y-m-d H:i:s')]);
                }
            }
            //dd($products);
            if ($products) {
                $data = $products->toArray();
                //dd($data);
                $products = [];
                foreach ($data as $row)
                {
                    //dd($row);

                    $row['sizeName'] = $row->sizes['sizeName'];
                    if (isset($row->_matchingData)) {
                        $row['product_title'] = $row->_matchingData['MainProducts']->product_title;
                    } else {
                        $row['product_title'] = $row->main_products['product_title'];
                    }

                    $hours = 0;
                    if ($row->exhibition_time && strtotime($row->exhibition_time) != strtotime('2000/1/1')) {
                        $row['exhibition_time'] = (string)date('Y/m/d H:i:s', strtotime($row->exhibition_time));
                    } else {
                        $row['exhibition_time'] = '';
                    }


                    if (isset($row->selling_check_time) && isset($row->selling_term))
                    {

                        $modifiedTime = $row->selling_check_time;
                        $expiredTime = $modifiedTime->modify('+' . $row->selling_term . ' hours');
                        if (strtotime($row->selling_check_time) > strtotime(Time::now())) {
                            $diffTime = date_diff($row->selling_check_time, $expiredTime);
                        } else {
                            $diffTime = date_diff(Time::now(), $expiredTime);
                        }


                        if ($diffTime->invert)
                            $row->remainingTime = 'あと0';
                        else
                        {
//                        $hours = $diffTime->h + ($diffTime->d * 24) + ($diffTime->m * 30 * 24) + ($diffTime->y * 365 * 24);
                            $hours = $diffTime->h + ($diffTime->days * 24);
                            $row->remainingTime = "あと".$hours . ":";
                            if ($diffTime->i >= 0 && $diffTime->i < 10)
                                $row->remainingTime .= '0' . $diffTime->i;
                            else
                                $row->remainingTime .= $diffTime->i;
                        }
                    }
                    else
                        $row->remainingTime = '';

                    $products[] = $row;
                }
            }

            $this->set(compact('products'));
            $this->set('_serialize', ['products']);

        }
    }

    public function profit() {
        $year_now = date('Y');
        $month_now = date('m');

        $current_month = date('Y-m-07 H:i:s');
        $last_one_month = date('Y-m-07 H:i:s',mktime(0, 0, 0, ($month_now - 1) , 7, $year_now));

        $tableUserProducts = TableRegistry::get('userProducts');
        $userProducts = $tableUserProducts->find('all')
            ->where(['userProducts.created >=' => $last_one_month, 'userProducts.created <=' => $current_month])
            ->andwhere(['Orders.status' => 'done'])
            ->andWhere([ 'OR' => [
                ['userProducts.order_status !=' => 'reject'],
                ['userProducts.order_status is' => null]
            ] ])
            //->group('userProducts.order_id')
            ->contain(['SubProducts' => function(Query $q){
                return $q->contain(['Sizes']);
            },'Orders']);

        dd($userProducts->toArray());
        $dataProducts = [];
        $order_id = [];
        $sub_product_id = [];
        foreach ($userProducts as $userProduct ) {
            $dataProducts[] = [
                'id' => $userProduct->id,
                'product_title' => $userProduct->product_title,
                'size_color' => $userProduct->sub_product->size->sizeName. '/'. $userProduct->color
            ];
        }
        $dataProducts = json_encode($userProducts->toArray(), true);
        //$dataOrder = $userProducts->select('order_id')->toArray();

       // dd($dataProducts);
        $this->set(compact('current_month', 'last_one_month', 'dataProducts'));
        $this->set('_serialize', ['current_month', 'last_one_month', 'dataProducts']);
    }
}