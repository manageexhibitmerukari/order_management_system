<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/23/17
 * Time: 9:01 AM
 */
namespace App\Controller;

class Cart extends AppController
{

    public function cart() {

    }

    public function addProduct($sku, $amount, $price) {
        $allProduct = $this->request->session()->read('cart');
        if ($allProduct != null) {
            if (array_key_exists($sku, $allProduct)) {
                $allProduct[$sku]['amount']+= $amount;
            } else {
                $allProduct[$sku]['amount'] = $amount;
//                $allProduct[$sku]['sell_price'] = $price;
            }
        } else {
            $allProduct[$sku]['amount'] = $amount;
//            $allProduct[$sku]['sell_price'] = $price;
        }
        $this->saveProduct($allProduct);
    }

    public function readProduct () {
        $cart = $this->request->session()->read('cart');
        if(isset($cart))
            foreach ($cart as $key => $row) {
                if (!isset($cart[$key]['amount']))
                    $this->removeCart();

                break;
            }
        return $this->request->session()->read('cart');
    }

    private function saveProduct ($data) {
        $this->request->session()->write('cart', $data);
    }

    public function removeProduct($key) {
        $allProduct = $this->readProduct();
        unset($allProduct[$key]);
        $this->saveProduct($allProduct);
    }

    public function removeCart() {
        $this->request->session()->write('cart', []);
    }

    public function updateProduct($product) {
        $allProduct = $this->readProduct();
        foreach ($product as $row) {
            if (array_key_exists($row['key'], $allProduct)) {
                $allProduct[$row['key']]['amount'] = $row['quantity'];
//                $allProduct[$row['key']]['sell_price'] = $row['sell_price'];
            }
        }
        $this->saveProduct($allProduct);
    }
}