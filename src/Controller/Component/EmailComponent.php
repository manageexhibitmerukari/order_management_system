<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 4/13/17
 * Time: 11:19
 */

namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\Log\Log;
use Cake\Network\Email\Email;

class EmailComponent extends Component
{
    private function sendEmail($subject, $sellerEmail, $mailContent, $ccEmails = null)
    {
        try {
            $email = new Email();

            $email
                ->from(['watermeru2016@gmail.com' => 'ナックルメロン'])
                ->to($sellerEmail)
                ->subject($subject);

            if (isset($ccEmails))
                $email->addCc($ccEmails);

            $email->send($mailContent);
        }
        catch (\Exception $e)
        {
            Log::error('Send email error: ' . $e->getMessage());
        }
    }

    public function afterSubmitCart($receiveEmail, $buyerName, $buyerEmail, $product_price_total, $product_ids, $company, $postal, $state, $town, $building, $phoneNumber, $houseNumber)
    {
        $subject = 'ご注文の確認';


        $mailContent = $buyerName."様
        
このたびは、クレイジープライスをご利用頂きまして、
大変に有り難うございます。

担当の堀場でございます。

ご入力頂きましたお届け先ご住所等の控えを、
以下に記載させて頂きました。

間違いが有りましたら、お気軽にフッターに記載の、
弊社窓口迄ご連絡下さい。

次に、以下の金額を、下記の銀行口座の中から
お手近な口座へお振り込み頂き、
お振込名義とどちらへお振り込み頂いたかを、
以下の報告フォオームからご一報くださいませ。

決済完了報告フォームはこちら▽
<--houkoku_url-->


ーーーーーーーーーーーーーーーーーーーーー

合計：".$product_price_total."円
ーーーーーーーーーーーーーーーーーーーーー

それでは、お手数をお掛けしますが、
どうぞよろしくお願い致します。
有り難うございます。



会社名			$company 御中
お名前			$buyerName 様
メルアド		$buyerEmail
郵便番号		〒$postal
都道府県		$state
市町村			$town
番地				$houseNumber
建物名等		$building
電話番号		$phoneNumber
商品ID			$product_ids
合計金額		".$product_price_total."円";

        $this->sendEmail($subject, $receiveEmail, $mailContent);
    }





    public function acceptOrder($sellerEmail, $sellerName, $orderId)
    {
        $subject = $sellerName."様ご注文の承認のお知らせ";


        $mailContent = $sellerName."様
        
お注文の確認
注文番号： $orderId

この度は、ご注文頂き誠にありがとうございます。
今回のご注文内容につきまして当社（ナックルメロン）より承認させていただきましたのでご報告させていただきます。

ご注文詳細は、マイページ→注文履歴よりご確認下さいませ。

このあと、今回の注文のご請求書をメールにて送付いたします。

その中の決済URLより

【クレジットカード払い】をご希望のお客様は本日より３日以内に決済お願い致します。

【銀行振込】をご希望のお客様はそちらへ振込先を明記しておりますので３営業日以内にお振込お願い致します。

３日以内に決済の確認が取れない場合は、注文をキャンセルさせて頂く場合がございますので予めご了承下さいませ。

その他ご不明な点がある場合は、ナックルメロンまでお気軽にお問い合わせ下さいませ。

ナックルメロン問い合わせ先

Knucklemeron.com@gmail.com。";

        $this->sendEmail($subject, $sellerEmail, $mailContent);
    }





    public function rejectOrder($sellerEmail, $sellerName, $orderId)
    {
        $subject = $sellerName."ご注文中止の知らせ";

        $mailContent = $sellerName."様
        
ご注文の確認
注文番号： $orderId

ご利用いただき、ありがとうございます。出品者がお客様のご注文決済を承ったことをお知らせいたします。

ご注文中止の理由は以下のようになっております。
注文して頂いた商品の在庫が消えてしまいました。

その他ご不明な点がある場合は、出品者までお気軽にご連絡ください。 
よろしくお願いします。";

        $this->sendEmail($subject, $sellerEmail, $mailContent);
    }

    public function afterSubmitCartNew($receiveEmail, $orderId, $products, $user, $receiveAddress, $ccEmails = null)
    {
        $subject = '注文完了のお知らせ';

        $totalPrice = 0;
        $content = "
本メールはお客様のご注文情報時に送信される自動配信メールです。

ショップからの確認の連絡、または商品の発送をもって売買契約成立となります。
ナックルメロンをご利用いただき、まことにありがとうございます。下記の内容でご注文を承りましたのでご連絡申しあげます。
ーーーーーーーーーーーーーーーーーーー

ご注文番号：$orderId
■ご注文内容■\r\n";

        foreach ($products as $product)
        {
            $totalPrice += $product['price']*$product['amount'];
            $content .= "【商品番号】". $product['parent_sku'] . "_" . $product['sku'] . "   " . $product['product_title'] . "   " . $product['price'] . "   " . $product['amount'] . "   " . $product['price']*$product['amount'] . "\r\n";
        }

$content .= "小計：¥$totalPrice

ご請求金額 ¥$totalPrice

ーーーーーーーーーーーーーーーーーーー
■ご注文者情報■
ご住所
〒$user->postal
$user->state$user->town$user->houseNumber$user->building

お名前 : $user->name
TEL : $user->phone_number
E-Mail  : $user->email
ーーーーーーーーーーーーーーーーーーー
■配送先情報■
ご住所
〒$receiveAddress->zip_code
$receiveAddress->city$receiveAddress->address1$receiveAddress->address2

お名前 : $receiveAddress->name
TEL : $receiveAddress->phone_number
E-Mail  : ---------
ーーーーーーーーーーーーーーーーーーー
お支払い方法に【銀行振込】 をご選択の方は、　
別途お送りする請求書に、　
お振込先の記載がございますのでそちらをご確認の上、請求書到着から3日以内にお振込をお願いいたします。

＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿

メールによるお問い合わせにて承っております。
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿

ショップURL：www.knucklemelon.com
お問い合わせ先：knucklemelon.com@gmail.com";

        $this->sendEmail($subject, $receiveEmail, $content, $ccEmails);
    }


    public function emailContact($firstName, $lastName, $email, $address = '',$issue ='', $phone = '', $content, $receiveEmail, $ccEmails = null ) {

        $subject = "【ナックルメロン】お客様の問い合わせ";
        if ($issue) {
            $subject .= " - $issue";
        }

        $body = "";
        $body .= "お客さんの問い合わせです。\n \n";
        $body .= "お客様情報：\n";
        $body .= "お名前： $firstName \n";
        $body .= "フリガラ： $lastName \n";
        $body .= "メールアドレス： $email \n";

        if ($address) {
            $body .= "住所： $address \n";
        }
        if ($phone) {
            $body .= "電話番号： $phone \n";
        }

        $body .= "\n";
        if ($issue) {
            $body .= "件名： $issue \n";
        }
        $body .= "問い合わせ内容：\n$content \n";


        $this->sendEmail($subject, $receiveEmail, $body, $ccEmails);

    }

    public function emailResetPass($receiveEmail, $userName, $newPass) {
        $subject = "パスワード再発行のお知らせ";
        $body = "";
        $body .= "$userName 様\n\n";
        $body .= "ご利用ありがとうございます。		
パスワードを再発行しました。		
新しいパスワードは以下の通りです。
$newPass \n
今後とも、よろしくお願いいたします。			
***************************************			
ショップURL：	www.knucklemelon.com		
問い合わせ先メールアドレス：knucklemelon.com@gmail.com			
***************************************";

        $this->sendEmail($subject, $receiveEmail, $body);
    }

    public function sendRegisterConfirm($receiveEmail, $activeUrl)
    {
        $subject = "仮会員登録完了のお知らせ";
        $content = "このメールは、仮会員登録の為の確認メールです。
本メールは自動配信しております。
本メールへ返信いただきましても、お返事致し兼ねますのでご注意ください。

ーーーーーーーーーーーーーーーーーーー

この度は、会員登録のお申込みありがとうございます。
本新規登録を完了するには下記URLをクリックして、引き続き登録を行って下さい。
↓↓↓↓↓
$activeUrl
ーーーーーーーーーーーーーーーーーーーーー

※現段階では本登録は完了しておりません。※

ーーーーーーーーーーーーーーーーーーーーー

本登録が完了してからの本サービスのご提供となりますのでご了承下さいませ。

本メールにお心当たりが無い方は下記問い合わせ先にご連絡お願いいたします。

ショップURL：www.knucklemelon.com

ショップ問い合せ：knucklemelon.com@gmail.com";

        $this->sendEmail($subject, $receiveEmail, $content);
    }

    public function afterActiveUser($receiveEmail, $name)
    {
        $subject = '本会員登録完了メール';

        $content = "このメールは、本会員登録の為の確認メールです。
本メールは自動配信しております。
本メールへ返信いただきましても、お返事致し兼ねますのでご注意ください。

ーーーーーーーーーーーーーーーーーーー

".$name."様

この度、当ショップKNUCKLE MELONの本会員登録が完了いたしました。
誠にありがとうございます。

今後共KNUCKLE MELONをよろしくお願いいたします。

ーーーーーーーーーーーーーーーーーーー

本メールはKNUCKLE MELONの本会員登録を行っていただいた方へお送りしております。お心当たりの無い方は、下記お問い合せ先までご連絡お願い致します。

ショップURL: www.knucklemelon.com

お問い合わせ先：knucklemelon.com@gmail.com";

        $this->sendEmail($subject, $receiveEmail, $content);
    }


    public function afterShipping($receiveEmail, $data)
    {
        $subject = '配送完了のお知らせ';

        $content = "このメールはお客様のご注文に関する大切なメールです。			
お取引が完了するまで保存して下さい。			
ーーーーーーーーーーーーーーーーーーー			
			
 ". $data->users['name']. "様			
			
この度はご注文頂きまして誠に有難うございます。			
			
商品の発送手続きが完了しましたのでご連絡致します。			
１〜２日後くらい（またはご指定配達日）で商品がお手元に届くかと思いますので宜しくお願いいたします。			
			
以下、承りましたご注文の内容となっております。			
			
ーーーーーーーーーーーーーーーーーーー			
			
ご注文番号：$data->order_id		
■ご注文内容■			
【商品番号】			
    $data->sku      $data->product_title      $data->amount      $data->price       			
			
			
ご請求金額 ¥ ". $data->amount*$data->price ." 		
			
ーーーーーーーーーーーーーーーーーーー			
			
■配送先情報■			
ご住所			
〒 ".$data->addresses['zip_code']. "\n". $data->addresses['city'] .$data->addresses['address1'].$data->addresses['address2']." 		
		
			
お名前 ". $data->addresses['name']."			
TEL ". $data->addresses['phone_number']."			
E-Mail 		
ーーーーーーーーーーーーーーーーーーー			
■配送方法■			
			
配送業者：			
			
お問い合わせ番号：	
ーーーーーーーーーーーーーーーーーーー			
			
この度はご注文誠に有難う御座いました。			
今後共よろしくお願い致します。			
			
ショップURL：www.knucklemelon.com			
お問い合わせメール：knucklemelon.com@gmail.com";

        $this->sendEmail($subject, $receiveEmail, $content);
    }

}