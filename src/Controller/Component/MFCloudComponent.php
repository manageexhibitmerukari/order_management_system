<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 6/28/17
 * Time: 11:32
 */

namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\Http\Client\Request;
use Cake\Http\Client\Response;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class MFCloudComponent extends Component
{
    private $xformHeader = [
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded"
    ];

    private $jsonHeader = [
        "Accept: application/json",
        "Content-Type: application/json"
    ];

    private $domain = 'https://invoice.moneyforward.com';

    private function __sendRequest($url, $method, $data, $header)
    {
        $uri = $this->domain.$url;

        $defaults = array(
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_SSL_VERIFYPEER => false,
        );

        if (in_array($method, [Request::METHOD_POST, Request::METHOD_PATCH]))
        {
            $defaults[CURLOPT_URL] = $uri;
            $defaults[CURLOPT_POSTFIELDS] = $data;
        }
        else if ($method == Request::METHOD_GET)
        {
            if (isset($data))
                $defaults[CURLOPT_URL] = $uri.'?'.$data;
            else
                $defaults[CURLOPT_URL] = $uri;

        }


        $ch = curl_init();
        curl_setopt_array($ch, $defaults);
        $response['response'] = curl_exec($ch);
        $response['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $response;
    }

    private function refreshToken()
    {
        $accessToken = null;

        $mfCloudTable = TableRegistry::get('MfCloud');
        $mfCloud = $mfCloudTable->find()->first();

        $url = "/oauth/token";
        $data = [
            'refresh_token' => $mfCloud->refresh_token,
            'grant_type' => 'refresh_token',
        ];

        $refreshTokenResponse = $this->__sendRequest($url, Request::METHOD_POST, http_build_query($data), $this->xformHeader);

        if ($refreshTokenResponse['code'] == Response::STATUS_OK)
        {
            $response = json_decode($refreshTokenResponse['response']);

            $accessToken = $response->access_token;

            $mfCloud->access_token = $accessToken;
            $mfCloud->refresh_token = $response->refresh_token;
            $mfCloud->expire_date = Time::createFromTimestamp($response->expires_in + $response->created_at);
            $mfCloudTable->save($mfCloud);
        }

        return $accessToken;
    }

    private function getToken()
    {
        $mfCloudTable = TableRegistry::get('MfCloud');
        $mfCloud = $mfCloudTable->find()->first();

        if ($mfCloud->expire_date->getTimestamp() <= time())
        {
            return $this->refreshToken();
        }
        else
        {
            return $mfCloud->access_token;
        }
    }

    public function register($name, $nameKana, $phoneNumber, $email, $prefecture, $address1, $address2)
    {
        $accessToken = $this->getToken();

        $url = "/api/v1/partners";
        $headers = $this->jsonHeader;
        $headers[] = "Authorization: BEARER $accessToken";

        $data = [
            'partner' => [
                'name' => $name,
                'name_kana' => $nameKana,
                'name_suffix' => '御中',
                'tel' => $phoneNumber,
                'email' => $email,
                'prefecture' => $prefecture,
                'address1' => $address1,
                'address2' => $address2,
            ]
        ];

        $response = $this->__sendRequest($url, Request::METHOD_POST, json_encode($data), $headers);

        if ($response['code'] == 401)
        {
            $accessToken = $this->refreshToken();

            if (is_null($accessToken))
                return null;
            else
            {
                $headers = $this->jsonHeader;
                $headers[] = "Authorization: BEARER $accessToken";

                $response = $this->__sendRequest($url, Request::METHOD_POST, json_encode($data), $headers);
                return $response['response'];
            }
        }
        else
        {
            return $response['response'];
        }
    }

    public function createBill($departmentDd, $items, $orderId)
    {
        $accessToken = $this->getToken();

        $url = "/api/v1/billings";
        $headers = $this->jsonHeader;
        $headers[] = "Authorization: BEARER $accessToken";

        $data = [
            'billing' => [
                'department_id' => $departmentDd,
                'billing_number' => $orderId,
                'billing_date' => date('Y-m-d'),
                'due_date' => date('Y-m-d', strtotime('+3 days')),
                'items' => $items,
            ]
        ];
        $response = $this->__sendRequest($url, Request::METHOD_POST, json_encode($data), $headers);

        if ($response['code'] == 401)
        {
            $accessToken = $this->refreshToken();

            if (is_null($accessToken))
                return null;
            else
            {
                $headers = $this->jsonHeader;
                $headers[] = "Authorization: BEARER $accessToken";

                $response = $this->__sendRequest($url, Request::METHOD_POST, json_encode($data), $headers);
                return $response['response'];
            }
        }
        else
        {
            return $response['response'];
        }
    }

    public function downloadBill($billId)
    {
        $accessToken = $this->getToken();

        $url = "/api/v1/billings/$billId.pdf";
        $headers = ["Authorization: BEARER $accessToken"];

        $response = $this->__sendRequest($url, Request::METHOD_GET, null, $headers);

        if ($response['code'] == 401)
        {
            $accessToken = $this->refreshToken();

            if (is_null($accessToken))
                return null;
            else
            {
                $headers[] = "Authorization: BEARER $accessToken";

                $response = $this->__sendRequest($url, Request::METHOD_GET, null, $headers);
                return $response['response'];
            }
        }
        else
        {
            return $response['response'];
        }
    }


    public function updatePartner($partnerId, $partnerInfo)
    {
        $url = "/api/v1/partners/$partnerId";
        $accessToken = $this->getToken();

        $headers = $this->jsonHeader;
        $headers[] = "Authorization: BEARER $accessToken";

        $data = [
            'partner' => [
                'name' => $partnerInfo['name'],
                'name_kana' => $partnerInfo['name_kana'],
                'departments' => [
                    [
                        'id' => $partnerInfo['department_id'],
                        'tel' => $partnerInfo['phone_number'],
                        'email' => $partnerInfo['email'],
                        'prefecture' => $partnerInfo['prefecture'],
                        'address1' => $partnerInfo['address1'],
                    ]
                ]
            ]
        ];

        if (!empty($address2))
        {
            $data['partner']['departments'][0]['address2'] = $address2;
        }

        $response = $this->__sendRequest($url, Request::METHOD_PATCH, json_encode($data), $headers);

        if ($response['code'] == 401)
        {
            $accessToken = $this->refreshToken();

            if (is_null($accessToken))
                return null;
            else
            {
                $headers = $this->jsonHeader;
                $headers[] = "Authorization: BEARER $accessToken";

                $response = $this->__sendRequest($url, Request::METHOD_PATCH, json_encode($data), $headers);
                return $response['response'];
            }
        }
        else
        {
            return $response['response'];
        }
    }
}