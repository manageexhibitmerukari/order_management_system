<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 3/15/17
 * Time: 11:24
 */

namespace App\Controller\Component;


use Cake\Controller\Component;

class SkuComponent extends Component
{
    private $header = [
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded"
    ];

    private $domain = 'http://160.16.61.164:8080';

    private function __sendRequest($url, $data, $header = null)
    {
        if (is_null($header))
            $header = $this->header;

        $defaults = array(
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_SSL_VERIFYPEER => false,
        );


        $ch = curl_init();
        curl_setopt_array($ch, $defaults);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }


    public function increaseAmount($username, $password, $products)
    {
        $url = $this->domain."/api/products/increase-amount";
        $data = [
            'username' => $username,
            'password' => $password,
            'products' => $products
        ];
        return $this->__sendRequest($url, $data);
    }
}