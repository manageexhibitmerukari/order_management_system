<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 4/5/17
 * Time: 20:52
 */

namespace App\Controller;


use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

/**
 * Users Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\ClientProductsTable $ClientProducts
 * @property \App\Model\Table\TransportOrdersTable $TransportOrders
 * @property \App\Model\Table\UserProductsTable $UserProducts
 * @property \App\Controller\Component\EmailComponent $Email
 * @property \App\Controller\Component\SkuComponent $Sku
 */

class OrdersController extends AppController
{
    public function isAuthorized($user) {
        if(in_array($this->request->action, ['transportOrderManagement', 'transportOrderDetail'])) {
            return true;
        } else {
            if(isset($user['level']) && $user['level'] == 5) {
                return true;
            } else {
                return false;
            }

        }
    }

    public static $STATUS_NEW = 'new';
    public static $STATUS_WAIT_PAYMENT = 'wait_payment';
    public static $STATUS_DONE = 'done';
    public static $STATUS_REJECT = 'reject';

    public function management()
    {
        $user = TableRegistry::get('Users')->get($this->Auth->user('id'));
        $orderProcessDayLimit = $user->order_process_day_limit;
        $this->set('orderProcessDayLimit', $orderProcessDayLimit);

        if ($this->request->is('ajax')) {
            $status = $this->request->data('status');
//            if($status == "done") {
//                $orders = $this->loadModel('Orders')->find()
//                    ->where(['Orders.status' => "done"])
//                    ->orWhere(['Orders.status' => "reject"])
//                    ->contain(['UserProducts','Users']);
//            } else {
//                $orders = $this->loadModel('Orders')->find()
//                    ->where(['Orders.status' => $status])
//                    ->contain(['UserProducts','Users']);
//            }
            $orders = $this->loadModel('Orders')->find()
                ->contain(['UserProducts','Users']);

            $ordersArr = [];
            $timeNow = Time::now()->format('Y-m-d');

            foreach ($orders as $order) {
                $orderProducts = $order->user_products;
                $notify = '';

                $totalPrice = 0;

                if($order->status != 'reject') {
                    foreach ($orderProducts as $orderProduct)
                        if ($orderProduct->order_status !='reject') {
                            $totalPrice += $orderProduct->amount * $orderProduct->price;
                        }
                }


                $expectProcessTime = $order->created->modify('+' . $orderProcessDayLimit . ' days')->format('Y-m-d');

                $expectProcessTime = new Time(date('Y-m-d H:i:s', strtotime($expectProcessTime)));
                $timeNow = new Time(date('Y-m-d H:i:s', strtotime($timeNow)));

                $numberOfDayNeedToProcess = date_diff($expectProcessTime, $timeNow)->days;

                if (!$numberOfDayNeedToProcess)
                    $notify = '今日';
                else
                {
                    if ($expectProcessTime > $timeNow)
                        $notify = $numberOfDayNeedToProcess . ' 日残り';
                    else
                        $notify = $numberOfDayNeedToProcess . ' 日超過';
                }

                $ordersArr[] = [
                    'id' => $order->id,
                    'username' => isset($order->user->name) ? $order->user->name : '',
                    'total_price' => number_format($totalPrice),
                    'buy_date' => date('Y-m-d H:i:s', $order->created->getTimestamp()),
                    'notify' => $notify,
                    'payment_method' => $order->payment_method,
                    'status' => $order->status
                ];
            }

            $this->set(compact('ordersArr'));
            $this->set('_serialize', ['ordersArr']);
        }
        else
        {
            $status = $this->request->query('status');
            if (is_null($status))
                $status = 'wait_payment';

            $this->set(compact('status'));
        }
    }

    public function detail($id = null)
    {
        if (isset($id) && $this->Orders->exists(['id' => $id]))
        {
            $order = $this->Orders->get($id, ['contain' => ['UserProducts' => function(Query $q){
                return $q->contain(['Sizes']);
            }]]);
            $orderProducts = $order->user_products;


            $this->set(compact('order', 'orderProducts'));
//            $this->set('_serialize', ['order', 'orderProducts']);
        }
        else
        {
            $this->redirect(['action' => 'management']);
        }
    }


    public function detailOrder($id = null) {

        if ($this->request->is('ajax')) {
            $id = $this->request->data('id');
        }
            $order = $this->Orders
                ->find()
                ->where(['id' => $id])
                ->contain(['UserProducts' => function(Query $q){
                    return $q->contain(['Sizes']);
                }])->first();

            $orderProducts = $order->user_products;

            $this->set(compact('order', 'orderProducts'));
            $this->set('_serialize', ['order', 'orderProducts']);

    }

    public function rejectDetail() {

        if ($this->request->is('ajax')) {
            $id = $this->request->data('id');
            $order_id = $this->request->data('order_id');
            $tableOrder = TableRegistry::get('orders');
            $tableUserProduct = TableRegistry::get('userProducts');
            $tableAddress = TableRegistry::get('addresses');
            $result = '';

            try{
                if (is_array($id) &&  count($id) > 0 && $order_id) {
                    $tableUserProduct->query()
                        ->update()
                        ->set(['order_status' => 'reject'])
                        ->where(['id IN' => $id])
                        ->execute();
                    $order = $tableOrder->find()
                        ->where(['orders.id' => $order_id])
                        ->contain(['Users'])
                        ->first();
                    $products = $tableUserProduct->find()
                        ->where(['OR' => [
                            ['order_status !=' => 'reject'],
                            ['order_status is' => null]
                        ], 'order_id' => $order_id])
                        ->contain(['SubProducts'])
                        ->toArray();

                    /*
                     * Check count products
                     * If count products > 0 send MF cloud
                     * else not send Mf cloud, update status order
                     *
                     */
                    if (count($products) > 0) {
                        $createBillItems = [];
                        $productsInfo = [];
                        $totalPrice = 0;
                        foreach ($products as $product){
                            //dd($product);
                            $productsInfo[] = [
                                'parent_sku' => $product->sub_product->parent_sku,
                                'sku' => $product->sku,
                                'product_title' => $product->product_title,
                                'price' => $product->price,
                                'amount' => $product->amount,
                            ];

                            //$unit_price = round($product->price + $product->amount*0.08, 2);
                            $totalPrice += $product->price * $product->amount;

                            $createBillItems[] = [
                                'name' => $product->product_title,
                                'quantity' => $product->amount,
                                'unit_price' => $product->price,
                                'code' => $product->sub_product->parent_sku . "_" . $product->sku
                            ];
                        }

                        $tax = round( $totalPrice * 0.08,2);

                        $createBillItems [] = [
                            'name' => '消費税（8%）',
                            'quantity' => 1,
                            'unit_price' => $tax,
                            'code' => 'ABC99998'
                        ];

                        $address = $tableAddress->find()->where(['id' => $order->user->address_id])->first();

                        $price_ship = $this->checkShipping_fee($address->zip_code, $totalPrice);

                        $createBillItems [] = [
                            'name' => '送料',
                            'quantity' => 1,
                            'unit_price' => $price_ship,
                            'code' => 'ABC99999'
                        ];

                        if($order->price_ship != $price_ship) {
                            $order->price_ship = $price_ship;
                            $tableOrder->save($order);
                        }

                        $this->loadComponent('MFCloud');
                        $createBillResponse = $this->MFCloud->createBill($order->user->department_id, $createBillItems, $order->id);
                        Log::debug("created mf cloud: ".$order->user->department_id."   ".json_encode($createBillItems)."   ".$order->id);
                        if ($createBillResponse)
                        {
                            $createBillResponse = json_decode($createBillResponse);
                            if (isset($createBillResponse->errors))
                            {
                                $msg = $createBillResponse->errors[0]->message;
                                $result = 'error';
                            }
                            else
                            {
                                $order->bill_id = $createBillResponse->id;
                                $tableOrder->save($order);
                                $result = 'success';
                            }
                        }
                        else
                        {
                            $result = 'error';
                        }
                    } elseif (count($products) == 0) {
                        $order->status = 'reject';
                        $tableOrder->save($order);
                        $result = 'success';
                    }


                } else {
                    $result = 'error';
                }
            } catch (\Exception $e){
                $result = 'error';
                $msg = $e->getMessage();
            }

            $this->set(compact('result', 'msg'));


        }
    }

    public function checkShipping_fee($zip_code, $price) {

        $fee_ship = 0;
        $zip_code = str_replace('-', '', $zip_code);
        $temp_zip_code = $zip_code;

        $tableShippingFee = TableRegistry::get('shippingFee');
        $shippingFeeAll = $tableShippingFee->find()->where(['type' => 'all'])->first();
        $shippingFeeArea = $tableShippingFee->find()->where(['type' => 'area']);

        /*
         * Lấy danh sach khu vực đã setting giá
         * Tìm kiếm xem zipcode truyền vào bằng zipcode của khu vực trong database hay không
         * Kiểm tra giá đơn hàng truyền vào:
         *      $price > $price_order : nếu lớn hơn giá đơn hàng trong database thì tính giá ship = $fee_up
         *      ngược lại tính giá ship = $fee_default
         *
         */
        if($shippingFeeAll) {

            $flag = true;

            if(count($shippingFeeArea->toArray()) > 0) {
                foreach ($shippingFeeArea as $fee_area) {
                    $list_area = explode(',', $fee_area->area);
                    $fee_default = (int)$fee_area->fee_default;
                    $price_order = (int)$fee_area->price_order;
                    $fee_up = (int)$fee_area->fee_up;

                    foreach ($list_area as $area) {
                        $zip_code = $temp_zip_code;
                        $length_area = strlen($area);
                        $zip_code = substr(trim($zip_code), 0,$length_area);

                        if($zip_code == $area) {

                            $flag = false;

                            if ($price >= $price_order) {
                                $fee_ship = $fee_up;

                                if($fee_ship == 0 && $price_order == 0) {
                                    $fee_ship = $fee_default;
                                }

                            } else {
                                $fee_ship = $fee_default;
                            }
                        }

                    }
                    if($fee_ship > 0 || !$flag) {
                        break;
                    }
                }
            }

            /**
             * Nếu sau khi tìm kiếm giá ship vẫn bằng không thì lấy giá ship chung của tất cả khu vực
             * */
            if($fee_ship == 0 && $flag) {
                $fee_default = (int)$shippingFeeAll->fee_default;
                $price_order = (int)$shippingFeeAll->price_order;
                $fee_up = (int)$shippingFeeAll->fee_up;

                if ($price >= $price_order) {

                    $fee_ship = $fee_up;

                    if($fee_ship == 0 && $price_order == 0) {
                        $fee_ship = $fee_default;
                    }
                } else {
                    $fee_ship = $fee_default;
                }
            }
        }

        return $fee_ship;

    }

    public function approveOrder()
    {
        if ($this->request->is('ajax'))
        {
            $result = 'error';
            $isReload = true;
            $message = '';

            $orderId = $this->request->data('order_id');

            #FIXME: lock row
            $order = $this->Orders
                ->find()
                ->where(['id' => $orderId, 'status' => self::$STATUS_NEW])
                ->contain(['UserProducts' => function(Query $q){
                    return $q->contain(['SubProducts']);
                }])
                ->first();

            if($order)
            {
                $checkResult = true;
                foreach ($order->user_products as $userProduct)
                {
                    if ($userProduct->amount > $userProduct->sub_product->amount)
                    {
                        $message = 'sp ' . $userProduct->product_title .' vuot qua so luong hien tai';
                        $checkResult = false;
                        break;
                    }
                }

                if ($checkResult)
                {
                    $order->status = self::$STATUS_WAIT_PAYMENT;
                    $this->Orders->save($order);

                    $result = 'success';
                }
                else
                    $isReload = false;
            }
            else
            {
                $message = 'order not found';
            }

            $this->set(compact('result', 'isReload', 'message'));
        }
    }

    public function rejectOrder()
    {
        if ($this->request->is('ajax'))
        {
            $this->loadComponent('Email');

            $result = 'error';
            $isReload = true;
            $message = '';

            $orderId = $this->request->data('order_id');

            #FIXME: lock row
            $order = $this->Orders
                ->find()
                ->where(['id' => $orderId])
                ->contain(['UserProducts' => function(Query $q){
                    return $q->contain(['SubProducts']);
                }])
                ->first();

            if($order)
            {

                foreach ($order->user_products as $userProduct)
                {
                    if($userProduct) {
                        $userProduct->order_status = 'reject';
                        $this->loadModel('UserProducts')->save($userProduct);
                    }

                }

                $order->status = self::$STATUS_REJECT;
                $this->Orders->save($order);
                $result = 'success';

                $user = $this->loadModel('Users')->get($order->user_id);
                //$this->Email->rejectOrder($user->email, $user->username, $orderId);
            }
            else
            {
                $message = 'order not found';
            }

            $this->set(compact('result', 'isReload', 'message'));
        }
    }

    public function finishOrder()
    {
        if ($this->request->is('ajax'))
        {
            $this->loadComponent('Email');
            $this->loadComponent('Sku');
            $this->loadModel('ClientProducts');

            $result = 'error';
            $isReload = true;
            $message = '';
            $errors = [];

            $orderId = $this->request->data('order_id');

            #FIXME: lock row
            $order = $this->Orders
                ->find()
                ->where(['id' => $orderId, 'status' => self::$STATUS_WAIT_PAYMENT])
                ->contain(['UserProducts' => function(Query $q){
                    return $q->contain(['SubProducts'  => function(Query $q){
                        return $q->contain(['MainProducts']);
                    }]);
                }])
                ->first();

            if($order)
            {
                $checkResult = true;

                if ($checkResult)
                {
                    $products = [];
                    #FIXME: lock row
                    foreach ($order->user_products as $userProduct)
                    {
                        $clientProduct = $this->ClientProducts->find()->where(['user_id' => $order->user_id, 'sku' => $userProduct->sku])->first();

                        if (is_null($clientProduct))
                        {
                            $clientProduct = $this->ClientProducts->newEntity();
                            $clientProduct = $this->ClientProducts->patchEntity($clientProduct, [
                                'user_id' => $order->user_id,
                                'sku' => $userProduct->sku,
                                'product_title' => $userProduct->product_title,
                                'description' => $userProduct->description,
                                'product_photo1' => $userProduct->product_photo1,
                                'product_photo2' => $userProduct->product_photo2,
                                'product_photo3' => $userProduct->product_photo3,
                                'product_photo4' => $userProduct->product_photo4,
                                'price' => $userProduct->price,
                                'purchase_price' => $userProduct->price,
                                'size_id' => $userProduct->size_id,
                                'color' => $userProduct->color,
                                'amount' => $userProduct->amount,
                                'category_id' => $userProduct->sub_product->main_product->category_id,
                                'product_item_condition_id' => $userProduct->sub_product->main_product->product_item_condition_id,
                                'shipping_charge_id' => $userProduct->sub_product->main_product->shipping_charge_id,
                                'shipping_method_id' => $userProduct->sub_product->main_product->shipping_method_id,
                                'sender_address' => $userProduct->sub_product->main_product->sender_address,
                                'shipping_duration_id' => $userProduct->sub_product->main_product->shipping_duration_id,
                                'brand_id' => $userProduct->sub_product->main_product->brand_id,
                                'furiru_category_id' => $userProduct->sub_product->main_product->furiru_category_id,
                                'furiru_size_id' => $userProduct->sub_product->furiru_size_id,
                                'furiru_brand_id' => $userProduct->sub_product->main_product->furiru_brand_id,
                                'furiru_product_status_id' => $userProduct->sub_product->main_product->furiru_product_status_id,
                                'furiru_shipping_charge_id' => $userProduct->sub_product->main_product->furiru_shipping_charge_id,
                                'furiru_shipping_method_id' => $userProduct->sub_product->main_product->furiru_shipping_method_id,
                                'furiru_shipping_duration_id' => $userProduct->sub_product->main_product->furiru_shipping_duration_id,
                                'furiru_city_id' => $userProduct->sub_product->main_product->furiru_city_id,
                                'furiru_request_required' => $userProduct->sub_product->main_product->furiru_request_required,
                                'rakuma_category' => $userProduct->sub_product->main_product->rakuma_category,
                                'rakuma_size' => $userProduct->sub_product->rakuma_size,
                                'rakuma_brand' => $userProduct->sub_product->main_product->rakuma_brand,
                                'rakuma_condition_type' => $userProduct->sub_product->main_product->rakuma_condition_type,
                                'rakuma_postage_type' => $userProduct->sub_product->main_product->rakuma_postage_type,
                                'rakuma_delivery_method' => $userProduct->sub_product->main_product->rakuma_delivery_method,
                                'rakuma_prefecture_code' => $userProduct->sub_product->main_product->rakuma_prefecture_code,
                                'rakuma_delivery_term' => $userProduct->sub_product->main_product->rakuma_delivery_term,
                            ]);

                            $errors[] = $clientProduct->errors();
                        }
                        else
                        {
                            $clientProduct->amount += $userProduct->amount;
                        }
                        $this->ClientProducts->save($clientProduct);

                        $products[] = [
                            'sku' => $clientProduct->sku,
                            'amount' => $userProduct->amount,
                        ];
                    }

                    $order->status = self::$STATUS_DONE;
                    $this->Orders->save($order);

                    $user = $this->loadModel('Users')->get($order->user_id);
                    $this->Email->acceptOrder($user->email, $user->username, $orderId);

                    if (!empty($user->username_sku))
                    {
                        $this->Sku->increaseAmount($user->username_sku, $user->password_sku, $products);
                    }
                    $result = 'success';
                }
                else
                    $isReload = false;
            }
            else
            {
                $message = 'order not found';
            }

            $this->set(compact('result', 'isReload', 'message', 'errors'));
        }
    }

    public function changeNumberOfDaysLimit()
    {
        if ($this->request->is('post'))
        {
            $numberOfDays = $this->request->data('numberOfDays');
            $userTable = TableRegistry::get('Users');
            $user = $userTable->get($this->Auth->user('id'));
            $user->order_process_day_limit = $numberOfDays;
            $userTable->save($user);

            $result = 'success';
            $this->set(compact('result'));
            $this->set('_serialize', ['result']);
        }
    }

    public function listOrderProduct()
    {
        if ($this->request->is('post'))
        {
            $orderTable = TableRegistry::get('Orders');
            $sizeTable = TableRegistry::get('Sizes');

            $status = $this->request->data('status');
            $listOrder = $orderTable->find()->where(['status' => $status])->contain(['UserProducts'])->toArray();
            $data = [];

            foreach ($listOrder as $order)
            {
                $orderProducts = $order->user_products;

                foreach ($orderProducts as $product)
                {
                    $size = $sizeTable->get($product->size_id);
                    $data[] = [
                        'username' => $order->username,
                        'userProductId' => $product->id,
                        'title' => $product->product_title,
                        'sku' => $product->sku,
                        'photo' => $product->product_photo1,
                        'price' => $product->price,
                        'amount' => $product->amount,
                        'color' => $product->color,
                        'size' => $size->sizeName
                    ];
                }
            }

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
        else
        {
            $status = $this->request->query('status');
            if (is_null($status))
                $status = 'wait_payment';

            $this->set(compact('status'));
        }
    }

    public function exportCsvListOrderProduct()
    {
        if($this->request->is('post'))
        {
            $isCheckAll = $this->request->data('checkAll');
            $status = $this->request->data('status');
            $isCheckAll = filter_var($isCheckAll, FILTER_VALIDATE_BOOLEAN);

            $orderTable = TableRegistry::get('Orders');
            $sizeTable = TableRegistry::get('Sizes');
            $userProductTable = TableRegistry::get('UserProducts');
            $subProductTable = TableRegistry::get('SubProducts');
            $mainProductTable = TableRegistry::get('MainProducts');
            $categoryTable = TableRegistry::get('Categories');

            $data = [];

            if ($isCheckAll)
            {
                $listOrder = $orderTable->find()->where(['status' => $status])->contain(['UserProducts'])->toArray();

                foreach ($listOrder as $order)
                {
                    $orderProducts = $order->user_products;

                    foreach ($orderProducts as $product)
                    {
                        if ($subProductTable->exists(['sku' => $product->sku]))
                        {
                            $subProduct = $subProductTable->find()->where(['sku' => $product->sku])->first();

                            if ($mainProductTable->exists(['sku' => $subProduct->parent_sku]))
                            {
                                $mainProduct = $mainProductTable->find()->where(['sku' => $subProduct->parent_sku])->first();

                                if ($categoryTable->exists(['id' => $mainProduct->category_id]))
                                    $category = $categoryTable->get($mainProduct->category_id);
                            }
                        }

                        $size = $sizeTable->get($product->size_id);
                        $data[] = [
                            'categoryName' => (isset($category->categoryName)) ? $category->categoryName : '',
                            'title' => $product->product_title,
                            'sku' => $product->sku,
                            'color' => $product->color,
                            'amount' => $product->amount,
                            'size' => $size->sizeName,
                            'price' => $product->price,
                            'status' => ($order->status == 'done') ? '完了済' : ''
                        ];

                        if (isset($category->categoryName))
                            $category->categoryName = '';
                    }
                }
            }
            else
            {
                $userProductIdList = $this->request->data('userProductIdList');

                foreach ($userProductIdList as $userProductId)
                {
                    $userProduct = $userProductTable->get($userProductId);
                    $order = $orderTable->get($userProduct->order_id);
                    $size = $sizeTable->get($userProduct->size_id);

                    if ($subProductTable->exists(['sku' => $userProduct->sku]))
                    {
                        $subProduct = $subProductTable->find()->where(['sku' => $userProduct->sku])->first();

                        if ($mainProductTable->exists(['sku' => $subProduct->parent_sku]))
                        {
                            $mainProduct = $mainProductTable->find()->where(['sku' => $subProduct->parent_sku])->first();

                            if ($categoryTable->exists(['id' => $mainProduct->category_id]))
                                $category = $categoryTable->get($mainProduct->category_id);
                        }
                    }

                    $data[] = [
                        'categoryName' => (isset($category->categoryName)) ? $category->categoryName : '',
                        'title' => $userProduct->product_title,
                        'sku' => $userProduct->sku,
                        'color' => $userProduct->color,
                        'amount' => $userProduct->amount,
                        'size' => $size->sizeName,
                        'price' => $userProduct->price,
                        'status' => ($order->status == 'wait_payment') ? '処理中' : ''
                    ];

                    if (isset($category->categoryName))
                        $category->categoryName = '';
                }
            }

            if ( ! file_exists(WWW_ROOT . 'files/csv/listProduct')) {
                mkdir(WWW_ROOT . 'files/csv/listProduct', 0777, true);
            }
            $fp = fopen(WWW_ROOT . 'files/csv/listProduct/item.csv', 'w');

            $header = array('カテゴリ', '商品名','SKU','オプション', '数量','オプション', '単価', ' ');

            $convert_header = Common::convertToCSVData($header);
            fputcsv($fp, $convert_header);

            foreach ($data as $row) {
                fputcsv($fp, Common::convertToCSVData($row));
            }
            fclose($fp);

            $result = 'success';

            $this->set(compact('result'));
            $this->set('_serialize', ['result']);
        }
    }

    public function createTransportOrder()
    {
        if ($this->request->is('ajax'))
        {
            $this->loadModel('TransportOrders');
            $result = 'success';
            $message = '';

            $orderIds = $this->request->data('order_ids');

            $orders = $this->Orders->find()->where(['id in' => $orderIds]);


            foreach ($orders as $order)
            {
                if (!is_null($order->transport_order_id))
                {
                    $result = 'error';
                    $message = '選択された' .$order->id . '受注はすでに調達依頼しました。他の受注選択してください。';
                }
            }

            if ($result == 'success')
            {
                $transportOrder = $this->TransportOrders->newEntity([
                    'status' => 'new'
                ]);

                $this->TransportOrders->save($transportOrder);

                $this->Orders->updateAll(['transport_order_id' => $transportOrder->id], ['id in' => $orderIds]);
            }

            $this->set(compact('result', 'message'));
            $this->set('_serialize', ['result', 'message']);
        }
        else
        {
            $this->redirect(['action' => 'management']);
        }
    }

    public function transportOrderManagement()
    {
        if ($this->request->is('ajax'))
        {
            $this->loadModel('TransportOrders');

            $orders = $this->TransportOrders->find()
                ->contain('Orders');

            $orderArr = [];

            foreach ($orders as $order)
            {
                $arr_reject = [];

                if($order->orders) {
                    foreach ($order->orders as $value) {
                        if ($value->status == 'reject') $arr_reject[] = 'reject';
                    }
                }

                if(count($arr_reject) != count($order->orders)) {
                    $orderArr[] = [
                        'id' => $order->id,
                        'last_user_update' => ($order->update_user) ? $order->update_user : '',
                        'status' => $order->status,
                        'ship_id' => ($order->ship_id) ? $order->ship_id : '',
                        'ship_service' => ($order->ship_service) ? $order->ship_service : '',
                        'last_time_update' => date('Y-m-d H:i:s', $order->modified->getTimestamp()),
                    ];
                }

            }

            $this->set(compact('orderArr'));
            $this->set('_serialize', ['orderArr']);
        }
    }

    public function updateTransportOrder()
    {
        if ($this->request->is('ajax'))
        {
            $this->loadModel('TransportOrders');

            $orderId = $this->request->data('order_id');
            $shipId = $this->request->data('ship_id');
            $shipService = $this->request->data('ship_service');
            $status = $this->request->data('status');

            $transportOrder = $this->TransportOrders->get($orderId);

            $transportOrder = $this->TransportOrders->patchEntity($transportOrder, [
                'ship_id' => $shipId,
                'ship_service' => $shipService,
                'status' => $status,
                'update_user' => $this->Auth->user('username'),
            ]);

            $this->TransportOrders->save($transportOrder);
        }
        else
        {
            $this->redirect(['action' => 'transportOrderManagement']);
        }
    }

    public function transportOrderDetail($id = null)
    {
        $this->loadModel('TransportOrders');
        $this->loadModel('UserProducts');

        if (isset($id) && $this->TransportOrders->exists(['id' => $id]))
        {
            $userProductsTable = TableRegistry::get('UserProducts');
            $userProducts = $userProductsTable->find();

            $userProducts
                ->contain('Sizes')
                ->select([
                    'price',
                    'total_amount' => $userProducts->func()->sum('amount'),
                    'Sizes.sizeName',
                    'product_photo1',
                    'product_title',
                    'sku',
                    'color',
                ])
                ->innerJoinWith('Orders', function (Query $q) use ($id) {
                    return $q->where(['transport_order_id' => $id]);
                })
                ->where(['OR' => [
                    ['order_status !=' => 'reject'],
                    ['order_status is' => null]
                ]])
                ->group('sku');

            //dd($userProducts);
            $this->set(compact('userProducts'));
        }
        else
        {
            $this->redirect(['action' => 'transportOrderManagement']);
        }
    }
}