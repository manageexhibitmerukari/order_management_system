<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/22/17
 * Time: 11:00 AM
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\AddressesTable $Addresses
 *
 * @property \App\Controller\Component\EmailComponent $Email
 * @property \App\Controller\Component\MFCloudComponent $MFCloud
 */

class UsersController extends AppController
{
    public function isAuthorized($user) {
        if(in_array($this->request->action, ['index'])) {
            if(isset($user['level']) && $user['level'] == 5) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['register', 'fqa', 'guide', 'termOfService', 'termOfService2','contact','resetPass']);
    }

    public function login()
    {
        if ($this->Auth->user())
        {
            if ($this->Auth->user('level') == 5)
                $this->redirect(['controller' => 'UserProducts', 'action' => 'productList']);
            else if ($this->Auth->user('level') == 4)
                $this->redirect(['controller' => 'Orders', 'action' => 'transportOrderManagement']);
            else
                $this->redirect(['controller' => 'UserProducts', 'action' => 'top']);
        }

        $error = null;

        if ($this->request->is('post'))
        {
            $user = $this->Auth->identify();
            if ($user && $user['level'] == 5)
            {
                $this->Auth->setUser($user);
                $this->Users->updateAll(['last_login' => Time::now()], ['id' => $this->Auth->user('id')]);
                $this->redirect(['controller' => 'UserProducts', 'action' => 'productList']);
            }
            else if ($user && $user['level'] == 4)
            {
                $this->Auth->setUser($user);
                $this->Users->updateAll(['last_login' => Time::now()], ['id' => $this->Auth->user('id')]);
                $this->redirect(['controller' => 'Orders', 'action' => 'transportOrderManagement']);
            }
            else
            {
                $error = 'ユーザーIDまたはパスワードが正しくありません。';
            }
        }

        $this->set(compact('error'));
    }

    public function add(){
        $user = $this->Users->newEntity();
        $error = null;
        if ($this->request->is('post'))
        {
            $password = $this->request->data('password');
            $confirm_password = $this->request->data('confirm_password');
            if ( $password === $confirm_password )
            {
                $username = $this->request->data('username');
                $note = $this->request->data('note');
                $email = $this->request->data('email');
                if ( ! $this->Users->exists(['username' => $username]))
                {
                    if ( ! $this->Users->exists(['email' => $email])) {
                        $user = $this->Users->patchEntity($user, [
                            'username' => $username,
                            'password' => $password,
                            'email' => $email,
                            'note' => $note,
                            'level' => 4,
                            'reg_date' => Time::now(),
                            'last_login' => Time::now(),
                            'parent_user' => $this->Auth->user('id')
                        ]);
                        $this->Users->save($user);
                        $this->redirect(['action' => 'list-account']);
                    } else{
                        $error = 'このメールはすでに登録されました';
                    }
                } else {
                    $error = '登録済みのログインIDです。';
                }
            } else{
                $error = 'パスワードの確認が違います。';
            }
        }
        $this->set(compact('error'));
        $this->set('_serialize', ['error']);
    }

    public function updateUserInfo() {
        if($this->request->is('ajax'))
        {
            $currentUser = $this->request->data['current_user'];
            $username = $this->request->data['username'];
            $password = $this->request->data['password'];
            $email = $this->request->data['email'];
            $phoneNumber = $this->request->data['phone_number'];
            $name = $this->request->data['name'];
            $company = $this->request->data['company'];
            $postal = $this->request->data['postal'];
            $state = $this->request->data['state'];
            $town = $this->request->data['town'];
            $building = $this->request->data['building'];

            if ($currentUser > 0)
            { // update user info
                $user = $this->Users->get($currentUser);

                if($this->Users->exists(['id is not' => $currentUser, 'username' => $username])) {
                    $result = "error";
                    $msg = 'このログインIDは既に使用されています。';
                }
                else if ($this->Users->exists(['id is not' => $currentUser, 'email' => $email]))
                {
                    $result = "error";
                    $msg = 'このメールはすでに使用されています。';
                }
                else
                {
                    $user = $this->Users->patchEntity($user, [
                        'username'  => $username,
                        'password'  => $password,
                        'email'  => $email,
                        'phone_number' => $phoneNumber,
                        'name' => $name,
                        'company' => $company,
                        'postal' => $postal,
                        'state' => $state,
                        'town' => $town,
                        'building' => $building,
                    ]);

                    if($this->Users->save($user)) {
                        $result = "success";
                        $msg = $user;
                    } else {
                        $result = "error";
                        $msg = 'The user could not be saved. Please, try again.';
                    }
                }
            }
            else
            { // add new user
                if($this->Users->exists(['username' => $username])) {
                    $result = "error";
                    $msg = 'このログインIDは既に使用されています。';
                }
                else if ($this->Users->exists(['email' => $email]))
                {
                    $result = "error";
                    $msg = 'このメールはすでに使用されています。';
                }
                else
                {
                    $user = $this->Users->newEntity();
                    $user = $this->Users->patchEntity($user, [
                        'username' => $username,
                        'password' => $password,
                        'email' => $email,
                        'level' => 4,
                        'reg_date' => Time::now(),
                        'last_login' => Time::now(),
                        'parent_user' => $this->Auth->user('id'),
                        'name' => $name,
                        'company' => $company,
                        'postal' => $postal,
                        'state' => $state,
                        'town' => $town,
                        'building' => $building,
                    ]);
                    if ($this->Users->save($user)) {
                        $result = "success";
                    }
                    else
                    {
                        $msg = '[CREATE] The user could not be saved. Please, try again.';
                    }
                }
            }

            $this->set(compact('result','msg'));
            $this->set('_serialize', ['result','msg']);
        } else {
            $this->redirect(['action' => 'index']);
        }
    }

    public function addAdmin() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post'))
        {
            if ($this->request->data('wm-password') == 'VN201501')
            {
                $user = $this->Users->patchEntity($user, [
                    'username' => $this->request->data('username'),
                    'password' => $this->request->data('password'),
                    'email' => $this->request->data('email'),
                    'level' => 5,
                    'reg_date' => Time::now(),
                    'last_login' => Time::now()
                ]);

                $user = $this->Users->save($user);
                if ($user)
                {
                    $user->parent_user = $user->id;
                    $this->Users->save($user);
                }
                $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function changePassword()
    {
        $result = null;
        $message = null;
        $id = $this->request->query('id');
        if ($this->request->is('post'))
        {
            $currentPassword = $this->request->data('current_password');
            $password1 = $this->request->data('password1');

            $user = $this->Users->get($id);

            $currentPassword = Security::hash($currentPassword, 'md5', true);

            if ($currentPassword == $user->password)
            {
                $user->password = $password1;
                $this->Users->save($user);

                $result = true;
                $message = 'パスワードを変更しました。';
            }
            else
            {
                $result = false;
                $message = '現在のパスワードが正しくありません。';
            }
        }

        $this->set(compact('result', 'message', 'id'));
    }

    public function changeUserPassword() {
        $this->viewBuilder()->layout('user');

        $pageName = "パスワード変更";
        $this->set(compact('pageName'));
        $this->set('_serialize', ['pageName']);
        if($this->request->is('post')) {
            $current_pass = $this->request->data('current_pass');
            $new_pass = $this->request->data('new_pass');
            $current_pass = Security::hash($current_pass, 'md5', true);

            $user = $this->Users->get($this->Auth->user('id'));

            if ($current_pass == $user->password) {

                if ($new_pass) {
                    $user->password = $new_pass;
                    $this->Users->save($user);
                    $this->Flash->success("パスワードを変更しました。");
                } else {
                    $this->Flash->error("エラー");
                }

            } else {
                $this->Flash->error("現在のパスワードが正しくありません。");
            }
        }



    }

    public function loginUser()
    {
        $this->viewBuilder()->layout('user');
        $pageName = 'ログイン';
        $this->set(compact('pageName'));
        if ($this->Auth->user())
        {
            $this->redirect(['controller' => 'UserProducts', 'action' => 'top']);
        }

        $error = null;

        if ($this->request->is('post'))
        {
            $user = $this->Auth->identify();

            if ($user && $user['created_by_admin'] && $user['is_active'] )
            {
                $this->Auth->setUser($user);
                $this->Users->updateAll(['last_login' => Time::now()], ['id' => $this->Auth->user('id')]);
                $this->redirect($this->Auth->redirectUrl());
            }
            else
            {
                $error = 'ユーザーIDまたはパスワードが正しくありません。';
//                $error = '7月7日19時サイトOPENとなっております。少々お待ち下さいませ。';

            }

            $this->set(compact('error'));
        }

        if ($this->request->is('ajax'))
        {
            $user = $this->Auth->identify();
            $msg = '';
            if ($user && $user['created_by_admin'])
            {
                $this->Auth->setUser($user);
                $this->Users->updateAll(['last_login' => Time::now()], ['id' => $this->Auth->user('id')]);
                $result = 'success';
            } else {
                $result = 'error';
                $msg = 'ユーザーIDまたはパスワードが正しくありません。';
            }

            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);
        }

    }

    public function logout()
    {
        if ($this->Auth->user()&&$this->Auth->user('level') == 5){
            $redirect = ['action' => 'login'];
        }
        else if ($this->Auth->user()&&$this->Auth->user('level') == 4){
            $redirect = ['action' => 'login'];
        } else {
            $redirect = ['controller' => 'userProducts',  'action' => 'top'];
        }
        $this->request->session()->destroy();
        $this->Auth->logout();
        return $this->redirect($redirect);
//        return $this->redirect(['controller' => 'userProducts',  'action' => 'top']);
    }

    public function index() {

        if ($this->request->is('ajax'))
        {
            $users = $this->Users->find()
                ->where(['level'=> 4, 'parent_user' => $this->Auth->user('id')])
                ->toArray();
            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        }
    }

    public function delete() {
        if($this->request->is('ajax')) {
            $userId = $this->request->data['user_id'];
            $user = $this->Users->get($userId);

            if($this->Users->delete($user))
            {
                $result = "success";
                $msg = 'Deleted';
                $this->set(compact('result','msg'));
                $this->set('_serialize', ['result','msg']);
            } else {
                $result = "error";
                $msg = 'User not found';
                $this->set(compact('result','msg'));
                $this->set('_serialize', ['result','msg']);
            }
        } else {
            $this->redirect(['controller' => 'Accounts', 'action' => 'index']);
        }
    }

    public function loginSku() {
        if ($this->request->is('ajax')) {
            $result = '';
            $msg = '';
            $header = array(
                "Accept: application/json"
            );
            $username = $this->request->data('username');
            $password = $this->request->data('password');
            $post = array(
                'username' => $username,
                'password' => $password
            );

            $defaults = array(
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_URL => 'http://160.16.61.164:8080/api/products/get-products',
                CURLOPT_POSTFIELDS => http_build_query($post),
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => 40,
                CURLOPT_SSL_VERIFYPEER => false
            );

            $ch = curl_init();
            curl_setopt_array($ch, $defaults);
            $response = curl_exec($ch);

            if ($response) {
                $response = json_decode($response);
                if ($response->result == 'OK') {
                    $result = 'success';
                    $tableUser = TableRegistry::get('users');
                    $data = [
                        'username_sku' => $username,
                        'password_sku' => $password
                    ];
                    $tableUser->updateAll($data, ['id' => $this->Auth->user('id')]);
                } else {
                    $msg = $response->message;
                }
            }
            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);
        }

    }

    public function registerSku() {
        if ($this->request->is('ajax')) {
            $result = '';
            $msg = '';
            $header = array(
                "Accept: application/json"
            );
            $username = $this->request->data('username');
            $password = $this->request->data('password');
            $post = array(
                'username' => $username,
                'password' => $password
            );

            $defaults = array(
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_URL => 'http://160.16.61.164:8080/api/users/register',
                CURLOPT_POSTFIELDS => http_build_query($post),
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => 40,
                CURLOPT_SSL_VERIFYPEER => false
            );

            $ch = curl_init();
            curl_setopt_array($ch, $defaults);
            $response = curl_exec($ch);
            if ($response) {
                $response = json_decode($response);
                if ($response->result == 'OK') {
                    $result = 'success';
                    $tableUser = TableRegistry::get('users');
                    $data = [
                        'username_sku' => $username,
                        'password_sku' => $password
                    ];
                    $tableUser->updateAll($data, ['id' => $this->Auth->user('id')]);
                } else {
                    $msg = $response->message;
                }
            }
            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);
        }
    }

    public function retailsList()
    {
        if ($this->request->is('ajax'))
        {
            $users = $this->Users->find()->where(['level' => 0, 'created_by_admin' => true]);

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        }
    }

    public function updateRetailInfo() {
        if($this->request->is('ajax'))
        {
            $this->loadModel('Addresses');

            $currentUser = $this->request->data['current_user'];
            $username = $this->request->data['username'];
            $password = $this->request->data['password'];
            $email = $this->request->data['email'];
            $phoneNumber = $this->request->data['phone_number'];
            $name = $this->request->data['name'];
            $company = $this->request->data['company'];
            $postal = $this->request->data['postal'];
            $state = $this->request->data['state'];
            $town = $this->request->data['town'];
            $building = $this->request->data['building'];
            $houseNumber = $this->request->data['house_number'];

            if ($currentUser > 0)
            { // update user info
                $user = $this->Users->get($currentUser);

                if($this->Users->exists(['id is not' => $currentUser, 'username' => $username])) {
                    $result = "error";
                    $msg = 'このログインIDは既に使用されています。';
                }
                else if ($this->Users->exists(['id is not' => $currentUser, 'email' => $email]))
                {
                    $result = "error";
                    $msg = 'このメールはすでに使用されています。';
                }
                else
                {
                    $user = $this->Users->patchEntity($user, [
                        'username'  => $username,
                        'password'  => $password,
                        'email'  => $email,
                        'phone_number' => $phoneNumber,
                        'name' => $name,
                        'company' => $company,
                        'postal' => $postal,
                        'state' => $state,
                        'town' => $town,
                        'building' => $building,
                        'house_number' => $houseNumber,
                    ]);

                    if($this->Users->save($user)) {
                        $result = "success";
                        $msg = $user;
                    } else {
                        $result = "error";
                        $msg = 'The user could not be saved. Please, try again.';
                    }
                }
            }
            else
            { // add new user
                if($this->Users->exists(['username' => $username])) {
                    $result = "error";
                    $msg = 'このログインIDは既に使用されています。';
                }
                else if ($this->Users->exists(['email' => $email]))
                {
                    $result = "error";
                    $msg = 'このメールはすでに使用されています。';
                }
                else
                {
                    $user = $this->Users->newEntity();
                    $user = $this->Users->patchEntity($user, [
                        'username' => $username,
                        'password' => $password,
                        'email' => $email,
                        'level' => 0,
                        'reg_date' => Time::now(),
                        'last_login' => Time::now(),
                        'parent_user' => $this->Auth->user('id'),
                        'phone_number' => $phoneNumber,
                        'name' => $name,
                        'company' => $company,
                        'postal' => $postal,
                        'state' => $state,
                        'town' => $town,
                        'building' => $building,
                        'created_by_admin' => true,
                        'house_number' => $houseNumber,
                    ]);
                    if ($this->Users->save($user)) {
                        $result = "success";

                        $fullAddress = "$postal\r\n$state$town$houseNumber$building\r\n$company\r\n電話番号: $phoneNumber";
                        $address = $this->Addresses->newEntity([
                            'user_id' => $user->id,
                            'name' => $name,
                            'zip_code' => $postal,
                            'city' => $state,
                            'address1' => $town.$houseNumber,
                            'address2' => $building,
                            'company_name' => $company,
                            'phone_number' => $phoneNumber,
                            'full_address' => $fullAddress
                        ]);

                        $this->Addresses->save($address);

                        $user->address_id = $address->id;
                        $this->Users->save($user);
                    }
                    else
                    {
                        $msg = '[CREATE] The user could not be saved. Please, try again.';
                    }
                }
            }

            $this->set(compact('result','msg'));
            $this->set('_serialize', ['result','msg']);
        } else {
            $this->redirect(['action' => 'index']);
        }
    }

    public function register()
    {
        $this->viewBuilder()->layout('user');
        $error = null;
        $success = false;
        $pageName = "会員登録";
        if ($this->request->is('post'))
        {
            $this->loadModel('Addresses');

            $username = $this->request->data['username'];
            $password = $this->request->data['password'];
            $email = $this->request->data['email'];
            $phoneNumber = $this->request->data['phone_number'];
            $name = $this->request->data['name'];
            $paymentName = $this->request->data['payment_name'];
            $nameKana = $this->request->data['name_kana'];
            $company = $this->request->data['company'];
            $postal = $this->request->data['postal'];
            $state = $this->request->data['state'];
            $town = $this->request->data['town'];
            $building = $this->request->data['building'];
            $houseNumber = $this->request->data['house_number'];

            if($this->Users->exists([
                'username' => $username,
                'OR' => [
                    ['is_active' => true],
                    ['is_active' => false, 'active_token_expire >' => new \DateTime()]
                ]
            ])) {
                $error = 'このログインIDは既に使用されています。';
            }
            else if ($this->Users->exists([
                'email' => $email,
                'OR' => [
                    ['is_active' => true],
                    ['is_active' => false, 'active_token_expire >' => new \DateTime()]
                ]
            ]))
            {
                $error = 'このメールはすでに使用されています。';
            }
            else
            {
                $activeToken = Security::hash($username, 'md5', true);
                $user = $this->Users->find()->where([
                    'OR' => [
                        ['username' => $username],
                        ['email' => $email]
                    ]
                ])->first();

                if (is_null($user))
                    $user = $this->Users->newEntity();

                $user = $this->Users->patchEntity($user, [
                    'username' => $username,
                    'password' => $password,
                    'email' => $email,
                    'level' => 0,
                    'reg_date' => Time::now(),
                    'last_login' => Time::now(),
                    'parent_user' => 1,
                    'phone_number' => $phoneNumber,
                    'name' => $name,
                    'payment_name' => $paymentName,
                    'name_kana' => $nameKana,
                    'company' => $company,
                    'postal' => $postal,
                    'state' => $state,
                    'town' => $town,
                    'building' => $building,
                    'created_by_admin' => true,
                    'house_number' => $houseNumber,
                    'is_active' => false,
                    'active_token' => $activeToken,
                    'active_token_expire' => new \DateTime('+1 day'),
                ]);

                if ($this->Users->save($user))
                {
                    $fullAddress = "$postal\r\n$state$town$houseNumber$building\r\n$company\r\n電話番号: $phoneNumber";
                    $address = $this->Addresses->newEntity([
                        'user_id' => $user->id,
                        'name' => $name,
                        'zip_code' => $postal,
                        'city' => $state,
                        'address1' => $town . $houseNumber,
                        'address2' => $building,
                        'company_name' => $company,
                        'phone_number' => $phoneNumber,
                        'full_address' => $fullAddress
                    ]);

                    $this->Addresses->save($address);

                    $user->address_id = $address->id;
                    $this->Users->save($user);

                    $this->loadComponent('Email');
                    $activeUrl = 'http://' . $this->request->host() . '/users/active-user?active_token='.$activeToken;
                    $this->Email->sendRegisterConfirm($email, $activeUrl);
                    $success = true;
                }
            }
        }

        $this->set(compact('error', 'success', 'pageName'));
    }

    public function activeUser()
    {
        $this->viewBuilder()->layout('user');

        $activeToken = $this->request->query('active_token');

        $message = '会員登録できませんでした。再度会員登録をしてください。';

        if (!empty($activeToken))
        {
            $user = $this->Users->find()->where([
                'active_token' => $activeToken,
                'is_active' => false,
                'active_token_expire >=' => new \DateTime(),
            ])->first();


            if ($user)
            {
                $paymentRegisterResult = true;

                $address = $this->loadModel('Addresses')->find()->where(['user_id' => $user->id])->first();
                $this->loadComponent('MFCloud');
                $registerResponse = $this->MFCloud->register($user->payment_name, $user->name_kana, $user->phone_number, $user->email, $user->state, $address->address1, $address->address2);

                if ($registerResponse)
                {
                    $registerResponse = json_decode($registerResponse);
                    if (isset($registerResponse->errors))
                    {
                        $message = $registerResponse->errors[0]->message;
                        $paymentRegisterResult = false;
                    }
                    else
                    {
                        $mfCloudId = $registerResponse->id;
                        $departmentId = $registerResponse->departments[0]->id;
                    }
                }
                else
                {
                    $paymentRegisterResult = false;
                }

                if ($paymentRegisterResult)
                {
                    $user->is_active = true;
                    $user->mf_cloud_id = $mfCloudId;
                    $user->department_id = $departmentId;
                    $this->Users->save($user);

                    $message = '会員登録が完了しました。';

                    $this->loadComponent('Email');
                    $this->Email->afterActiveUser($user->email, $user->name);
                }
            }
        }

        $this->set(compact('message'));
    }

    public function addressManagement()
    {
        $this->viewBuilder()->layout('user');

        $pageName = "お届け先";

        $this->loadModel('Addresses');
        $addresses = $this->Addresses->find()->where(['user_id' => $this->Auth->user('id')]);

        foreach ($addresses as $address)
            $address->full_address = nl2br($address->full_address);

        $user = $this->Users->get($this->Auth->user('id'));
        $currentAddressId = $user->address_id;

        $this->set(compact('addresses', 'currentAddressId', 'pageName'));
    }

    public function editAddress($id = null)
    {
        $this->viewBuilder()->layout('user');
        $this->loadModel('Addresses');

        $pageName = "お届け先編集";

        if (isset($id) && $this->Addresses->exists(['id' => $id, 'user_id' => $this->Auth->user('id')]))
        {
            $address = $this->Addresses->get($id);

            if ($this->request->is(['patch', 'post', 'put']))
            {
                $data = $this->request->data();
                $data['full_address'] = $data['zip_code']."\r\n".$data['city'].$data['address1'].$data['address2']."\r\n".$data['company_name']."\r\n電話番号: ".$data['phone_number'];
                $address = $this->Addresses->patchEntity($address, $data);
                if ($this->Addresses->save($address)) {


                }

                $this->redirect(['action' => 'addressManagement']);
            }

            $this->set(compact('address', 'pageName'));
            $this->set('_serialize', ['address', 'pageName']);
        }
        else
        {
            $this->redirect(['action' => 'addressManagement']);
        }
    }

    public function addAddress()
    {
        $this->viewBuilder()->layout('user');
        $this->loadModel('Addresses');

        $pageName = "お届け先追加";

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $address = $this->Addresses->newEntity();
            $data = $this->request->data();
            $data['full_address'] = $data['zip_code']."\r\n".$data['city'].$data['address1'].$data['address2']."\r\n".$data['company_name']."\r\n電話番号: ".$data['phone_number'];
            $data['user_id'] = $this->Auth->user('id');
            $address = $this->Addresses->patchEntity($address, $data);
            if ($this->Addresses->save($address)) {

                $this->redirect(['action' => 'addressManagement']);
            }
        }
        $this->set(compact('pageName'));
    }

    public function changeAddress($id = null)
    {
        $this->viewBuilder()->layout('user');
        $this->loadModel('Addresses');

        if (isset($id) && $this->Addresses->exists(['id' => $id, 'user_id' => $this->Auth->user('id')]))
        {
            $user = $this->Users->get($this->Auth->user('id'));
            $this->Users->patchEntity($user, [
                'address_id' => $id
            ]);
            $this->Users->save($user);
        }

        $this->redirect(['controller' => 'userProducts', 'action' => 'showCart']);
    }

    public function deleteAddress($id = null)
    {
        $this->viewBuilder()->layout('user');
        $this->loadModel('Addresses');

        if (isset($id) && $this->Addresses->exists(['id' => $id, 'user_id' => $this->Auth->user('id')]))
        {
            $address = $this->Addresses->get($id);
            $this->Addresses->delete($address);
        }

        $this->redirect(['action' => 'addressManagement']);

    }

    public function resetPass() {
        $this->viewBuilder()->layout('user');

        if ($this->Auth->user())
        {
            $this->redirect(['controller' => 'UserProducts', 'action' => 'top']);
        }

        if($this->request->is('post')) {

            $email = $this->request->data['email'];
            $user = $this->Users->find()->where(['email' => $email])->first();
            //dd($user);
            if ($user) {

                $new_pass = $this->randomPassword();
                $user->password = $new_pass;

                if($this->Users->save($user)) {

                    $this->loadComponent('Email');
                    $this->Email->emailResetPass($user->email, $user->username, $new_pass);
                    $this->Flash->success("新しいパスワードを入力したメールアドレスへ送りました。ご確認した上で再ログインしてパスワードを更新してください。
");
                }
            } else {
                $this->Flash->error("メールアドレスが未会員登録です。会員登録済のメールアドレスを入力してください。");
            }

        }
    }


    public  function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }



    public function fqa () {
        $this->viewBuilder()->layout('user');

        $pageName = "よくある質問";

        $this->set(compact('pageName'));

    }

    public function guide() {
        $this->viewBuilder()->layout('user');

        $pageName = "ご利用ガイド";

        $this->set(compact('pageName'));
    }

    public function termOfService() {
        $this->viewBuilder()->layout('user');

        $pageName = "利用規約";

        $this->set(compact('pageName'));
    }

    public function termOfService2() {
        $this->viewBuilder()->layout('user');

        $pageName = "特定商取引法に基づく表記";

        $this->set(compact('pageName'));
    }

    public function contact() {
        $this->viewBuilder()->layout('user');

        $pageName = "お問い合わせ";

        $this->set(compact('pageName'));

        if ($this->request->is('post')) {
            //dd($this->request->data);
            $firstname = $this->request->data['firstname'];
            $lastname = $this->request->data['lastname'];
            $email = $this->request->data['email'];
            $address = $this->request->data['address'];
            $issue = $this->request->data['issue'];
            $phone = $this->request->data['phone'];
            $content = $this->request->data['content'];


            $user = $this->Users->find()->where(['level' => 5])->first();

            //dd($user);
            $this->loadComponent('Email');
            $this->Email->emailContact($firstname,$lastname, $email,$address,$issue,$phone,$content,Configure::read('Contact.receiveEmail'),Configure::read('Contact.ccEmails'));
            $this->Flash->success('問い合わせ内容を送りました。担当者からメールで連絡します。
連絡が届くまでしばらくお待ちください。');

            //dd($this->request->data);
        }
    }

    public function updateInfo()
    {
        $this->viewBuilder()->layout('user');
        $error = null;
        $result = null;

        $pageName = "会員情報管理";

        $user = $this->Users->get($this->Auth->user('id'));

        if ($this->request->is(['post', 'put']))
        {
            $username = $this->request->data['username'];
            $email = $this->request->data['email'];
            $phoneNumber = $this->request->data['phone_number'];
            $name = $this->request->data['name'];
            $paymentName = $this->request->data['payment_name'];
            $nameKana = $this->request->data['name_kana'];
            $company = $this->request->data['company'];
            $postal = $this->request->data['postal'];
            $state = $this->request->data['state'];
            $town = $this->request->data['town'];
            $building = $this->request->data['building'];
            $houseNumber = $this->request->data['house_number'];

            if($this->Users->exists([
                'username' => $username,
                'id is not' => $user->id,
                'OR' => [
                    ['is_active' => true],
                    ['is_active' => false, 'active_token_expire >' => new \DateTime()]
                ]
            ])) {
                $error = 'このログインIDは既に使用されています。';
                $result = 'error';
            }
            else if ($this->Users->exists([
                'email' => $email,
                'id is not' => $user->id,
                'OR' => [
                    ['is_active' => true],
                    ['is_active' => false, 'active_token_expire >' => new \DateTime()]
                ]
            ]))
            {
                $error = 'このメールはすでに使用されています。';
                $result = 'error';
            }
            else
            {
                if (isset($user->mf_cloud_id))
                {
                    $this->loadComponent('MFCloud');
                    $partnerInfo = [
                        'department_id' => $user->department_id,
                        'name' => $paymentName,
                        'name_kana' => $nameKana,
                        'phone_number' => $phoneNumber,
                        'email' => $email,
                        'prefecture' => $state,
                        'address1' => $town.$houseNumber,
                        'address2' => $building,
                    ];
                    $updatePartnerResponse = $this->MFCloud->updatePartner($user->mf_cloud_id, $partnerInfo);

                    if ($updatePartnerResponse)
                    {
                        $updatePartnerResponse = json_encode($updatePartnerResponse);
                        if (isset($updatePartnerResponse->errors))
                        {
                            $error = $updatePartnerResponse->errors[0]['message'];
                            $result = 'error';
                        }
                    }
                }

                if ($result !== 'error')
                {
                    $user = $this->Users->patchEntity($user, [
                        'username' => $username,
                        'email' => $email,
                        'phone_number' => $phoneNumber,
                        'name' => $name,
                        'payment_name' => $paymentName,
                        'name_kana' => $nameKana,
                        'company' => $company,
                        'postal' => $postal,
                        'state' => $state,
                        'town' => $town,
                        'building' => $building,
                        'house_number' => $houseNumber,
                    ]);

                    $this->Users->save($user);
                    $result = 'success';
                }
            }
        }

        $this->set(compact('error', 'result', 'user', 'pageName'));
        $this->set('_serialize', ['error', 'result', 'user', 'pageName']);
    }
}