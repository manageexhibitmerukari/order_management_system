<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/23/17
 * Time: 2:11 PM
 */

namespace App\Controller;

use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Common extends AppController
{

    public static function findCsvFilePath($path)
    {
        $it = new RecursiveDirectoryIterator($path);
        foreach (new RecursiveIteratorIterator($it) as $file) {
            if (basename($file) == 'item.csv') {
                $fileInfo = $file->getPathname();
                return $fileInfo;
            }
        }

        return null;
    }

    public static function convertImage($originalImage, $outputImage, $quality)
    {
        // jpg, png, gif or bmp?
        $image = new \Imagick($originalImage);
        $imageInfo = $image->identifyImage();

        if ($imageInfo['mimetype'] == 'image/jpeg')
            $imageTmp=imagecreatefromjpeg($originalImage);
        else if ($imageInfo['mimetype'] == 'image/png')
            $imageTmp=imagecreatefrompng($originalImage);
        else
            return 0;

        // quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, $outputImage, $quality);
        imagedestroy($imageTmp);

        return 1;
    }

    public static function convertToCSVData($data) {
        $arr = [];
        foreach ($data as $row) {
            $arr[] = mb_convert_encoding($row, 'CP932', 'ASCII,JIS,UTF-8,eucJP-win,SJIS-win');
        }
        return $arr;
    }

    public static function substrImage($image) {
        $name = '';
        if ($image != '') {
            $arr = explode('/', $image);
            $name = end($arr);
        }
        return $name;
    }

    public static function sendMailUser($title, $content, $nTo, $mTo){

        $nFrom = 'Watermelon';
        $mFrom = 'vuquocdat_t57@hus.edu.vn';
        $mPass = '12002193';
        $mail             = new \PHPMailer();
        $body             = $content;
        $mail->isSMTP();
        $mail->CharSet   = "utf-8";
        $mail->SMTPDebug  = 0;
        $mail->Debugoutput = 'html';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = "tls";
        $mail->Host       = "smtp.gmail.com";
        $mail->Port       = 587;
        $mail->Username   = $mFrom;
        $mail->Password   = $mPass;
        $mail->SetFrom($mFrom, $nFrom);

        $mail->Subject    = $title;
        $mail->MsgHTML($body);
        $address = $mTo;
        $mail->AddAddress($address, $nTo);

        if(!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function create($destination = '', $files = array(), $overwrite = false) {
        if (file_exists($destination) && !$overwrite) {
            return false;
        }
        $validFiles = array();
        if (is_array($files)) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    $validFiles[] = $file;
                }
            }
        }
        if (count($validFiles) < 1) {
            return false;
        }

        $zip = new \ZipArchive();
        $type = $overwrite ? \ZIPARCHIVE::OVERWRITE : \ZIPARCHIVE::CREATE;
        if ($zip->open($destination, $type) !== true) {
            return false;
        }
        foreach ($validFiles as $file) {
            $zip->addFile($file, basename($file));
        }
        $zip->close();
        return file_exists($destination);
    }

    public static function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}