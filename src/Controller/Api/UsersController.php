<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 4/18/17
 * Time: 14:14
 */

namespace App\Controller\Api;

use Cake\I18n\Time;
use App\Controller\Common;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */

class UsersController extends AppController
{
    public function login()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $result = "OK";
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function addUsers()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $result = 'OK';
            $users = $this->request->data('users');

            foreach ($users as $userData)
            {
                $userObj = $this->Users->find()->where(['username' => $userData['username']])->first();

                if (is_null($userObj))
                {
                    $userObj = $this->Users->newEntity();
                    $userObj->parent_user = 3;
                    $userObj->note = '';
                    $userObj->reg_date = Time::now();
                    $userObj->level = 0;
                    $userObj->username = $userData['username'];
                }

                $userObj->password = $userData['password'];
                $userObj->email = $userData['email'];
                $userObj->username_sku = $userData['sku_username'];
                $userObj->password_sku = $userData['sku_password'];

                $this->Users->save($userObj);
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
    public function addUser() {
        $result = 'error';
        $data = null;
        $message = "";

        $userRequest = $this->request->data('user');

        $user = $this->Users->newEntity();
        $user = $this->Users->patchEntity($user, [
            'username' => $userRequest['username'],
            'email' => $userRequest['email'],
            'password' => $userRequest['password'],
            'parent_user' => 3,
            'level' => 0,
            'reg_date' => Time::now(),
        ]);

        if ($this->Users->save($user)) {
            $result = 'OK';
        }

        $this->returnResponse($result, $data, $message);
    }

    public function updateSkuInfo()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $result = 'OK';
            $skuUsername = $this->request->data('sku_username');
            $skuPassword = $this->request->data('sku_password');

            $userObj = $this->Users->get($user['id']);

            $userObj->username_sku = $skuUsername;
            $userObj->password_sku = $skuPassword;

            $this->Users->save($userObj);
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function deleteUser()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $userObj = $this->Users->get($user['id']);
            $this->Users->delete($userObj);
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
}