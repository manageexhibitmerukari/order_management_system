<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 4/11/17
 * Time: 11:43
 */

namespace App\Controller\Api;


class ShippingRequestsController extends AppController
{
    public function addShippingRequest()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $shippingRequests = $this->request->data('shipping_requests');

            $isInvalid = false;

            foreach ($shippingRequests as $shippingRequest)
            {
                $clientProduct = $this->loadModel('ClientProducts')->find()->where(['sku' => $shippingRequest['sku'], 'user_id' => $user['id']])->first();
                if (is_null($clientProduct))
                {
                    $isInvalid = true;

                    $message = "倉庫にはこの商品" .$shippingRequest['sku']. "が存在していません。";

                    break;
                }
                else if ($clientProduct->amount <= 0)
                {
                    $isInvalid = true;

                    $message = "発送できませんでした。この商品" .$shippingRequest['sku']. "は在庫が切れました。";

                    break;
                }
            }


            if (!$isInvalid)
            {
                $result = 'OK';
                foreach ($shippingRequests as $shippingRequest)
                {
                    $shippingRequestObj = $this->loadModel('ShippingRequests')->find()->where(['item_code' => $shippingRequest['item_code'], 'user_id' => $user['id']])->first();

                    if (is_null($shippingRequestObj))
                    {
                        $shippingRequestObj = $this->loadModel('ShippingRequests')->newEntity();
                    }
                    else if ($shippingRequestObj->status == 'done')
                        continue;

                    $shippingRequestObj = $this->loadModel('ShippingRequests')->patchEntity($shippingRequestObj, [
                        'user_id' => $user['id'],
                        'buyer_address' => $shippingRequest['buyer_address'],
                        'sku' => $shippingRequest['sku'],
                        'status' => 'new',
                        'item_code' => $shippingRequest['item_code'],
                    ]);

                    $this->loadModel('ShippingRequests')->save($shippingRequestObj);
                }
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function getShippingRequest()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $itemCode = $this->request->data('item_code');

            $shippingRequest = $this->loadModel('ShippingRequests')
                ->find()
                ->where(['item_code' => $itemCode, 'user_id' => $user['id']])
                ->select(['buyer_address', 'sku', 'status', 'item_code'])
                ->first();

            if ($shippingRequest)
            {
                $data['shipping_request'] = $shippingRequest;

                $result = 'OK';
            }
            else
            {
                $message = 'この商品がみつかりませんでした。';
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
}