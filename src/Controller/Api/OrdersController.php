<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 4/18/17
 * Time: 14:14
 */

namespace App\Controller\Api;

use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */

class OrdersController extends AppController
{
    public function getList()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $result = 'OK';

            $orders = $this->loadModel('Orders')->find()->where(['user_id' => $user['id'], 'status != ' => 'reject'])->contain(['UserProducts']);
            $ordersArr = [];

            foreach ($orders as $order) {
                $orderProducts = $order->user_products;

                $totalPrice = 0;

                foreach ($orderProducts as $orderProduct)
                    $totalPrice += $orderProduct->amount * $orderProduct->price;

                $ordersArr[] = [
                    'id' => $order->id,
                    'status' => $order->status,
                    'total_price' => $totalPrice,
                    'buy_date' => date('Y-m-d H:i', $order->created->getTimestamp()),
                ];
            }

            $data['orders'] = $ordersArr;
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function getDetail()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $orderId = $this->request->data('order_id');
            if ($this->Orders->exists(['id' => $orderId, 'user_id' => $user['id']]))
            {
                $order = $this->Orders->get($orderId, ['contain' => ['UserProducts' => function(Query $q){
                    return $q->contain(['Sizes']);
                }]]);

                $data['order'] = $order;

                $result = 'OK';
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function reject()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();
        if ($user)
        {
            $orderId = $this->request->data('order_id');
            if ($this->Orders->exists(['id' => $orderId, 'user_id' => $user['id']]))
            {
                $orderId = $this->request->data('order_id');

                #FIXME: lock row
                $order = $this->Orders
                    ->find()
                    ->where(['id' => $orderId, 'status in' => [\App\Controller\OrdersController::$STATUS_NEW, \App\Controller\OrdersController::$STATUS_WAIT_PAYMENT]])
                    ->contain(['UserProducts' => function(Query $q){
                        return $q->contain(['SubProducts']);
                    }])
                    ->first();

                if($order)
                {
                    foreach ($order->user_products as $userProduct)
                    {
                        $subProduct = $userProduct->sub_product;

                        if ($subProduct)
                        {
                            $subProduct->amount += $userProduct->amount;
                            $this->loadModel('SubProducts')->save($subProduct);
                        }
                    }


                    $order->status = \App\Controller\OrdersController::$STATUS_REJECT;
                    $this->Orders->save($order);

                    $result = 'OK';
                }
                else
                {
                    $message = 'order not found';
                }
            }
            else
            {
                $message = 'order not found';
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
}