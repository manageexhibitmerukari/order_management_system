<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/18/17
 * Time: 4:35 PM
 */

namespace App\Controller\Api;



use App\Controller\Common;
use App\Controller\Component\EmailComponent;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\ClientProductsTable $ClientProducts
 * @property EmailComponent $Email
 */

class ProductsController extends AppController
{

    public function listMainProduct ()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user)
        {
            $products = $this->loadModel('mainProducts')
                ->find()
                ->order(['mainProducts.id' => 'ASC'])
                ->join([
                    'sub' => [
                        'table' => 'sub_products',
                        'conditions' => 'sub.parent_sku = mainProducts.sku'
                    ]
                ])
                ->group('mainProducts.id')
                ->toArray();

            foreach ($products as $mainProduct)
            {
                $check = false;

                $subProducts = $this->loadModel('SubProducts')
                    ->find()
                    ->where(['parent_sku' => $mainProduct->sku])
                    ->toArray();

                foreach ($subProducts as $subProduct)
                {
                    if ($subProduct->selling_check_time && $subProduct->selling_term)
                    {
                        $modifiedTime = $subProduct->selling_check_time;
                        $expiredTime = $modifiedTime->modify('+' . $subProduct->selling_term . ' hours');
                        $diffTime = date_diff(Time::now(), $expiredTime);

                        if (!$diffTime->invert)
                        {
                            $check = true;
                            break;
                        }
                    }
                    else
                    {
                        $check = true;
                        break;
                    }
                }

                if($check)
                    $mainProduct->isShow = 1;
                else
                    $mainProduct->isShow = 0;
            }

            $data['products'] = $products;
            $result = 'OK';

        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function listSubProduct ()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $id = $this->request->data('id');
            $productMain = $this->loadModel('mainProducts')->find()->where(['id' => $id])->first();
            $tableSize = TableRegistry::get('sizes');
            $sub = $this->loadModel('subProducts')->find()->where(['parent_sku' => $productMain['sku']])->toArray();
            $products = [];

            foreach ($sub as $row)
            {
                $hours = 0;
                $size = $tableSize->find()->where(['id' => $row['size_id']])->first();
                $product = [
                    'id' => $row['id'],
                    'product_title' => $productMain['product_title'],
                    'sku' => $row['sku'],
                    'product_photo1' => $row['product_photo1'],
                    'price' => $row['price'],
                    'amount' => $row['amount'],
                    'sizeName' => $size['sizeName'],
                    'color' => $row['color']
                ];

                if ($row->selling_check_time && $row->selling_term)
                {
                    $modifiedTime = $row->selling_check_time;
                    $expiredTime = $modifiedTime->modify('+' . $row->selling_term . ' hours');
                    $diffTime = date_diff(Time::now(), $expiredTime);

                    if ($diffTime->invert)
                    {
                        $row->isShow = 0;
                        $row->remainingTime = 'あと0';
                    }
                    else
                    {
                        $row->isShow = 1;
//                        $hours = $diffTime->h + ($diffTime->d * 24) + ($diffTime->m * 30 * 24) + ($diffTime->y * 365 * 24);
                        $hours = $diffTime->h + ($diffTime->days * 24);
                        $row->remainingTime = "あと".$hours . ":";
                        if ($diffTime->i >= 0 && $diffTime->i < 10)
                            $row->remainingTime .= '0' . $diffTime->i;
                        else
                            $row->remainingTime .= $diffTime->i;
                    }
                }
                else
                {
                    $row->isShow = 1;
                    $row->remainingTime = '';
                }

                $product['isShow'] = $row->isShow;
                $product['remainingTime'] = $row->remainingTime;

                $products[] = $product;
            }

            $data['products'] = $products;
            $result = 'OK';

        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function addCart() {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $cart = $this->request->data('cart');

            $tableSub = TableRegistry::get('subProducts');
            $tableCart = TableRegistry::get('carts');

            $sub = $tableSub->find()->where(['id' => $cart['id']])->first();

            if ($cart['amount'] <= $sub['amount']) {
                if ($tableCart->exists(['sub_product_id' => $cart['id'], 'user_id' => $user['id']])) {
                    $data_cart = $tableCart->find()->where(['sub_product_id' => $cart['id'], 'user_id' => $user['id']])->first();

                    if ( ($data_cart['amount'] + $cart['amount']) <= $sub['amount']) {
                        $data_cart['amount'] = $data_cart['amount'] + $cart['amount'];
                        $tableCart->save($data_cart);
                        $result = 'OK';
                    } else {
                        $amount = $sub['amount'] - $data_cart['amount'];
                        $message = "入力可能数量は、". $amount ."以下です。";
                    }
                } else {
                    $data_cart = $tableCart->newEntity();
                    $data_cart = $tableCart->patchEntity($data_cart, [
                        'user_id' => $user['id'],
                        'sub_product_id' => $cart['id'],
                        'amount' => $cart['amount'],
                        'sell_price' => $sub['price'],
                    ]);
                    $tableCart->save($data_cart);
                    $result = 'OK';
                }
            } else {
                $amount = 0;
                if ($tableCart->exists(['sub_product_id' => $cart['id'], 'user_id' => $user['id']])) {
                    $data_cart = $tableCart->find()->where(['sub_product_id' => $cart['id'], 'user_id' => $user['id']])->first();
                    $amount = $data_cart['amount'];
                }
                $amount = $sub['amount'] - $amount;
                $message = "入力可能数量は、". $amount ."以下です。";
            }

        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function showCart () {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $tableCart = TableRegistry::get('carts');
            $tableSub = TableRegistry::get('subProducts');

            $carts = $tableCart->find()->where(['user_id' => $user['id']])->order(['id' => 'DESC'])->toArray();

            foreach ($carts as $cart) {
                $product = $tableSub->find()
                    ->select(['m.product_title', 's.sizeName'])
                    ->autoFields(true)
                    ->join([
                        'm' => [
                            'table' => 'main_products',
                            'conditions' => 'subProducts.parent_sku = m.sku'
                        ],
                        's' => [
                            'table' => 'sizes',
                            'conditions' => 'subProducts.size_id = s.id'
                        ]
                    ])->where(['subProducts.id' => $cart['sub_product_id']])->first();

                $data['products'][] = [
                    'id' => $cart['id'],
                    'product_title' => $product['m']['product_title'],
                    'product_photo1' => $product['product_photo1'],
                    'price' => $product['price'],
                    'color' => $product['color'],
                    'size' => $product['s']['sizeName'],
                    'amount' => $cart['amount'],
                    'total_price' => $cart['amount'] * $product['price'],
                    'sell_price' => $cart['sell_price'],
                ];
            }
            $result = 'OK';
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function deleteCart () {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $id = $this->request->data('id');

            $tableCart = TableRegistry::get('carts');

            $cart = $tableCart->get($id);
            $tableCart->delete($cart);

            $result = 'OK';
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function updateCart () {
        $result = 'OK';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $products = $this->request->data('products');

            $cartsTable = TableRegistry::get('carts');
            $subProductsTable = TableRegistry::get('subProducts', ['contain' => 'MainProducts']);

            foreach ($products as $id => $product)
            {
                $cart = $cartsTable->get($id);
                $sub = $subProductsTable->find()->where(['id' => $cart['sub_product_id']])->first();

                if ($product['amount'] <= $sub['amount']) {
                } else {
                    $amount = $sub['amount'];
                    $message = "入力可能数量は、". $amount ."以下です(" . $sub->main_product->product_title . ")。";

                    $result = 'error';
                    break;
                }
            }

            if ($result == 'OK')
                foreach ($products as $id => $product)
                {
                    $productCart = $cartsTable->get($id);

                    $productCart->amount = $product['amount'];
                    $productCart->sell_price = $product['sell_price'];

                    $cartsTable->save($productCart);
                }
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function submitCart () {
        $result = '';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $tableSub = TableRegistry::get('subProducts');
            $tableCart = TableRegistry::get('carts');
            $tableMain = TableRegistry::get('mainProducts');
            $tableOrder = TableRegistry::get('orders');
            $tableUserProduct = TableRegistry::get('userProducts');

            $buyerEmail = $this->request->data('buyer_email');
            $arr_sub_id = [];

            $carts = $tableCart->find()->select(['sub_product_id'])->where(['user_id' => $user['id']])->toArray();
            foreach ($carts as $row) {
                $arr_sub_id[] = $row['sub_product_id'];
            }

            $data_order = array(
                'user_id' => $user['id'],
                'username' => $user['username'],
                'status' => 'wait_payment'
            );


            $order = $tableOrder->newEntity();
            $tableOrder->patchEntity($order, $data_order);
            if ($tableOrder->save($order)) {
                $subs = $tableSub->find()->where(['id IN ' => $arr_sub_id])->toArray();
                $id_order = $order->id;
                $i = 0;
                $name_product_error = '';
                $amount_product_error = '';
                foreach ($subs as $sub) {
                    $cart = $tableCart->find()->where(['sub_product_id' => $sub['id']])->first();
                    $main = $tableMain->find()->where(['sku' => $sub['parent_sku']])->first();
                    if ($cart['amount'] > $sub['amount']) {
                        $name_product_error = $name_product_error . $main['product_title'] . ",";
                        $amount_product_error = $amount_product_error . $sub['amount'] . ",";
                        $result = "error";
                    }
                }

                if ($result == 'error') {

                    $message = '商品の数量は'.substr($amount_product_error, 0 , -1).'以下で変更してください。対象商品: '.substr($name_product_error, 0 , -1).' です。';

                } else {
                    $result = 'OK';
                    $totalPrice = 0;
                    $productIds = '';
                    foreach ($subs as $sub) {
                        $i++;
                        $main = $tableMain->find()->where(['sku' => $sub['parent_sku']])->first();
                        $cart = $tableCart->find()->where(['sub_product_id' => $sub['id']])->first();

                        $photo1 = $this->copyImgOrder($sub['product_photo1'], $id_order.$i, 1);
                        $photo2 = $this->copyImgOrder($sub['product_photo2'], $id_order.$i, 2);
                        $photo3 = $this->copyImgOrder($sub['product_photo3'], $id_order.$i, 3);
                        $photo4 = $this->copyImgOrder($sub['product_photo4'], $id_order.$i, 4);
                        $data_product = array(
                            'order_id' => $id_order,
                            'product_title' => $main['product_title'],
                            'description' => $main['description'],
                            'sku' => $sub['sku'],
                            'product_photo1' => $photo1,
                            'product_photo2' => $photo2,
                            'product_photo3' => $photo3,
                            'product_photo4' => $photo4,
                            'price' => $sub['price'],
                            'size_id' => $sub['size_id'],
                            'color' => $sub['color'],
                            'amount' => $cart['amount']
                        );
                        $products = $tableUserProduct->newEntity();
                        $tableUserProduct->patchEntity($products, $data_product);
                        $tableUserProduct->save($products);

                        $sub->amount = $sub['amount'] - $cart['amount'];
                        $tableSub->save($sub);

                        $tableCart->deleteAll(['id '=> $cart['id']]);

                        $totalPrice += $sub['price']*$cart['amount'];
                        $productIds .= $sub->sku.',';
                    }

                    $productIds = rtrim($productIds, ",");

                    $this->loadComponent('Email');
                    $admin = $this->loadModel('Users')->find()->where(['level' => 5])->first();
                    $this->Email->afterSubmitCart($buyerEmail, $user['name'], $buyerEmail, $totalPrice, $productIds, $user['company'], $user['postal'], $user['state'], $user['town'], empty($user['building']) ? '' : $user['building'], empty($user['house_number']) ? '' : $user['house_number']);
                    $this->Email->afterSubmitCart($admin->email, $user['name'], $buyerEmail, $totalPrice, $productIds, $user['company'], $user['postal'], $user['state'], $user['town'], empty($user['building']) ? '' : $user['building'], empty($user['house_number']) ? '' : $user['house_number']);
                }
            }

        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function getParentCategory () {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $tableCategory = TableRegistry::get('categories');

            $category = $tableCategory->find()->where(['parentID' => 0])->toArray();

            $data['category'] = $category;
            $result = 'OK';
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
    public function getChildCategory () {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $tableCategory = TableRegistry::get('categories');
            $id = $this->request->data('id');

            $category = $tableCategory->find()->where(['parentID' => $id])->toArray();

            $data['category'] = $category;
            $result = 'OK';
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    function copyImgOrder($photo, $id, $key) {
        $folder = WWW_ROOT . 'img/order';
        if (!is_dir($folder) ){
            mkdir($folder, 0777,true);
        }
        if ($photo == '' || !file_exists(WWW_ROOT . $photo))
            return '';
        $arr = explode('.', $photo);
        $photo = str_replace('/img', 'img', $photo);
        copy(WWW_ROOT.$photo, $folder .'/' . $id .'_'.$key.'.'.$arr[1]);
        return '/img/order/'. $id.'_'.$key.'.'.$arr[1];
    }

    public function clientProductsList()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $this->loadModel('ClientProducts');

            $products = $this->ClientProducts
                ->find()
                ->where(['user_id' => $user['id']])
                ->contain(['Sizes'])->toArray();

            foreach ($products as $product)
            {
                $product->merukari = 0;
                $product->furiru = 0;
                $product->rakuma = 0;

                $subProductTable = TableRegistry::get('SubProducts');

                if ($subProductTable->exists(['sku' => $product->sku]))
                {
                    $subProduct = $subProductTable->find()->where(['sku' => $product->sku])->first();
                    $mainProductTable = TableRegistry::get('MainProducts');
                    if ($mainProductTable->exists(['sku' => $subProduct->parent_sku]))
                    {
                        $mainProduct = $mainProductTable->find()->where(['sku' => $subProduct->parent_sku])->first();
                        $product->merukari = $mainProduct->merukari;
                        $product->furiru = $mainProduct->furiru;
                        $product->rakuma = $mainProduct->rakuma;
                    }
                }
            }

            $data['products'] = $products;

            $result = 'OK';
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function searchProduct() {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $request_search = $this->request->data('request_search');
            $tableCategory = TableRegistry::get('Categories');
            $tableMain = TableRegistry::get('mainProducts');

            if ($request_search['parent_category'] == '') {
                $where = ['product_title like' => '%' . $request_search['keyword'] . '%'];
            }
            else if ($request_search['child_category'] == '') {
                $array_id = [$request_search['parent_category']];
                $child_id = [$request_search['parent_category']];
                $i = 0;
                do {
                    $child = [];
                    $result_category = $tableCategory->find()->select(['id'])->where(['parentID IN' => $child_id])->toArray();
                    $i++;
                    foreach ($result_category as $row) {
                        $array_id[] = $row['id'];
                        $child[] = $row['id'];
                    }
                    $child_id = $child;

                } while (count($child) > 0);
                $where = ['product_title like' => '%' . $request_search['keyword'] . '%', 'category_id IN' => $array_id];
            } else {
                $array_id = [$request_search['child_category']];
                $child_id = [$request_search['child_category']];
                $i = 0;
                do {
                    $child = [];
                    $result_category = $tableCategory->find()->select(['id'])->where(['parentID IN' => $child_id])->toArray();
                    $i++;
                    foreach ($result_category as $row) {
                        $array_id[] = $row['id'];
                        $child[] = $row['id'];
                    }
                    $child_id = $child;

                } while (count($child) > 0);
                $where = ['product_title like' => '%' . $request_search['keyword'] . '%', 'category_id IN' => $array_id];
            }
            $data['products'] = $tableMain->find()->where($where)->toArray();
            $result = 'OK';

        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";

        }
        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function downloadProduct () {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user){
            $user_id = $user['id'];
            if ( ! file_exists(WWW_ROOT . 'files/csv/user/' . $user_id)) {
                mkdir(WWW_ROOT . 'files/csv/user/' . $user_id, 0777, true);
            }
            $this->unlinkFolder(WWW_ROOT . 'files/csv/user/'. $user_id);

            $arr_id = $this->request->data('product_id_array');
            $tableClient = TableRegistry::get('clientProducts');

            $products = $tableClient->find()->where(['id IN ' => $arr_id, 'user_id' => $user_id])->toArray();
            $list_record = [];

            foreach ($products as $product) {
                $this->copyImgToZip($product['product_photo1'], $user_id);
                $this->copyImgToZip($product['product_photo2'], $user_id);
                $this->copyImgToZip($product['product_photo3'], $user_id);
                $this->copyImgToZip($product['product_photo4'], $user_id);
                $array = [
                    'sku' => $product['sku'],
                    'product_title' => $product['product_title'],
                    'description' => $product['description'],
                    'product_photo1' => Common::substrImage($product['product_photo1']),
                    'product_photo2' => Common::substrImage($product['product_photo2']),
                    'product_photo3' => Common::substrImage($product['product_photo3']),
                    'product_photo4' => Common::substrImage($product['product_photo4']),
                    'sell_price' => $product['price'],
                    'amount' => $product['amount'],
                    'shipping_fee' => '',
                    'note' => '',
                    'category_id' => $product['category_id'],
                    'brand_id' => $product['brand_id'],
                    'size_id' => $product['size_id'],
                    'product_item_condition_id' => $product['product_item_condition_id'],
                    'shipping_charge_id' => $product['shipping_charge_id'],
                    'shipping_method_id' => $product['shipping_method_id'],
                    'sender_address' => $product['sender_address'],
                    'shipping_duration_id' => $product['shipping_duration_id'],
                    'furiru_category_id' => $product['furiru_category_id'],
                    'furiru_size_id' => $product['furiru_size_id'],
                    'furiru_brand_id' => $product['furiru_brand_id'],
                    'furiru_product_status_id' => $product['furiru_product_status_id'],
                    'furiru_shipping_charge_id' => $product['furiru_shipping_charge_id'],
                    'furiru_shipping_method_id' => $product['furiru_shipping_method_id'],
                    'furiru_shipping_duration_id' => $product['furiru_shipping_duration_id'],
                    'furiru_city_id' => $product['furiru_city_id'],
                    'furiru_request_required' => $product['furiru_request_required'],
                    'rakuma_category' => $product['rakuma_category'],
                    'rakuma_size' => $product['rakuma_size'],
                    'rakuma_brand' => $product['rakuma_brand'],
                    'rakuma_condition_type' => $product['rakuma_condition_type'],
                    'rakuma_postage_type' => $product['rakuma_postage_type'],
                    'rakuma_delivery_method' => $product['rakuma_delivery_method'],
                    'rakuma_prefecture_code' => $product['rakuma_prefecture_code'],
                    'rakuma_delivery_term' => $product['rakuma_delivery_term'],
                    'color' => $product['color']
                ];
                $list_record[] = $array;
            }

            $fp = fopen(WWW_ROOT . 'files/csv/user/'.$user_id.'/item.csv', 'w');

            $header = array('管理番号', '商品名','商品説明','商品画像1', '商品画像2','商品画像3', '商品画像4', '商品価格', '在庫数', '送料', '備考', 'カテゴリ',
                'ブランド', 'サイズ', '商品の状態', '配送料の負担', '配送方法', '発送元の地域', '発送までの日付', 'フリルカテゴリ', 'フリルサイズ', 'フリルブランド',
                'フリル商品の状態', 'フリル配送料の負担', 'フリル配送方法', 'フリル発送日の目安', 'フリル発送元の地域', 'フリル購入申請', 'ラクマカテゴリ', 'ラクマサイズ',
                'ラクマブランド', 'ラクマ商品の状態', 'ラクマ配送料の負担', 'ラクマ配送方法', 'ラクマ発送元の地域', 'ラクマ発送までの日付',  '色');

            $convert_header = Common::convertToCSVData($header);
            fputcsv($fp, $convert_header);

            foreach ($list_record as $row) {
                fputcsv($fp, Common::convertToCSVData($row));
            }
            fclose($fp);
            if (!is_dir(WWW_ROOT . 'files/csv/user/'. $user_id.'/zip'))
                mkdir(WWW_ROOT . 'files/csv/user/'. $user_id.'/zip', 0777, true);

            $files = scandir(WWW_ROOT . 'files/csv/user/' . $user_id);
            $folders = [];
            foreach ($files as $file){
                if (is_file(WWW_ROOT . 'files/csv/user/' . $user_id . '/'. $file))
                    $folders[] = WWW_ROOT . 'files/csv/user/' . $user_id . '/' .$file;
            }
            $folder_zip = WWW_ROOT . 'files/csv/user/'.$user_id.'/zip/sub.zip';
            if (file_exists($folder_zip))
                Common::create($folder_zip, $folders, true);
            else
                Common::create($folder_zip, $folders, false);

            $result = 'OK';

            $data['file_path'] = '/files/csv/user/'.$user_id.'/zip/sub.zip';
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    function unlinkFolder($url) {
        $files = scandir($url);

        foreach ($files as $file) {
            if (is_file($url . '/' . $file))
                unlink($url . '/' . $file);
        }
    }

    function copyImgToZip($image, $user_id) {
        if ($image != '' && file_exists(WWW_ROOT . $image)) {
            $arr = explode('/', $image);
            copy(WWW_ROOT . $image, WWW_ROOT . '/files/csv/user/'.$user_id. '/'. end($arr));
        }
    }

    public function getClientProduct()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $this->loadModel('ClientProducts');
            $productId = $this->request->data('product_id');

            $clientProduct = $this->ClientProducts->find()->where(['id' => $productId, 'user_id' => $user['id']])->first();

            if ($clientProduct)
            {
                $result = 'OK';
                $data['product'] = $clientProduct;
            }
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function updateClientProduct()
    {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        if ($user) {
            $this->loadModel('ClientProducts');
            $productId = $this->request->data('product_id');
            $productData = $this->request->data('product');

            $clientProduct = $this->ClientProducts->find()->where(['id' => $productId, 'user_id' => $user['id']])->first();

            if ($clientProduct)
            {
                $result = 'OK';

                $clientProduct = $this->ClientProducts->patchEntity($clientProduct, $productData);

                $this->ClientProducts->save($clientProduct);
                $data['product'] = $clientProduct;
            }
        } else {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
}