<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ShippingFee Controller
 *
 * @property \App\Model\Table\BannersTable $Banners
 */
class ShippingFeeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tableShippingFee = TableRegistry::get('shippingFee');
        $shippingFeeAll = $tableShippingFee->find()->where(['type' => 'all'])->first();
        $shippingFeeArea = $tableShippingFee->find()->where(['type' => 'area']);
        //$fee = $this->checkShipping_fee(57, 1001);
       // dd($fee);
        $this->set(compact('shippingFeeAll', 'shippingFeeArea'));
        $this->set('_serialize', ['shippingFeeAll', 'shippingFeeArea']);


    }

    public function checkShipping_fee($zip_code, $price) {

        $fee_ship = 0;
        $zip_code = str_replace('-', '', $zip_code);
        $temp_zip_code = $zip_code;

        $tableShippingFee = TableRegistry::get('shippingFee');
        $shippingFeeAll = $tableShippingFee->find()->where(['type' => 'all'])->first();
        $shippingFeeArea = $tableShippingFee->find()->where(['type' => 'area']);

        /*
         * Lấy danh sach khu vực đã setting giá
         * Tìm kiếm xem zipcode truyền vào bằng zipcode của khu vực trong database hay không
         * Kiểm tra giá đơn hàng truyền vào:
         *      $price > $price_order : nếu lớn hơn giá đơn hàng trong database thì tính giá ship = $fee_up
         *      ngược lại tính giá ship = $fee_default
         *
         */
        if($shippingFeeAll) {

            $flag = true;

            if(count($shippingFeeArea->toArray()) > 0) {
                foreach ($shippingFeeArea as $fee_area) {
                    $list_area = explode(',', $fee_area->area);
                    $fee_default = (int)$fee_area->fee_default;
                    $price_order = (int)$fee_area->price_order;
                    $fee_up = (int)$fee_area->fee_up;

                    foreach ($list_area as $area) {
                        $zip_code = $temp_zip_code;
                        $length_area = strlen($area);
                        $zip_code = substr(trim($zip_code), 0,$length_area);
                        if($zip_code == $area) {
                           // dd($area);
                            $flag = false;

                            if ($price >= $price_order) {
                                $fee_ship = $fee_up;
                                if($fee_ship == 0 && $price_order == 0) {
                                    $fee_ship = $fee_default;
                                }
                            } else {
                                $fee_ship = $fee_default;
                            }
                        }

                    }
                    if($fee_ship > 0 || !$flag) {
                        break;
                    }
                }
            }

            /**
             * Nếu sau khi tìm kiếm giá ship vẫn bằng không thì lấy giá ship chung của tất cả khu vực
             * */
            if($fee_ship == 0 && $flag) {
                $fee_default = (int)$shippingFeeAll->fee_default;
                $price_order = (int)$shippingFeeAll->price_order;
                $fee_up = (int)$shippingFeeAll->fee_up;

                if ($price >= $price_order) {
                    $fee_ship = $fee_up;
                    if($fee_ship == 0 && $price_order == 0) {
                        $fee_ship = $fee_default;
                    }
                } else {
                    $fee_ship = $fee_default;
                }
            }
        }

//        if($fee_ship == 0) {
//            $list_zip_code_1700 = ['04','05','06','07','08','09','90'];
//            $zip_code = substr(trim($zip_code), 0,2);
//            /*
//             * Kiểm tra 2 ký tự đầu của zip code nếu tồn tại trong m
//             * thì sét giá ship = 1700
//             * không thì sét giá ship = 1000
//             * */
//            if(in_array($zip_code, $list_zip_code_1700)) {
//                $fee_ship = 1700;
//            } else {
//                $fee_ship = 1000;
//            }
//        }
        return $fee_ship;

    }

    public function save() {
        $status = 'error';
        $message = 'Error';
        $tableShippingFee = TableRegistry::get('shippingFee');
        if ($this->request->is('ajax')) {
            $fee_all = $this->request->data('fee_all');
            $fee_area = $this->request->data('fee_area');
            if($fee_all) {
                $type = 'all';
                $fee_default_all = 0;
                $price_order_all = 0;
                $fee_up_all = 0;

                if(isset($fee_all['fee_default_all']) && $fee_all['fee_default_all']) $fee_default_all = $fee_all['fee_default_all'];
                if(isset($fee_all['price_order_all']) && $fee_all['price_order_all']) $price_order_all = $fee_all['price_order_all'];
                if(isset($fee_all['fee_up_all']) && $fee_all['fee_up_all']) $fee_up_all = $fee_all['fee_up_all'];

                if(isset($fee_all['id_fee_all']) && $fee_all['id_fee_all']) {
                    $id = (int)$fee_all['id_fee_all'];
                    $shippingFeeAll = $tableShippingFee->get($id);
                    $shippingFeeAll->fee_default = $fee_default_all;
                    $shippingFeeAll->price_order = $price_order_all;
                    $shippingFeeAll->fee_up = $fee_up_all;
                    $shippingFeeAll->type = $type;
                    if($tableShippingFee->save($shippingFeeAll)) {
                        $status = 'success';
                    }

                } else {
                    $shippingFeeAll = $tableShippingFee->newEntity();
                    $shippingFeeAll->fee_default = $fee_default_all;
                    $shippingFeeAll->price_order = $price_order_all;
                    $shippingFeeAll->fee_up = $fee_up_all;
                    $shippingFeeAll->type = $type;
                    if($tableShippingFee->save($shippingFeeAll)) {
                        $status = 'success';
                    }
                }
            }

            if($fee_area && count($fee_area) > 0) {
                foreach ($fee_area as $item) {
                    $type = 'area';
                    $area = '';
                    $fee_default = 0;
                    $price_order = 0;
                    $fee_up = 0;

                    if(isset($item['area']) && $item['area']) $area = $item['area'];
                    if(isset($item['fee_default']) && $item['fee_default']) $fee_default = $item['fee_default'];
                    if(isset($item['price_order']) && $item['price_order']) $price_order = $item['price_order'];
                    if(isset($item['fee_up']) && $item['fee_up']) $fee_up = $item['fee_up'];

                    if(!isset($item['id']) || !$item['id']) {
                        $shippingFeeArea = $tableShippingFee->newEntity();
                        $shippingFeeArea->area = $area;
                        $shippingFeeArea->fee_default = $fee_default;
                        $shippingFeeArea->price_order = $price_order;
                        $shippingFeeArea->fee_up = $fee_up;
                        $shippingFeeArea->type = $type;

                        if($tableShippingFee->save($shippingFeeArea)) {
                            $status = 'success';
                        } else {
                            $status = 'error';
                        }

                    }

                }
            }

            $this->set(compact('status'));
            $this->set('_serialize', ['status']);
        }
    }


    public function delete() {
        $status = 'error';
        $message = 'Error';
        $tableShippingFee = TableRegistry::get('shippingFee');
        if ($this->request->is('ajax')) {
            $id = $this->request->data('id');

            if ($id) {
                $tableShippingFee->query()->delete()->where(['id' => $id])->execute();
                $status = 'success';
            }
            $this->set(compact('status'));
            $this->set('_serialize', ['status']);
        }
    }

}
