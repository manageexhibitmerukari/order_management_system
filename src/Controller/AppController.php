<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'passwordHasher' => [
                        'className' => 'Fallback',
                        'hashers' => [
                            'Default',
                            'Weak' => [
                                'hashType' => 'md5'
                            ]
                        ]
                    ],
                ]
            ],
            'authorize' => ['Controller'],
//            'loginRedirect'=> [
//                'controller'=>'UserProducts',
//                'action'=>'upload'
//            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'loginUser',
            ],
        ]);
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function isAuthorized($user)
    {
        if(isset($user['level']) && $user['level'] == 5) {
            return true;
        }
        return false;
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['login', 'detailProduct', 'search', 'top', 'loginUser', 'managerProduct', 'listClientProduct', 'logoutUser', 'managerShipping', 'activeUser']);

        if( ! $this->request->is('ajax'))
        {
            $currentUser = null;
            $lastProductCreatedTime = '00:00';
            $oneDayAgoDate = date('m/d', strtotime('-1 day'));
            $twoDaysAgoDate = date('m/d', strtotime('-2 days'));
            $todayProductCount = 0;
            $oneDayAgoProductCount = 0;
            $twoDaysAgoProductCount = 0;

            if(in_array($this->request->action, ['listClientProduct', 'managerShipping', 'top', 'showCart', 'detailProduct',
                'managerOrder', 'managerProduct', 'productManagement','loginUser', 'register',
                'addressManagement', 'editAddress', 'addAddress', 'changeAddress', 'fqa',
                'guide', 'termOfService', 'termOfService2', 'contact', 'updateInfo',
                'activeUser', 'contact', 'resetPass', 'changeUserPassword'
            ]))
            {
                $productStatisticTable = $this->loadModel('ProductStatistic');

                $mainProductsTable = TableRegistry::get('MainProducts');
                $todayProducts = $mainProductsTable->find('all')->where(['created >=' => new \DateTime(date('Y-m-d'))]);

                $todayProductCount = $todayProducts->count();
                if ($todayProductCount > 0)
                    $lastProductCreatedTime = $todayProducts->last()->created->format('H:i');



                $oneDayAgoFullDate = date('Y-m-d', strtotime('-1 day'));
                if ($productStatisticTable->exists(['date' => $oneDayAgoFullDate]))
                {
                    $oneDayAgoProductCount = $productStatisticTable->find()->where(['date' => $oneDayAgoFullDate])->first()->product_count;
                }
                else
                {
                    $oneDayAgoProducts = $mainProductsTable->find();
                    $oneDayAgoProducts->where(function ($exp, $q) {
                        return $exp->between('created', new \DateTime('-1 day midnight'), new \DateTime(date('Y-m-d')));
                    });

                    $oneDayAgoProductCount = $oneDayAgoProducts->count();

                    $productStatistic = $productStatisticTable->newEntity([
                        'date' => $oneDayAgoFullDate,
                        'product_count' => $oneDayAgoProductCount
                    ]);
                    $productStatisticTable->save($productStatistic);
                }



                $twoDaysAgoFullDate = date('Y-m-d', strtotime('-2 days'));
                if ($productStatisticTable->exists(['date' => $twoDaysAgoFullDate]))
                {
                    $twoDaysAgoProductCount = $productStatisticTable->find()->where(['date' => $twoDaysAgoFullDate])->first()->product_count;
                }
                else
                {
                    $twoDaysAgoProducts = $mainProductsTable->find();
                    $twoDaysAgoProducts->where(function ($exp, $q) {
                        return $exp->between('created', new \DateTime('-2 days midnight'), new \DateTime('-1 days midnight'));
                    });

                    $twoDaysAgoProductCount = $twoDaysAgoProducts->count();

                    $productStatistic = $productStatisticTable->newEntity([
                        'date' => $twoDaysAgoFullDate,
                        'product_count' => $twoDaysAgoProductCount
                    ]);
                    $productStatisticTable->save($productStatistic);
                }
            }

            if($this->Auth->user()) {
                $currentUser = $this->loadModel('Users')->get($this->Auth->user('id'));

                $carts = $this->loadModel('Carts');
                $countProduct = $carts->find()->where(['user_id' => $currentUser->id])->count();

            }

            $tableCategory = TableRegistry::get('Categories');
            $categories = $tableCategory->find()->where(['parentId' => 0 ])->toArray();

            $tableBanners = TableRegistry::get('Banners');

            $data = $tableBanners->find()->where(['position IS NOT' => null, 'type' => 'side_banner'])->order(['position' => 'ASC']);

            $data2 = $tableBanners->find()->where(['position IS' => null, 'type' => 'side_banner' ])->order(['id' => 'DESC']);

            $bannerSides = [];
            if($data) {
                foreach ($data as $row) {

                    $banner = array(
                        'id' => $row->id,
                        'title' => $row->title,
                        'link' => $row->link,
                        'description' => $row->description,
                        'image' => $row->image,
                        'type' => $row->type,
                        'position' => $row->position
                    );
                    array_push($bannerSides, $banner);
                }
            }

            if($data2) {
                foreach ($data2 as $row2) {

                    $banner = array(
                        'id' => $row2->id,
                        'title' => $row2->title,
                        'link' => $row2->link,
                        'description' => $row2->description,
                        'image' => $row2->image,
                        'type' => $row2->type,
                        'position' => $row2->position
                    );
                    array_push($bannerSides, $banner);
                }
            }

            $this->set(compact(
                'currentUser',
                'lastProductCreatedTime',
                'oneDayAgoDate',
                'twoDaysAgoDate',
                'todayProductCount',
                'oneDayAgoProductCount',
                'twoDaysAgoProductCount',
                'countProduct',
                'categories',
                'bannerSides'
            ));
            $this->set('_serialize', [
                'currentUser',
                'lastProductCreatedTime',
                'oneDayAgoDate',
                'twoDaysAgoDate',
                'todayProductCount',
                'oneDayAgoProductCount',
                'twoDaysAgoProductCount',
                'countProduct',
                'categories',
                'bannerSides'
            ]);
        }
        else
        {
            $this->Auth->allow();
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
}
