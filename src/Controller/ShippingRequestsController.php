<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/7/17
 * Time: 6:46 PM
 */

namespace App\Controller;

use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

class ShippingRequestsController extends AppController {


    public function listShipping() {

        $tableSub = TableRegistry::get('userProducts');

        $products = $tableSub->find()
            ->select([
                'userProducts.id',
                'userProducts.order_id',
                'userProducts.product_title',
                'userProducts.product_photo1',
                'userProducts.amount',
                'userProducts.ship_status',
                'userProducts.price',
                'addresses.full_address',
                'addresses.name',
                'addresses.phone_number',
                'addresses.zip_code',
                'addresses.city',
                'addresses.address1',
                'addresses.address2',
                'orders.user_id',
                'sub_products.parent_sku',
                'sub_products.sku',
                'main_products.id'
            ])
            ->innerJoin('orders', ['userProducts.order_id = orders.id'])
            ->innerJoin('users', ['users.id = orders.user_id'])
            ->leftJoin('addresses', ['addresses.id = users.address_id'])
            ->innerJoin('sub_products', ['userProducts.sku = sub_products.sku'])
            ->innerJoin('main_products', ['main_products.sku = sub_products.parent_sku'])

            ->where([
                'orders.status' => 'done',
                'OR' => [
                    ['order_status !=' => 'reject'],
                    ['order_status is' => null]
                ]
            ])
            ->order(['userProducts.ship_status'=> 'ASC','orders.created' => 'ASC']);

            //dd($products->toArray());
            $this->set(compact('products'));
            $this->set('_serialize', ['products']);


    }

    public function changeStatus() {

        if ($this->request->is('ajax')) {
            $result = 'error';
            $msg = '';
            $id = $this->request->data('id');
            $user_id = $this->request->data('user_id');
            $order_id = $this->request->data('order_id');


            $tableSub = TableRegistry::get('userProducts');

            $product = $tableSub->find()->where(['id' => $id])->first();

            $product->ship_status = 1;

            if ($tableSub->save($product)) {

                $data = $tableSub->find()
                    ->select([
                        'userProducts.id',
                        'userProducts.order_id',
                        'userProducts.product_title',
                        'userProducts.product_photo1',
                        'userProducts.amount',
                        'userProducts.sku',
                        'userProducts.ship_status',
                        'userProducts.price',
                        'addresses.full_address',
                        'addresses.zip_code',
                        'addresses.name',
                        'addresses.phone_number',
                        'addresses.city',
                        'addresses.address1',
                        'addresses.address2',
                        'orders.user_id',
                        'users.email',
                        'users.name'
                    ])
                    ->innerJoin('orders', ['userProducts.order_id = orders.id'])
                    ->innerJoin('users', ['users.id = orders.user_id'])
                    ->leftJoin('addresses', ['addresses.id = users.address_id'])
                    ->where(['orders.status' => 'done', 'orders.id' => $order_id, 'userProducts.id' => $id]);

                $info = $data->first();
                //dd($info);
                $this->loadComponent('Email');

                $this->Email->afterShipping($info->users['email'], $info);

                $result = 'success';

            }

            $this->set(compact('result', 'msg'));
            $this->set('_serialize', ['result', 'msg']);

        }
    }


    function getExport()
    {
        if ($this->request->is('ajax')) {
            $arr_id = $this->request->data('id');
            $all = $this->request->data('all');
            $tableShipping = TableRegistry::get('shippingRequests');
            $tableClient = TableRegistry::get('clientProducts');

            if ($all === 'all') {
                $records = $tableShipping->find()->where(['status' => 'new'])->toArray();
            } else {
                $records = $tableShipping->find()->where(['id IN ' => $arr_id])->toArray();
            }
            $list_record = [];

            foreach ($records as $row) {
                $product = $tableClient->find()->where(['sku' => $row['sku'], 'user_id' => $row['user_id']])->first();
                $array = [
                    'sku' => $row['sku'],
                    'product_title' => $product['product_title'],
                    'buyer_address' => $row['buyer_address'],
                    'price' => $product['price'],
                    'amount' => $product['amount'],
                    'status' => $row['status'],
                    'last_export' => date_format(Time::now(),"Y/m/d H:i:s")
                ];
                $list_record[] = $array;

                $data = [
                    'num_export' => $row['num_export'] + 1,
                    'last_export' => Time::now()
                ];

                $tableShipping->updateAll($data, ['id' => $row['id']]);
            }

            if ( ! file_exists(WWW_ROOT . 'files/csv/admin')) {
                mkdir(WWW_ROOT . 'files/csv/admin', 0777, true);
            }
            $fp = fopen(WWW_ROOT . 'files/csv/admin/item.csv', 'w');

            $header = array('SKU','商品名','顧客の住所', '価格','在庫数', '商品の状態', '出力日付');

            $convert_header = Common::convertToCSVData($header);
            fputcsv($fp, $convert_header);

            foreach ($list_record as $row) {
                fputcsv($fp, Common::convertToCSVData($row));
            }
            fclose($fp);
            $result = 'success';

            $this->set(compact('result'));
            $this->set('_serialize', ['result']);
        }
    }
}