<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Banners Controller
 *
 * @property \App\Model\Table\BannersTable $Banners
 */
class BannersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $msg = '';

        if ($this->request->is('ajax')) {

            $data = $this->Banners->find()->where(['position IS NOT' => null ])->order(['position' => 'ASC']);

            $data2 = $this->Banners->find()->where(['position IS' => null ])->order(['id' => 'DESC']);

            $banners = [];
            if($data) {
                foreach ($data as $row) {

                    $banner = array(
                        'id' => $row->id,
                        'title' => $row->title,
                        'link' => $row->link,
                        'description' => $row->description,
                        'image' => $row->image,
                        'type' => $row->type,
                        'position' => $row->position
                    );
                    array_push($banners, $banner);
                }
            }

            if($data2) {
                foreach ($data2 as $row2) {

                    $banner = array(
                        'id' => $row2->id,
                        'title' => $row2->title,
                        'link' => $row2->link,
                        'description' => $row2->description,
                        'image' => $row2->image,
                        'type' => $row2->type,
                        'position' => $row2->position
                    );
                    array_push($banners, $banner);
                }
            }

            $this->set(compact('banners'));
            $this->set('_serialize', ['banners']);
        }
        $this->set(compact('msg'));
    }

    /**
     * View method
     *
     * @param string|null $id Banner id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $banner = $this->Banners->get($id, [
            'contain' => []
        ]);

        $this->set('banner', $banner);
        $this->set('_serialize', ['banner']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $banner = $this->Banners->newEntity();
        if ($this->request->is('post')) {

            $banner = $this->Banners->patchEntity($banner, $this->request->data);

            $banner->image = '/';
            if ($this->Banners->save($banner)) {

                $id = $banner->id;
                if (!empty($_FILES['photo_1']['tmp_name']) && $_FILES['photo_1']['tmp_name'] ) {
                    $img = $_FILES['photo_1']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                    $img_url = 'img/upload/banner/'.$banner->type.'/'.$id.'.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $banner->image = '/'.$img_url;
                }

                $this->Banners->save($banner);

                $this->Flash->success(__('The banner has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The banner could not be saved. Please, try again.'));
        }
        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Banner id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $id = $this->request->query('id');
        $banner = $this->Banners->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $banner = $this->Banners->patchEntity($banner, $this->request->data);

            if ($this->Banners->save($banner)) {

                $id = $banner->id;
                if (!empty($_FILES['photo_1']['tmp_name']) && $_FILES['photo_1']['tmp_name'] ) {
                    $img = $_FILES['photo_1']['tmp_name'];
                    $type = strtolower(pathinfo($_FILES['photo_1']['name'], PATHINFO_EXTENSION));
                    $img_url = 'img/upload/banner/'.$banner->type.'/'.$id.'.'.$type;
                    file_put_contents(WWW_ROOT.$img_url, file_get_contents($img));
                    $banner->image = '/'.$img_url;
                }

                $this->Banners->save($banner);

                $this->Flash->success(__('The banner has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The banner could not be saved. Please, try again.'));
        }
        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banner id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        if ($this->request->is('ajax')) {
            $result = 'error';
            $all = isset($this->request->data['all']) ? $this->request->data('all') : '';
            $msg = '';
            $id = $this->request->data('id');

            if ($all == true) {
                $where = '';
                $msg = 'all';
            } else {
                $where = ['id IN ' => $id];
            }

            if ($this->Banners->deleteAll($where)) {
                $result = 'success';
            } else {
                $result = "error";
            }

            $this->set(compact('result', 'msg'));

        }

    }
}
