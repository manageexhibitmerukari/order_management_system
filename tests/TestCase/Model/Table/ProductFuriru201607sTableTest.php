<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductFuriru201607sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductFuriru201607sTable Test Case
 */
class ProductFuriru201607sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductFuriru201607sTable
     */
    public $ProductFuriru201607s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_furiru201607s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductFuriru201607s') ? [] : ['className' => 'App\Model\Table\ProductFuriru201607sTable'];
        $this->ProductFuriru201607s = TableRegistry::get('ProductFuriru201607s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductFuriru201607s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
