<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201507sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201507sTable Test Case
 */
class Product201507sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201507sTable
     */
    public $Product201507s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201507s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201507s') ? [] : ['className' => 'App\Model\Table\Product201507sTable'];
        $this->Product201507s = TableRegistry::get('Product201507s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201507s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
