<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCategoryFuriru10013sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCategoryFuriru10013sTable Test Case
 */
class ProductCategoryFuriru10013sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCategoryFuriru10013sTable
     */
    public $ProductCategoryFuriru10013s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_category_furiru10013s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCategoryFuriru10013s') ? [] : ['className' => 'App\Model\Table\ProductCategoryFuriru10013sTable'];
        $this->ProductCategoryFuriru10013s = TableRegistry::get('ProductCategoryFuriru10013s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCategoryFuriru10013s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
