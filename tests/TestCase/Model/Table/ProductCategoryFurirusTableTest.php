<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCategoryFurirusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCategoryFurirusTable Test Case
 */
class ProductCategoryFurirusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCategoryFurirusTable
     */
    public $ProductCategoryFurirus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_category_furirus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCategoryFurirus') ? [] : ['className' => 'App\Model\Table\ProductCategoryFurirusTable'];
        $this->ProductCategoryFurirus = TableRegistry::get('ProductCategoryFurirus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCategoryFurirus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
