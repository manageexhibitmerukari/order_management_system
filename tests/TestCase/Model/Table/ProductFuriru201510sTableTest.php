<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductFuriru201510sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductFuriru201510sTable Test Case
 */
class ProductFuriru201510sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductFuriru201510sTable
     */
    public $ProductFuriru201510s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_furiru201510s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductFuriru201510s') ? [] : ['className' => 'App\Model\Table\ProductFuriru201510sTable'];
        $this->ProductFuriru201510s = TableRegistry::get('ProductFuriru201510s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductFuriru201510s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
