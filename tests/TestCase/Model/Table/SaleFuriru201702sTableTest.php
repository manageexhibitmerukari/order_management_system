<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleFuriru201702sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleFuriru201702sTable Test Case
 */
class SaleFuriru201702sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SaleFuriru201702sTable
     */
    public $SaleFuriru201702s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_furiru201702s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleFuriru201702s') ? [] : ['className' => 'App\Model\Table\SaleFuriru201702sTable'];
        $this->SaleFuriru201702s = TableRegistry::get('SaleFuriru201702s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleFuriru201702s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
