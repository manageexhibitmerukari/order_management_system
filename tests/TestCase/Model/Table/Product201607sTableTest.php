<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201607sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201607sTable Test Case
 */
class Product201607sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201607sTable
     */
    public $Product201607s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201607s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201607s') ? [] : ['className' => 'App\Model\Table\Product201607sTable'];
        $this->Product201607s = TableRegistry::get('Product201607s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201607s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
