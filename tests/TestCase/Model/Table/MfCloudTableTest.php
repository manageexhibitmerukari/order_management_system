<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MfCloudTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MfCloudTable Test Case
 */
class MfCloudTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MfCloudTable
     */
    public $MfCloud;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mf_cloud'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MfCloud') ? [] : ['className' => 'App\Model\Table\MfCloudTable'];
        $this->MfCloud = TableRegistry::get('MfCloud', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MfCloud);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
