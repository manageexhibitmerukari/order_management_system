<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SellerFurirusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SellerFurirusTable Test Case
 */
class SellerFurirusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SellerFurirusTable
     */
    public $SellerFurirus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.seller_furirus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SellerFurirus') ? [] : ['className' => 'App\Model\Table\SellerFurirusTable'];
        $this->SellerFurirus = TableRegistry::get('SellerFurirus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SellerFurirus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
