<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductFuriru201702sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductFuriru201702sTable Test Case
 */
class ProductFuriru201702sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductFuriru201702sTable
     */
    public $ProductFuriru201702s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_furiru201702s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductFuriru201702s') ? [] : ['className' => 'App\Model\Table\ProductFuriru201702sTable'];
        $this->ProductFuriru201702s = TableRegistry::get('ProductFuriru201702s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductFuriru201702s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
