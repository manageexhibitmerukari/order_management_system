<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientProductsTable Test Case
 */
class ClientProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientProductsTable
     */
    public $ClientProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.client_products',
        'app.users',
        'app.main_products',
        'app.categories',
        'app.sub_products',
        'app.sizes',
        'app.groups',
        'app.user_products',
        'app.orders',
        'app.shipping_requests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClientProducts') ? [] : ['className' => 'App\Model\Table\ClientProductsTable'];
        $this->ClientProducts = TableRegistry::get('ClientProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClientProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
