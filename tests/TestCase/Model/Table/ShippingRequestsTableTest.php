<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShippingRequestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShippingRequestsTable Test Case
 */
class ShippingRequestsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ShippingRequestsTable
     */
    public $ShippingRequests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.shipping_requests',
        'app.users',
        'app.client_products',
        'app.sizes',
        'app.groups',
        'app.sub_products',
        'app.main_products',
        'app.categories',
        'app.user_products',
        'app.orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ShippingRequests') ? [] : ['className' => 'App\Model\Table\ShippingRequestsTable'];
        $this->ShippingRequests = TableRegistry::get('ShippingRequests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ShippingRequests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
