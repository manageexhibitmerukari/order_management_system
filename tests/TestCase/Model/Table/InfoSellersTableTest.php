<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfoSellersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfoSellersTable Test Case
 */
class InfoSellersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InfoSellersTable
     */
    public $InfoSellers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.info_sellers',
        'app.sellers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InfoSellers') ? [] : ['className' => 'App\Model\Table\InfoSellersTable'];
        $this->InfoSellers = TableRegistry::get('InfoSellers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InfoSellers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
