<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201608sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201608sTable Test Case
 */
class Product201608sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201608sTable
     */
    public $Product201608s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201608s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201608s') ? [] : ['className' => 'App\Model\Table\Product201608sTable'];
        $this->Product201608s = TableRegistry::get('Product201608s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201608s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
