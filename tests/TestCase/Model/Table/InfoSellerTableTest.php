<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfoSellerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfoSellerTable Test Case
 */
class InfoSellerTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InfoSellerTable
     */
    public $InfoSeller;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.info_seller',
        'app.sellers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InfoSeller') ? [] : ['className' => 'App\Model\Table\InfoSellerTable'];
        $this->InfoSeller = TableRegistry::get('InfoSeller', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InfoSeller);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
