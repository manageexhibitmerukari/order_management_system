<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCategoryFuriru10004sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCategoryFuriru10004sTable Test Case
 */
class ProductCategoryFuriru10004sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCategoryFuriru10004sTable
     */
    public $ProductCategoryFuriru10004s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_category_furiru10004s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCategoryFuriru10004s') ? [] : ['className' => 'App\Model\Table\ProductCategoryFuriru10004sTable'];
        $this->ProductCategoryFuriru10004s = TableRegistry::get('ProductCategoryFuriru10004s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCategoryFuriru10004s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
