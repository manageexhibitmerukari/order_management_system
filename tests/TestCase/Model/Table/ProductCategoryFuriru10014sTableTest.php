<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCategoryFuriru10014sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCategoryFuriru10014sTable Test Case
 */
class ProductCategoryFuriru10014sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCategoryFuriru10014sTable
     */
    public $ProductCategoryFuriru10014s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_category_furiru10014s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCategoryFuriru10014s') ? [] : ['className' => 'App\Model\Table\ProductCategoryFuriru10014sTable'];
        $this->ProductCategoryFuriru10014s = TableRegistry::get('ProductCategoryFuriru10014s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCategoryFuriru10014s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
