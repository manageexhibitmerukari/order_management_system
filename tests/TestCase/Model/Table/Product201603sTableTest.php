<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201603sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201603sTable Test Case
 */
class Product201603sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201603sTable
     */
    public $Product201603s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201603s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201603s') ? [] : ['className' => 'App\Model\Table\Product201603sTable'];
        $this->Product201603s = TableRegistry::get('Product201603s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201603s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
