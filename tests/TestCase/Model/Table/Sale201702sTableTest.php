<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Sale201702sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Sale201702sTable Test Case
 */
class Sale201702sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Sale201702sTable
     */
    public $Sale201702s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale201702s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sale201702s') ? [] : ['className' => 'App\Model\Table\Sale201702sTable'];
        $this->Sale201702s = TableRegistry::get('Sale201702s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sale201702s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
