<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCategoryFuriru10006sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCategoryFuriru10006sTable Test Case
 */
class ProductCategoryFuriru10006sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCategoryFuriru10006sTable
     */
    public $ProductCategoryFuriru10006s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_category_furiru10006s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCategoryFuriru10006s') ? [] : ['className' => 'App\Model\Table\ProductCategoryFuriru10006sTable'];
        $this->ProductCategoryFuriru10006s = TableRegistry::get('ProductCategoryFuriru10006s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCategoryFuriru10006s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
