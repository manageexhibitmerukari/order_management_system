<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201512sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201512sTable Test Case
 */
class Product201512sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201512sTable
     */
    public $Product201512s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201512s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201512s') ? [] : ['className' => 'App\Model\Table\Product201512sTable'];
        $this->Product201512s = TableRegistry::get('Product201512s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201512s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
