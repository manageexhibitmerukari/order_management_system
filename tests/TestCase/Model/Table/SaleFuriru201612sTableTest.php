<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleFuriru201612sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleFuriru201612sTable Test Case
 */
class SaleFuriru201612sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SaleFuriru201612sTable
     */
    public $SaleFuriru201612s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_furiru201612s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleFuriru201612s') ? [] : ['className' => 'App\Model\Table\SaleFuriru201612sTable'];
        $this->SaleFuriru201612s = TableRegistry::get('SaleFuriru201612s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleFuriru201612s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
