<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SkusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SkusTable Test Case
 */
class SkusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SkusTable
     */
    public $Skus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.skus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Skus') ? [] : ['className' => 'App\Model\Table\SkusTable'];
        $this->Skus = TableRegistry::get('Skus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Skus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
