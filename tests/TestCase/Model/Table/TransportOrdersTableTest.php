<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TransportOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TransportOrdersTable Test Case
 */
class TransportOrdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TransportOrdersTable
     */
    public $TransportOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.transport_orders',
        'app.orders',
        'app.users',
        'app.client_products',
        'app.categories',
        'app.main_products',
        'app.sub_products',
        'app.sizes',
        'app.groups',
        'app.user_products',
        'app.carts',
        'app.shipping_requests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TransportOrders') ? [] : ['className' => 'App\Model\Table\TransportOrdersTable'];
        $this->TransportOrders = TableRegistry::get('TransportOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TransportOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
