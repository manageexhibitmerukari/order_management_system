<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductFuriru201506sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductFuriru201506sTable Test Case
 */
class ProductFuriru201506sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductFuriru201506sTable
     */
    public $ProductFuriru201506s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_furiru201506s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductFuriru201506s') ? [] : ['className' => 'App\Model\Table\ProductFuriru201506sTable'];
        $this->ProductFuriru201506s = TableRegistry::get('ProductFuriru201506s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductFuriru201506s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
