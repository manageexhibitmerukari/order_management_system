<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201602sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201602sTable Test Case
 */
class Product201602sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201602sTable
     */
    public $Product201602s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201602s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201602s') ? [] : ['className' => 'App\Model\Table\Product201602sTable'];
        $this->Product201602s = TableRegistry::get('Product201602s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201602s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
