<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleFuriru201701sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleFuriru201701sTable Test Case
 */
class SaleFuriru201701sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SaleFuriru201701sTable
     */
    public $SaleFuriru201701s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_furiru201701s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleFuriru201701s') ? [] : ['className' => 'App\Model\Table\SaleFuriru201701sTable'];
        $this->SaleFuriru201701s = TableRegistry::get('SaleFuriru201701s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleFuriru201701s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
