<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201605sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201605sTable Test Case
 */
class Product201605sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201605sTable
     */
    public $Product201605s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201605s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201605s') ? [] : ['className' => 'App\Model\Table\Product201605sTable'];
        $this->Product201605s = TableRegistry::get('Product201605s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201605s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
