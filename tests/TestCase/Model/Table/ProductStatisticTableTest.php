<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductStatisticTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductStatisticTable Test Case
 */
class ProductStatisticTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductStatisticTable
     */
    public $ProductStatistic;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_statistic'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductStatistic') ? [] : ['className' => 'App\Model\Table\ProductStatisticTable'];
        $this->ProductStatistic = TableRegistry::get('ProductStatistic', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductStatistic);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
