<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubProductsTable Test Case
 */
class SubProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubProductsTable
     */
    public $SubProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sub_products',
        'app.sizes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SubProducts') ? [] : ['className' => 'App\Model\Table\SubProductsTable'];
        $this->SubProducts = TableRegistry::get('SubProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
