<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201604sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201604sTable Test Case
 */
class Product201604sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201604sTable
     */
    public $Product201604s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201604s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201604s') ? [] : ['className' => 'App\Model\Table\Product201604sTable'];
        $this->Product201604s = TableRegistry::get('Product201604s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201604s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
