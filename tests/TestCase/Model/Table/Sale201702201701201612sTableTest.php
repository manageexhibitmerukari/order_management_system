<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Sale201702201701201612sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Sale201702201701201612sTable Test Case
 */
class Sale201702201701201612sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Sale201702201701201612sTable
     */
    public $Sale201702201701201612s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale201702201701201612s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sale201702201701201612s') ? [] : ['className' => 'App\Model\Table\Sale201702201701201612sTable'];
        $this->Sale201702201701201612s = TableRegistry::get('Sale201702201701201612s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sale201702201701201612s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
