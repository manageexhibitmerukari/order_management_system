<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201606sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201606sTable Test Case
 */
class Product201606sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201606sTable
     */
    public $Product201606s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201606s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201606s') ? [] : ['className' => 'App\Model\Table\Product201606sTable'];
        $this->Product201606s = TableRegistry::get('Product201606s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201606s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
