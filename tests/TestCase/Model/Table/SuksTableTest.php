<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuksTable Test Case
 */
class SuksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SuksTable
     */
    public $Suks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.suks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Suks') ? [] : ['className' => 'App\Model\Table\SuksTable'];
        $this->Suks = TableRegistry::get('Suks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Suks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
