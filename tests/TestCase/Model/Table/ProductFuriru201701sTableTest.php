<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductFuriru201701sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductFuriru201701sTable Test Case
 */
class ProductFuriru201701sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductFuriru201701sTable
     */
    public $ProductFuriru201701s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_furiru201701s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductFuriru201701s') ? [] : ['className' => 'App\Model\Table\ProductFuriru201701sTable'];
        $this->ProductFuriru201701s = TableRegistry::get('ProductFuriru201701s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductFuriru201701s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
