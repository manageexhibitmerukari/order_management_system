<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserProductsTable Test Case
 */
class UserProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserProductsTable
     */
    public $UserProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_products',
        'app.orders',
        'app.users',
        'app.user_profiles',
        'app.main_products',
        'app.categories',
        'app.sub_products',
        'app.sizes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserProducts') ? [] : ['className' => 'App\Model\Table\UserProductsTable'];
        $this->UserProducts = TableRegistry::get('UserProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
