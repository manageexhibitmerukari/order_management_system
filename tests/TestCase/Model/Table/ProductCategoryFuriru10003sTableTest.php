<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCategoryFuriru10003sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCategoryFuriru10003sTable Test Case
 */
class ProductCategoryFuriru10003sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCategoryFuriru10003sTable
     */
    public $ProductCategoryFuriru10003s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_category_furiru10003s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCategoryFuriru10003s') ? [] : ['className' => 'App\Model\Table\ProductCategoryFuriru10003sTable'];
        $this->ProductCategoryFuriru10003s = TableRegistry::get('ProductCategoryFuriru10003s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCategoryFuriru10003s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
