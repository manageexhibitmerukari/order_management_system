<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Product201601sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Product201601sTable Test Case
 */
class Product201601sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Product201601sTable
     */
    public $Product201601s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product201601s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Product201601s') ? [] : ['className' => 'App\Model\Table\Product201601sTable'];
        $this->Product201601s = TableRegistry::get('Product201601s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Product201601s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
