<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Sale201612sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Sale201612sTable Test Case
 */
class Sale201612sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Sale201612sTable
     */
    public $Sale201612s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale201612s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sale201612s') ? [] : ['className' => 'App\Model\Table\Sale201612sTable'];
        $this->Sale201612s = TableRegistry::get('Sale201612s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sale201612s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
