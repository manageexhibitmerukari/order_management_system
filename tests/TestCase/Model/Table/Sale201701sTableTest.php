<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Sale201701sTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Sale201701sTable Test Case
 */
class Sale201701sTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Sale201701sTable
     */
    public $Sale201701s;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale201701s'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sale201701s') ? [] : ['className' => 'App\Model\Table\Sale201701sTable'];
        $this->Sale201701s = TableRegistry::get('Sale201701s', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sale201701s);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
